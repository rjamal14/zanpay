<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVerificationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_verification', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_user')->nullable()->constrained('users')->onUpdate('cascade')->onDelete('set null');
			$table->integer('email_verification_token');
			$table->integer('phone_verification_token');
			$table->timestamp('created_at')->nullable();
			$table->string('created_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('updated_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_verification');
	}
}
