<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPostpaidTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_postpaid', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_biller')->nullable()->constrained('biller')->onUpdate('cascade')->onDelete('set null');
			$table->string('sku');
			$table->string('name');
			$table->foreignId('id_category')->nullable()->constrained('product_category')->onUpdate('cascade')->onDelete('set null');
			$table->foreignId('id_brand')->nullable()->constrained('product_brand')->onUpdate('cascade')->onDelete('set null');
			$table->integer('price');
			$table->integer('stock');
			$table->timestamp('start_cut_off')->nullable();
			$table->timestamp('end_cut_off')->nullable();
			$table->text('description')->nullable();
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('updated_ip')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
			$table->string('deleted_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_postpaid');
	}
}
