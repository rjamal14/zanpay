<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDepositTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_deposit', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_user')->nullable()->constrained('users')->onUpdate('cascade')->onDelete('set null');
			$table->foreignId('id_bank')->nullable()->constrained('bank')->onUpdate('cascade')->onDelete('set null');
			$table->integer('nominal');
			$table->integer('unique_code');
			$table->string('image');
			$table->smallInteger('status');
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('updated_ip')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
			$table->string('deleted_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_deposit');
	}
}
