<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_group_users', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_user_groups')->nullable()->constrained('user_groups')->onUpdate('cascade')->onDelete('set null');
			$table->foreignId('id_users')->nullable()->constrained('users')->onUpdate('cascade')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_group_users');
	}
}
