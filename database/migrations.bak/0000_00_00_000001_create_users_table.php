<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('code');
			$table->string('referal_code');
			$table->string('user_va');
			$table->string('photo');
			$table->string('name');
			$table->string('email');
			$table->timestamp('email_verified_at')->nullable();
			$table->string('phone');
			$table->timestamp('phone_verified_at')->nullable();
			$table->string('password');
			$table->smallInteger('status');
			$table->timestamp('created_at')->nullable();
			$table->string('created_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('updated_ip')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
			$table->string('deleted_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
