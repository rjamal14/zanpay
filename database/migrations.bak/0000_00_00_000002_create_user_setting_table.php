<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSettingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_setting', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_users')->nullable()->constrained('users')->onUpdate('cascade')->onDelete('set null');
			$table->string('languange');
			$table->boolean('dark_mode')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_setting');
	}
}
