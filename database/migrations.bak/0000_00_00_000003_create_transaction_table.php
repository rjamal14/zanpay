<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction', function (Blueprint $table) {
			$table->id();
			$table->string('serial');
			$table->foreignId('id_users')->nullable()->constrained('users')->onUpdate('cascade')->onDelete('set null');
			$table->string('custid');
			$table->integer('user_price');
			$table->integer('user_balance_before');
			$table->integer('user_balance_after');
			$table->integer('biller_price');
			$table->integer('biller_balance_before');
			$table->integer('biller_balance_after');
			$table->text('request');
			$table->text('respond');
			$table->text('added_data');
			$table->smallInteger('status');
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->string('updated_ip')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
			$table->string('deleted_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transaction');
	}
}
