<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Product;
use App\Imports\ImportProduct;
use App\Models\ProductBiller;
class ImportProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $file = __DIR__. '/../Excels/t.xlsx';
        $employee = Excel::Import(new ImportProduct, $file);

        // Excel::filter('chunk')->load($file)->chunk(250, function($results) {
        //     foreach ($results as $row) {
        //         $groupID = isset($row['group']) ? $row['group'] : 101010;
        //         if($groupID != 0 ){
        //             $product = Product::create([
        //                 "biller_id"=> 1,
        //                 "group_id"=>$groupID,
        //                 "sku_code"=>$row["kode"],
        //                 "name"=>$row["nama"],
        //                 "nominal"=>$row["nominal"],
        //                 "price"=>$row["harga_jual"],
        //                 "cashback"=>0,
        //                 "description"=>"",
        //                 "created_by"=>"import",
        //                 "is_trouble"=>$row["gangguan"] == false ? 1 : 0,
        //             ]);
        //             ProductBiller::create([
        //                 "product_id"=> $product->id,
        //                 "biller_id"=> 1,
        //                 "sku_biller"=>$row["kode"],
        //                 "price_basic"=>$row["harga_beli"],
        //                 "is_trouble"=>$row["gangguan"] == false ? 1 : 0,
        //                 "created_by"=>"import",
        //             ]);
        //         }
        //     }
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
