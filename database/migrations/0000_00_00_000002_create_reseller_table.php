<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResellerTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reseller', function (Blueprint $table) {
			$table->id();
			$table->string('kode');
			$table->string('nik');
			$table->string('foto');
			$table->string('nama');
			$table->text('alamat');
			$table->foreignId('id_reseller_group')->nullable()->constrained('reseller_group')->onUpdate('cascade')->onDelete('set null');
			$table->string('email');
			$table->timestamp('email_verified_at')->nullable();
			$table->string('phone');
			$table->timestamp('phone_verified_at')->nullable();
			$table->string('password');
			$table->integer('pin')->nullable();
			$table->string('kode_upline')->nullable();
			$table->integer('markup')->nullable();
			$table->integer('saldo')->nullable();
			$table->integer('min_saldo')->nullable();
			$table->text('deskripsi')->nullable();
			$table->boolean('aktif')->default(true);
			$table->timestamp('created_at')->nullable();
			$table->string('created_by')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reseller');
	}
}
