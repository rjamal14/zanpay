<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function (Blueprint $table) {
			$table->id();
			$table->string('kode');
			$table->string('nama');
			$table->foreignId('id_provider')->nullable()->constrained('provider')->onUpdate('cascade')->onDelete('set null');
			$table->boolean('fisik')->default(false);
			$table->boolean('tanpa_kode')->default(false);
			$table->integer('nominal')->nullable();
			$table->integer('harga_beli')->default(0);
			$table->integer('harga_jual')->default(0);
			$table->boolean('kosong')->default(false);
			$table->boolean('gangguan')->default(false);
			$table->integer('stok')->default(0);
			$table->integer('kunatitas')->default(0);
			$table->boolean('aktif')->default(false);
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product');
	}
}
