<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = [
            [
                'foto' => "https://www.freeiconspng.com/uploads/no-image-icon-1.jpg",
                'nama' => 'Admin',
                'alamat' => "",
                'email' => "admin@zanpay.co.id",
                'email_verified_at' => date('Y-m-d H:i:s'),
                'phone_verified_at' => date('Y-m-d H:i:s'),
                'phone' => "",
                'password' =>  app('hash')->make('123123'),
                'pin' =>  '123123',
            ],
        ];
        DB::table('users')->insert($user);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
