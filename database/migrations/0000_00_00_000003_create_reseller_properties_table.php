<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResellerPropertiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reseller_properties', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_reseller')->nullable()->constrained('reseller')->onUpdate('cascade')->onDelete('set null');
			$table->string('location');
			$table->string('ip');
			$table->string('device_id');
			$table->string('regid');
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reseller_properties');
	}
}
