<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupsPrivilegesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_group_privileges', function (Blueprint $table) {
			$table->id();
			$table->foreignId('id_user_groups')->nullable()->constrained('user_groups')->onUpdate('cascade')->onDelete('set null');
			$table->foreignId('id_module_actions')->nullable()->constrained('module_actions')->onUpdate('cascade')->onDelete('set null');
			$table->boolean('privileged')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_group_privileges');
	}
}
