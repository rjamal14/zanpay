<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHlrTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hlr', function (Blueprint $table) {
			$table->id();
			$table->string('nama');
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('hlr');
	}
}
