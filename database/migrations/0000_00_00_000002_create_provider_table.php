<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provider', function (Blueprint $table) {
			$table->id();
			$table->string('kode');
			$table->string('nama');
			$table->boolean('kosong')->default(false);
			$table->boolean('gangguan')->default(false);
			$table->string('prefix_no')->nullable();
			$table->string('kode_area')->nullable();
			$table->foreignId('id_hlr')->nullable()->constrained('hlr')->onUpdate('cascade')->onDelete('set null');
			$table->integer('min_no')->nullable();
			$table->integer('max_no')->nullable();
			$table->string('kode_grup')->nullable();
			$table->boolean('is_active')->default(false);
			$table->timestamp('created_at');
			$table->string('created_by')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('provider');
	}
}
