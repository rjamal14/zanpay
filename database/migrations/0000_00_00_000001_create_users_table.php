<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('foto');
			$table->string('nama');
			$table->string('alamat');
			$table->string('email');
			$table->timestamp('email_verified_at')->nullable();
			$table->string('phone');
			$table->timestamp('phone_verified_at')->nullable();
			$table->string('password');
			$table->integer('pin')->nullable();
			$table->boolean('aktif')->default(true);
			$table->timestamp('created_at')->nullable();
			$table->string('created_by')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->string('updated_by')->nullable();
			$table->boolean('is_deleted')->default(false);
			$table->timestamp('deleted_at')->nullable();
			$table->string('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
