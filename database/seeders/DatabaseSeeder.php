<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = DB::table('users');
		$id_users = null;
		$user_setting = DB::table('user_setting');
		$user_groups = DB::table('user_groups');
		$id_user_groups = null;
		$user_group_users = DB::table('user_group_users');
		$modules = DB::table('modules');
		$id_modules = null;
		$module_actions = DB::table('module_actions');
		$id_module_actions = null;
		$user_group_privileges = DB::table('user_group_privileges');
		if ($users->count() === 0) {
			$id_users = $users->insertGetId([
				'name' => 'Johan Firmansyah',
				'email' => 'johanskizo@gmail.com',
				'email_verified_at' => '2021-06-10 00:00:00',
				'phone' => '081214837269',
				'phone_verified_at' => '2021-06-10 00:00:00',
				'password' => Hash::make('bismillah'),
			]);
		}
		if ($user_setting->count() === 0 && $id_users) {
			$user_setting->insert([
				'id_users' => $id_users,
				'languange' => 'en',
			]);
		}
		if ($user_groups->count() === 0) {
			$id_user_groups = $user_groups->insertGetId([
				'name' => 'System Administrator',
				'created_at' => '2021-06-10 00:00:00',
				'created_by' => $id_users,
			]);
		}
		if ($user_group_users->count() === 0 && $id_user_groups && $id_users) {
			$user_group_users->insert([
				'id_user_groups' => $id_user_groups,
				'id_users' => $id_users,
			]);
		}
		if ($modules->count() === 0) {
			$id_modules = $modules->insertGetId([
				'index' => 0,
				'code' => 'dashboard',
				'name' => 'Dashboard',
				'icon' => 'dashboard',
				'class' => 'Dashboard',
				'path' => '/module/dashboard',
				'is_active' => true,
				'created_at' => '2021-06-10 00:00:00',
				'created_by' => $id_users,
			]);
		}
		if ($module_actions->count() === 0 && $id_modules) {
			$id_module_actions = $module_actions->insertGetId([
				'id_modules' => $id_modules,
				'name' => 'read',
				'created_at' => '2021-06-10 00:00:00',
				'created_by' => $id_users,
			]);
		}
		if ($user_group_privileges->count() === 0 && $id_user_groups && $id_module_actions) {
			$user_group_privileges->insert([
				'id_user_groups' => $id_user_groups,
				'id_module_actions' => $id_module_actions,
				'privileged' => true,
			]);
		}
	}
}
