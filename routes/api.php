<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ModulesController;
use App\Http\Controllers\ModuleActionsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\UserGroupsController;
use App\Http\Controllers\UserGroupPrivilegesController;

use App\Http\Controllers\GlobalStoreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::prefix('auth')->group(function () {
	Route::post('/register', [AuthController::class, 'register']);
	Route::post('/login', [AuthController::class, 'login']);
	Route::get('/logout', [AuthController::class, 'logout']);
	Route::get('/user', [AuthController::class, 'user']);
	Route::get('/user_setting/get', [AuthController::class, 'userSetting']);
	Route::put('/user_setting/put', [AuthController::class, 'userSetting']);
	Route::get('/modules', [AuthController::class, 'modules']);
});

Route::prefix('modules')->group(function () {
	Route::get('read/{path}', [ModulesController::class, 'read']);
	Route::post('insert', [ModulesController::class, 'insert']);
	Route::put('update', [ModulesController::class, 'update']);
	Route::delete('delete', [ModulesController::class, 'delete']);
});

Route::prefix('module_actions')->group(function () {
	Route::get('read/{path}', [ModuleActionsController::class, 'read']);
	Route::post('insert', [ModuleActionsController::class, 'insert']);
	Route::put('update', [ModuleActionsController::class, 'update']);
	Route::delete('delete', [ModuleActionsController::class, 'delete']);
});

Route::prefix('users')->group(function () {
	Route::get('read/{path}', [UsersController::class, 'read']);
	Route::put('update', [UsersController::class, 'update']);
	Route::delete('delete', [UsersController::class, 'delete']);
});

Route::prefix('user_groups')->group(function () {
	Route::get('read/{path}', [UserGroupsController::class, 'read']);
	Route::post('insert', [UserGroupsController::class, 'insert']);
	Route::put('update', [UserGroupsController::class, 'update']);
	Route::delete('delete', [UserGroupsController::class, 'delete']);
});

Route::prefix('user_group_privileges')->group(function () {
	Route::get('read/{path}', [UserGroupPrivilegesController::class, 'read']);
	Route::put('update', [UserGroupPrivilegesController::class, 'update']);
});

Route::get('global_store/{path}', [GlobalStoreController::class, 'read']);
