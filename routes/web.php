<?php

use Illuminate\Support\Facades\Route;
use App\Imports\ImportProduct;
use App\Http\Controllers\Chat\ChatController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

app()->router->group([

	// 'namespace' => 'CMS\v1\Attendance',
	'domain' => '',
	// 'middleware' => ['auth']

], function ($router) {

	Route::get('notify', [ChatController::class, 'notification']);


	Route::get('/login', 'App\Http\Controllers\Auth\AuthController@index')->name('backend.login');
	Route::post('/login', 'App\Http\Controllers\Auth\AuthController@authenticate');
	Route::post('/doOtp', 'App\Http\Controllers\Auth\AuthController@doOtp');
	Route::post('/resendOtp', 'App\Http\Controllers\Auth\AuthController@resendOtp');
	Route::get('/logout', 'App\Http\Controllers\Auth\AuthController@logout')->name('logout');
	Route::get('/otp', 'App\Http\Controllers\Auth\AuthController@otp')->name('Dashboard-otp');
});


Route::get('test', function () {
	event(new App\Events\NewMessage(1, 1, 1));
	return "Event has been sent!";
});



app()->router->group([

	// 'namespace' => 'CMS\v1\Attendance',
	'domain' => '',
	'middleware' => ['auth']

], function ($router) {


	Route::get('/', 'App\Http\Controllers\Dashboard\DashboardController@index')->name('dashboard-index');
	Route::post('/dashboard-badge', 'App\Http\Controllers\Dashboard\DashboardController@badge')->name('dashboard-badge');
	Route::get('/import/asd', function () {
		$file = __DIR__ . '/../Database/Excels/t.xlsx';
		$employee = Excel::Import(new ImportProduct, $file);
	});

	Route::get('/tiket_deposit', 'App\Http\Controllers\Transaksi\DepositController@index')->name('deposit-index');
	Route::get('/tiket_deposit-list', 'App\Http\Controllers\Transaksi\DepositController@fnData')->name('deposit-list');
	Route::get('/tiket_deposit-edit/{id}', 'App\Http\Controllers\Transaksi\DepositController@edit')->name('edit-deposit');
	Route::post('/tiket_deposit-update/{id}', 'App\Http\Controllers\Transaksi\DepositController@update')->name('update-deposit');
	Route::get('/tiket_deposit_edit/{id}', 'App\Http\Controllers\Transaksi\DepositController@depositEdit');
	Route::post('/tiket_deposit_update/{id}', 'App\Http\Controllers\Transaksi\DepositController@depositUpdate');
	Route::get('/tiket_deposit-update-status/{id}', 'App\Http\Controllers\Transaksi\DepositController@updateStatus')->name('update-depositStatus');
	Route::get('/tiket_deposit-manual', 'App\Http\Controllers\Transaksi\DepositController@depositManual')->name('update-depositManual');
	Route::post('/tiket_deposit-manual', 'App\Http\Controllers\Transaksi\DepositController@depositManualStore')->name('update-depositManualStore');
	Route::get('/document-update-status/{id}', 'App\Http\Controllers\Document\DocumentController@updateStatus')->name('update-documentstatus');


	Route::get('/dashboard', 'App\Http\Controllers\Dashboard\DashboardController@index')->name('Dashboard-index');
	Route::get('/platform-chart', 'App\Http\Controllers\Dashboard\DashboardController@platformChart')->name('Dashboard-platformChart');
	Route::get('/transaksi-chart', 'App\Http\Controllers\Dashboard\DashboardController@transaksiChart')->name('Dashboard-trchart');
	Route::get('/saldo-biller-chart2', 'App\Http\Controllers\Dashboard\DashboardController@saldoBillerChart2')->name('Dashboard-trchart');
	Route::get('/product-sold-chart', 'App\Http\Controllers\Dashboard\DashboardController@productSoldChart')->name('Dashboard-trchart');
	Route::get('/reseller_sultan-list', 'App\Http\Controllers\Dashboard\DashboardController@fnDataReseller')->name('rsultan-list');
	Route::get('/dashboard-pekerjaan-list', 'App\Http\Controllers\Dashboard\DashboardController@fnDataDaftarPekerjaan')->name('d-ldist');
	Route::get('/update-dashboard-pekerjaan-list', 'App\Http\Controllers\Dashboard\DashboardController@fnUpdateDaftarPekerjaan')->name('d-ldist');


	Route::get('/transaksi', 'App\Http\Controllers\Transaksi\TransaksiController@transaksiIndex')->name('Transaksi-index');
	Route::get('/live-transaksi', 'App\Http\Controllers\Transaksi\TransaksiController@transaksiLiveIndex')->name('Transaksi-index');
	Route::get('/transaksi-edit/{id}', 'App\Http\Controllers\Transaksi\TransaksiController@transaksiEdit')->name('Transaksi-edit');
	Route::post('/transaksi-update/{id}', 'App\Http\Controllers\Transaksi\TransaksiController@transaksiUpdate')->name('Transaksi-update');
	Route::post('/transaksi-pending-resend', 'App\Http\Controllers\Transaksi\TransaksiController@resend')->name('Transaksi-resend');
	Route::post('/transaksi-pending-cancel', 'App\Http\Controllers\Transaksi\TransaksiController@cancel')->name('Transaksi-cancel');
	Route::post('/transaksi-bulk-resend', 'App\Http\Controllers\Transaksi\TransaksiController@resendBulk')->name('Transaksi-bresend');
	Route::post('/transaksi-bulk-cancel', 'App\Http\Controllers\Transaksi\TransaksiController@cancelBulk')->name('Transaksi-bcancel');
	Route::get('/transaksi-module-status', 'App\Http\Controllers\Transaksi\TransaksiController@transactionModuleStatus')->name('Transaksi-module-status');

	Route::get('/transaksi-list', 'App\Http\Controllers\Transaksi\TransaksiController@fnDataTransaksi')->name('Transaksi-list');
	Route::get('/transaksi-live-list', 'App\Http\Controllers\Transaksi\TransaksiController@fnDataTransaksiLive')->name('TransaksiLive-list');
	Route::get('/inbox-biller-list', 'App\Http\Controllers\Transaksi\TransaksiController@fnDataInboxBiller')->name('inbox-biller-list');
	Route::get('/saldo_agen', 'App\Http\Controllers\Reseller\ResellerSaldoController@index')->name('resellerSaldo-index');


	Route::get('/produk_terlaris', 'App\Http\Controllers\Product\ProductController@indexTerlaris');
	Route::get('/produk-terlaris-list', 'App\Http\Controllers\Product\ProductController@fnDataTerlaris');
	Route::get('/download-produk', 'App\Http\Controllers\Product\ProductController@download');


	Route::get('/produk_parsing_list', 'App\Http\Controllers\Product\ParsingController@fnData');
	Route::get('/produk_parsing', 'App\Http\Controllers\Product\ParsingController@index');
	Route::post('/produk_parsing', 'App\Http\Controllers\Product\ParsingController@store');
	Route::get('/produk_parsing/{id}', 'App\Http\Controllers\Product\ParsingController@detail');
	Route::post('/produk_parsing/{id}', 'App\Http\Controllers\Product\ParsingController@update');
	Route::post('/produk_parsing_delete/{id}', 'App\Http\Controllers\Product\ParsingController@delete');

	Route::get('/regex_list', 'App\Http\Controllers\Product\RegexController@fnData');
	Route::get('/regex', 'App\Http\Controllers\Product\RegexController@index');
	Route::post('/regex', 'App\Http\Controllers\Product\RegexController@store');
	Route::get('/regex/{id}', 'App\Http\Controllers\Product\RegexController@detail');
	Route::post('/regex/{id}', 'App\Http\Controllers\Product\RegexController@update');
	Route::post('/regex-delete/{id}', 'App\Http\Controllers\Product\RegexController@delete');
	Route::post('/generate-regex/', 'App\Http\Controllers\Product\RegexController@generate');
	Route::get('/group-combobox-bybiller/', 'App\Http\Controllers\Product\RegexController@groupByBiller');
	Route::get('/group-combobox-bybiller1/', 'App\Http\Controllers\Product\RegexController@tipeByGroup');
	Route::get('/group-combobox-bybiller2/', 'App\Http\Controllers\Product\RegexController@providerByTipe');







	Route::get('/reseller-saldo-list', 'App\Http\Controllers\Reseller\ResellerSaldoController@fnData')->name('reseller-list');
	Route::get('/reseller-data-list', 'App\Http\Controllers\Reseller\ResellerSaldoController@fnDatav2')->name('reseller-list');
	Route::get('/reseller-saldo-edit/{id}', 'App\Http\Controllers\Reseller\ResellerSaldoController@editReseller')->name('edit-Rese');
	Route::post('/reseller-saldo-update/{id}', 'App\Http\Controllers\Reseller\ResellerSaldoController@updateReseller')->name('update-ResellerSaldo');


	Route::get('/mutasi_saldo_agen', 'App\Http\Controllers\Transaksi\MutasiSaldoResellerController@index')->name('mutasiSaldoReseller-index');

	Route::get('/mutasi_saldo_reseller-list', 'App\Http\Controllers\Transaksi\MutasiSaldoResellerController@fnData')->name('mutasiSaldoReseller-list');

	Route::get('/saldo_biller', 'App\Http\Controllers\Biller\BillerSaldoController@index')->name('BillerSaldo-index');
	Route::get('/saldo-biller-chart', 'App\Http\Controllers\Biller\BillerSaldoController@chart')->name('BillerSaldo-chart');

	Route::get('/biller-saldo-list', 'App\Http\Controllers\Biller\BillerSaldoController@fnData')->name('biller-list');
	Route::get('/biller-saldo-edit/{id}', 'App\Http\Controllers\Biller\BillerSaldoController@editBiller')->name('edit-Rese');
	Route::post('/biller-saldo-update/{id}', 'App\Http\Controllers\Biller\BillerSaldoController@updateBiller')->name('update-BillerSaldo');

	Route::get('/mutasi_saldo_biller', 'App\Http\Controllers\Transaksi\MutasiSaldoBillerController@index')->name('mutasiSaldoBiller-index');

	Route::get('/mutasi_saldo_biller-list', 'App\Http\Controllers\Transaksi\MutasiSaldoBillerController@fnData')->name('mutasiSaldoBiller-list');

	Route::get('/reseller', 'App\Http\Controllers\Reseller\ResellerController@index')->name('reseller-index');
	Route::get('/reseller-commission', 'App\Http\Controllers\Reseller\ResellerController@indexComm');
	// Route::get('/reseller-commission-list', 'App\Http\Controllers\Reseller\ResellerController@fnDataComm');
	Route::get('/reseller-commission-list/{id}', 'App\Http\Controllers\Reseller\ResellerController@detailComm');

	Route::get('/reseller-list', 'App\Http\Controllers\Reseller\ResellerController@fnData')->name('reseller-list');
	Route::get('/reseller-history-list/{id}', 'App\Http\Controllers\Reseller\ResellerController@fnHistoryData')->name('resellerHistory-list');
	Route::post('/add-reseller', 'App\Http\Controllers\Reseller\ResellerController@addReseller')->name('add-Reseller');
	Route::post('/delete-reseller/{id}', 'App\Http\Controllers\Reseller\ResellerController@deleteReseller')->name('delete-Reseller');
	Route::get('/reseller-edit/{id}', 'App\Http\Controllers\Reseller\ResellerController@editReseller')->name('edit-Reseller');
	Route::post('/reseller-update/{id}', 'App\Http\Controllers\Reseller\ResellerController@updateReseller')->name('update-Reseller');


	Route::get('/transfer', 'App\Http\Controllers\Transfer\TransferController@index')->name('transfer-index');
	Route::get('/transfer-list', 'App\Http\Controllers\Transfer\TransferController@fnData')->name('transfer-list');
	Route::get('/notification', 'App\Http\Controllers\Notification\NotificationController@index')->name('notification-index');
	Route::get('/notification-list', 'App\Http\Controllers\Notification\NotificationController@fnData')->name('notification-list');
	Route::get('/message', 'App\Http\Controllers\Message\MessageController@index')->name('message-index');
	Route::get('/message-list', 'App\Http\Controllers\Message\MessageController@fnData')->name('message-list');
	Route::get('/chat-history-list/{id}', 'App\Http\Controllers\Chat\ChatController@fnData')->name('Chat-list');
	Route::post('/send-chat/{id}', 'App\Http\Controllers\Chat\ChatController@sendChat')->name('Chat-send');
	Route::post('/create-chat', 'App\Http\Controllers\Chat\ChatController@createChat')->name('create-chat');
	Route::get('/chat-recent', 'App\Http\Controllers\Chat\ChatController@chatRecent');


	Route::get('/document', 'App\Http\Controllers\Document\DocumentController@index')->name('document-index');
	Route::get('/document-list', 'App\Http\Controllers\Document\DocumentController@fnData')->name('document-list');

	Route::get('/product_menu', 'App\Http\Controllers\Product\ProductMenuController@index')->name('product_menu-index');
	Route::post('/product_menu-store', 'App\Http\Controllers\Product\ProductMenuController@store')->name('product_menu-store');
	Route::get('/product_menu-list', 'App\Http\Controllers\Product\ProductMenuController@fnData')->name('product_menu-list');
	Route::get('/product_menu-edit/{id}', 'App\Http\Controllers\Product\ProductMenuController@edit')->name('edit-ProductMenu');
	Route::post('/product_menu-update/{id}', 'App\Http\Controllers\Product\ProductMenuController@update')->name('update-ProductMenu');



	Route::get('/voucher_log', 'App\Http\Controllers\Voucher\VoucherLogController@index')->name('voucher_log-index');
	Route::get('/voucher_log-list', 'App\Http\Controllers\Voucher\VoucherLogController@fnData')->name('voucher_log-list');

	Route::get('/voucher', 'App\Http\Controllers\Voucher\VoucherController@index')->name('voucher-index');
	Route::get('/voucher-combobox-bygroup', 'App\Http\Controllers\Voucher\VoucherController@groupCombobox')->name('voucher-groupCombobox');
	Route::post('/voucher-combobox-bygroup2', 'App\Http\Controllers\Voucher\VoucherController@groupCombobox2')->name('voucher-groupCombobox2');
	Route::get('/voucher-combobox-byref', 'App\Http\Controllers\Voucher\VoucherController@groupComboboxRef')->name('voucher-grouprev');
	Route::post('/add-voucher', 'App\Http\Controllers\Voucher\VoucherController@store')->name('voucher-store');
	Route::get('/voucher-list', 'App\Http\Controllers\Voucher\VoucherController@fnData')->name('voucher-list');
	Route::get('/voucher-edit/{id}', 'App\Http\Controllers\Voucher\VoucherController@edit')->name('edit-voucher');
	Route::post('/voucher-update/{id}', 'App\Http\Controllers\Voucher\VoucherController@update')->name('update-voucher');
	Route::post('/delete-voucher/{id}', 'App\Http\Controllers\Voucher\VoucherController@delete')->name('update-voucher');

	Route::get('/product_group', 'App\Http\Controllers\Product\ProductGroupController@index')->name('product_group-index');
	Route::get('/product_group-list', 'App\Http\Controllers\Product\ProductGroupController@fnData')->name('product_group-list');
	Route::get('/product_group-edit/{id}', 'App\Http\Controllers\Product\ProductGroupController@edit')->name('edit-ProductGroup');
	Route::post('/product_group-update/{id}', 'App\Http\Controllers\Product\ProductGroupController@update')->name('update-ProductGroup');
	Route::post('/product-group-delete/{id}', 'App\Http\Controllers\Product\ProductGroupController@delete')->name('delete-ProductGroup');
	Route::post('/add-product-group', 'App\Http\Controllers\Product\ProductGroupController@store')->name('add-ProductGroup');

	Route::get('/product-category', 'App\Http\Controllers\Product\ProductCategoryController@index')->name('product-category-index');
	Route::get('/product-category-list', 'App\Http\Controllers\Product\ProductCategoryController@fnData')->name('product-category-list');
	Route::get('/product-category-edit/{id}', 'App\Http\Controllers\Product\ProductCategoryController@edit')->name('edit-ProductCategory');
	Route::post('/product-category-update/{id}', 'App\Http\Controllers\Product\ProductCategoryController@update')->name('update-ProductCategory');
	Route::post('/product-category-delete/{id}', 'App\Http\Controllers\Product\ProductCategoryController@deleteData')->name('update-ProductCategory');
	Route::post('/product-category-create', 'App\Http\Controllers\Product\ProductCategoryController@create')->name('update-ProductCategory');
	Route::get('/product-category-bygroupid/{id}', 'App\Http\Controllers\Product\ProductCategoryController@byGroupId')->name('updatdwad12');
	Route::get('/product-biller-byBiller/{id}', 'App\Http\Controllers\Product\ProductCategoryController@byBiller')->name('updatdwad12');

	Route::get('/product_biller', 'App\Http\Controllers\Product\ProductBillerController@index')->name('product_biller-index');
	Route::get('/product_biller-list', 'App\Http\Controllers\Product\ProductBillerController@fnData')->name('product_biller-list');
	Route::post('/product_biller-import', 'App\Http\Controllers\Product\ProductBillerController@import')->name('product_biller-list');
	Route::post('/product_biller-import-do', 'App\Http\Controllers\Product\ProductBillerController@doimport')->name('product_biller-list');
	Route::get('/product_biller-edit/{id}', 'App\Http\Controllers\Product\ProductBillerController@edit')->name('edit-productBiller');
	Route::post('/product_biller-update/{id}', 'App\Http\Controllers\Product\ProductBillerController@update')->name('update-productBiller');
	Route::post('/product_biller-delete/{id}', 'App\Http\Controllers\Product\ProductBillerController@delete')->name('delete-productBiller');
	Route::post('/add-product-biller', 'App\Http\Controllers\Product\ProductBillerController@store')->name('add-productBiller');
	Route::get('/product-combobox-bygroup', 'App\Http\Controllers\Product\ProductBillerController@groupCombobox')->name('productBiller-groupCombobox');
	Route::get('/group-combobox-bygroup', 'App\Http\Controllers\Product\ProductMenuController@groupCombobox')->name('prodmenuuctBiller-groupCombobox');
	Route::get('/product-combobox-bygroup2', 'App\Http\Controllers\Product\ProductBillerController@groupCombobox2')->name('2productBiller-groupCombobox');
	Route::get('/product-combobox-bygroup3', 'App\Http\Controllers\Product\ProductBillerController@groupCombobox3')->name('3productBiller-groupCombobox');
	Route::get('/parsing-combobox-bygroup2', 'App\Http\Controllers\Product\ParsingController@groupCombobox2')->name('2parsingproductBiller-groupCombobox');

	Route::get('/team', 'App\Http\Controllers\Web\WebController@teamIndex')->name('team-index');
	Route::get('/team-list', 'App\Http\Controllers\Web\WebController@fnTeamData')->name('team-data');
	Route::get('/team-edit/{id}', 'App\Http\Controllers\Web\WebController@teamEdit')->name('team-edit');
	Route::post('/add-team', 'App\Http\Controllers\Web\WebController@teamAdd')->name('team-add');
	Route::post('/delete-team/{id}', 'App\Http\Controllers\Web\WebController@deleteTeam')->name('team-add');
	Route::post('/team-update/{id}', 'App\Http\Controllers\Web\WebController@updateTeam')->name('update-team');

	Route::get('/faqs', 'App\Http\Controllers\Web\WebController@faqsIndex')->name('faqs-index');
	Route::get('/faqs-list', 'App\Http\Controllers\Web\WebController@fnFaqsData')->name('faqs-data');
	Route::get('/faqs-edit/{id}', 'App\Http\Controllers\Web\WebController@faqsEdit')->name('faqs-edit');
	Route::post('/add-faqs', 'App\Http\Controllers\Web\WebController@faqsAdd')->name('faqs-add');
	Route::post('/delete-faqs/{id}', 'App\Http\Controllers\Web\WebController@deleteFaqs')->name('faqs-add');
	Route::post('/faqs-update/{id}', 'App\Http\Controllers\Web\WebController@updateFaqs')->name('update-faqs');


	Route::get('/contact_message', 'App\Http\Controllers\Web\WebController@contactMessageIndex')->name('contactMessage-index');
	Route::get('/contact-message-list', 'App\Http\Controllers\Web\WebController@fnContactMessageData')->name('contactMessage-data');
	Route::get('/contact_message-edit/{id}', 'App\Http\Controllers\Web\WebController@contactMessageEdit')->name('contactMessage-edit');


	Route::get('/grup_reseller', 'App\Http\Controllers\Reseller\ResellerGroupController@index')->name('reseller-group-index');

	Route::get('/reseller-group-list', 'App\Http\Controllers\Reseller\ResellerGroupController@fnData')->name('resellerGroup-list');
	Route::post('/add-reseller-group', 'App\Http\Controllers\Reseller\ResellerGroupController@addResellerGroup')->name('add-ResellerGroup');
	Route::post('/delete-reseller-group/{id}', 'App\Http\Controllers\Reseller\ResellerGroupController@deleteResellerGroup')->name('delete-ResellerGroup');
	Route::get('/reseller-group-edit/{id}', 'App\Http\Controllers\Reseller\ResellerGroupController@editResellerGroup')->name('edit-ResellerGroup');
	Route::post('/reseller-group-update/{id}', 'App\Http\Controllers\Reseller\ResellerGroupController@updateResellerGroup')->name('update-ResellerGroup');

	Route::get('/news_category', 'App\Http\Controllers\News\NewsCategoryController@index')->name('newsCategory-index');
	Route::get('/news-category-list', 'App\Http\Controllers\News\NewsCategoryController@fnData')->name('NewsGroup-list');
	Route::post('/add-news-category', 'App\Http\Controllers\News\NewsCategoryController@add')->name('add-NewsGroup');
	Route::post('/delete-news-category/{id}', 'App\Http\Controllers\News\NewsCategoryController@delete')->name('delete-NewsGroup');
	Route::get('/news-category-edit/{id}', 'App\Http\Controllers\News\NewsCategoryController@edit')->name('edit-NewsGroup');
	Route::post('/news-category-update/{id}', 'App\Http\Controllers\News\NewsCategoryController@update')->name('update-NewsGroup');

	Route::get('/merchandise_category', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@index')->name('MerchandiseCategory-index');
	Route::get('/merchandise-category-list', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@fnData')->name('MerchandiseGroup-list');
	Route::post('/add-merchandise-category', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@add')->name('add-MerchandiseGroup');
	Route::post('/delete-merchandise-category/{id}', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@delete')->name('delete-MerchandiseGroup');
	Route::get('/merchandise-category-edit/{id}', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@edit')->name('edit-MerchandiseGroup');
	Route::post('/merchandise-category-update/{id}', 'App\Http\Controllers\Merchandise\MerchandiseCategoryController@update')->name('update-MerchandiseGroup');
	Route::get('/merchandise', 'App\Http\Controllers\Merchandise\MerchandiseController@index')->name('merchandise-index');
	Route::get('/merchandise-list', 'App\Http\Controllers\Merchandise\MerchandiseController@fnData')->name('Merchandise-list');
	Route::get('/merchandise-image-list/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@fnImageData')->name('Merchandise-list');
	Route::get('/merchandise-image-list2/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@getImage')->name('Merchandise-list');
	Route::post('/merchandise-image-delete/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@deleteImage')->name('Merchandise-list');
	Route::post('/add-merchandise', 'App\Http\Controllers\Merchandise\MerchandiseController@add')->name('add-Merchandise');
	Route::post('/delete-merchandise/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@delete')->name('delete-Merchandise');
	Route::get('/merchandise-edit/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@edit')->name('edit-Merchandise');
	Route::post('/merchandise-update/{id}', 'App\Http\Controllers\Merchandise\MerchandiseController@update')->name('update-Merchandise');



	Route::get('/news', 'App\Http\Controllers\News\NewsController@index')->name('news-index');
	Route::get('/news-list', 'App\Http\Controllers\News\NewsController@fnData')->name('News-list');
	Route::post('/add-news', 'App\Http\Controllers\News\NewsController@add')->name('add-News');
	Route::post('/delete-news/{id}', 'App\Http\Controllers\News\NewsController@delete')->name('delete-News');
	Route::get('/news-edit/{id}', 'App\Http\Controllers\News\NewsController@edit')->name('edit-News');
	Route::post('/news-update/{id}', 'App\Http\Controllers\News\NewsController@update')->name('update-News');



	Route::get('/banner', 'App\Http\Controllers\Banner\BannerController@index')->name('banner-index');
	Route::get('/banner-list', 'App\Http\Controllers\Banner\BannerController@fnData')->name('Banner-list');
	Route::post('/add-banner', 'App\Http\Controllers\Banner\BannerController@add')->name('add-Banner');
	Route::post('/delete-banner/{id}', 'App\Http\Controllers\Banner\BannerController@delete')->name('delete-Banner');
	Route::get('/banner-edit/{id}', 'App\Http\Controllers\Banner\BannerController@edit')->name('edit-Banner');
	Route::post('/banner-update/{id}', 'App\Http\Controllers\Banner\BannerController@update')->name('update-Banner');



	Route::get('/feature', 'App\Http\Controllers\Web\WebController@featureIndex')->name('feature-index');
	Route::get('/feature-list', 'App\Http\Controllers\Web\WebController@featureData')->name('Web-list');
	Route::post('/add-feature', 'App\Http\Controllers\Web\WebController@featureStore')->name('add-Web');
	Route::post('/delete-feature/{id}', 'App\Http\Controllers\Web\WebController@featureDelete')->name('delete-Web');
	Route::get('/feature-edit/{id}', 'App\Http\Controllers\Web\WebController@featureEdit')->name('edit-Web');
	Route::post('/feature-update/{id}', 'App\Http\Controllers\Web\WebController@featureUpdate')->name('update-Web');


	Route::get('/daftar-pekerjaan', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@index')->name('DaftarPekerjaan-index');
	Route::get('/daftar-pekerjaan-list', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@fnData')->name('DaftarPekerjaan-list');
	Route::post('/add-daftar-pekerjaan', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@add')->name('add-DaftarPekerjaan');
	Route::post('/delete-daftar-pekerjaan/{id}', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@delete')->name('delete-DaftarPekerjaan');
	Route::get('/daftar-pekerjaan-edit/{id}', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@edit')->name('edit-DaftarPekerjaan');
	Route::post('/daftar-pekerjaan-update/{id}', 'App\Http\Controllers\DaftarPekerjaan\DaftarPekerjaanController@update')->name('update-DaftarPekerjaan');


	Route::get('/biller', 'App\Http\Controllers\Biller\BillerController@index')->name('biller-index');

	Route::get('/biller-list', 'App\Http\Controllers\Biller\BillerController@fnData')->name('biller-list');
	Route::post('/add-biller', 'App\Http\Controllers\Biller\BillerController@add')->name('add-Biller');
	Route::post('/delete-biller/{id}', 'App\Http\Controllers\Biller\BillerController@delete')->name('delete-Biller');
	Route::get('/biller-edit/{id}', 'App\Http\Controllers\Biller\BillerController@edit')->name('edit-Biller');
	Route::post('/biller-update/{id}', 'App\Http\Controllers\Biller\BillerController@update')->name('update-Biller');
	Route::get('/biller-edit-saldo/{id}', 'App\Http\Controllers\Biller\BillerController@editSaldo')->name('edit-BillerSaldo');
	Route::post('/biller-update-saldo/{id}', 'App\Http\Controllers\Biller\BillerController@updateSaldo')->name('update-BillerSaldo');
	Route::get('/biller-saldo-sync', 'App\Http\Controllers\Biller\BillerController@syncSaldo')->name('syncSaldo-BillerSaldo');
	Route::post('/biller-saldo-sync', 'App\Http\Controllers\Biller\BillerController@doSyncSaldo')->name('dOsyncSaldo-BillerSaldo');


	// Route::get('/produk_prepaid', function () {
	// 	return view('produk_prepaid');
	// });

	Route::get('/produk_prepaid', 'App\Http\Controllers\Product\ProductController@prePaidIndex')->name('product-prepaid-index');

	Route::get('product-prepaid-list', 'Product\ProductController@fnDataPrepaid')->name('product-prepaid-list');

	Route::post('/add-product', 'App\Http\Controllers\Product\ProductController@addProduct')->name('add-product');
	Route::post('/delete-product/{id}', 'App\Http\Controllers\Product\ProductController@deleteProduct')->name('delete-product');
	Route::get('/product-edit/{id}', 'App\Http\Controllers\Product\ProductController@editProduct')->name('edit-product');
	Route::post('/product-update/{id}', 'App\Http\Controllers\Product\ProductController@updateProduct')->name('update-product');
	Route::post('/product-import', 'App\Http\Controllers\Product\ProductController@import')->name('import-product');
	Route::post('/product-import-do', 'App\Http\Controllers\Product\ProductController@doimport')->name('doimport-product');


	Route::get('/produk_postpaid', function () {
		return view('produk_postpaid');
	});
	Route::get('/product-postpaid-list', 'App\Http\Controllers\Product\ProductController@fnDataPostpaid')->name('product-postpaid-list');
	Route::get('/produk_postpaid', 'App\Http\Controllers\Product\ProductController@postPaidIndex')->name('product-postpaid-index');


	Route::get('/provider', function () {
		return view('provider');
	});


	Route::get('/provider', 'App\Http\Controllers\Provider\ProviderController@index')->name('Provider-prepaid-index');
	Route::get('provider-list', 'App\Http\Controllers\Provider\ProviderController@fnData')->name('Provider-prepaid-list');
	Route::post('/add-provider', 'App\Http\Controllers\Provider\ProviderController@add')->name('add-provider');
	Route::post('/delete-provider/{id}', 'App\Http\Controllers\Provider\ProviderController@delete')->name('delete-provider');
	Route::get('/provider-edit/{id}', 'App\Http\Controllers\Provider\ProviderController@edit')->name('edit-provider');
	Route::post('/provider-update/{id}', 'App\Http\Controllers\Provider\ProviderController@update')->name('update-provider');

	Route::get('/profile', 'App\Http\Controllers\Profile\ProfileController@index')->name('profile-prepaid-index');
	Route::post('/profile-update/{id}', 'App\Http\Controllers\Profile\ProfileController@update')->name('update-profile');
	Route::post('/profile-foto-update/{id}', 'App\Http\Controllers\Profile\ProfileController@updateFoto')->name('update-foto-profile');
	Route::post('/profile-password-update/{id}', 'App\Http\Controllers\Profile\ProfileController@updatePassword')->name('update-password-profile');

	Route::get('/payment_setting', function () {
		return view('payment_setting');
	});

	Route::get('/payment_method', 'App\Http\Controllers\Payment\PaymentController@index')->name('Payment-index');
	Route::get('/payment-list', 'App\Http\Controllers\Payment\PaymentController@fnData')->name('Payment-list');
	Route::post('/add-payment', 'App\Http\Controllers\Payment\PaymentController@create')->name('add-Payment');
	Route::post('/delete-payment/{id}', 'App\Http\Controllers\Payment\PaymentController@delete')->name('delete-Payment');
	Route::get('/payment-edit/{id}', 'App\Http\Controllers\Payment\PaymentController@edit')->name('edit-Payment');
	Route::post('/payment-update/{id}', 'App\Http\Controllers\Payment\PaymentController@update')->name('update-Payment');



	Route::get('/inbox', function () {
		return view('inbox');
	});

	Route::get('/outbox', function () {
		return view('outbox');
	});




	Route::get('/parsing', function () {
		return view('parsing');
	});

	// Route::get('/chat', function () {
	// 	return view('chat');
	// });
	Route::get('/chat', 'App\Http\Controllers\Chat\ChatController@index')->name('Chat-index');
	Route::get('/whatsapp', 'App\Http\Controllers\Chat\ChatController@whatsapp')->name('whatsapp-index')->middleware('xframe');
	// Route::get('/whatsapp', 'App\Http\Controllers\Chat\ChatController@whatsapp')->name('whatsapp-index');


	Route::get('video_promosi', 'App\Http\Controllers\Web\WebController@videoIndex')->name('video-index');
	Route::post('video_promosi', 'App\Http\Controllers\Web\WebController@videoUpdate')->name('video-update');



	Route::get('home_page', 'App\Http\Controllers\Web\WebController@webHomeIndex')->name('web_home-index');
	Route::post('home_page', 'App\Http\Controllers\Web\WebController@webHomeUpdate')->name('web_home-update');

	Route::get('terms', 'App\Http\Controllers\Web\WebController@termsIndex')->name('terms-index');
	Route::post('terms', 'App\Http\Controllers\Web\WebController@termsUpdate')->name('terms-update');
	Route::get('terms_list', 'App\Http\Controllers\Web\WebController@termsList');
	Route::get('terms_list_edit/{id}', 'App\Http\Controllers\Web\WebController@termsListEdit');
	Route::post('terms_list_update/{id}', 'App\Http\Controllers\Web\WebController@termsListUpdate');
	Route::post('terms_list_store', 'App\Http\Controllers\Web\WebController@termsListStore');
	Route::post('terms_list_delete/{id}', 'App\Http\Controllers\Web\WebController@termsListDelete');

	Route::get('terms_privacy', 'App\Http\Controllers\Web\WebController@termsPrivacyIndex')->name('terms-index');
	Route::post('terms_privacy', 'App\Http\Controllers\Web\WebController@termsPrivacyUpdate')->name('terms-update');
	Route::get('terms_privacy_list', 'App\Http\Controllers\Web\WebController@termsPrivacyList');
	Route::get('terms_privacy_list_edit/{id}', 'App\Http\Controllers\Web\WebController@termsPrivacyListEdit');
	Route::post('terms_privacy_list_update/{id}', 'App\Http\Controllers\Web\WebController@termsPrivacyListUpdate');
	Route::post('terms_privacy_list_store', 'App\Http\Controllers\Web\WebController@termsPrivacyListStore');
	Route::post('terms_privacy_list_delete/{id}', 'App\Http\Controllers\Web\WebController@termsPrivacyListDelete');
	Route::get('terms_privacy_list', 'App\Http\Controllers\Web\WebController@termsPrivacyList');


	Route::get('about_us', 'App\Http\Controllers\Web\WebController@aboutUsIndex')->name('about_us-index');
	Route::post('about_us', 'App\Http\Controllers\Web\WebController@aboutUsUpdate')->name('about_us-update');
	Route::get('contact_us', 'App\Http\Controllers\Web\WebController@contactUsIndex')->name('contactUs-index');
	Route::post('contact_us', 'App\Http\Controllers\Web\WebController@contactUsUpdate')->name('contactUs-update');
	Route::get('social_media', 'App\Http\Controllers\Web\WebController@socialMediaIndex')->name('socialMediaData-index');
	Route::get('social-media-list', 'App\Http\Controllers\Web\WebController@socialMediaData')->name('socialMediaData-index');
	Route::post('social_media', 'App\Http\Controllers\Web\WebController@contactUsUpdate')->name('contactUs-update');
	Route::post('add-social-media', 'App\Http\Controllers\Web\WebController@socialMediaStore')->name('socialMediaStore-update');
	Route::get('social-media-edit/{id}', 'App\Http\Controllers\Web\WebController@socialMediaEdit')->name('socialMediaStore-update');
	Route::post('update-social-media/{id}', 'App\Http\Controllers\Web\WebController@socialMediaUpdate')->name('socialMediaStore-update');
	Route::post('delete-social-media/{id}', 'App\Http\Controllers\Web\WebController@socialMediaDelete')->name('socialMediaStore-update');

	
	Route::get('user', 'App\Http\Controllers\User\UserController@index')->name('user-index')->middleware('Admin');
	Route::get('user-list', 'App\Http\Controllers\User\UserController@fnData')->name('user-list')->middleware('Admin');
	Route::post('user-create', 'App\Http\Controllers\User\UserController@store')->name('user-create')->middleware('Admin');
	Route::delete('user-delete/{id}', 'App\Http\Controllers\User\UserController@delete')->name('user-delete')->middleware('Admin');
	Route::get('user-detail/{id}', 'App\Http\Controllers\User\UserController@show')->name('user-detail')->middleware('Admin');
	Route::post('user-update/{id}', 'App\Http\Controllers\User\UserController@update')->name('user-update')->middleware('Admin');

	Route::get('/calendar', function () {
		return view('calendar');
	});
});
