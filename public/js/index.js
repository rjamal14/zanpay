
 $(document).on("click", ".logoutBtn", function(e) {
    showConfirm('Apakah anda Yakin Untuk Logout ? ', function(result) 
    {
        if (result) {
            window.location.href =  base_URL +'/logout';
        }
    }, '<span style="color: blue;">Logout</span>', {
        confirm: {
            label: 'Logout',
            className: 'btn-danger'
        },
        cancel: {
            label: 'Cancel',
            className: 'btn-default'
        }
    });
});
function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Logout',
							className: 'btn-danger'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-default'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}

    $(function() {
    "use strict";

    // progress bars
    $('.progress .progress-bar').progressbar({
            display_text: 'none'
    });

    $('.sparkbar').sparkline('html', { type: 'bar' });

    $('.sparkline-pie').sparkline('html', {
        type: 'pie',
        offset: 90,
        width: '100%',
        height: '100%',
        sliceColors: ['#29bd73', '#182973', '#ffcd55']
    })


    // top products
    var dataStackedBar = {
            labels: ['Biller 1','Biller 2','Biller 3','Biller 4','Biller 5'],
            series: [
                [2350,3205,4520,2351,5632],
                [2541,2583,1592,2674,2323],
                [1212,5214,2325,4235,2519],
            ]
    };
    new Chartist.Bar('#chart-top-products', dataStackedBar, {
            height: "255px",
            stackBars: true,
            axisX: {
                showGrid: false
            },
            axisY: {
                labelInterpolationFnc: function(value) {
                    return (value / 1000) + 'k';
                }
            },
            plugins: [
                Chartist.plugins.tooltip({
                    appendToBody: true
                }),
                Chartist.plugins.legend({
                    legendNames: ['E-Money', 'Token PLN', 'Pulsa & Data']
                })
            ]
    }).on('draw', function(data) {
            if (data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 35px'
                });
            }
    });


    // notification popup
    toastr.options.closeButton = true;
    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.showDuration = 1000;
    // toastr['info']('Hello, welcome to HexaBit, a unique admin Template.');

    var chart = c3.generate({

        bindto: '#User_Statistics', // id of chart wrapper
        data: {
            columns: [
                // each columns data
								['data1', 261000, 265000, 301000, 262000, 269000, 263000, 266000, 267000, 264000, 269000],
                ['data2', 200000, 200000, 300000, 260000, 200000, 260000, 260000, 260000, 260000, 260000],
								['data3', 60000, 65000, 1000, 2000, 69000, 3000, 6000, 7000, 4000, 9000],
            ],

            labels: true,
            type: 'line', // default type of chart
            colors: {
                'data1': hexabit.colors["green"],
                'data2': hexabit.colors["yellow"],
								'data3': hexabit.colors["red"],
            },
            names: {
                // name of each serie
                'data1': 'Sukses',
                'data2': 'Proses',
								'data3': 'Gagal',
            }
        },

        axis: {
            x: {
                type: 'category',
                // name of each category
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug','Sept','Oct']
            },
        },

        legend: {
            show: true, //hide legend
        },

        padding: {
            bottom: 10,
            top: 0
        },
    });
});
