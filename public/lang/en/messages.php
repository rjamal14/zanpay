<?php

return [
	'add' => 'Add',
	'update' => 'Update',
	'delete' => 'Delete',
	'password' => 'Password',
	'success' => ':Attribute success.',
	'error' => ':Attribute error.',
	'add_data_success' => 'Data added successfully.',
	'update_data_success' => 'Data updated successfully.',
	'delete_data_success' => 'Data deleted successfully.',
	'add_data_error' => 'Failed to add data.',
	'update_data_error' => 'Failed to update data.',
	'delete_data_error' => 'Failed to delete data.',
	'Email not found.' => 'Email not found.',
	'Password incorect.' => 'Password incorect.',
	'Account blocked.' => 'Account blocked.',
	'User has no group privileges.' => 'User has no group privileges.',
	'Invalid credentials.' => 'Invalid credentials.',
	'Please check your email for verification code.' => 'Please check your email for verification code.',
];
