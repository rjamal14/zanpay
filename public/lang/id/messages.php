<?php

return [
	'add' => 'Tambah',
	'update' => 'Perbarui',
	'delete' => 'Hapus',
	'password' => 'Kata sandi',
	'success' => ':Attribute sukses.',
	'error' => ':Attribute gagal.',
	'add_data_success' => 'Data berhasil ditambahkan.',
	'update_data_success' => 'Data berhasil diperbarui.',
	'delete_data_success' => 'Data berhasl dihapus.',
	'add_data_error' => 'Gagal menambahkan data.',
	'update_data_error' => 'Gagal memperbarui data.',
	'delete_data_error' => 'Gagal menghapus data.',
	'Email not found.' => 'Email tidak ditemukan.',
	'Password incorect.' => 'Kata sandi salah.',
	'Account blocked.' => 'Akun dibekukan.',
	'User has no group privileges.' => 'Pengguna tidak memiliki hak akses grup.',
	'Invalid credentials.' => 'Kredensial tidak valid.',
	'Please check your email for verification code.' => 'Silakan periksa email Anda untuk kode verifikasi.',
];
