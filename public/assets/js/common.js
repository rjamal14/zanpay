var $tableState,
optValidate = {
    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    errorClass: 'validation-error-label',
    successClass: 'validation-valid-label',
    highlight: function(element, errorClass) {
        $(element).removeClass('input-error');
    },
    unhighlight: function(element, errorClass) {
        $(element).removeClass('input-error');
    },

    // Different components require proper error label placement
    errorPlacement: function(error, element) {

        /*
        // Styled checkboxes, radios, bootstrap switch
        if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
            if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent().parent().parent() );
            }
            else {
                error.appendTo( element.parent().parent().parent().parent().parent() );
            }
        }

        // Unstyled checkboxes, radios
        else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
            error.appendTo( element.parent().parent().parent() );
        }

        // Input with icons and Select2
        else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
            error.appendTo( element.parent() );
        }

        // Inline checkboxes, radios
        else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
            error.appendTo( element.parent().parent() );
        }

        // Input group, styled file input
        else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
            error.appendTo( element.parent().parent() );
        }

        else {
            error.insertAfter(element);
        }
        */
       element.addClass('input-error')
    },
    validClass: "validation-valid-label"
},
showPassword = (elem) => {
    var x = document.getElementById(elem);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
},
singleDatePicker = (elem = '') => {
    if (elem == '') {
        elem = '.datepicker';
    }
    $(elem).daterangepicker({ 
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        maxDate: moment(),
    });
},
setSwitch = () => {
    var infoSwitch = document.querySelectorAll('.switchery');
    for (var i = 0; i < infoSwitch.length; i++) {
        var switchery = new Switchery(infoSwitch[i], { color: '#C20F2F' });
    }
},
setAutocomplete = (elem, url, placeholder = 'Please Select One', dataParam = {}, limit = 10, onSelectCallback = function () {}) => {
    $(elem).select2({
        width: '100%',
        placeholder: placeholder,
        // dropdownParent: $('#modal-input'),
        minimumInputLength: 2,
        ajax: {
            url: _baseURL + url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                let query = {
                    keyword: params.term,
                    page: params.page || 1
                }
                let queryParam = {
                    ...query,
                    ...dataParam
                }
                return queryParam;
            },
            delay: 250,
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * limit) < data.total_count
                    }
                };
            },
            cache: true
        }
    })
    .on('select2:select', onSelectCallback);
}

jQuery.extend(jQuery.validator.messages, {
    required: 'This field is required'
});

// MAIN FUNCTION
$(function() {

    var infoSwitch = document.querySelectorAll('.switchery');
    for (var i = 0; i < infoSwitch.length; i++) {
        var switchery = new Switchery(infoSwitch[i], { color: '#C20F2F' });
    }
    
    $('.select2').select2({
        minimumResultsForSearch: -1
    });

    const numeric = AutoNumeric.multiple('.numeric', {
        minimumValue: '0',
        decimalPlaces: "0"
    });

    const currency = AutoNumeric.multiple('.currency', {
        maximumValue: '10000000000000000000000'
    });

    const percentage = AutoNumeric.multiple('.percentage', {
        minimumValue: '0'
    });

    $('.datepicker').daterangepicker({ 
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: "DD/MM/YYYY"
        },
        maxDate: moment(),
    });

    $('.input-styled').uniform();

    // EXTEND DEFAULT AJAX REQUEST
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="x-app-token"]').attr('content')
        },
        statusCode: {
            401: function() { 
                window.location.href = _baseURL + '/login?redirect=' + window.location.href; //or what ever is your login URI 
            },
            419: function() { 
                window.location.href = _baseURL + '/login?redirect=' + window.location.href; //or what ever is your login URI 
            },
            403: function (xhr) {
                toast(xhr.responseJSON.message, 'error')
            }
        },
    });

    // DATATABLE DEFAULT
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header filter-right length-right"lf><"datatable-scroll"tr><"datatable-footer"p>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            searchPlaceholder: locale.search_here,
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '<i class="icon-arrow-right22"></i>', 'previous': '<i class="icon-arrow-left22"></i>' },
            processing: 'Loading data.. Please wait..',
            emptyTable: locale.unavailable_data,
            zeroRecords: locale.unavailable_data,
            info: locale.showing + ' _START_ ' + locale.to + ' _END_ ' + locale.of + ' _TOTAL_ ' + locale.entries,
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
        console.log(message);
    };

    // $.fn.dataTable.Debounce = function ( table, options ) {
    //     var tableId = table.settings()[0].sTableId;
    //     $('.dataTables_filter input[aria-controls="' + tableId + '"]') // select the correct input field
    //         .unbind() // Unbind previous default bindings
    //         .bind('input', (delayTable(function (e) { // Bind our desired behavior
    //             table.search($(this).val()).draw();
    //             return;
    //         }, 700))); // Set delay in milliseconds
    // }
     
    // function delayTable(callback, ms) {
    //     var timer = 0;
    //     return function () {
    //         var context = this, args = arguments;
    //         clearTimeout(timer);
    //         timer = setTimeout(function () {
    //             callback.apply(context, args);
    //         }, ms || 0);
    //     };
    // }

    // CHECK ACTIVE MENU
    $('.menu-item.active').closest('.nav-item').find('.navbar-nav-link').addClass('active');

    // CLOSE RIGHT LAYER
    $(document).on('click', '.dismiss-layer', function() {
        dismissLayer();
    });
});

function blockUI($element = 'body', bg = 'rgba(0,0,0,0.98)') {
    $($element).block({ 
        message: '<i class="icon-spinner4 spinner text-white"></i> <span class="d-block text-white">Loading..</span>',
        overlayCSS: {
            backgroundColor: bg,
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function unblockUI($element = 'body')
{
    $($element).unblock();
}

function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
{
    if (!$buttons) {
        $buttons = {
            confirm: {
                label: 'Ya',
                className: 'btn-success'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-danger'
            }
        };
    }
    bootbox.confirm({
        title: $title,
        message: $message,
        buttons: $buttons,
        callback: $callback
    });
}

// TOAST
function toast(message = '', type = 'success', callback = {}) {
    new Noty({
        layout: 'topRight',
        theme: 'limitless',
        text: message,
        type: type,
        callbacks: callback,
        timeout: 2500
    }).show();
}

function deleteTableList(oTable, url, id, message = 'Are you sure want to delete this data?') {
    bootbox.confirm({
        title: 'Delete Data',
        message: message,
        buttons: {
            confirm: {
                label: 'Delete',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if(result) {
                $.ajax({
                    url: url,
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    type: 'post',
                    success: function(d) {
                        if(d.status == 200) {
                            toast(d.message, 'success')
                            oTable.draw(true);
                        } else {
                            bootbox.alert('Oops. Error when processing your request.<br/><strong>' + d.message + '</strong>'); 
                        }
                    },
                    error: function(d, statusText, xhr) { 
                        bootbox.alert('Oops. Error when processing your request. ' + xhr); 
                    },
                    beforeSend: function () {
                        blockUI('body')
                    },
                    complete: function () {
                        unblockUI('body')
                    }
                });
            }
        }
    });
}

function reloadTable(oTable) {
    oTable.draw(true);
}

// ======================== MODAL ===============================/
function showModal(template = '', title = '', size = 'lg', footer = true, label = {}) {
    var $modal = $('#modal-input');
    $modal.find('.modal-title').empty().html(title);
    $modal.find('.modal-dialog')
        .removeClass('modal-xs')
        .removeClass('modal-lg')
        .removeClass('modal-full')
        .addClass('modal-' + size);
    $modal.find('.modal-body').empty().html(template);

    // BUTTONS
    if (typeof label.save_label !== 'undefined') {
        $('.saveLabel').empty().html(label.save_label);
    } else {
        $('.saveLabel').empty().html(locale.save);
    }
    // var $saveLabel = typeof label.save_label !== 'undefined' ? label.save_label : 'Save';
    // $modal.find('.modal-footer .btnSave').empty().html($saveLabel);

    // FOOTER
    if (!footer) {
        $modal.find('.btnSave').hide();
    } else {
        $modal.find('.btnSave').show();
    }

    $modal.modal('show');
}

// ====================== RIGHT LAYER =============================/
function leftLayer(template, size = 'xs', buttons = [], label = {}) {
    $("body").css({"overflow":"hidden"});
    $('.page-layer .page-layer-1').removeClass('layer-xs').removeClass('layer-lg').addClass('layer-' + size);

    // SET TEMPLATE
    $('.page-layer .page-layer-container').empty().html(template);

    // SET SELECT2
    $('.page-layer .page-layer-container .select').each(function(i,val) {
        $(this).select2();
    })

    // SET BUTTON
    var $cButton = '';
    if (buttons && buttons.includes('cancel')) {
        $cButton += `
        <button class="btn btn-default dismiss-layer">
            <i class="icon-cross2 position-left"></i> Batal
        </button>
        `;
    }
    if (buttons && buttons.includes('draft')) {
        $cButton += `
        <button class="btn bg-purple btnDraftCreate">
            <i class="icon-floppy-disk position-left"></i> Draft
        </button>
        `;
    }
    if (buttons && buttons.includes('save')) {
        var $saveLabel = typeof label.save_label !== 'undefined' ? label.save_label : 'Simpan';
        $cButton += `
        <button class="btn btn-primary btnSaveCreate">
            <i class="icon-checkmark4 position-left"></i> `+ $saveLabel +`
        </button>
        `;
    }
    if (buttons && buttons.includes('delete')) {
        $cButton += `
        <button class="btn btn-danger btnDeleteLayer pull-left">
            <i class="icon-trash"></i>
        </button>
        `;
    }
    if (buttons && buttons.includes('void')) {
        $cButton += `
        <button class="btn btn-danger btnVoid pull-left">
            <i class="icon-cross2 position-left"></i> Void
        </button>
        `;
    }
    if (buttons && buttons.includes('ok')) {
        $cButton += `
        <button class="btn btn-default dismiss-layer">
            <i class="icon-arrow-right8 position-left"></i> Tutup
        </button>
        `;
    }
    $('.page-layer-bottom').empty().append($cButton)

    // INIT NICE SCROLL
    $(".page-layer-content").scrollTop(0);
    // $(".page-layer-content").getNiceScroll().resize();

    // SWITCHERY

    if ($('.page-layer-content .switchery-layer').length > 0) {
        var infoSwitch = document.querySelectorAll('.switchery-layer');
        for (var i = 0; i < infoSwitch.length; i++) {
            var switchery = new Switchery(infoSwitch[i], { color: '#27ad38' });
        }
    }

    if ($('.page-layer-content .styled-layer').length > 0) {
        $('.styled-layer').uniform();
    }

    $('.page-layer').addClass('active');
}

function dismissLayer(dataTable = false) {
    if (dataTable) {
        dataTable.rows('.selected').deselect();
    }
    if ($tableState) {
        $tableState.rows('.selected').deselect();
    }
    $('.page-layer').removeClass('active');
    setTimeout(function() {
        $('.page-layer .page-layer-container').empty();
        $('.page-layer .page-layer-bottom').empty();
    }, 500);
    
    $("body").css({"overflow":"auto"});

    
}
 // NUMERIC INPUT
 $(document).on("keypress keyup blur", '.numeric-input', function (event) {    
    var self = $(this);
    self.val(self.val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || self.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
    {
      event.preventDefault();
    }
});