$(function() {
    var imgCount = 0, $stateID = false;
    var oDataList = $('.manufacturing_task-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/production/manufacturing_tasks/fetch',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".styled").uniform();
        },
        columns: [
            {data: 'name', name: 'name', width: '20%'},
            {data: 'description', name: 'description'},
            {data: 'staff_table', name: 'staff_id', width: '20%'},
            {data: 'created_table', name: 'created_at', width: '10%'},
            {data: 'modified_table', name: 'updated_at', width: '10%'},
            {data: 'active_table', name: 'is_active', searchable: false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
            {data: 'order', className:'order', searchable:false, orderable: false},
            {data: 'position', name:'position', visible:false, orderable: false},
            {data: 'idReorder', name:'idReorder', visible:false, orderable: false},
        ],
        "order": [[ 8, "asc" ]],
        rowReorder: {
            dataSrc: 'idReorder',
            selector: 'a.reorder',
        },
    });
    // var debounce = new $.fn.dataTable.Debounce(oDataList);

    oDataList.on( 'row-reorder', function ( e, diff, edit ) {
        var arrReorder = [];
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            // get id row
            let idQ = diff[i].oldData;
            let idNewQ = idQ.substr (0, idQ.indexOf ( '_' ));
            let pos = diff[i].newData.substr ( diff[i].newData.indexOf ( '_' ) + 1 );
            console.log(diff[i])
            console.log(pos)
            console.log(idNewQ)
            arrReorder[i] = {id:idNewQ, position:pos}
            // updateOrder(idNewQ, pos);
        }
        updateOrder(arrReorder)
    } );

    function updateOrder(position) {
        $.ajax({
            url: _baseURL + '/production/manufacturing_tasks/reorder',
            method: 'POST',
            dataType: 'json',
            data: {
                position: position,
            },
            success: function (d) {
                console.log(position)
                oDataList.draw(false)
            },
            error: function(xhr) {
                console.log(xhr)
            },
            beforeSend: function () {
                blockUI('body')
            },
            complete: function () {
                unblockUI('body')
            }
        });
    }
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    // DELETE
    $('.manufacturing_task-list').on('click', '.btn-delete-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var data = oDataList.row(tr).data();
        deleteTableList(oDataList, 'manufacturing_tasks/delete', data.id);
        return false;
    });

    // EDIT
    $('.manufacturing_task-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/manufacturing_tasks/edit/' + userData.id
    });

    // DETAIL
    $('.manufacturing_task-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/manufacturing_tasks/detail/' + userData.id
    });

    setAutocomplete('.select-staff', '/ac/users', 'Please select staff..', {is_active: 1});
});