$(function() {
    var imgCount = 0, $stateID = false;
    var oDataList = $('.part-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/production/parts/fetch',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".styled").uniform();
        },
        columns: [
            // {data: 'id', name: 'id', width: '10%'},
            {data: 'photo_table', name: 'photo', width: '5%',searchable:false, orderable: false},
            {data: 'name', name: 'name'},
            {data: 'code', name: 'code'},
            {data: 'price_table', name: 'price', searchable:false, class: "text-right" },
            {data: 'stock', name: 'stock', searchable:false},
            {data: 'low_threshold', name: 'low_threshold', searchable:false},
            {data: 'status_table', name: 'status', searchable:false, width: '10%', class: "text-center"},
            {data: 'supplier_table', name: 'status', searchable:false,  class: "text-center"},
            {data: 'active_table', name: 'is_active', searchable: false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
        ],
        "order": [[ 2, "asc" ]]
    });
    // var debounce = new $.fn.dataTable.Debounce(oDataList);

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    // DELETE
    $('.part-list').on('click', '.btn-delete-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var data = oDataList.row(tr).data();
        deleteTableList(oDataList, 'parts/delete', data.id);
        return false;
    });

    // EDIT
    $('.part-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/parts/edit/' + userData.id
    });

    // DETAIL
    $('.part-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/parts/detail/' + userData.id
    });

    $('.btn-remove-image-exits').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card-img-actions').remove();
    })

    $(document).on('click', '.btn-add-image', function (e) {
        e.preventDefault();
        $('.img-init-' + imgCount).click();
        
    });

    $(document).on('click', '.btn-remove-image', function (e) {
        e.preventDefault();
        var c = $(this).attr('data-count');
        $(this).closest('.card-img-actions').remove();
        $('.img-init-' + c).remove();
    });

    $(document).on('change', '.img-part', function(){
        readURL(this);
    });

    function readURL(input) {
        $.each(input.files, function(i, val) {
            if (val) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.img-list-container').append(`
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="img-fluid border-1 border-grey-300 p-1 border-dashed" src="`+e.target.result+`" width="100" height="100" alt="">
                        <div class="card-img-actions-overlay">
                            <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon btn-remove-image" data-count="`+imgCount+`">
                                <i class="icon-cross"></i>
                            </a>
                        </div>
                    </div>
                    `);
                    imgCount++;
                    $('.file-container').append('<input type="file" name="photo[]" multiple class="img-part img-init-'+imgCount+'" accept="image/*" style="display:none;">')
                }
                reader.readAsDataURL(val);
            }
        })
        /*
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.img-list-container').append(`
                <div class="card-img-actions d-inline-block mb-3">
                    <img class="img-fluid border-1 border-grey-300 p-1 border-dashed" src="`+e.target.result+`" width="100" height="100" alt="">
                    <div class="card-img-actions-overlay">
                        <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon btn-remove-image" data-count="`+imgCount+`">
                            <i class="icon-cross"></i>
                        </a>
                    </div>
                </div>
                `);
                imgCount++;
                $('.file-container').append('<input type="file" name="photo[]" class="img-part img-init-'+imgCount+'" accept="image/*" style="display:none;">')
            }
            reader.readAsDataURL(input.files[0]);
        }
        */
    }

    setAutocomplete('.select-supplier', '/ac/suppliers', 'Please select supplier..', {});
});