$(function() {
    var imgCount = 0,
    oDataList = $('.product-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/production/products/fetch',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".styled").uniform();
        },
        columns: [
            {data: 'id', name: 'id', width: '10%'},
            {data: 'photo_table', name: 'photo', width: '5%',searchable:false, orderable: false},
            {data: 'name', name: 'name'},
            {data: 'code', name: 'code'},
            {data: 'model', name: 'model'},
            {data: 'description', name: 'description'},
            {data: 'working_hour', name: 'working_hour', searchable:false, width: '10%'},
            {data: 'status_table', name: 'is_active', searchable: false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
        ],
        "order": [[ 2, "asc" ]]
    });

    // var debounce = new $.fn.dataTable.Debounce(oDataList);

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    // DELETE
    $('.product-list').on('click', '.btn-delete-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var data = oDataList.row(tr).data();
        deleteTableList(oDataList, 'products/delete', data.id);
        return false;
    });

    // EDIT
    $('.product-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/products/edit/' + userData.id
    });

    // DETAIL
    $('.product-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/production/products/detail/' + userData.id
    });

    $('.btn-remove-image-exits').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card-img-actions').remove();
    })

    $(document).on('click', '.btn-add-image', function (e) {
        e.preventDefault();
        $('.img-init-' + imgCount).click();
        
    });

    $(document).on('click', '.btn-remove-image', function (e) {
        e.preventDefault();
        var c = $(this).attr('data-count');
        $(this).closest('.card-img-actions').remove();
        $('.img-init-' + c).remove();
    });

    $(document).on('change', '.img-part', function(){
        readURL(this);
    });

    $('.btn-add-file').on('click', function (e) {
        $('.3dfile-container').append(`
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10 mt-1">
                <input type="file" name="3dfile[]" class="input-styled" data-fouc>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 pl-0 mt-1">
                <button class="btn btn-light btn-remove-file" type="button">
                    <i class="icon-trash mr-2 text-danger"></i> Remove
                </button>
            </div>
        </div>
        `);
        $('.input-styled').uniform();
    });

    $('.3dfile-container').on('click', '.btn-remove-file', function (e) {
        $(this).closest('.row').remove();
    });

    $('.remove-3dfile').on('click', function (e){
        e.preventDefault();
        $(this).closest('.existing-3dfile').remove();
    })

    setAutocomplete('.select-part', '/ac/production/parts', 'Please select parts..', {
        is_active: 1
    });

    function readURL(input) {
        $.each(input.files, function(i, val) {
            if (val) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.img-list-container').append(`
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="img-fluid border-1 border-grey-300 p-1 border-dashed" src="`+e.target.result+`" width="100" height="100" alt="">
                        <div class="card-img-actions-overlay">
                            <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon btn-remove-image" data-count="`+imgCount+`">
                                <i class="icon-cross"></i>
                            </a>
                        </div>
                    </div>
                    `);
                    imgCount++;
                    $('.file-container').append('<input type="file" name="photo[]" multiple class="img-part img-init-'+imgCount+'" accept="image/*" style="display:none;">')
                }
                reader.readAsDataURL(val);
            }
        })
        // if (input.files && input.files[0]) {
        //     var reader = new FileReader();
        //     reader.onload = function (e) {
        //         $('.img-list-container').append(`
        //         <div class="card-img-actions d-inline-block mb-3">
        //             <img class="img-fluid border-1 border-grey-300 p-1 border-dashed" src="`+e.target.result+`" width="100" height="100" alt="">
        //             <div class="card-img-actions-overlay">
        //                 <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon btn-remove-image" data-count="`+imgCount+`">
        //                     <i class="icon-cross"></i>
        //                 </a>
        //             </div>
        //         </div>
        //         `);
        //         imgCount++;
        //         $('.file-container').append('<input type="file" name="photo[]" class="img-part img-init-'+imgCount+'" accept="image/*" style="display:none;">')
        //     }
        //     reader.readAsDataURL(input.files[0]);
        // }
    }

     // IMPORT FROM DT TABLE TO PRODUCT LIST
     $(document).on('click', '.enableInput', function (e) { 
        $('.inputQtyTr').prop('disabled', false);
     });
    


    $('.select-part').on('select2:unselect', function (e) {
        var el = document.getElementById(e.params.data.id);
        el.remove()

    });
    $('.select-part').on('select2:select', function (e) {
        console.log(e)
            // if($(".partsTr").length){
            // } else {
                var c='';
                $.ajax({
                    url: _baseURL + '/production/parts/fetch-v2?perPage=1000',
                    data:  {ids:e.params.data.id},
                    type: 'get',
                    dataType: 'json',
                    success: function (d) {
                          
                            $.each(d, function (i,val) {
                                c += `
                                <tr class ='partsTr ' id='`+val.id+`'>
                                    <td><span>`+val.name+`</span></td>
                                    <td><span>`+val.unit_name_string+`</span></td>
                                    <td><input type="text"  class="form-control inputQtyTr numeric-input" name="partsQty[]" value=""><input type="hidden"  class="form-control item_total partsV"  name="partsIDs[]" value="`+val.id+`"></td>
                                </tr>
                                `;
                                            $('.table-add-product tbody').append(c);
                                    
                            
                            });
        
                                    
                    },
                    error: function(xhr) {
                        console.log(xhr)
                    },
                    beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
                console.log(c)
            // }
            
            // e.preventDefault();
            // var c='', $dtCheck = true;
    
            // $checkedDtValue =[];
            // var rows = oProductOutletList.rows({ 'search': 'applied' }).nodes();
            // $('input[type="checkbox"]', rows).each(function () {
            //     if ($(this).is(':checked')) {
            //         var tr = $(this).closest("tr");
            //         var row = oProductOutletList.row(tr).data();
            //         $checkedDtValue.push(row);
            //     }
            // });
    
            // if ($checkedDtValue.length > 0) {
            //     $.each($checkedDtValue, function (i,val) {
            //         // CHECK IF DATA EXIST
            //         $dtCheck = true;
            //         $('.table-add-product tbody tr').each(function (it, valt) {
            //             if ($(this).find('td a.btnRemoveItemDt').attr('data-id') == val.item_id) {
            //                 $dtCheck = false;
            //             }
            //         });
            //         if ($dtCheck) {
            //             c += `
            //             <tr>
            //                 <td><span>`+val.item_name+`</span></td>
            //                 <td><input type="text" class="form-control item_qty number_format to-calculate" name="item_qty[]" value="`+numberFormat(0)+`"></td>
            //                 <td><input type="text" class="form-control item_price currency_format to-calculate" name="item_price[]" value="`+currencyFormat(val.item_harga_modal)+`"></td>
            //                 <td><input type="text" disabled class="form-control item_total currency_format" name="item_total[]" value="`+currencyFormat(0)+`"></td>
            //                 <td><a href="#" class="btn btn-link btnRemoveItemDt" data-id="`+val.item_id+`"><i class="icon-cancel-circle2"></i></a></td>
            //             </tr>
            //             `;
            //         }
            //     });
            //     if ($('.table-add-product tbody tr').find('.btnRemoveItemDt').length > 0) {
            //         if (c != '') {
            //             $('.table-add-product tbody').append(c);
            //         }
            //     } else {
            //         $('.table-add-product tbody').empty().append(c);
            //     }
                
            //     // CLEAN UP DATA TABLE
            //     $('.table-add-product tbody tr').each(function (it, valt) {
            //         var $that = $(this);
            //         $dtCheck = true;
            //         $.each($checkedDtValue, function (i,val) {
            //             if ($that.find('td a.btnRemoveItemDt').attr('data-id') == val.item_id) {
            //                 $dtCheck = false;
            //             }
            //         });
    
            //         if ($dtCheck) {
            //             $that.remove();
            //         }
            //     });
    
            //     $('.totalItemCount').html($checkedDtValue.length + ' barang');
            //     calculateTotal();
            // } else {
            //     $('.table-add-product tbody').empty().append(`
            //         <tr class="unavailable_data">
            //             <td colspan="5" class="text-center">
            //                 <span>Tidak ada data</span>
            //             </td>
            //         </tr>
            //     `);
            //     $('.totalItemCount').empty();
            // }
            // $('#modal-dt-product-outlet').modal('hide');
            // $(".page-layer-content").getNiceScroll().resize();
    });
    
});