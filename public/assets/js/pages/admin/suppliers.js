$(function() {
    
    var oDataList = $('.supplier-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/admin/suppliers/fetch',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".input-styled").uniform();
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'abn', name: 'abn', width: '10%'},
            {data: 'address', name: 'address'},
            {data: 'contact_name', name: 'contact_name'},
            {data: 'email', name: 'email'},
            {data: 'status_table', name: 'status', searchable:false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
        ],
        "order": [[ 0, "asc" ]]
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    // DELETE
    $('.supplier-list').on('click', '.btn-delete-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var data = oDataList.row(tr).data();
        deleteTableList(oDataList, 'suppliers/delete', data.id);
        return false;
    });

    // EDIT
    $('.supplier-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/suppliers/edit/' + userData.id
    });

    // DETAIL
    $('.supplier-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/suppliers/detail/' + userData.id
    });
});