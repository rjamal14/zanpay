$(function() {
    var $editState = false, $stateID = false;
    var oDataList = $('.user-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/admin/users/fetch',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".styled").uniform();
        },
        columns: [
            {data: 'user_code', name: 'user_code', width: '10%'},
            {data: 'username', name: 'username', width: '10%'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'role_name', name: 'roles_id', searchable:false},
            {data: 'status_table', name: 'status', searchable:false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
        ],
        "order": [[ 1, "desc" ]]
    });
    // var debounce = new $.fn.dataTable.Debounce(oDataList);

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    // EDIT
    $('.user-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/users/edit/' + userData.id
    });

    // DETAIL
    $('.user-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/users/detail/' + userData.id
    });

    $(document).on('click', '.btn-add-image', function (e) {
        e.preventDefault();
        $('.img-init').click(); 
    });

    $(document).on('change', '.img-init', function(){
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('.image-container').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
});