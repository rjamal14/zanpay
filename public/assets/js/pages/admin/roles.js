$(function() {
    // ROLE
    var oDataList = $('.role-list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _baseURL + '/admin/roles/fetch?a=roles',
            data: function (d) {
                
            }
        },
        "fnDrawCallback": function( oSettings ) {
            $(".input-styled").uniform();
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'status_table', name: 'status', searchable:false, width: '10%', class: "text-center"},
            {data: 'action', searchable:false, orderable: false, width: '10%', class: "text-center"},
        ],
        "order": [[ 0, "desc" ]]
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });

    var oDataPriviledge = $('.priviledge-list').DataTable({
        dom: '<"datatable-scroll"tr>',
        fixedHeader: true,
        columnDefs: [
            {targets:[0,1,2,3,4], orderable: false}
        ],
        ordering: false
    });

    // EDIT
    $('.role-list').on('click', '.btn-edit-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/roles/edit/' + userData.id
    });

    // DETAIL
    $('.role-list').on('click', '.btn-detail-table', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        userData = oDataList.row(tr).data();
        blockUI();
        window.location.href = _baseURL + '/admin/roles/detail/' + userData.id
    });

    $('.view_check').on('click', function () {
        checkAll($(this), '.view-list')
    });

    $('.add_check').on('click', function () {
        checkAll($(this), '.add-list')
    });

    $('.edit_check').on('click', function () {
        checkAll($(this), '.edit-list')
    });

    $('.delete_check').on('click', function () {
        checkAll($(this), '.delete-list')
    });

    $('.view-list').on('click', function() {
        checkInitList('view')
    })

    $('.add-list').on('click', function() {
        checkInitList('add')
    })

    $('.edit-list').on('click', function() {
        checkInitList('edit')
    })

    $('.delete-list').on('click', function() {
        checkInitList('delete')
    });

    $('.priviledge-list').on('click', '.check-row', function () {
        checkAllRow($(this));
    });

    function checkAll(elem, name) {
        if (elem.is(":checked")) {
            $.each($(name), function (i,val) {
                $(this).prop('checked', true)
            })
        } else {
            $.each($(name), function (i,val) {
                $(this).prop('checked', false)
            })
        }
        checkInitRow()
    }

    function checkInitList(name) {
        var checkAll = true
        $.each($('.' + name + "-list"), function (i,val) {
            if (!$(this).is(":checked")) {
                checkAll = false
            }
        })
        if (checkAll) {
            $('.' + name + '_check').prop('checked', true)
        } else {
            $('.' + name + '_check').prop('checked', false)
        }
        checkInitRow()
        return false;
    }

    function checkInitRow() {
        $.each($('.priviledge-items tr'), function (i, val) {
            var checkRow = true
            if (!$(this).find('.view-list').is(":checked")) {
                checkRow = false
            }
            if (!$(this).find('.add-list').is(":checked")) {
                checkRow = false
            }
            if (!$(this).find('.edit-list').is(":checked")) {
                checkRow = false
            }
            if (!$(this).find('.delete-list').is(":checked")) {
                checkRow = false
            }
            if (checkRow) {
                $(this).find('.check-row').prop('checked', true)
            } else {
                $(this).find('.check-row').prop('checked', false)
            }
        })
    }

    function checkAllRow(elem) {
        if (elem.is(":checked")) {
            elem.closest('tr').find('input[type=checkbox]').prop('checked', true);
        } else {
            elem.closest('tr').find('input[type=checkbox]').prop('checked', false);
        }
        checkInitList('view')
        checkInitList('add')
        checkInitList('edit')
        checkInitList('delete')
    }

    checkInitList('view')
    checkInitList('add')
    checkInitList('edit')
    checkInitList('delete')
    checkInitRow()
});