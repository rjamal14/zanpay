$(function() {
    singleDatePicker()
    $('.btn-modal-save').on('click', function (e) {
        $("#frmCreate").validate(optValidate);
        if ($('#frmCreate').valid()) {
            var form = $('#frmCreate')[0];
            var formData = new FormData(form);
            $.ajax({
                url: _baseURL + '/account/profile/edit',
                type: 'post',
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                success: function(d) {
                    if (d.status == 200) {
                        window.location.reload()
                    } else {
                        toast(d.message, 'error');
                    }
                },
                error: function(xhr) {
                    toast(JSON.stringify(xhr[0]), 'error');
                    console.log(xhr);
                },
                beforeSend: function () {
                    blockUI();
                },
                complete: function () {
                    unblockUI();
                }
            });
            
            e.stopImmediatePropagation();
            return false;
        }
        return false;
    });

    $(document).on('click', '.btn-add-image', function (e) {
        e.preventDefault();
        $('.img-init').click(); 
    });

    $(document).on('change', '.img-init', function(){
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('.image-container').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
})