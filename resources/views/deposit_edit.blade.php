<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit deposit Saldo</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
                <div class="card-body">
               
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="name" value="{{$data['reseller']['name']}}"  readonly>
                    </div>
                  </div>
	
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
						<select name="status" id="status" class="form-control" required>
									<option value="0" {{$data['status'] == 0 ? "selected":""}}>Requested</option>
									<option value="1" {{$data['status'] == 1 ? "selected":""}}>Confirmed</option>
									<option value="2" {{$data['status'] == 2 ? "selected":""}}>Canceled</option>
									<option value="3" {{$data['status'] == 3 ? "selected":""}}>Approved</option>
									<option value="4" {{$data['status'] == 4 ? "selected":""}}>Rejected</option>
</select>
                    </div>
                  </div>

                
                    </div>
                  
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editDeposit"  data-id='{{$data["no"]}}' class="btn btn-info editDeposit">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});














    $(document).on("click", "#editDeposit", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate deposit ? ', function(result) {
				if (result) {
				updatedeposit($dataID);
				}
			}, '<span style="color: blue;">Edit deposit</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
	});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function updatedeposit(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/tiket_deposit-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("deposit Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#btnEdit').fadeIn();
            	$('#deposit-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>