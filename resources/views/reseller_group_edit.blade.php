<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Reseller Group</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
					<div class="card-body">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Nama Group</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" value ="{{$data['name']}}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Deskripsi</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="description" value ="{{$data['description']}}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Komisi Point</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" name="point" value ="{{$data['point']}}"  required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Cashback Referral</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" name="cashback" value ="{{$data['cashback']}}" required>
							</div>
						</div>
                	</div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editResellerGroup"  data-id='{{$data["id"]}}' class="btn btn-info editResellerGroup">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editResellerGroup", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Reseller Group ? ', function(result) {
				if (result) {
				updateResellerGroup($dataID);
				}
			}, '<span style="color: blue;">Edit Reseller Group</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function updateResellerGroup(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/reseller-group-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Reseller Group Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
              $('#reseller-group-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>