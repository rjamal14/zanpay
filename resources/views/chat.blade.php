<x-package-header />
<meta name="csrf-token" content="{{ csrf_token() }}" >
<style>
#divChat {
  height: 100px;
  overflow: auto;
  display: flex;
  flex-direction: column-reverse;
}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Chat</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Chat</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card chat-app">
					<div class="chat">
						<div class="chat-header clearfix">
							<div class="row clearfix">
								<div class="col-lg-6">
									<a href="javascript:void(0);" data-toggle="modal" class ="senderAvatar" data-target="#view_info">
										<!-- <img src="../assets/images/xs/avatar2.jpg" alt="avatar" id="contactImage"/> -->
									</a>
									<div class="chat-about">
										<h6 class="m-b-0 sender" ></h6>
										<div class="resellerGroup">
										
										</div>
									</div>
								</div>
							
							</div>
						</div>
						<div id ="divChat"class="chat-history overflow-auto" style="height: 400px;">
							<ul class="m-b-0 chatHistory">
								
							</ul>
						</div>
						<div class="chat-message clearfix">
							<div class="input-group mb-0">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="icon-paper-plane"></i></span>
								</div>
								<textarea type="text" row="5" id="chatInput2" class="form-control" placeholder="Isi Pesan Disini..."></textarea>
								<input type="hidden" row="5" id="chatInput" class="form-control" placeholder="Isi Pesan Disini...">
								<input type="hidden" row="5" id="resellerId" class="form-control" placeholder="Isi Pesan Disini...">
							</div>
						</div>
					</div>
					<div id="plist" class="people-list overflow-auto" style="height: 400px;">
						<div class="input-group mb-3">
							<input type="text" class="form-control" id="searchContact" placeholder="Search...">
							<div class="input-group-append">
								<span class="input-group-text"><i class="icon-magnifier"></i></span>
							</div>
						</div>
						<div>
							<ul class="right_chat list-unstyled mb-0 contactList">
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />

<script>
var element = document.getElementById("divChat");
element.scrollTop = element.scrollHeight;


	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var search ="";
	var $idMessagesss =`{{$aa}}`;
	if($('#chatInput2').val() != "" || $('#chatInput2').val() != "false"){
		$idMessagesss = $('#chatInput').val()
	} else {
		$idMessagesss =`{{$id}}`;
	}
	console.log(	'atas id messagee',`{{$aa}}`);
	$('#resellerId').val(`{{$aa}}`);
	getContact(search);
	getChatHistory(`{{$aa}}`);
	

	function getContact(search) {
		console.log(search)
		$.ajax({
			url: "{{ url('reseller-list') }}",
			dataType: 'json',
			data:{
				'search':search
			},
			success: function (d) {
				c ="";
				$.each(d.data, function (i,val) {
					c += `
						<a href="javascript:void(0);" class="contactDetail" data-id="`+val.id+`">
							<div class="media">
								<img class="media-object " src="`+val.rawImage+`" alt="">
								<div class="media-body">
									<span class="name">`+val.name2+`</span>
									<span class="message">Reseller `+val.group.name+`</span>
								</div>
							</div>
						</a>
					`;
				})		
				$('.contactList').empty().append(c);
			},
			beforeSend: function () {
				
			},
			complete: function () {
				$(document.body).css({'cursor' : 'default'});
			}
		});
	}


	$('#chatInput2').on("keypress", function(e) {
        if (e.keyCode == 13) {
            var messageDetailId = "";
			var resellerId = $('#resellerId').val()
			var isiMessage = $(this).val();
			messageDetailId = $('#chatInput').val()


			if(messageDetailId != "false"){
				$.ajax({
					url: "{{ url('send-chat') }}"+"/"+messageDetailId,
					data:{
						'sender':0,
						'reseller_id':resellerId,
						'message':isiMessage,
					},
					type: "post",
					dataType: 'json',
					success: function (d) {
						$('.chatHistory').empty();
						if(d.data.length > 0){
								$.each(d.data, function (i,val) {
								if (val.sender === null){
									var clas = 'other-message float-right';
									var clasImage = 'text-right';
									var avatarChat =	`<img src="`+val.adminAvatar+`" alt="avatar">	`
									// c += `
									// 	<li class="clearfix">
									// 			<div class="message-data text-right">
									// 				<span class="message-data-time">`+val.datetime+`</span>
									// 				<img src="`+val.adminAvatar+`" alt="avatar">
									// 			</div>
									// 			<div class="message other-message float-right">`+val.message+`</div>
									// 		</li>
									// `;
								} else {
									var clas = 'my-message';
									var clasImage = '';
									var avatarChat =``;
									// var avatarChat = val.sender.avatar;
									// c += `
									// 		<li class="clearfix">
									// 			<div class="message-data">
									// 				<span class="message-data-time">`+val.datetime+`</span>
									// 				<img src="`+val.avatar+`" alt="avatar">
									// 			</div>
									// 			<div class="message my-message">`+val.message+`</div>
									// 		</li>
									// `;
									
								}
									c += `
											<li class="clearfix">
												<div class="message-data `+clasImage+`">
													<span class="message-data-time">`+val.datetime+`</span>
													`+avatarChat+`
												</div>
												<div class="message `+clas+`">`+val.message+`</div>
											</li>
									`;

							})
							
							$('.chatHistory').empty().append(c);

							
						}	
						$('#chatInput').val(d.messageId);
						$('#resellerId').val(d.resellerId);
					
						$('#chatInput2').val("")
					},
					beforeSend: function () {
						$(document.body).css({'cursor' : 'wait'});

					},
					complete: function () {
						$(document.body).css({'cursor' : 'default'});
					}
				});
			} else {
				$.ajax({
					url: "{{ url('create-chat') }}",
					data:{
						'sender':0,
						'reseller_id':resellerId,
						'message':isiMessage,
					},
					type: "post",
					dataType: 'json',
					success: function (d) {
						$('.chatHistory').empty();
						if(d.data.length > 0){
								$.each(d.data, function (i,val) {
								if (val.sender === null){
									var clas = 'other-message float-right';
									var clasImage = 'text-right';
									var avatarChat =	`<img src="`+val.adminAvatar+`" alt="avatar">	`
									// c += `
									// 	<li class="clearfix">
									// 			<div class="message-data text-right">
									// 				<span class="message-data-time">`+val.datetime+`</span>
									// 				<img src="`+val.adminAvatar+`" alt="avatar">
									// 			</div>
									// 			<div class="message other-message float-right">`+val.message+`</div>
									// 		</li>
									// `;
								} else {
									var clas = 'my-message';
									var clasImage = '';
									var avatarChat =``;
									// var avatarChat = val.sender.avatar;
									// c += `
									// 		<li class="clearfix">
									// 			<div class="message-data">
									// 				<span class="message-data-time">`+val.datetime+`</span>
									// 				<img src="`+val.avatar+`" alt="avatar">
									// 			</div>
									// 			<div class="message my-message">`+val.message+`</div>
									// 		</li>
									// `;
									
								}
									c += `
											<li class="clearfix">
												<div class="message-data `+clasImage+`">
													<span class="message-data-time">`+val.datetime+`</span>
													`+avatarChat+`
												</div>
												<div class="message `+clas+`">`+val.message+`</div>
											</li>
									`;

							})
							
							$('.chatHistory').empty().append(c);


							

							
						
							
						}	
						$('#chatInput').val(d.messageId);
						$('#resellerId').val(d.resellerId);
					
						$('#chatInput2').val("")
					},
					beforeSend: function () {
						
					},
					complete: function () {
						$(document.body).css({'cursor' : 'default'});
					}
				});
			}
            // alert( $(this).val());
            return false; // prevent the button click from happening
        }
	});
	$('#searchContact').on("keyup", function(e) {
        getContact($(this).val())
        }
	);

	function getChatHistory($id) {
		console.log($id);
		console.log($('#chatInput').val());
		$.ajax({
			url: "{{ url('chat-history-list') }}"+"/"+$id,
			dataType: 'json',
			success: function (d) {
				c ="";
				var $sender = d.sender.name;
				var $senderAvatar = '<img src="'+d.sender.avatar+'"  style="height:40px!important;" alt="avatar">';	
				// var $senderAvatar = '<div class="media"><img class="media-object " src="'+d.sender.avatar+'" alt=""><div class="media-body">';	
				$('.senderAvatar').empty().append($senderAvatar);
				$('.sender').html($sender);
				var $groupReseller=	'<small>Reseller '+d.sender.group.name+'</small>';
				$('.resellerGroup').empty().append($groupReseller);
				$('.chatHistory').empty();
				if(d.data.length > 0){
						$.each(d.data, function (i,val) {
						if (val.sender === null){
							var clas = 'other-message float-right';
							var clasImage = 'text-right';
							var avatarChat =	`<img src="`+val.adminAvatar+`" alt="avatar">	`
							// c += `
							// 	<li class="clearfix">
							// 			<div class="message-data text-right">
							// 				<span class="message-data-time">`+val.datetime+`</span>
							// 				<img src="`+val.adminAvatar+`" alt="avatar">
							// 			</div>
							// 			<div class="message other-message float-right">`+val.message+`</div>
							// 		</li>
							// `;
						} else {
							var clas = 'my-message';
							var clasImage = '';
							var avatarChat =``;
							// var avatarChat = val.sender.avatar;
							// c += `
							// 		<li class="clearfix">
							// 			<div class="message-data">
							// 				<span class="message-data-time">`+val.datetime+`</span>
							// 				<img src="`+val.avatar+`" alt="avatar">
							// 			</div>
							// 			<div class="message my-message">`+val.message+`</div>
							// 		</li>
							// `;
							
						}
							c += `
									<li class="clearfix">
										<div class="message-data `+clasImage+`">
											<span class="message-data-time">`+val.datetime+`</span>
											`+avatarChat+`
										</div>
										<div class="message `+clas+`">`+val.parsedMessage+`</div>
									</li>
							`;

					})
					
					$('.chatHistory').empty().append(c);


					

					
				
					
				}	
				$('#chatInput').val(d.messageId);
				$('#resellerId').val(d.resellerId);
			

			},
			beforeSend: function () {
				
				$('#chatInput').val('');
				$('#resellerId').val('');
			},
			complete: function () {
				$(document.body).css({'cursor' : 'default'});
				element.scrollTop = element.scrollHeight;
			}
		});
	}


	var pusher = new Pusher('{{env("MIX_PUSHER_APP_KEY")}}', {
      cluster: '{{env("PUSHER_APP_CLUSTER")}}',
      encrypted: true
    });
 
    var channel = pusher.subscribe('new-message');
	$(document).on('click','.contactDetail', function() {
		$id = $(this).attr('data-id');
		$('#chatInput2').val("");
		$('#resellerId').val($id);
		getChatHistory($id);
    });
    channel.bind('App\\Events\\NewMessage', function(data) {
	
		console.log('resellerID: ',data.resellerId);
		console.log('#resellerId: ',$('#resellerId').val());
			if ($('#resellerId').val() == data.resellerId) {
				getChatHistory(data.resellerId);
			}
    });
	




  


				
</script>