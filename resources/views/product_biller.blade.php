<meta name="csrf-token" content="{{ csrf_token() }}" />
<x-package-header />
<style>
	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 200px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {}

	.table-custom td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-6 col-sm-12">
				<h2>Produk Biller</h2>
			</div>

			<div class="col-md-6  col-sm-6 text-right">
				<a class="btn  btn-sm btn-primary mr-3" href="{{url('/download-produk?type=biller')}}"><i class="fa fa-download"></i>&nbsp;Export Excel</a>
				<!-- <a   class="btn  btn-sm btn-primary mr-3 btnExport" href="#"  ><i class="fa fa-download"></i>&nbsp;Export Excel</a> -->
				<a class="btn  btnImport btn-sm btn-success mr-3" href="#"><i class="fa fa-upload"></i>&nbsp;Import Excel</a>
				<button id="addBtn" class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
				<!-- <button class="btn btn-sm btn-primary mr-1 exportCsv">Export CSV</button> -->
			</div>
		</div>
	</div>
	<div class="card-body" id="importForm" style="display: none;">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Import Excel</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" role="form" id="frmImport" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">File</label>
						<div class="col-sm-10">
							<input name="productFile" accept=".xlsx" type="file" id="productFile" class="form-control" required>
						</div>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<button type="" id="importBtn" class="btn btn-info importBtn">Import</button>
						<button id="cancelBtnImport" type="button" class="btn btn-default float-right">Cancel</button>
					</div>
					<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>
<div class="card-body" id="editForm"></div>
<div class="card-body" id="addForm" style="display: none;">
	<div class="card card-primary">

		<div class="card-header">
			<h3 class="card-title"> Tambah Produk Biller</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
			@csrf
			<div class="card-body">

				<!-- $productNew->biller_id = $request->biller_id;
            $productNew->group_id = $request->group_id;
            $productNew->category = 0;
            $productNew->sku_code = $request->sku_biller;
            $productNew->name = $request->description;
            $productNew->price = $request->price_basic;
            $productNew->nominal = ceil((int)$request->price_basic / 1000) * 1000; ;
            $productNew->admin_fee = $request->admin_fee;
            $productNew->commission = 0;
            $productNew->biller_id = $request->biller_id; -->

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Biller</label>
					<div class="col-sm-10">
						<select name="biller_id" id="billerID" class="form-control productSelect" required>
						lect name="parsingType" id="parsingType" class="form-control productSelect">
							<option value="">-- Pilih --</option>
							@foreach($biller as $b)
							<option value="{{ $b['id']}}">{{$b['name']}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tipe (parsing)</label>
					<div class="col-sm-10">
						<select name="parsingType" id="parsingType" class="form-control productSelect">
							<option value="">-- Pilih --</option>
							<!-- @foreach($tipe as $ty)
							<option value="{{ $ty['id']}}">{{$ty['text']}}</option>
							@endforeach -->
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Jenis</label>
					<div class="col-sm-10">
						<select name="group_id" id="groupID" class="form-control productSelect">
							<option value="">-- Pilih --</option>
							<!-- @foreach($group as $g)
							<option value="{{ $g['id']}}">{{$g['command']." - ".$g['provider']}}</option>
							@endforeach -->
						</select>
					</div>
				</div>



				<div class="form-group row">
					<label class="col-sm-2 col-form-label">SKU Biller</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="sku_biller" required>
					</div>
				</div>
				<!-- <div class="form-group row">
				<label class="col-sm-2 col-form-label">Description</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="description" required>
				</div>
			</div> -->
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Harga Dasar</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="price_basic" required>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Admin Fee</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="admin_fee" required>
					</div>
				</div>


				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Prioritas</label>
					<div class="col-sm-10">
						<select name="priority" id="priority" class="form-control" required>
							<option value="1">Tinggi</option>
							<option value="2">Normal</option>
							<option value="3">Rendah</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Produk SKU</label>
					<div class="col-sm-10">
						<select name="product_id" id="productID" class="form-control productSelect">
							<option value="0">--Silahkan Pilih Group--</option>
						</select>
					</div>
				</div>

			</div>

			<!-- /.card-body -->
			<div class="card-footer">
				<button type="" id="addProduct" data-id='' class="btn btn-info addProduct">Submit</button>
				<button id="cancelBtnAdd" type="button" class="btn btn-default cancelBtnAdd float-right">Cancel</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>

</div>
<div class="container-fluid">

	<div class="row clearfix">
		<div class="col-lg-12">
			<div class="card">
				<div class="header">
					<div class="d-flex" style="margin-bottom: 25.75px;margin-top: 25.75px;">
						<div class="mr-auto">
							<table border="0" class="table-custom-info">
								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Biller:</label>
									<div class="col-lg-12">
										<select multiple="multiple" class="form-control fftable js-filterBiller-basic-multiple" id="filterBiller">
											@foreach($biller as $b)
											<option value="{{$b['id']}}">{{$b['name']}}</option>
											@endforeach
										</select>
									</div>
								</th>
								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Tipe:</label>
									<div class="col-lg-12">
										<select multiple="multiple" class="form-control fftable js-filterTipe-basic-multiple" id="filterTipe">
											@foreach($tipe as $v)
											<option value="{{$v['id']}}">{{$v['text']}}</option>
											@endforeach
										</select>
									</div>
								</th>
								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Jenis:</label>
									<div class="col-lg-12">
										<select multiple="multiple" class="form-control fftable js-filterJenis-basic-multiple" id="filterJenis">
											@foreach($jenis as $j)
											<option value="{{$j['id']}}">{{$j['text']}}</option>
											@endforeach
										</select>
									</div>
								</th>



							</table>
						</div>
					</div>
					<div class="body">

						<div class="table-responsive">


							<table class="table table-bordered table-hover c_list  table-custom " id="product_biller-table">
								<thead>
									<tr>
										<th>No.</th>
										<th>Biller</th>
										<th>SKU Biller</th>
										<th>SKU Produk</th>
										<th>Tipe</th>
										<th>Jenis</th>
										<!-- <th>Deskripsi</th> -->
										<th>Harga Dasar</th>
										<th>Admin Fee</th>
										<th>Komisi</th>
										<th>Prioritas</th>
										<th>Status</th>
										<th>Update Terakhir</th>
										<th>Diupdate Oleh</th>
										<th></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script>
	$(document).on("change", "#billerID", function() {
		$thisId = $(this).children('option:selected').val();
		$.ajax({
			url: '{{url("/product-combobox-bygroup3?")}}biller=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#parsingType").empty();
				$("#groupID").empty();
				$("#productID").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit2 = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit2 += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#parsingType").append(appendsEdit2);
				} else {
					$("#parsingType").empty();
					$("#parsingType").append('<option data-id=""  value="">Tipe tidak memiliki jenis</option>');
				}
			},
		});
	});
	$(document).on("change", "#billerIDEdit", function() {
		$thisId = $(this).children('option:selected').val();
		$.ajax({
			url: '{{url("/product-combobox-bygroup3?")}}biller=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#parsingTypeEdit").empty();
				$("#groupIDEdit").empty();
				$("#productIDEdit").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit2 = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit2 += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#parsingTypeEdit").append(appendsEdit2);
				} else {
					$("#parsingTypeEdit").empty();
					$("#parsingTypeEdit").append('<option data-id=""  value="">Tipe tidak memiliki jenis</option>');
				}
			},
		});
	});
	$(document).on("change", "#parsingType", function() {
		$thisId = $(this).children('option:selected').val();
		$.ajax({
			url: '{{url("/product-combobox-bygroup2?")}}group_name=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#groupID").empty();
				$("#productID").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit2 = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit2 += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#groupID").append(appendsEdit2);
				} else {
					$("#groupID").empty();
					$("#groupID").append('<option data-id=""  value="">Tipe tidak memiliki jenis</option>');
				}
			},
		});
	});
	$(document).on("change", "#parsingTypeEdit", function() {
		$thisId = $(this).children('option:selected').val();
		$.ajax({
			url: '{{url("/product-combobox-bygroup2?")}}group_name=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#groupIDEdit").empty();
				$("#productIDEdit").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit212 = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit212 += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#groupIDEdit").append(appendsEdit212);
				} else {
					$("#groupIDEdit").empty();
					$("#groupIDEdit").append('<option data-id=""  value="">Tipe tidak memiliki jenis</option>');
				}
			},
		});
	});

	$(document).on("change", "#groupIDEdit", function() {
		$thisId = $(this).children('option:selected').val();
		$('#activity').prop('required', true);
		$('#jnis').prop('required', false);
		$('#povider').prop('required', false);
		$('.Aktivitas').show();
		$('.Jnis').hide();
		$('.Povider').hide();
		$.ajax({
			url: '{{url("/product-combobox-bygroup?")}}group_id=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#productIDEdit").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit2 = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit2 += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#productIDEdit").append(appendsEdit2);
				} else {
					$("#productIDEdit").empty();
					$("#productIDEdit").append('<option data-id=""  value="">Jenis tidak memiliki produk</option>');
				}
			},
		});
	});


	$(document).on("change", "#groupID", function() {
		$thisId = $(this).children('option:selected').val();
		$('#activity').prop('required', true);
		$('#jnis').prop('required', false);
		$('#povider').prop('required', false);
		$('.Aktivitas').show();
		$('.Jnis').hide();
		$('.Povider').hide();
		$.ajax({
			url: '{{url("/product-combobox-bygroup?")}}group_id=' + $thisId,
			type: 'GET',
			dataType: 'json',
			beforeSend: function() {
				$("#productID").empty();
			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit = '<option data-id=""  value="">Pilih</option>';

				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

					});
					$("#productID").append(appendsEdit);
				} else {
					$("#productID").empty();
					$("#productID").append('<option data-id=""  value="">Jenis tidak memiliki produk</option>');
				}
			},
		});
	});

	function selectTrigger() {
		$('.productSelect').select2({
			"width": '100%'
		});
		$('.productSelectEdit').select2({
			"width": '100%'
		});
	}
	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('.productSelect').select2({
			"width": '100%'
		});
	});

	$(document).on('click', '.btnImport', function() {
		$('#importForm').show();
	})
	$(document).on('click', '#cancelBtnImport', function() {
		$('#importForm').hide();
	})
	// $(document).on('click', '#btnExport', function() {
	// 	bootbox.confirm({
	// 		title: 'Download Produk Biller',
	// 		message: d.message,
	// 		buttons: {
	// 			confirm: {
	// 				label: 'Import',
	// 				className: 'btn-success'
	// 			},
	// 			cancel: {
	// 				label: 'Cancel',
	// 				className: 'btn-danger'
	// 			}
	// 		},
	// 		callback: function(result) {
	// 			if(result){
	// 				$.ajax({
	// 				url:"{{url('/download-produk?type=biller')}}",
	// 					dataType: 'json',  // <-- what to expect back from the PHP script, if anything
	// 					cache: false,
	// 					contentType: false,
	// 					processData: false,
	// 					type: 'get',
	// 					success: function(d){
	// 					},
	// 					beforeSend: function(){
	// 					$(document.body).css({'cursor' : 'wait'});
	// 					},
	// 					complete: function(){
	// 						$(document.body).css({'cursor' : 'default'});
	// 					},
	// 				});
	// 			}
	// 		}
	// 	})

	// })
	$(document).on('click', '#importBtn', function(e) {
		e.preventDefault();
		var file_data = $('#productFile').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		$.ajax({
			url: "{{ url('product_biller-import') }}",
			dataType: 'json', // <-- what to expect back from the PHP script, if anything
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(d) {
				bootbox.confirm({
					title: 'Import Produk Biller',
					message: d.message,
					buttons: {
						confirm: {
							label: 'Import',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if (result) {
							$.ajax({
								url: "{{ url('product_biller-import-do') }}",
								dataType: 'json', // <-- what to expect back from the PHP script, if anything
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
								type: 'post',
								success: async function(dd) {
									if(dd.data.gagal>0){
										var messageError = "";
										await dd.data.message_error.forEach((val)=>{
											messageError += val+'<br>'
										})

										messageError += `<br><br><b>Total Success : ${dd.data.sukses}</b>`
										messageError += `<br><b>Total Gagal &nbsp;&nbsp;&nbsp;&nbsp;: ${dd.data.gagal}</b>`
										bootbox.alert(messageError);
									}else{
										bootbox.alert(dd.message);
									}
									console.log(dd)
								},
								error: function(dd, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function() {
									$(document.body).css({
										'cursor': 'wait'
									});
									// $('.page-loader-wrapper').show();
									// $.blockUI({ overlayCSS: { backgroundColor: '#00f' } });
								},
								complete: function() {
									$(document.body).css({
										'cursor': 'default'
									});
									$('#product_biller-table').DataTable().ajax.reload();
									// $('.page-loader-wrapper').hide();
									// $.unblockUI();
								},
							});
						}
					}
				});
				console.log(d); // <-- display response from the PHP script, if any
			},
			beforeSend: function() {
				$(document.body).css({
					'cursor': 'wait'
				});
				// $('.page-loader-wrapper').show();
				// $.blockUI({ overlayCSS: { backgroundColor: '#00f' } });
			},
			complete: function() {
				$(document.body).css({
					'cursor': 'default'
				});
				// $('.page-loader-wrapper').hide();
				// $.unblockUI();
			},
		});

	})
	// $(function() {
	//     var _baseURL = "<?php echo url(''); ?>";
	// 	var filterJenis = [];
	//     var filterTipe = [];
	//     var filterBiller = [];
	// 		$('.js-filterBiller-basic-multiple').select2({
	// 		placeholder: "Semua Biller",
	// 	});
	// 	$('.js-filterJenis-basic-multiple').select2({
	// 		placeholder: "Semua Jenis",
	// 	});
	// 	$('.js-filterTipe-basic-multiple').select2({
	// 		placeholder: "Semua Tipe",
	// 	});


	// 	////
	// 	$(document.body).on("change",".js-filterTipe-basic-multiple",function(){
	// 		filterTipe = $('.js-filterTipe-basic-multiple').val()
	// 		filterBiller = $('.js-filterBiller-basic-multiple').val()
	// 		filterJenis = $('.js-filterJenis-basic-multiple').val()
	// 		getTable(filterJenis,filterTipe,filterBiller);
	// 	});
	// 	$(document.body).on("change",".js-filterBiller-basic-multiple",function(){
	// 		filterTipe = $('.js-filterTipe-basic-multiple').val()
	// 		filterBiller = $('.js-filterBiller-basic-multiple').val()
	// 		filterJenis = $('.js-filterJenis-basic-multiple').val()
	// 		getTable(filterJenis,filterTipe,filterBiller);
	// 	});
	// 	getTable(filterJenis,filterTipe,filterBiller);
	// 	function getTable(filterJenis,filterTipe,filterBiller) {
	// 		// if ( $.fn.dataTable.isDataTable( '#product-postpaid-table' ) ) {
	// 		// 	table = $('#example').DataTable();
	// 		// }
	// 		var oDataList = $('#product_biller-table').DataTable({
	//         processing: true,
	//         serverSide: true,
	//         autoWidth: false,
	//         destroy: true,
	//         "bAutoWidth" : false,
	//         ajax: {
	//           url: "{{ url('product_biller-list') }}",
	//           data:  {
	//                 "filterJenis": filterJenis,
	//                 "filterTipe": filterTipe,
	//                 "filterBiller": filterBiller
	//                 },

	// 			beforeSend: function(){
	// 				$(document.body).css({'cursor' : 'wait'});

	// 				},
	// 			complete: function(){
	// 				$(document.body).css({'cursor' : 'default'});

	// 			},
	// 		},
	// 		dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	// 		columns: [


	// 			{data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
	// 				orderable: false},
	// 			{
	// 				data: 'nama',
	// 				name: 'nama',

	// 			},
	// 			{
	// 				data: 'sku_biller',
	// 				name: 'sku_biller',

	// 			},
	// 			{
	// 				data: 'description',
	// 				name: 'description',

	// 			},
	// 			{
	// 				data: 'price_basic',
	// 				name: 'price_basic',

	// 			},
	// 			{
	// 				data: 'prioritas',
	// 				name: 'prioritas',

	// 			},
	// 			{
	// 				data: 'status',
	// 				name: 'status',

	// 			},
	// 			{
	// 				data: 'action',
	// 				name:'Tindakan',
	// 				searchable: false,
	// 				orderable: false
	// 			},
	// 		],
	// 		buttons: [

	// 			{ 
	// 				extend: 'csv',
	// 				title: 'Export Menu Produk'+ $.now(),
	// 			},
	// 		],

	//         select: {
	//             style: 'single'
	//         },




	//    		});

	//     }


	// });



	$(function() {
		var _baseURL = "<?php echo url(''); ?>";
		var filterJenis = [];
		var filterTipe = [];
		var filterBiller = [];
		$(document.body).on("change", ".js-filterTipe-basic-multiple", function() {
			filterTipe = $('.js-filterTipe-basic-multiple').val()
			filterBiller = $('.js-filterBiller-basic-multiple').val()
			filterJenis = $('.js-filterJenis-basic-multiple').val()
			getTable(filterJenis, filterTipe, filterBiller);
		});
		$(document.body).on("change", ".js-filterBiller-basic-multiple", function() {
			filterTipe = $('.js-filterTipe-basic-multiple').val()
			filterBiller = $('.js-filterBiller-basic-multiple').val()
			filterJenis = $('.js-filterJenis-basic-multiple').val()
			getTable(filterJenis, filterTipe, filterBiller);
		});
		$(document.body).on("change", ".js-filterJenis-basic-multiple", function() {
			filterTipe = $('.js-filterTipe-basic-multiple').val()
			filterBiller = $('.js-filterBiller-basic-multiple').val()
			filterJenis = $('.js-filterJenis-basic-multiple').val()
			getTable(filterJenis, filterTipe, filterBiller);
		});
		$('.js-filterBiller-basic-multiple').select2({
			placeholder: "Semua Biller",
		});
		$('.js-filterJenis-basic-multiple').select2({
			placeholder: "Semua Jenis",
		});
		$('.js-filterTipe-basic-multiple').select2({
			placeholder: "Semua Tipe",
		});
		getTable(filterJenis, filterTipe, filterBiller);

		function getTable(filterJenis, filterTipe, filterBiller) {
			// if ( $.fn.dataTable.isDataTable( '#product-postpaid-table' ) ) {
			// 	table = $('#example').DataTable();
			// }
			var oDataList = $('#product_biller-table').DataTable({
				processing: true,
				serverSide: true,
				destroy: true,
				autoWidth: false,
				"bAutoWidth": false,
				ajax: {
					url: "{{ url('product_biller-list') }}",
					data: {
						"filterJenis": filterJenis,
						"filterTipe": filterTipe,
						"filterBiller": filterBiller
					},

					beforeSend: function() {
						$(document.body).css({
							'cursor': 'wait'
						});
						$('.fftable').prop('disabled', true);
						// $.blockUI({ overlayCSS: { backgroundColor: '#00f' } });
					},
					complete: function(data) {
						$('.fftable').prop('disabled', false);
						$(document.body).css({
							'cursor': 'default'
						});
						// $('.page-loader-wrapper').hide();
						// $.unblockUI();
					},
				},

				columns: [

					{
						data: 'DT_RowIndex',
						name: 'DT_RowIndex',
						searchable: false,
						orderable: false
					},
					{
						data: 'nama',
						name: 'nama',

					},
					{
						data: 'sku_biller',
						name: 'sku_biller',

					},
					{
						data: 'skuProduct',
						name: 'product.sku_code',
						render: function(data, type) {
							return data
						}

					},
					{
						data: 'tipe',
						name: 'tipe',

					},
					{
						data: 'jenis',
						name: 'jenis',

					},
					// {
					// 	data: 'desc',
					// 	name: 'desc',

					// },
					{
						data: 'price_basic',
						name: 'price_basic',
						render: function(data, type) {
							return 'Rp '+numeral(data).format('0,0')
						}

					},
					{
						data: 'admin_fee',
						name: 'admin_fee',
						render: function(data, type) {
							return 'Rp '+numeral(data).format('0,0')
						}

					},
					{
						data: 'commission',
						name: 'commission',
						render: function(data, type) {
							return 'Rp '+numeral(data).format('0,0')
						}

					},
					{
						data: 'prioritas',
						name: 'prioritas',

					},
					{
						data: 'status',
						name: 'status',

					},
					{
						data: 'tanggal',
						name: 'tanggal',

					},
					{
						data: 'updatedBy',
						name: 'updatedBy',

					},
					{
						data: 'action',
						name: 'Tindakan',
						searchable: false,
						orderable: false
					},
				],
				columnDefs: [

					{
						render: function(data, type, full, meta) {
							return "<div class='text-wrap width-120'>" + data + "</div>";
						},
						targets: 5
					}

				],
				select: {
					style: 'single'
				},
				buttons: [{
						extend: 'excel',
						title: 'Export biller Tanggal: ' + $.now(),
					},
					{
						extend: 'csv',
						title: 'Export Product' + $.now(),
					},
				],




			});
			//    $(document).on("click", ".exportCsv", function(e) {
			// 		showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
			// 			if (result) {
			// 				oDataList.button( '.buttons-csv' ).trigger();

			// 			}
			// 		}, '<span style="color: blue;">Export Biller</span>', {
			// 			confirm: {
			// 				label: 'Export',
			// 				className: 'btn-primary'
			// 			},
			// 			cancel: {
			// 				label: 'Cancel',
			// 				className: 'btn-default'
			// 			}
			// 		});
			// });
			// $(document).on("click", ".exportExcel", function(e) {
			// 		showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
			// 			if (result) {
			// 				oDataList.button( '.buttons-excel' ).trigger();

			// 			}
			// 		}, '<span style="color: blue;">Export Biller</span>', {
			// 			confirm: {
			// 				label: 'Export',
			// 				className: 'btn-primary'
			// 			},
			// 			cancel: {
			// 				label: 'Cancel',
			// 				className: 'btn-default'
			// 			}
			// 		});
			// });
		}







	});


	////

	$("#fileCreate").change(function(e) {
		var _URL2 = window.URL || window.webkitURL;
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL2.createObjectURL(file);
			$('#image_preview_create').empty();
			$('#image_preview_create').append("<img src='" + objectUrl + "' width='150px' style='border-radius: 2px;'> &nbsp");
		}
	});

	function deleteProduct($id) {
		// Setup validation
		deleteTableList(
			[$id],
			true
		);
		return false;

	}

	function deleteTableList($ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini') {
		var url = "{{url('/product_biller-delete')}}";
		if ($ids) {
			bootbox.confirm({
				title: 'Delete Data',
				message: message,
				buttons: {
					confirm: {
						label: 'Delete',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				},
				callback: function(result) {
					if (result) {
						console.log(url)
						$.ajax({
							url: url + '/' + $ids,
							data: {
								"_token": "{{ csrf_token() }}",
							},
							dataType: 'json',
							type: 'post',
							success: function(d) {
								if (d.status == 200) {

									$('#product_biller-table').DataTable().ajax.reload();
									toastr.success("Produk Biller berhasil dihapus")
								} else {
									toastr.error(d.message)
								}
							},
							error: function(d, statusText, xhr) {
								bootbox.alert('Oops. Error when processing your request. ' + xhr);
							},
							beforeSend: function() {},
							complete: function() {}
						});
					}
				}
			});
		} else {
			bootbox.alert("error");
		}

		return false;
	}
	$(document).on("click", ".addProduct", function(e) {
		e.preventDefault();
		showConfirm('Apakah anda Yakin ingin menambahkan produk ? ', function(result) {
			if (result) {
				addProduct();
			}
		}, '<span style="color: blue;">Tambah Produk</span>', {
			confirm: {
				label: 'Tambah',
				className: 'btn-primary'
			},
			cancel: {
				label: 'Cancel',
				className: 'btn-default'
			}
		});
	});


	function addProduct() {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreate").validate(optValidate);

		if ($("#frmCreate").valid()) {
			var form = $("#frmCreate")[0];
			var formData = new FormData(form);

			console.log($("#frmCreate").serialize());
			formData.append("type", 'prepaid');


			// formData.append("destinationDescription", $content.getData());
			var _baseURL = "<?php echo url(''); ?>";
			$.ajax({
				url: _baseURL + "/add-product-biller",
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#product_biller-table').DataTable().ajax.reload();
						$('#addForm').fadeOut();
						$('#addBtn').fadeIn();
						toastr.success("Produk Berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;
		}

	}

	$(function() {
		var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,
			$isImgEditable = false,
			$choosenBahanBaku = [],
			$variants, $choosenVariant = [],
			$status_filter = "",
			$outlet_filter = "",
			$is_paid = "",
			$paid_approval = "";

		var datePicker = $('.daterange-ranges').daterangepicker({
				startDate: moment(),
				endDate: moment(),
				ranges: {
					'Hari ini': [moment(), moment()],
					'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
					'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
					'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
					'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
					'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
					'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
				},
				locale: {
					cancelLabel: 'Batal',
					applyLabel: 'Proses',
					daysOfWeek: [
						"Sen",
						"Sel",
						"Rab",
						"Kam",
						"Jum",
						"Sab",
						"Min"
					],
					monthNames: [
						"Januari",
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					],
				},
				opens: 'right',
				showCustomRangeLabel: false,
				alwaysShowCalendars: true,
				applyButtonClasses: 'btn-small btn-primary',
				cancelClass: 'btn-small btn-default',
				maxDate: new Date(),

			},
			// Datepickerrange callback
			datepickerRangeCallback = function(start, end) {
				$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
			}, datepickerRangeCallback);
		$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + moment().format('DD MMM YYYY'));





		$(document).on("click", ".exportCsv", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-csv').trigger();

				}
			}, '<span style="color: blue;">Export Menu Produk</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		$(document).on("click", ".exportExcel", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-excel').trigger();

				}
			}, '<span style="color: blue;">Export Menu Produk</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
	});
	var optValidate = {
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		errorClass: 'validation-error-label',
		successClass: 'validation-valid-label',
		highlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		// Different components require proper error label placement
		errorPlacement: function(error, element) {

			// Styled checkboxes, radios, bootstrap switch
			if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
				if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent().parent().parent());
				} else {
					error.appendTo(element.parent().parent().parent().parent().parent());
				}
			}

			// Unstyled checkboxes, radios
			else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
				error.appendTo(element.parent().parent().parent());
			}

			// Input with icons and Select2
			else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
				error.appendTo(element.parent());
			}

			// Inline checkboxes, radios
			else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
				error.appendTo(element.parent().parent());
			}

			// Input group, styled file input
			else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
				error.appendTo(element.parent().parent());
			} else {
				error.insertAfter(element);
			}
		},
		validClass: "validation-valid-label"


	};

	function editData(id, url = "{{url("/product_biller-edit/")}}") {
		$.ajax({
			url: url + '/' + id,
			type: 'GET',
			dataType: 'HTML',
			beforeSend: function() {
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();

			},
			success: function(data) {
				$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({
					scrollTop: 0
				}, "slow");
			},
			error: function(data) {
				$('#loadBar').hide();
				alert(url + '/' + id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
		});
	}

	$('.btnEdit').click(function() {
		$('#addForm').fadeOut();
	});
	$('#addBtn').click(function() {
		$('#addBtn').fadeOut();
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
		$('#addForm').fadeIn();
		$('.productSelect').select2({
			"width": '100%'
		});
	});

	$('#cancelBtnAdd').click(function() {
		$('#addForm').fadeOut();
		$('#addBtn').fadeIn();
	});


	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}
</script>