<div class="card card-primary">
	<div class="card-header">
		<h3 class="card-title"> Edit Biller</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->
	<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
		@csrf
		<div class="card-body">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="name" value="{{$data['name']}}" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Avatar</label>
				<div class="col-sm-10">
					<div id="image_preview_edit">
						<img src="{{$data['avatar']}}" width="150px;" style="border-radius: 2px;">
					</div>
					<input type="file" id="fileEdit" name="avatar" onclick="preview_image_edit()">
				</div>
			</div>



			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Biller URL </label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="biller_url" value="{{$data['biller_url']}}" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">API key</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="apikey" value="{{$data['apikey']}}" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Phone</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="phone" value="{{$data['phone']}}">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="email" value="{{$data['email']}}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Username</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="username" value="{{$data['username']}}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="password" value="{{$data['password']}}">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Added Data</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="added_data" value="{{$data['added_data']}}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10">
					<select class="form-control" name="status">
						<option value="1" {{$data["status"]==1 ? 'selected':''}}>Aktif</option>
						<option value="0" {{$data["status"]==0 ? 'selected':''}}>Non Aktif</option>
					</select>
				</div>
			</div>


		</div>


		<!-- /.card-body -->
		<div class="card-footer">
			<button type="" id="editBiller" data-id='{{$data["id"]}}' class="btn btn-info editBiller">Submit</button>
			<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
		</div>
		<!-- /.card-footer -->
	</form>
</div>









<div class="card card-primary">
	<div class="card-header">
		<h3 class="card-title"> Sync Saldo {{$data['name']}}</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->

	<div class="card-body">
		<form class="form-horizontal" role="form" id="frmEditSaldo" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Saldo Saat Ini</label>
				<div class="col-sm-8">
					<input disabled type="text" id='curSaldoo' class="form-control" name="saldo" value="{{$a}}" required>
				</div>
				<div class="col-sm-2">
					<button type="button" onclick="cekSAldo('{{$data['id']}}','{{$data['name']}}')" class="btn btn-info cekSaldoo float-left ">Sync Saldo Biller</button>
				</div>
			</div>
		</form>
	</div>

	<!-- /.card-body -->
	<div class="card-footer" style="height:60px!important;">
		<!-- <button type="submit" id="editBillerSaldo"  data-id='{{$data["id"]}}' class="btn btn-info editBillerSaldo">Submit</button> -->
		<button id="cancelBtnEditSaldo" type="button" class="btn btn-default cancelBtnEditSaldo float-right">Cancel</button>
	</div>
	<!-- /.card-footer -->
</div>

<script>
	$('#cancelBtnEdit').click(function() {
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
		$('#biller-table').DataTable().ajax.reload();
	});
	$('#cancelBtnEditSaldo').click(function() {
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
		$('#biller-table').DataTable().ajax.reload();
	});



	$(document).on("click", "#editBiller", function(e) {
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmEdit").validate(optValidate);
		e.stopImmediatePropagation();
		e.preventDefault();
		console.log($(this).attr("data-id"));
		if ($("#frmEdit").valid()) {

			$dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Biller ? ', function(result) {
				if (result) {
					UpdateBiller($dataID);
				}
			}, '<span style="color: blue;">Edit Biller</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});


		}

	});
	$(document).on("click", "#editBillerSaldo", function(e) {
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmEditSaldo").validate(optValidate);
		e.stopImmediatePropagation();
		e.preventDefault();
		console.log($(this).attr("data-id"));
		if ($("#frmEditSaldo").valid()) {

			$dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin menambahkan saldo biller ? ', function(result) {
				if (result) {
					UpdateBillerSaldo($dataID);
				}
			}, '<span style="color: blue;">Tambah Saldo Biller</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});


		}

	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
	$("#fileEdit").change(function(e) {
		var _URL2 = window.URL || window.webkitURL;
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL2.createObjectURL(file);
			img.onload = function() {

				_URL2.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : ' + this.width + " x " + this.height);
				$('#dimRec').text(' Current Dimension : ' + this.width + " x " + this.height);

				$('#fileEdit').removeClass('is-invalid');
				$('#btnEditEvent').prop('disabled', false);
				$('#dimRec').removeClass('error');
			};
			img.src = objectUrl;
			preview_image_edit();
		}
	});

	function preview_image_edit() {
		$('#image_preview_edit').empty();
		$('#image_preview_edit').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
	}

	function UpdateBiller(idProduct) {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmEdit").validate(optValidate);

		if ($("#frmEdit").valid()) {
			var form = $("#frmEdit")[0];
			var formData = new FormData(form);

			console.log($("#frmEdit").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/biller-update/" + idProduct,
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						toastr.success("Biller Berhasil diupdate")
						$('#editForm').fadeOut();
						$('#addform').fadeOut();
						$('#editBtn').fadeIn();
						$('#biller-table').DataTable().ajax.reload();


					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {

				},
			});

			return false;
		}

	}

	function UpdateBillerSaldo(idProduct) {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmEditSaldo").validate(optValidate);

		if ($("#frmEditSaldo").valid()) {
			var form = $("#frmEditSaldo")[0];
			var formData = new FormData(form);

			console.log($("#frmEditSaldo").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/biller-update-saldo/" + idProduct,
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						toastr.success("Saldo Biller Berhasil ditambahkan")
						$('#editForm').fadeOut();
						$('#addform').fadeOut();
						$('#editBtn').fadeIn();
						$('#biller-table').DataTable().ajax.reload();


					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {

				},
			});

			return false;
		}

	}
</script>