<x-package-header />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Inbox</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<button id="addBtn" class="btn btn-sm btn-primary mr-1">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
		
	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
          <br>
          <center><i class="fa fa-spinner fa-spin"></i></center>
        </div>
        <div  class="card-body" id="editForm"></div>
	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
					</div>
					<div class="body">
						<div class="table-responsive">
							<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom inbox-table"  id="inbox-table">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Nama</th>
										<th>Email</th>
										<th>No. HP</th>
										<th>Subject</th>
										<th>Isi Pesan</th>
										<th>Alamat IP</th>
										<th></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>	


	var _URL = window.URL || window.webkitURL;
	
	
	function editData(id,url ="{{url("/contact_message-edit/")}}")
	{
		$.ajax({
			url:url+'/'+id,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
	
			},
			success:function(data){
				$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url+'/'+id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
		});	
	}
	
	

	$(function() {
    	var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		var oDataList = $('#inbox-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
		
			"bAutoWidth" : false,
			"lengthChange" : false,
			ajax: {
				url: "{{ url('contact-message-list') }}",
				data: {
                },
			},
		    "fnDrawCallback": function( data ,e) {
			
							
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
				{
					data: 'tanggal',
					name: 'tanggal',
					searchable: false,
					orderable: false
			
				},
				{
					data: 'sender_name',
					name: 'sender_name',
					orderable: false
			
				},
				{
					data: 'sender_email',
					name: 'sender_email',
					orderable: false
			
				},
				{
					data: 'sender_phone',
					name: 'sender_phone',
					orderable: false
			
				},
				{
					data: 'subject',
					name: 'subject',
					orderable: false
				},
				{
					data: 'content',
					name: 'content',
					orderable: false
				},
				{
					data: 'sender_ip',
					name: 'sender_ip',
					orderable: false
				},
				{
					data: 'action',
					name:'Tindakan',
					searchable: false,
					orderable: false
				},
				//13
			
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Team Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
			],
			select: {
				style: 'single'
			},
		});
		$('.btnEdit').click(function(){
				$('#addForm').fadeOut();
		});

	
		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});
	
		$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});
	

	


		
		

	
	});

</script>