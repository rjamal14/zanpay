<x-package-header />
<style>
	.bg-orange-700 {
  background-color: #F57C00;
  border-color: #F57C00;
  color: #fff;
}
	.alpha-danger {
  background-color: #FFEBEE;
  border-color: #E53935;
}
.alpha-success {
  background-color: #E8F5E9;
  border-color: #43A047;
}
	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
	}

	.table-custom td {
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Tiket Deposit</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Tiket Deposit</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-12" id="loadBar" style="display: none;">
          <br>
          <center><i class="fa fa-spinner fa-spin"></i></center>
        </div>


        <div  class="card-body" id="editForm2"></div>
        <div  class="card-body" id="editForm"></div>
        <div  class="card-body" id="depositManualForm"></div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="body">
					<button type="" id="depositManual"  class="btn btn-info depositManual">Deposit Manual</button>

						<div class="table-responsive check-all-parent">
						<table  class="table table-bordered table-hover c_list table-custom deposit-table"  id="deposit-table">
								<thead>
									<tr>

									
										<th>No.</th>
										<th>Jam & Tanggal</th>
										<th>Jam & Tanggal Update</th>
										<th>ID Reseller</th>
										<th>Nama</th>
										<th>Nominal</th>
										<th>Kode Unik</th>
										<th>Jumlah</th>
										<th>Payment Method</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /><script>
	
	$(document).on("click", ".img-init2", function() {
        $(".product_img2").click();
    });

    $(document).on("change", ".product_img2", function() {
        readURL2(this);
    });

    $(document).on("click", ".img-delete2", function() {
        $isImgEditable = false;
        $(".image-container-product2").removeClass("image-exist");
        $(".product_img2").val("");
        $(".image-container-product2").find("img").remove();

    });
	var _URL = window.URL || window.webkitURL;
	$("#file").change(function (e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL.createObjectURL(file);
        img.onload = function () {
        
            _URL.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#reomencedDimension').text(' Current Dimension : '+this.width + " x " + this.height);
           
              $('#file').removeClass('is-invalid');
              $('#btnCreateEvent').prop('disabled', false);
              $('#reomencedDimension').removeClass('error');
        };
        img.src = objectUrl;
		preview_image();
    }
});
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if ($(".image-container-product2 img").length != 0) {
                    $(".image-container-product2 img").attr(
                        "src",
                        e.target.result
                    );
                } else {
                    $(".image-container-product2").append(
                        '<img src="' + e.target.result + '">'
                    );
                }
                $(".image-container-product2").addClass("image-exist");
                $isImgEditable = true;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

	
	
	$(function() {
    var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		
    var datePicker = $('.daterange-ranges').daterangepicker({
        startDate:moment(),
        endDate: moment(),
        ranges: {
            'Hari ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
            'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],        
            '2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
        },
        locale: { 
            cancelLabel: 'Batal', 
            applyLabel: 'Proses',
			daysOfWeek: [
							"Sen",
							"Sel",
							"Rab",
							"Kam",
							"Jum",
							"Sab",
							"Min"
						],
			monthNames: [
				"Januari",
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
            	'Desember'
        	],
        },
        opens: 'right',
        showCustomRangeLabel: false,
        alwaysShowCalendars: true,
        applyButtonClasses: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default',
		maxDate: new Date(),
		
		},
		// Datepickerrange callback
		datepickerRangeCallback =  function(start, end) {
		$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
		}, datepickerRangeCallback);
		$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' +  moment().format('DD MMM YYYY'));
		
		var oDataList = $('#deposit-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
		
			"bAutoWidth" : false,
			ajax: {
				url: "{{ url('tiket_deposit-list') }}",
				data: function (d) {
                
            	}
			},
		    "fnDrawCallback": function( data ,e) {
				$('.request-status').select2({
					search: false,
					minimumResultsForSearch: -1,
					containerCssClass: 'bg-orange-700'
            	});
			},
		
			"createdRow": function( row, data, dataIndex ) {
            if (data.status == 1 || data.status == 3 ) {
                $(row).addClass('alpha-success');
            } else if (data.status == 2 || data.status == 4) {
                $(row).addClass('alpha-danger');
            }
        },
			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
		

			

				{data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
					orderable: false},
				// {	
				//     data: 'status',
				//     name: 'status',
			
				// },
				{
					data: 'jamTanggal',
					name: 'date_created',
			
				},
				{
					data: 'jamTanggalu',
					name: 'date_updated',
			
				},
				{
					data: 'reseller.reseller_code',
					name: 'reseller.reseller_code',
			
				},
				{
					data: 'reseller.name',
					name: 'reseller.name',
			
				},
				{
					data: 'jumlah',
					name: 'jumlah',
			
				},
				{
					data: 'unique_code',
					name: 'unique_code',
			
				},
			
				{
					data: 'amount',
					name: 'amount',
			
				},
				{
					data: 'payment',
					name: 'payment',
			
				},
			
				{
					data: 'status',
					name: 'status',
			
				},
		
				
				{
					data: 'actionBaru',
					name:'Tindakan',
					searchable: false,
					orderable: false
				},
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Tiket Deposit Saldo Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
				
		
		
			],
			select: {
				style: 'single'
			},
			"order": [[ 0, "desc" ]]
		
		});
		datePicker.on('apply.daterangepicker', function(ev, picker) {
			oDataList.draw(true);

	
		});

		$('#deposit-table').on('change', '.request-status', function () {
			var tr = $(this).closest('tr');
			var data = oDataList.row(tr).data();
			updateStatus(data.no, $(this).val());
		});
		
		function updateStatus($dataID, $status) {
        $.ajax({
            url: _baseURL + '/tiket_deposit-update-status/' + $dataID,
            dataType: 'json',
            data: {
                stat: 1,
                id: $dataID,
                status: $status
            },
            success: function (d) {
                if (d.status == 200) {
					toastr.success("deposit Berhasil diupdate")
					$('#deposit-table').DataTable().ajax.reload();
                } else {
					toastr.success(d.message)
                }
            },
            error: function(xhr) {
                console.log(xhr)
            },
            beforeSend: function () {
            },
            complete: function () {
            }
        });
    }
	
		$(document).on("click", ".exportCsv", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-csv' ).trigger();
					
					}
				}, '<span style="color: blue;">Export Transaksi</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
		$(document).on("click", ".exportExcel", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-excel' ).trigger();
					
					}
				}, '<span style="color: blue;">Export Deposit</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
	});
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});
	$('#depositManual').click(function(){
		manualDeposit()
		});
 	function manualDeposit(url ="{{url("/tiket_deposit-manual/")}}"){
			$.ajax({
			url:url,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#depositManualForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
     
			},
			success:function(data){
        		$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#depositManualForm').append(data);
				$('#depositManualForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
			});	
	}

	function editData(id,url ="{{url("/tiket_deposit-edit/")}}"){
		$.ajax({
		url:url+'/'+id,
		type:'GET',
		dataType:'HTML',
		beforeSend:function(){
			$('#editForm').empty();
			$('.btnEdit').fadeOut();
			$('#loadBar').show();
	
		},
		success:function(data){
			$('#addform').fadeOut();
			$('#loadBar').hide();
			$('#formWrapper').fadeOut();
			$('#editForm').append(data);
			$('#editForm').fadeIn();
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error:function(data) {
			$('#loadBar').hide();
			alert(url+'/'+id);
			alert(data);
			alert('500 : Internal server error');
			$('.btnEdit').fadeIn()
		}
		});	
	}
	function editDepositBaru(id,url ="{{url("/tiket_deposit_edit/")}}"){
		$.ajax({
		url:url+'/'+id,
		type:'GET',
		dataType:'HTML',
		beforeSend:function(){
			$('#editForm2').empty();
			$('.btnEdit').fadeOut();
			$('#loadBar').show();
	
		},
		success:function(data){
			$('#addform').fadeOut();
			$('#loadBar').hide();
			$('#formWrapper').fadeOut();
			$('#editForm2').append(data);
			$('#editForm2').fadeIn();
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error:function(data) {
			$('#loadBar').hide();
			alert(url+'/'+id);
			alert(data);
			alert('500 : Internal server error');
			$('.btnEdit').fadeIn()
		}
		});	
	}

		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});
		$('#addBtn').click(function(){
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
     		$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
		});
	
		$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});
	
		
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}


		function preview_image() {
    $('#image_preview').empty();
    $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
  }


		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}
		var _baseURL = "<?php echo url(''); ?>";
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
					if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo( element.parent().parent().parent().parent() );
					}
					else {
						error.appendTo( element.parent().parent().parent().parent().parent() );
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo( element.parent().parent().parent() );
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo( element.parent() );
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo( element.parent().parent() );
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo( element.parent().parent() );
				}

				else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"

		
		};
	

</script>
