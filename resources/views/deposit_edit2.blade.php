<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit deposit Saldo</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              	<form class="form-horizontal" role="form" id="frmEdit222" enctype="multipart/form-data">
			        @csrf
                <div class="card-body">
               
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">ID Reseler</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="reseller_code" value="{{$data['reseller']['reseller_code']}}"  readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="name" value="{{$data['reseller']['name']}}"  readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nominal</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nominal" name="nominal" value="{{$data['nominal']}}"  readonly>
						</div>
					</div>

					
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Kode Unik</label>
						<div class="col-sm-10">
							<input type="number" class="form-control uniqueCode" id="unique_code" name="unique_code" value="{{$data['unique_code']}}" >
						</div>
					</div>

					
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">jumlah</label>
						<div class="col-sm-10">
							<input type="number" class="form-control jumlahAmount" id="jumlah_amount" name="amount" value="{{$data['amount']}}"  >
						</div>
					</div>
		
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Payment method</label>
						<div class="col-sm-10">
							<select name="status" id="status" class="form-control" required>
							@foreach ($payment as $p)
								<option value="{{$p['id']}}" {{$data['bank_code'] == $p['id']? "selected":""}}>{{$p['name']}}  - {{$p['behalf']}} - {{$p['account']}}</option>
							@endforeach
</select>
						</div>
					</div>
			
					<div class="form-group row">
					<label class="col-sm-2 col-form-label">Status</label>
						<div class="col-sm-10">
							<select name="statusRequest"  class="form-control" required>
							<option value="3">Approve</option>
							<option value="4">Reject</option>
							</select>
						</div>
					</div>
				
                
                </div>
                  
                        <!-- /.card-body -->
					<div class="card-footer">
                      <button type="" id="editDeposit222" data-id='{{$data["no"]}}'   class="btn btn-info editDeposit222">Submit</button>
                      <button id="cancelBtnEdit2222" type="button" class="btn btn-default cancelBtnEdit2222 float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>



$("#unique_code").click(function() {
	this.select();
});


$("#unique_code").on('keyup blur', function() {
	var nominal  = `{{$data['nominal']}}`;
	if($(this).val() == ""){
		var cur  = 0;
	} else {
		var cur  = $(this).val();
	}
	
	var newNominal  = parseInt(nominal) + parseInt(cur);
	$( "#jumlah_amount" ).val(newNominal)
});

$("#jumlah_amount").on('blur', function() {
	var nominal  = `{{$data['nominal']}}`;
	if($('#unique_code').val() == ""){
		var cur  = 0;
	} else {
		var cur  = parseInt($('#unique_code').val());
	}
	if(parseInt(nominal) > parseInt($(this).val()) ){
		$(this).val(parseInt(nominal) + cur);
	} else {
		$('#unique_code').val( parseInt($(this).val())-nominal);
	}
	

});




            	$('#cancelBtnEdit2222').click(function(){
			$('#editForm2').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editDeposit222", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit222").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit222").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate deposit ? ', function(result) {
				if (result) {
				updatedeposit2($dataID);
				}
			}, '<span style="color: blue;">Edit deposit</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function updatedeposit2(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit222").validate(optValidate);

			if ($("#frmEdit222").valid()) {
				var form = $("#frmEdit222")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit222").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/tiket_deposit_update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("deposit Berhasil diupdate")
							$('#editForm2').fadeOut();
							$('#addform').fadeOut();
							$('#btnEdit').fadeIn();
            	$('#deposit-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>