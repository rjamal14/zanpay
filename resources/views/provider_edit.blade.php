<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Prefix</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
                <div class="card-body">
               
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Provider {{$data['provider']}}</label>
                    <div class="col-sm-10">
						<select name="provider"  id="providerIDEdit" class="form-control select2" required>
							@foreach ($provider as $p )
							<option value="{{$p['id']}}" {{ $p['id'] == $data['provider'] ? "selected='selected'" : ""}}>{{$p['text']}}</option>
							@endforeach
						</select>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Prefix</label>
                    <div class="col-sm-10">
						<textarea class="form-control" name="prefix_header"   required>{{$data['prefix_header']}}</textarea>
                    </div>
                </div>
                    </div>
                  
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editProvider"  data-id='{{$data["no"]}}' class="btn btn-info editProvider">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editProvider", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Provider ? ', function(result) {
				if (result) {
				updateProvider($dataID);
				}
			}, '<span style="color: blue;">Edit Provider</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
   

		function updateProvider(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/provider-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Provider Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
            	$('#provider-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>