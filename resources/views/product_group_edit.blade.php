<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Product Group</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
					<div class="card-body">
					<div class="form-group row">
							<label class="col-sm-2 col-form-label">Icon</label>
							<div class="col-sm-10">
								<div id="image_preview_edit">   
									<img src ="{{$data['provider_icon']}}" width="150px;" style="border-radius: 2px;">
								</div>
									<input type="file" id="fileEdit" name="avatar" onclick="preview_image_edit()">
							</div>
                		</div>

					


						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Tipe</label>
							<div class="col-sm-10">
							<select name="group_name" style="width: 100%" id="typeEdit" class="form-control" required>
								@foreach ($type as $t)
								<option value="{{$t}}" {{$data['group_name'] == $t ? "selected":""}}>{{$t}}</option>
								@endforeach
							</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Jenis</label>
							<div class="col-sm-10">
								<select name="command" style="width: 100%"  id="command" class="form-control js-example-tagss" required>
									@foreach($jenis as $j)
									<option value="{{$j['text']}}" {{$data['command'] == $j['text'] ? 'selected':''}} >{{$j['text']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Provider</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="provider" value ="{{$data['provider']}}" required>
							</div>
						</div>
						<div class="form-group row">
				<label class="col-sm-2 col-form-label">Prefix HLR</label>
				<div class="col-sm-10">

				<select name="prefix" style="width: 100%" class="form-control " required>
					<option value="0" >Pilih </option>
					@foreach($prefix as $pEdit)
					<option value="{{$pEdit['no']}}"  {{$data['hlr_prefix'] == $pEdit['no'] ? 'selected':''}}  >{{$pEdit['provider']}} - {!!$pEdit['prefix_header']!!}</option>
					@endforeach
				</select>

				</div>
			</div>

					
						
                	</div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editResellerGroup"  data-id='{{$data["id"]}}' class="btn btn-info editResellerGroup">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>

$(document).ready(function() {
			$(".js-example-tagss").select2({
		tags: true
	});
		});
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
			$(".js-example-tags").select2({
		tags: true
	});
		});
		$("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});
		function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
    $(document).on("click", "#editResellerGroup", function(e) {
		e.stopImmediatePropagation();
			e.preventDefault();
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
    
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateResellerGroup($dataID);
				}
			}, '<span style="color: blue;">Edit Data </span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function updateResellerGroup(idProduct) {
			// Setup validation
			var _baseURL = "<?php echo url(''); ?>";
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/product_group-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
              $('#product_menu-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
						$(".js-example-tags").select2({
		tags: true
	});
					},
				});

				return false;
			}

		}
          </script>