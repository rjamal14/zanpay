<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
              <div class="card-body">
                  @if($data['productBiller']===null)
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Tipe (parsing)</label>
                      <div class="col-sm-10">
                      <select name="parsingType" id="parsingTypeEdit" class="form-control">
                        <option value="" >Pilih</option>
                        @foreach($tipe as $typ)
                        <option value="{{ $typ['id']}}"  {{ $data['productGroup']['group_name']== $typ['id'] ? 'selected':''}}>{{$typ['text']}}</option>
                        @endforeach
                      </select>
                      </div>
                    </div>

                  <!-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Group</label>
                              <div class="col-sm-10">
                                <select name="group_id" id="group_idEdit" class="form-control productSelectEdit" >

                                        @foreach ($group as $g)
                                        <option value="{{$g['id']}}" {{$data['group_id'] == $g['id'] ? 'selected' : ""}}>{{$g['command'].' - '.$g['provider']}}</option>
                                        @endforeach
                                        
                                </select>
                              </div>
                      </div> -->
                  @else
                  <div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe (parsing)</label>
						<div class="col-sm-10">
						<select name="parsingType" id="parsingTypeEdit" class="form-control" disabled>
							<option value="" >Pilih</option>
							@foreach($tipe as $typ)
							<option value="{{ $typ['id']}}" {{ $data['productGroup']['group_name']== $typ['id'] ? 'selected':''}}>{{$typ['text']}}</option>
							@endforeach
						</select>
						</div>
					</div>
                      <!-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Group</label>
                              <div class="col-sm-10">
                                <select name="group_id" id="group_idEdit" class="form-control productSelectEdit" disabled >

                                        @foreach ($group as $g)
                                        <option value="{{$g['id']}}" {{$data['group_id'] == $g['id'] ? 'selected' : ""}}>{{$g['command'].' - '.$g['provider']}}</option>
                                        @endforeach
                                        
                                </select>
                              </div>
                      </div> -->
                  @endif   
                  <div class="form-group row">
								@if ($data['productBiller'] == null)
                <label class="col-sm-2 col-form-label">Jenis</label>
						<div class="col-sm-10">
						<select name="group_id" id="groupIDEdit" class="form-control productSelectEdit">
							<option value="">-- Pilih Group --</option>
							@foreach($group as $g)
							<option value="{{ $g['id']}}" {{( isset($data['productGroup']) ? ($g['id']==$data['productGroup']['id'] ? 'selected':''):'')}}>{{$g['command']." - ".$g['provider']}}</option>
							@endforeach
						</select>
								@else
								<label class="col-sm-2 col-form-label">Jenis</label>
						<div class="col-sm-10">
						<select name="group_id" id="groupIDEdit" class="form-control productSelectEdit" disabled>
							<option value="">-- Pilih Group --</option>
							@foreach($group as $g)
							<option value="{{ $g['id']}}" {{( isset($data['productGroup']) ? ($g['id']==$data['productGroup']['id'] ? 'selected':''):'')}}>{{$g['command']." - ".$g['provider']}}</option>
							@endforeach
						</select>
								@endif
					
						</div>
					</div>  
              @if(count($cat) > 0)
              <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kategori</label>
                      <div class="col-sm-10">
                        <select name="category_id" id="category_idEdit" class="form-control" required>

                              <option value="0" {{$data['category_id'] == 0 ? 'selected' : ""}}>--Silahkan Pilih Kategori--</option>
                                @foreach ($cat as $ca)
                                <option value="{{$ca['id']}}" {{$data['category_id'] == $ca['id'] ? 'selected' : ""}}>{{$ca['name']}}</option>
                                @endforeach
                                
                        </select>
                      </div>
              </div>
              @else
              <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kategori</label>
                      <div class="col-sm-10">
                      <select name="category_id" id="category_idEdit" class="form-control">
                        <option value="0" >--Tidak Ada Kategori--</option>
                      </select>

                      </div>
              </div>

              @endif


            <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Product SKU</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" value="{{$data['sku_code']}}" name="sku_code" required>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                <label class="fancy-radio custom-color-green"><input name="is_deleted" value="0" type="radio"><span><i></i>Aktif</span></label>
                <label class="fancy-radio custom-color-red"><input name="is_deleted" value="1" type="radio"><span><i></i>Tidak Aktif</span></label>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="name" value="{{$data['name']}}" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
              <input type="text" class="form-control" value="{{$data['description']}}" name="description" required>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="command" value="{{$data['productGroup']['command']}}" disabled>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nominal</label>
                        <div class="col-sm-10">
                <input type="number" class="form-control" name="nominal" value="{{$data['nominal']}}"  required>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Harga Dasar</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control"  value="{{isset($data['productBiller']) ? $data['productBiller']['price_basic'] : $data['price'] }}" id="price-basicE" readonly name="price_basic" required>
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Harga Jual</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" value="{{$data['price']}}"  name="price">
						<input type="hidden" class="form-control" value="prepaid"  name="type" required>
                    </div>
                  </div>
                      
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Provider</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="provider" value="{{$data['productGroup']['command']}}" disabled>
                        </div>
                      </div> -->
                  
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cashback</label>
                        <div class="col-sm-10">
                <input type="number" class="form-control" name="cashback" value="{{$data['commission']}}" required>
                        </div>
                      </div>
                <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Biller</label>
                          <div class="col-sm-10">
                            <select name="biller_id" id="biller_idE" class="form-control" required>
                      
                            @foreach ($biller as $bill)
                            <option value="{{$bill['id']}}"  {{$data['biller_id'] == $bill['id'] ? 'selected' : ""}}>{{$bill['name'] }}</option>
                            @endforeach
                            </select>
                                      </div>
                                </div>

                                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">SKU Biller</label>
                    <div class="col-sm-10">
                        <select name="sku_billerEdit"  id="sku_billerE" style="width: 100%" placeholder="Pilih SKU Biller" class="form-control" required>
                          @foreach ($pbiller as $bill)
                          <option value="{{$bill['sku_biller']}}" data-price-basic-e="{{$bill['price_basic']}}" {{$data['id'] == $bill['product_id'] ? 'selected' : ""}}>{{$bill['description'] == null ? $bill['sku_biller'].'| Belum ada deskripsi' : $bill['sku_biller'].'|'.$bill['description']}}</option>
                          @endforeach
                        </select>
                    </div>
                  </div> -->


                                <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <label class="fancy-radio custom-color-green"><input name="is_trouble" value="0" type="radio"     {{$data['is_trouble'] == 0 ? "checked='checked'":"" }}><span><i></i>Normal</span></label>
                          <label class="fancy-radio custom-color-red"><input name="is_trouble" value="1" type="radio" 
                        {{$data['is_trouble'] == 1 ? "checked='checked'":"" }}><span><i></i>Ganguan</span></label>
                        </div>
                      </div>
                
                    </div>
                  
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editProduct"  data-id='{{$data["id"]}}' class="btn btn-info editProduct">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>

$( document ).ready(function() {
      $('.productSelectEdit').select2({
        "width":'100%'
      });
    });

$(document).ready(function() {
			$('#sku_billerE').select2({
        placeholder:'Pilih SKU Biller'
      });
		});

		

	$(document).on('change', '#biller_idE', function() {
		let bId  = $(this).val();
		let gId  = $('#group_id').val();
		getProductBillerE(bId,gId);
   	})

     $(document).on('change', '#sku_billerE', function() {
	console.log($(this).find(':selected').data('price-basic-e'));
  $('#price-basicE').val($(this).find(':selected').data('price-basic-e'));
   })

	function getProductBillerE(id,gId,url ="{{url("/product-biller-byBiller/")}}") {
		console.log(url);
		$.ajax({
		url:url+'/'+id+"?group_id="+gId,
		type:'GET',
		dataType:'json',
			beforeSend:function(){
        $('#price-basicE').val('');
			},
			success:function(data){
				var appendsEdit='';

				$("#sku_billerE").empty();
				if(data.data.length > 0){
					$.each(data.data, function (i,val) {
						appendsEdit+='<option data-price-basic-e="'+val.price_basic+'" value="'+val.sku_biller+'">'+ val.text +'</option>';
						console.log(val);
				
					});
          $('#price-basicE').val(data.data[0].price_basic);
					$("#sku_billerE").removeAttr('disabled');
				} else {
						appendsEdit='<option value="">Biller tidak memiliki sku di group ini</option>';
            $('#biller_idE').val();
					$("#sku_billerE").prop("disabled", true);
				}	
				$("#sku_billerE").append(appendsEdit);
			},
			error:function(data) {
				$("#sku_billerE").empty();
				appendsEdit='<option value="">Biller tidak memiliki sku di group ini</option>';
 $('#biller_idE').val();
					$("#sku_billerE").prop("disabled", true);
					$("#sku_billerE").append(appendsEdit);
			
			}
		});	


	}















            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editProduct", function(e) {
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate produk ? ', function(result) {
				if (result) {
				updateProduct($dataID);
				}
			}, '<span style="color: blue;">Edit Produk</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function updateProduct(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/product-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              $('#product-postpaid-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
            toastr.success("Produk Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
					},
				});

				return false;
			}

		}
          </script>