		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Edit Social Media</h3>
			</div>
			<div class="card-body">
				<br>
			<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
				@csrf
			
				

				<div class="form-group row">
						<label class="col-sm-2 col-form-label">Title</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{$data['title']}}"name="title" required>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col-sm-10">
						<select id="icon" class="form-control select2s" style="width:100%;" name="icon">
							<option value="fa-facebook" {{$data['icon'] == "fa-facebook" ?"selected":"" }} >&#xf09a; fa-facebook</option>
							<option value="fa-facebook-f" {{$data['icon'] == "fa-facebook-f" ?"selected":"" }}>&#xf09a; fa-facebook-f</option>
							<option value="fa-facebook-official" {{$data['icon'] == "fa-facebook-official" ?"selected":"" }}>&#xf230; fa-facebook-official</option>
							<option value="fa-facebook-square" {{$data['icon'] == "fa-facebook-square" ?"selected":"" }}>&#xf082; fa-facebook-square</option>
							<option value="fa-twitter-square" {{$data['icon'] == "fa-twitter-square" ?"selected":"" }}>&#xf081; fa-twitter-square</option>
							<option value="fa-twitter" {{$data['icon'] == "fa-twitter" ?"selected":"" }}>&#xf099; fa-twitter</option>
							<option value="fa-instagram" {{$data['icon'] == "fa-instagram" ?"selected":"" }}>&#xf16d; fa-instagram</option>
							<option value="fa-linkedin" {{$data['icon'] == "fa-linkedin" ?"selected":"" }}>&#xf0e1; fa-linkedin</option>
							<option value="fa-linkedin-square" {{$data['icon'] == "fa-linkedin-square" ?"selected":"" }}>&#xf08c; fa-linkedin-square</option>
							<option value="fa-youtube" {{$data['icon'] == "fa-youtube" ?"selected":"" }}>&#xf167; fa-Youtube</option>
						</select>
					</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">URL</label>
						<div class="col-sm-10">
							<input type="text" class="form-control"  value="{{$data['address']}}" name="address" required>
						</div>
					</div>
				
				<div class="card-footer">
					<button type="" id="editDaftarPekerjaan"  data-id='{{$data["id"]}}' class="btn btn-info editDaftarPekerjaan">Submit</button>
					<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
				</div>
			</form>
        	</div>
        </div>


          <script>


    $(document).on("click", "#editDaftarPekerjaan", function(e) {
        e.stopImmediatePropagation();
			e.preventDefault();
		console.log($(this).attr("data-id"));
		if ($("#frmEdit").valid()) {
		
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateReseller($dataID);
				}
			}, '<span style="color: blue;">Edit Data</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
	});
	function updateReseller(idProduct) {
			// Setup validation
	

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);
				var _baseURL = "<?php echo url(''); ?>";
				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/update-social-media/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('.btnEdit ').fadeIn();
            	$('#social-media-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
		{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
		}
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});
</script>