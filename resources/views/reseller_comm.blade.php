<x-package-header />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
<div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Downline dan Komisi</h2>
                </div>            
            </div>
        </div>

        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-8">
				<div class="card">
					<div class="header">
						<div class="d-flex" >
							<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
								<table border="0.1" >
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Group:</label>
										<div class="col-lg-12">
									
											<input type="search" class="form-control" id="filterGroup" placeholder="Filter Group ..." >
										</div>
									</th>
									<!-- <th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Platform:</label>
										<div class="col-lg-12">
											<input type="search" class="form-control" id="filterPlatform" placeholder="Filter Platform ..." >
										</div>
									</th> -->
							
									<!-- <th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label>
										<div class="col-lg-12">
											<select name="filterStatus" id="filterStatus" class="form-control select2" required>
											<option value=""> Semua Status</option>
											<option value="0"> Belum Verifikasi Email</option>
											<option value="1"> Belum Upload Dokumen</option>
											<option value="2"> Verifikasi</option>
											<option value="3"> Suspend</option>
										</select>
										</div>
									</th> -->

								
								
								
								</table>
							</div>
						
						</div>
					
					</div>
					<div class="body">
						<div class="table-responsive">
						<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom reseller-table"  id="reseller-table">
								<thead>
									<tr>
										<th>Avatar</th>
										<th>ID</th>
										<th>Virtual Account</th>
										<th>Nama</th>
										<th>Nomor HP</th>
										<!-- <th>PIN</th> -->
										<th>Email</th>
										<!-- <th>Platform</th> -->
										<th>Grup</th>
										<th>Downline</th>
										<th>Saldo</th>
										<!-- <th>Tanggal Join</th>
										<th>Aktivitas Terakhir</th>
										<th>Aksi</th> -->
									</tr>
								</thead>
								
							</table>
						</div>
					</div>
				</div>
                </div>
                <div class="col-lg-4">
                    <div class="card profile-header">
                        <div class="body">
                            <div class="text-center">
                            	<div class="avatarReseller">
                               		<img src="{{$data['avatar']}}" class="rounded-circle m-b-15 " style="width:90px;height:90px;" alt="">
								</div>
                                <div>
                                    <!-- <h4 class="m-b-0"><strong>Christy</strong> Wert</h4> -->
                                    <h4 class="m-b-0 resellerName">{{$data['name']}}</h4>
                                    <span class= "reseller_code">{{$data['reseller_code']}}</span>
                                </div>
                                <div class="progress progress-xs m-b-25 m-t-25">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                        <!-- <span class="sr-only">60% Complete</span> -->
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-4">
                                        <h6 class="downline_count">{{$data['downline_count']}}</h6>
                                        <span>Downline</span>
                                    </div>
                                    <div class="col-4">
                                        <h6 class="transaksi_downline">{{$data['transaksi_downline']}}</h6>
                                        <span>Transaksi Downline</span>
                                    </div>
                                    <div class="col-4">
                                        <h6 class='komisi'>{{$data['komisi']}}</h6>
                                        <span>Total Komisi</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <span class="badge badge-default mb-2 nohp">{{$data['phone']}}</span>
                            <span class="badge badge-primary mb-2 emailReseller">{{$data['email']}}</span>
                            <span class="badge badge-success mb-2 platformReseller">{{$data['platform']}}</span>
                            <span class="badge badge-info mb-2 deviceDetail">{{$data['attribute']['device_detail']}}</span>
							<!-- @if ($data['referral_code'] == null || $data['referral_code'] == "" )
							<span class="badge badge-warning mb-2 referalReseller">Tidak ada referral</span>	
							@else
							<span class="badge badge-warning mb-2 referalReseller">{{$data['referral_code']}}</span>
							@endif -->
							<span class="badge badge-warning mb-2 referalReseller">{{$data['reference']}}</span>
                           
                            <!-- <span class="badge badge-danger mb-2">Conference</span> -->
                            <hr>
                            <h6>Downline</h6>
                            <ul class="list-unstyled team-info m-t-20 downlineList">
								@if (empty($data['downline']))
								<span class="badge badge-default mb-2 ">Tidak ada Downline</span>
								@else
									@foreach ($data['downline'] as $v )
									<li><img src="{{$v['avatar']}}" data-toggle="tooltip" data-placement="top" title="{{$v['name']}}" alt="Avatar"></li>
									@endforeach
								@endif                           
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /><script>

	
	
	$(function() {
    var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		
		var filterStatus = $('#filterStatus').val();
		var filterGroup = $('#filterGroup').val();
		var filterPlatform =$('#filterPlatform').val();
   
		var oDataList = $('#reseller-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			pageType:"full",
			"bAutoWidth" : false,
			"lengthChange" : false,
			ajax: {
				url: "{{ url('reseller-list') }}",
				data: {
                    filterStatus: filterStatus,
                    filterPlatform: filterPlatform,
                    filterGroup: filterGroup,
             
                },
			},
		    "fnDrawCallback": function( data ,e) {
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
				{
					data: 'avatar',
					name: 'avatar',
					searchable: false,
					orderable: false
			
				},
				{
					data: 'reseller_code',
					name: 'reseller_code',
					orderable: false
			
				},
				{
					data: 'virtual_account',
					name: 'virtual_account',
					orderable: false
			
				},
				{
					data: 'name',
					name: 'name',
					orderable: false
			
				},
				{
					data: 'phone',
					name: 'phone',
					orderable: false
			
				},
				// {
				// 	data: 'pinP',
				// 	name: 'pinP',
			
				// },
				
				{
				    data: 'email',
				    name: 'email',
					orderable: false
			
				},
			//6
			
				{
					data: 'groupName',
					name: 'groupName',
					orderable: false
			
				},
				{
					data: 'downline_count',
					name: 'downline_count',
					orderable: false
			
				},
				{
					data: 'Balance',
					name: 'Balance',
			
				},
			
			],
			buttons: [
			
			],
			columnDefs: [
			],
			select: {
				style: 'single'
			},
		});
    

		$('#reseller-table tbody').on('click', 'tr', function (e) {
        if (e.target.nodeName != 'A' && e.target.nodeName != 'I') {
            var data = oDataList.row( this ).data();
            getDetail(data.reseller_code);
            //console.log(data.item_cat_id);
        } else {
            //e.stopPropagation();
        }
    });
	function getDetail(id){
		$.ajax({
			url:"{{ url('reseller-commission-list') }}"+'/'+id,
			type:'GET',
			dataType:'json',
			beforeSend:function(){
				$(document.body).css({'cursor' : 'wait'});
     
			},
			success:function(data){
				console.log(data.atr);
				$(document.body).css({'cursor' : 'default'});
				$('.resellerName').html(data.data.name);
				$('.reseller_code').html(data.data.reseller_code);
				$('.downline_count').html(data.data.downline_count);
				$('.transaksi_downline').html(data.data.transaksi_downline);
				$('.komisi').html(data.data.komisi);
				$('.nohp').html(data.data.phone);
				$('.emailReseller').html(data.data.email);
				$('.platformReseller').html(data.data.platform);
				$('.deviceDetail').html(data.data.attribute.device_detail);
				// if(data.data.referral_code == null || data.data.referral_code == '' ){
				// 	$('.referalReseller').html('Tidak Ada Referral');
				// } else {
				// }
					$('.referalReseller').html(data.data.reference);
				
				var $senderAvatar = '<img src="'+data.data.avatar+'"  class="rounded-circle m-b-15" alt="" style="width:90px;height:90px;">';	
				$('.avatarReseller').empty().append($senderAvatar);
				if(data.data.downline.length > 0){
					var c="";
					$.each(data.data.downline, function (i,val) {
						c += `
							<li><img src="`+val.avatar+`" data-toggle="tooltip" data-placement="top" title="`+val.name+`" alt="Avatar"></li>
						`;
					})	
					$('.downlineList').empty().append(c);
				}	else {
					$('.downlineList').empty().append('<span class="badge badge-default mb-2 ">Tidak ada Downline</span>');
				}
			},
			error:function(data) {
				$(document.body).css({'cursor' : 'default'});
			},
			complete: function(){
				$(document.body).css({'cursor' : 'default'});
			
			},
		});	
	
	}

	$('#filterGroup').on('keyup', function(){
			oDataList
			.column(6)
			.search(this.value)
			.draw();
			});
		$('#filterPlatform').on('keyup', function(){
			oDataList
			.column(6)
			.search(this.value)
			.draw();
			});
	
		$('#filterStatus').on('change', function(){
			oDataList
			.column(13)
			.search(this.value)
			.draw();
			});
	


	getTable(filterStatus,filterGroup,filterPlatform);


    // $(document).on('change', '#filterStatus', function() {
    //         filterStatus=$(this).val();
	// 		console.log(filterStatus);
    //         filterGroup = $('#filterGroup').val();
    //         filterPlatform = $('#filterPlatform option:selected').text();
    //         $('.reseller-table').DataTable().clear().destroy();
	// 		getTable(filterStatus,filterGroup,filterPlatform);
   	// 	 })
	// 	$(document).on('change', '#filterGroup', function() {
    //         filterGroup=$(this).val();
    //         filterStatus = $('#filterStatus').val();
    //         filterPlatform = $('#filterPlatform option:selected').text();
    //         $('.reseller-table').DataTable().clear().destroy();
	// 		getTable(filterStatus,filterGroup,filterPlatform);
   	// 	 })
	// 	$(document).on('change', '#filterPlatform', function() {
    //         filterPlatform = $('#filterPlatform option:selected').text();
    //         filterGroup = $('#filterGroup').val();
    //         filterStatus = $('#filterStatus').val();
    //         $('.reseller-table').DataTable().clear().destroy();
	// 		getTable(filterStatus,filterGroup,filterPlatform);
   	// 	 })
	
		$(document).on("click", ".exportCsv", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-csv' ).trigger();
					
					}
				}, '<span style="color: blue;">Export Transaksi</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
		$(document).on("click", ".exportExcel", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-excel' ).trigger();
					
					}
				}, '<span style="color: blue;">Export Transaksi</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
	});
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});

 	function cashbackHistory(id,nama){
		
				$('#history').fadeIn();
				$('#reseller-history-table').DataTable().destroy();
				getTableHistory(id);
				$('.namaReseller').empty().html(nama);
				document.querySelector('#history').scrollIntoView({ behavior: 'smooth' })
		
				
    }
	
	function getTableHistory(id) {
		$('#reseller-history-table').DataTable({
				"paging" : false,
				ajax: {
					url: "{{ url('reseller-history-list') }}" + '/' + id,
					
				},
		    "fnDrawCallback": function( data ,e) {

			
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
			
				{
					data: 'tanggal',
					name: 'tanggal',
					searchable: false,
					orderable: false
			
				},
				{
					data: 'description',
					name: 'description',
					searchable: false,
					orderable: false
			
				},
				{
					data: 'debit',
					name: 'debit',
					searchable: false,
					orderable: false
			
				},
				{
					data: 'kredit',
					name: 'kredit',
					searchable: false,
					orderable: false
				},
				{
					data: 'point_after',
					name: 'point_after',
					searchable: false,
					orderable: false
				},
			
			],
			buttons: [
			
			],
			columnDefs: [
		
			],
			select: {
				style: 'single'
			},
		});
    }
		
 	function editData(id,url ="{{url("/reseller-edit/")}}"){
			$.ajax({
			url:url+'/'+id,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
     
			},
			success:function(data){
        $('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url+'/'+id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
			});	
		}

		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});
		$('#addBtn').click(function(){
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
     		$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
		});
	
		$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});
	
		$(document).on("click", ".addReseller", function(e) {
			e.preventDefault();
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);
			if ($("#frmCreate").valid()) {
				showConfirm('Apakah anda Yakin ingin menambahkan Reseller ? ', function(result) {
					if (result) {
						addReseller();
					}
				}, '<span style="color: blue;">Tambah Reseller</span>', {
					confirm: {
						label: 'Tambah',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			} else {
				return ;
			}
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}


		function preview_image() {
    $('#image_preview').empty();
    $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
  }


		function addReseller() {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);

			if ($("#frmCreate").valid()) {
				var form = $("#frmCreate")[0];
				var formData = new FormData(form);

				console.log($("#frmCreate").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/add-reseller",
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
							$('#reseller-table').DataTable().ajax.reload();
							$('#addForm').fadeOut();
							$('#addForm').empty();
							$('#addBtn').fadeIn();
							toastr.success("reseller Berhasil ditambahkan")
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
					},
				});

				return false;
			}

		}

		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini'){
			if($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if(result) {
							console.log(url)
							$.ajax({
								url: url+'/'+$ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if(d.status == 200) {
									
										$('#reseller-table').DataTable().ajax.reload();
										toastr.success("Data Berhasil di Hapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function () {
								},
								complete: function () {
								}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}	

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}
		var _baseURL = "<?php echo url(''); ?>";
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
					if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo( element.parent().parent().parent().parent() );
					}
					else {
						error.appendTo( element.parent().parent().parent().parent().parent() );
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo( element.parent().parent().parent() );
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo( element.parent() );
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo( element.parent().parent() );
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo( element.parent().parent() );
				}

				else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"

		
		};
		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"delete-product", [$id],
				true
			);
			return false;

		}

		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"delete-reseller", [$id],
				true
			);
			return false;
		}
    	

</script>