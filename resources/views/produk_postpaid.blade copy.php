<x-package-header />
<style>
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-110{
    width:110px;
}
.width-150{
    width:150px;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}
	.table-custom th {
		font-size: 12px;
	}

	.table-custom td {
		font-size: 11.5px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Produk</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<!-- <button class="btn btn-sm btn-primary mr-3">
					<i class="fa fa-download"></i> Export CSV
				</button>
				<button class="btn btn-sm btn-primary mr-3" data-toggle="modal" data-target="#filterModal">
					<icon class="fa fa-filter"></icon> Filter
				</button>
				<button class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-sort"></icon> Sortir
				</button> -->
				<button id="addBtn" class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
          <br>
          <center><i class="fa fa-spinner fa-spin"></i></center>
        </div>

        <div class="card-body" id="addForm" style="display: none;">
          	<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Tambah Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
			  @csrf
                <div class="card-body">


				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Group</label>
                      <div class="col-sm-10">
                        <select name="group_id" id="group_id" class="form-control" required>
							<option value="">Pilih Group</option>

						@foreach ($group as $g)
						<option value="{{$g['id']}}">{{$g['command'].' - '.$g['provider']}}</option>
                        @endforeach
                        
						</select>
                      </div>
                </div>

				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Kategori</label>
                      <div class="col-sm-10">
                        <select name="category_id" id="category_id" disabled class="form-control">

						<option value="">Group Produk tidak memiliki kategori</option>
                        
						</select>
                      </div>
                </div>


				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Product SKU</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="sku_code" required>
                    </div>
                  </div>
                  <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
						<label class="fancy-radio custom-color-green"><input name="is_deleted" value="0" type="radio"><span><i></i>Aktif</span></label>
						<label class="fancy-radio custom-color-red"><input name="is_deleted" value="1" type="radio"><span><i></i>Tidak Aktif</span></label>
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="name" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
					<input type="text" class="form-control" name="description" required>
                    </div>
                  </div>
                  <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="command" required>
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nominal</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" name="nominal" required>
                    </div>
                  </div>
                  <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Provider</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="provider" required>
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Harga Dasar</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" name="price_basic" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Harga Jual</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" name="price" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Cashback</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" name="cashback" required>
            <input type="hidden" class="form-control" value="postpaid"  name="type" required>
                    </div>
                  </div>
				  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Biller</label>
                      <div class="col-sm-10">
                        <select name="biller_id" id="biller_id" class="form-control" required>
                        @foreach ($biller as $bill)
						<option value="{{$bill['id']}}">{{$bill['name']}}</option>
                        @endforeach
						</select>
                      </div>
                </div>
			    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">SKU Biller</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="sku_biller" required>
                    </div>
                  </div>
                
             
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="addProduct"   class="btn btn-info addProduct">Submit</button>
                  <button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
        	</div>
        </div>

        <div  class="card-body" id="editForm"></div>

	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
						<div class="d-flex" style="margin-bottom: 25.75px;margin-top: 25.75px;">
							<div class="mr-auto">
								<table border="0" class="table-custom-info">
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Jenis:</label>
									<div class="col-lg-12">
									<select  multiple="multiple" class="form-control js-filterJenis-basic-multiple" id="filterJenis" >
										@foreach($jenis as $v)
										<option value="{{$v['id']}}">{{$v['text']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Provider:</label>
									<div class="col-lg-12">
									<select  multiple="multiple" name="filterProvider[]" class="form-control js-filterProvider-basic-multiple" id="filterProvider" >
										@foreach($provider as $v)
										<option value="{{$v['id']}}">{{$v['text']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Biller:</label>
									<div class="col-lg-12">
									<select  multiple="multiple" class="form-control js-filterBiller-basic-multiple" id="filterBiller" >
										@foreach($biller as $v)
										<option value="{{$v['id']}}">{{$v['name']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
								
								</table>
							</div>
						</div>
					<div class="body">
						<div class="table-responsive check-all-parent">
						<font size="2"  >
							<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom "  id="product-postpaid-table">
								<thead>
									<tr>
										<th>No.
											{{-- <label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label> --}}
										</th>
										<!-- <th>Status</th> -->
										<th>Produk SKU</th>
										<th width="100px;">Nama</th>
										<!-- <th>Grup</th> -->
										<th>Deskripsi</th>
										<th>Nominal</th>
										<th>Jenis</th>
										<th>Provider</th>
										<th>Harga Dasar</th>
										<th>Harga Jual</th>
										<th>Cashback</th>
										<th>Biller</th>
										<th>Gangguan</th>
										<!-- <th>Deskripsi</th> -->
										<th>Aksi</th>
									</tr>
								</thead>
							</table>
						</font>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.js"></script>
<script src="https://malsup.github.io/jquery.blockUI.js"></script>
	<script type="text/javascript">

$(document).on('change', '#group_id', function() {
            let gId  = $(this).val();
			getCategory(gId);
   		 })
			function getCategory(id,url ="{{url("/product-category-bygroupid/")}}") {
				$.ajax({
				url:url+'/'+id,
				type:'GET',
				dataType:'json',
					beforeSend:function(){
					},
					success:function(data){
						var toAppend='';

						$("#category_id").empty();
						if(data.data.length > 0){
							$.each(data.data, function (i,val) {
								toAppend+='<option value="'+val.id+'">'+ val.name +'</option>';
								console.log(val);
						
							});
							$("#category_id").removeAttr('disabled');
						} else {
							 toAppend='<option value="">Group Produk tidak memiliki kategori</option>';

							$("#category_id").prop("disabled", true);
						}	
						$("#category_id").append(toAppend);
					},
					error:function(data) {
						$("#category_id").empty();
						toAppend='<option value="">Group Produk tidak memiliki kategori</option>';

							$("#category_id").prop("disabled", true);
							$("#category_id").append(toAppend);
					
					}
				});	
	  
      
    		}




		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});
  function editData(id,url ="{{url("/product-edit/")}}"){
			$.ajax({
			url:url+'/'+id+'?type=prepaid',
			type:'GET',
			dataType:'HTML',
				beforeSend:function(){
					$('#editForm').empty();
					$('.btnEdit').fadeOut();
					$('#loadBar').show();
		
		
					
				},
				success:function(data){
					$('#addform').fadeOut();
					$('#loadBar').hide();
					$('#formWrapper').fadeOut();
					$('#editForm').append(data);
					$('#editForm').fadeIn();
					$("html, body").animate({ scrollTop: 0 }, "slow");
				},
				error:function(data) {
					$('#loadBar').hide();
					alert(url+'/'+id);
					alert(data);
					alert('500 : Internal server error');
					$('.btnEdit').fadeIn()
				}
			});	
		}

    	
		$('#addBtn').click(function(){
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
      $('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
		});
	
		$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});
	
		$(document).on("click", ".addProduct", function(e) {
			e.preventDefault();
			showConfirm('Apakah anda Yakin ingin menambahkan produk ? ', function(result) {
				if (result) {
				addProduct();
				}
			}, '<span style="color: blue;">Tambah Produk</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function addProduct() {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);

			if ($("#frmCreate").valid()) {
				var form = $("#frmCreate")[0];
				var formData = new FormData(form);

				console.log($("#frmCreate").serialize());
				formData.append("type", 'prepaid');
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/add-product",
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
							$('#product-postpaid-table').DataTable().ajax.reload();
							$('#addForm').fadeOut();
							$('#addBtn').fadeIn();
							toastr.success("Produk Berhasil ditambahkan")
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
					},
				});

				return false;
			}

		}

		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini'){
			if($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if(result) {
							console.log(url)
							$.ajax({
								url: url+'/'+$ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if(d.status == 200) {
									
										$('#product-postpaid-table').DataTable().ajax.reload();
										toastr.success("Produk berhasil dihapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function () {
								},
								complete: function () {
								}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}	

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}
		var _baseURL = "<?php echo url(''); ?>";
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
					if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo( element.parent().parent().parent().parent() );
					}
					else {
						error.appendTo( element.parent().parent().parent().parent().parent() );
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo( element.parent().parent().parent() );
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo( element.parent() );
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo( element.parent().parent() );
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo( element.parent().parent() );
				}

				else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"

		
		};
		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"delete-product", [$id],
				true
			);
			return false;

		}


	$(function() {
    var _baseURL = "<?php echo url(''); ?>";
    var filterJenis = [];
    var filterProvider = [];
    var filterBiller = [];
	$(document.body).on("change",".js-filterProvider-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterBiller = $('.js-filterBiller-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterBiller);
	});
	$(document.body).on("change",".js-filterBiller-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterBiller = $('.js-filterBiller-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterBiller);
	});
	$(document.body).on("change",".js-filterJenis-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterBiller = $('.js-filterBiller-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterBiller);
	});
	$('.js-filterBiller-basic-multiple').select2({
		placeholder: "Semua Biller",
	});
	$('.js-filterJenis-basic-multiple').select2({
		placeholder: "Semua Jenis",
	});
	$('.js-filterProvider-basic-multiple').select2({
		placeholder: "Semua Provider",
	});
	getTable(filterJenis,filterProvider,filterBiller);
	function getTable(filterJenis,filterProvider,filterBiller) {
		// if ( $.fn.dataTable.isDataTable( '#product-postpaid-table' ) ) {
		// 	table = $('#example').DataTable();
		// }
		var oDataList = $('#product-postpaid-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        autoWidth: false,
        "bAutoWidth" : false,
        ajax: {
          url: "{{ url('product-postpaid-list') }}",
          data:  {
                "type": "postpaid",
                "filterJenis": filterJenis,
                "filterProvider": filterProvider,
                "filterBiller": filterBiller
                },

			beforeSend: function(){
				$(document.body).css({'cursor' : 'wait'});
				// $('.page-loader-wrapper').show();
				// $.blockUI({ overlayCSS: { backgroundColor: '#00f' } });
				},
			complete: function(){
				$(document.body).css({'cursor' : 'default'});
				// $('.page-loader-wrapper').hide();
				// $.unblockUI();
			},
		},
      
        columns: [
        
        
          {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
                orderable: false},
            // {
            //     data: 'status',
            //     name: 'status',
		
            // },
            {
                data: 'sku_code',
                name: 'sku_code',
		
            },
            {
				data: 'name',
                name: 'name',
                width: '50px',
		
            },
            {
                data: 'description',
                name: 'description',
		
            },
            {
                data: 'nominal',
                name: 'nominal',
		
            },
            {
				data: 'jenis',
                name: 'jenis',
		
            },
            {
				data: 'product_group.provider',
                name: 'product_group.provider',
            },
            {
                data: 'product_biller.price_basic',
                name: 'product_biller.price_basic',
            },
            {
                data: 'price',
                name: 'price',
            },
            {
                data: 'commission',
                name: 'commission',
            },
            {
                data: 'biller.name',
                name: 'biller.name',
            },
            {
                data: 'isTrouble',
                name: 'isTrouble',
            },
            // {
            //     data: 'deskripsi',
            //     name: 'deskripsi',
            // },
           
            {
				data: 'action',
				name:'Tindakan',
                searchable: false,
                orderable: false
            },
        ],
		    columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-110'>" + data + "</div>";
                    },
                    targets: 3
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-120'>" + data + "</div>";
                    },
                    targets: 2
                }
     
        ],
        select: {
            style: 'single'
        },
	

	
	
   		});
    }
   
	
       function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"delete-product", [$id],
				true
			);
			return false;

		}
    	


	

	});
	




	</script>
