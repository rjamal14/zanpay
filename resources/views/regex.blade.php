<meta name="csrf-token" content="{{ csrf_token() }}" />
<x-package-header />
<style>
	
	#product-category-table {
		font-size: 10px;
	}
	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 120px;
	}

	.width-180 {
		width: 180px;
	}

	.width-110 {
		width: 110px;
	}

	.width-150 {
		width: 150px;
	}

	.width-100 {
		width: 100px;
	}
	.width-80 {
		width: 70px;
	}

	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 150px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {}

	.table-custom td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 11px;
	}
	.table-custom td {
		font-size: 9px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-6 col-sm-12">
				<h2>Regex</h2>
			</div>
			<div class="col-md-6 col-sm-6 text-right">

				<button id="addBtn" class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
	</div>
	<div class="card-body" id="addForm" style="display: none;">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Tambah Regex</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
				@csrf
				<div class="card-body">

					<div class="card-body">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Biller</label>
							<div class="col-sm-10">
								<select name="biller_id" id="billerID" class="form-control select2" required>
									<option value="">Pilih Biller</option>
									@foreach ($biller as $t)
									<option value="{{$t['id']}}">{{$t['name']}} </option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Status</label>
							<div class="col-sm-10">
								<select name="status" id="statusID" class="form-control" required>
									<!-- <option value=''>Sukses</option> -->
									<option value='0'>Sukses</option>
									<option value='1'>Pending</option>
									<option value='2'>Gagal</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Kata Kunci</label>
							<div class="col-sm-10">
								<textarea type="text" class="form-control" name="added_data" id="added_dataID" required></textarea>
							</div>
						</div>

						<div id="formHidden">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Group</label>
								<div class="col-sm-10">
									<select name="groupID" id="groupID" class="form-control select2" required>

									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Tipe Produk</label>
								<div class="col-sm-10">
									<select name="tipeID" id="tipeID" class="form-control select2" required>

										</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Provider</label>
								<div class="col-sm-10">
									<select name="provider_id" id="providerID" class="form-control select2" required>

									</select>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Contoh Respon</label>
							<div class="col-sm-10">
								<textarea type="text" class="form-control" name="sample_respond" id="sampleRespondID" required></textarea>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Tipe Respon</label>
							<div class="col-sm-10">
								<select name="type_respond" id="" class="form-control" required>
									<option value='0' selected>Text</option>
								</select>
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label ">Parameter Wajib</label>
							<br>
							<br>
						</div>
						<div class="col-sm-12 form-group row">
								<div class="col-sm-12 row">
									<label class="col-sm-2 col-form-label">Refid Biller</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Refid transaksi Anda "><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">Trxid</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Refid Digiflazz transaksi Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">Tujuan</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Tujuan Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">Kode Produk</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah SKU Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">Harga</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Harga Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">Sisa Saldo</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Sisa Saldo Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
								<br>
								<div class="col-sm-12 row" style="margin-top:5px!important;">
									<label class="col-sm-2 col-form-label">SN</label>
									<div class="col-sm-3">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID1 ParamWajibID form-control">
									</div>
									<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
										-
									</div>
									<div class="col-sm-3" style="margin-left:-30px!important;">
										<input type="text" name="refid" id="ParamWajibID[]" class="ParamWajibID2 ParamWajibID form-control">

									</div>
									<div class="col-sm-1" style="margin-left:-25x!important;">
										<a style="border-radius:15px!important;" class="btn btn-sm  btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah SN Anda"><i class="fa fa-question"></i></a>
									</div>
								</div>
					

						</div>
					
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Parameter Opsional</label>
							<div class="col-sm-10">
								<textarea type="text" class="form-control" name="parameter" rows="5" id="parameterID" required>Token=[awal ;, akhir ;]</textarea>
							</div>
						</div>
						<hr>



						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Preview</label>
							<div class="col-sm-8">
								<textarea type="text" class="form-control" rows='8' name="preview" id="previewID" readonly required></textarea>
								<input type="hidden" class="form-control" rows='8' name="preview_wajib" id="previewIDW">
								<input type="hidden" class="form-control" rows='8' name="preview_opsi" id="previewIDO">
								<input type="hidden" class="form-control" rows='8' name="preview_wajib2" id="previewIDp">
							</div>
							<div class="col-sm-2">
								<button type="submit" id="generatePreview" class="btn btn-success">Lihat Hasil</button>
							</div>
						</div>
					</div>





				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" id="addProduct" class="btn btn-info addProduct">Submit</button>
					<button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>

	<div class="card-body" id="editForm"></div>
	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						<div class="d-flex" style="margin-bottom: 25.75px;margin-top: 25.75px;">
							<div class="mr-auto">
								<table border="0" class="table-custom-info">
									<th width="200px">
										<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Biller:</label>
										<div class="col-lg-12">
											<select class="form-control fftable js-filterBiller-basic-multiple select2" id="filterBiller">
												<option value="">Semua Biller</option>
												@foreach($biller as $b)
												<option value="{{$b['id']}}">{{$b['name']}}</option>
												@endforeach
											</select>
										</div>
									</th>
									<th width="200px">
										<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Tipe:</label>
										<div class="col-lg-12">
											<select class="form-control fftable js-filterTipe-basic-multiple select2" id="filterTipe">
												<option value="">Semua Tipe</option>
												@foreach($tipe as $v)
												<option value="{{$v['id']}}">{{$v['text']}}</option>
												@endforeach
											</select>
										</div>
									</th>
									<th width="200px">
										<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label>
										<div class="col-lg-12">
											<select class="form-control fftable js-filterJenis-basic-multiple select2" id="filterJenis">
												<option value="">Semua Status</option>
												<option value="0">Sukses</option>
												<option value="1">Pending</option>
												<option value="2">Gagal</option>
											</select>
										</div>
									</th>



								</table>
							</div>
						</div>
						<div class="body">

							<div class="table-responsive">


								<table class="table table-bordered table-hover c_list  table-custom " id="product-category-table">
									<thead>
										<tr>
											<th>No.</th>
											<th>Biller</th>
											<th>Group</th>
											<th>Tipe Produk</th>
											<th>Provider </th>
											<th>Contoh Respon</th>
											<th>Tipe Respon</th>
											<th>Parameter</th>
											<th>Preview </th>
											<th>Status </th>
											<th>Tgl. Update </th>
											<th>Di update oleh</th>
											<th></th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<x-package-footer />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		})
		$(document).ready(function() {
			$(".select2").select2({
				tags: true
			});
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		});
		$(document).on("change", "#parameterID", function(e) {
			e.preventDefault();
			$("#previewID").val("");
			$("#previewIDW").val("");
			$("#previewIDO").val("");
			$("#previewIDp").val("");
		});
		$(document).on("change", "#parameterIDEdit", function(e) {
			e.preventDefault();
			$("#previewIDEdit").val("");
			$("#previewIDWEdit").val("");
			$("#previewIDOEdit").val("");
			$("#previewIDpEdit").val("");
		});
		$(document).on("change", ".ParamWajibID", function(e) {
			e.preventDefault();
			$("#previewID").val("");
			$("#previewIDW").val("");
			$("#previewIDO").val("");
			$("#previewIDp").val("");
		});
		$(document).on("change", ".ParamWajibIDEdit", function(e) {
			e.preventDefault();
			$("#previewIDEdit").val("");
			$("#previewIDWEdit").val("");
			$("#previewIDOEdit").val("");
			$("#previewIDpEdit").val("");
		});
		$(document).on("change", "#sampleRespondID", function(e) {
			e.preventDefault();
			$("#previewID").val("");
			$("#previewIDW").val("");
			$("#previewIDO").val("");
			$("#previewIDp").val("");
		});
		$(document).on("change", "#sampleRespondIDEdit", function(e) {
			e.preventDefault();
			$("#previewIDEdit").val("");
			$("#previewIDWEdit").val("");
			$("#previewIDOEdit").val("");
			$("#previewIDpEdit").val("");
		});
		$(document).on("click", "#generatePreview", function(e) {
			e.preventDefault();
			let xx = true;
			var param2awal = [];
			var param2akhir = [];
			$.each($('.ParamWajibID'), function(i, val) {
				console.log(val.value);
				if (val.value == "") {
					toastr.error('Mohon periksa kembali data inputan');
					xx = false;
					return;
				}
			});
			$.each($('.ParamWajibID1'), function(i, val) {
				param2awal.push(val.value);
			});
			$.each($('.ParamWajibID2'), function(i, val) {
				param2akhir.push(val.value);
			});
			if (xx) {

				var param = $('#parameterID').val();
				var sample = $('#sampleRespondID').val();
				$.ajax({
					url: '{{url("/generate-regex")}}',
					type: 'post',
					data: {
						param: param,
						param2awal: param2awal,
						param2akhir: param2akhir,
						sample: sample
					},
					dataType: 'json',
					beforeSend: function() {
						$("#previewID").val("");
						$("#previewIDW").val("");
						$("#previewIDO").val("");
						$("#previewIDp").val("");
					},
					success: function(data) {
						console.log(data.data);
						if (data.status == 200) {
							$("#previewID").val(data.data);
							$("#previewIDW").val(data.data2);
							$("#previewIDO").val(data.data1);
							$("#previewIDp").val(data.data22);
							toastr.success(data.message)
						} else {
							$("#previewID").val("");
							$("#previewIDW").val("");
							$("#previewIDO").val("");
							$("#previewIDp").val("");
							toastr.error(data.message)
						}

					},
					error: function(data) {
						console.log(data);
						toastr.error(data.message)
					}
				});
			}

		});
		$(document).on("click", "#generatePreviewEdit", function(e) {
			e.preventDefault();
			let xx = true;
			var param2awal = [];
			var param2akhir = [];
			$.each($('.ParamWajibIDEdit'), function(i, val) {
				console.log(val.value);
				if (val.value == "") {
					toastr.error('Mohon periksa kembali data inputan');
					xx = false;
					return;
				}
			});
			$.each($('.ParamWajibID1Edit'), function(i, val) {
				param2awal.push(val.value);
			});
			$.each($('.ParamWajibID2Edit'), function(i, val) {
				param2akhir.push(val.value);
			});
			if (xx) {

				var param = $('#parameterIDEdit').val();
				var sample = $('#sampleRespondIDEdit').val();
				$.ajax({
					url: '{{url("/generate-regex")}}',
					type: 'post',
					data: {
						param: param,
						param2awal: param2awal,
						param2akhir: param2akhir,
						sample: sample
					},
					dataType: 'json',
					beforeSend: function() {
						$("#previewIDEdit").val("");
						$("#previewIDWEdit").val("");
						$("#previewIDOEdit").val("");
						$("#previewIDpEdit").val("");
					},
					success: function(data) {
						console.log(data.data);
						if (data.status == 200) {
							$("#previewIDEdit").val(data.data);
							$("#previewIDWEdit").val(data.data2);
							$("#previewIDOEdit").val(data.data1);
							$("#previewIDpEdit").val(data.data22);
							toastr.success(data.message)
						} else {
							$("#previewIDEdit").val("");
							$("#previewIDWEdit").val("");
							$("#previewIDOEdit").val("");
							$("#previewIDpEdit").val("");
							toastr.error(data.message)
						}

					},
					error: function(data) {
						console.log(data);
						toastr.error(data.message)
					}
				});
			}

		});
		$(document).on("change", "#billerID", function() {
			$thisId = $(this).children('option:selected').val();
			$.ajax({
				url: '{{url("/group-combobox-bybiller?")}}biller_id=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {
					$("#groupID").empty();
					$("#tipeID").empty();
					$("#providerID").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '';

					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';
						});
						$("#groupID").append(append);
					} else {
						$("#groupID").empty();
						$("#groupID").append('');
					}
				},
			});
		});
		$(document).on("change", "#billerIDEdit", function() {
			$thisId = $(this).children('option:selected').val();
			$.ajax({
				url: '{{url("/group-combobox-bybiller?")}}biller_id=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {
					$("#groupIDEdit").empty();
					$("#tipeIDEdit").empty();
					$("#providerIDEdit").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '<option value="">Tidak ada data</option>';

					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';
						});
						$("#groupIDEdit").append(append);
					} else {
						$("#groupIDEdit").empty();
						$("#groupIDEdit").append('<option value="">Tidak ada data</option>');
					}
				},
			});
		});
		$(document).on("change", "#statusID", function() {
			if ($(this).children('option:selected').val() != 0) {
				$('#formHidden').hide();
				$("#tipeID").empty();
				$("#providerID").empty();
			} else {
				$('#formHidden').show();
				$('.select2').select2({
				});
			}
		});
		$(document).on("change", "#statusIDEdit", function() {
			// if ($('#billerIDEdit').children('option:selected').val() != "" && $(this).children('option:selected').val() == 0) {
			// 	$("#billerIDEdit").val("").trigger('change');
			// }
			if ($(this).children('option:selected').val() != "0") {
				$('#formHiddenEdit').hide();
				$("#tipeIDEdit").empty();
				$("#providerIDEdit").empty();
			
			} else {
				$('#formHiddenEdit').show();
				$('.select2').select2({
				});
			}
		});
		$(document).on("change", "#groupID", function() {
			$thisId = $(this).children('option:selected').val();
			if ($thisId == 'Regex Global') {
				console.log($("#tipeID"))
				$("#providerID").attr('disabled', true);
				$("#tipeID").attr('disabled', true);
		
				return false;
			}else {
				$("#tipeID").attr('disabled', false);
				$("#providerID").attr('disabled', false);
				$.ajax({
				url: '{{url("/group-combobox-bybiller1?")}}holder_id=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {
					$("#tipeID").empty();
					$("#providerID").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '';

					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

						});
						$("#tipeID").append(append);
					} else {
						$("#tipeID").empty();
						$("#tipeID").append('<option value="">Tidak ada data</option>');
					}
				},
			});
			}
		
		});
		$(document).on("change", "#groupIDEdit", function() {
			$thisId = $(this).children('option:selected').val();
			if ($thisId == 'Regex Global') {
				$("#providerIDEdit").attr('disabled', true);
				$("#tipeIDEdit").attr('disabled', true);
				$("#tipeIDEdit").empty();
					$("#providerIDEdit").empty();
				return;
			}else {
				
				$("#providerIDEdit").attr('disabled', false);
				$("#tipeIDEdit").attr('disabled', false);
				$.ajax({
				url: '{{url("/group-combobox-bybiller1?")}}holder_id=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {
					$("#tipeIDEdit").empty();
					$("#providerIDEdit").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '<option value="">Tidak ada data</option>';

					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

						});
						$("#tipeIDEdit").append(append);
					} else {
						$("#tipeIDEdit").empty();
						$("#tipeIDEdit").append('<option value="">Tidak ada data</option>');
					}
				},
			});
			}
			
		});
		$(document).on("change", "#tipeID", function() {
			$thisId = $(this).children('option:selected').val();
			
			$.ajax({
				url: '{{url("/group-combobox-bybiller2?")}}command=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {

					$("#providerID").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '';

					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';

						});
						$("#providerID").append(append);
					} else {
						$("#providerID").empty();
						$("#providerID").append('');
					}
				},
			});
		});
	
		$(document).on("change", "#tipeIDEdit", function() {
			$thisId = $(this).children('option:selected').val();
			$.ajax({
				url: '{{url("/group-combobox-bybiller2?")}}command=' + $thisId,
				type: 'GET',
				dataType: 'json',
				beforeSend: function() {

					$("#providerIDEdit").empty();
				},
				success: function(data) {
					console.log(data.data);
					var append = '';
					if (data.data.length > 0) {
						$.each(data.data, function(i, val) {
							append += '<option data-id="' + val.id + '"  value="' + val.id + '">' + val.text + '</option>';
						});
						$("#providerIDEdit").append(append);
					} else {
						$("#providerIDEdit").empty();
						$("#providerIDEdit").append('');
					}
				},
			});
		});
	
	
	
	

		$(function() {
			var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,
				$isImgEditable = false,
				$choosenBahanBaku = [],
				$variants, $choosenVariant = [],
				$status_filter = "",
				$outlet_filter = "",
				$is_paid = "",
				$paid_approval = "";

			var datePicker = $('.daterange-ranges').daterangepicker({
					startDate: moment(),
					endDate: moment(),
					ranges: {
						'Hari ini': [moment(), moment()],
						'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
						'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
						'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
						'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
						'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
						'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
						'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
					},
					locale: {
						cancelLabel: 'Batal',
						applyLabel: 'Proses',
						daysOfWeek: [
							"Sen",
							"Sel",
							"Rab",
							"Kam",
							"Jum",
							"Sab",
							"Min"
						],
						monthNames: [
							"Januari",
							'Februari',
							'Maret',
							'April',
							'Mei',
							'Juni',
							'Juli',
							'Agustus',
							'September',
							'Oktober',
							'November',
							'Desember'
						],
					},
					opens: 'right',
					showCustomRangeLabel: false,
					alwaysShowCalendars: true,
					applyButtonClasses: 'btn-small btn-primary',
					cancelClass: 'btn-small btn-default',
					maxDate: new Date(),

				},
				// Datepickerrange callback
				datepickerRangeCallback = function(start, end) {
					$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
				}, datepickerRangeCallback);
			$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + moment().format('DD MMM YYYY'));

			var _baseURL = "<?php echo url(''); ?>";
			var filterJenis = [];
			var filterTipe = [];
			var filterBiller = [];
			$(document.body).on("change", ".js-filterTipe-basic-multiple", function() {
				filterTipe = $('.js-filterTipe-basic-multiple').val()
				filterBiller = $('.js-filterBiller-basic-multiple').val()
				filterJenis = $('.js-filterJenis-basic-multiple').val()
				getTable(filterJenis, filterTipe, filterBiller);
			});
			$(document.body).on("change", ".js-filterBiller-basic-multiple", function() {
				filterTipe = $('.js-filterTipe-basic-multiple').val()
				filterBiller = $('.js-filterBiller-basic-multiple').val()
				filterJenis = $('.js-filterJenis-basic-multiple').val()
				getTable(filterJenis, filterTipe, filterBiller);
			});
			$(document.body).on("change", ".js-filterJenis-basic-multiple", function() {
				filterTipe = $('.js-filterTipe-basic-multiple').val()
				filterBiller = $('.js-filterBiller-basic-multiple').val()
				filterJenis = $('.js-filterJenis-basic-multiple').val()
				getTable(filterJenis, filterTipe, filterBiller);
			});
			$('.js-filterBiller-basic-multiple').select2({
				placeholder: "Semua Biller",
				allowClear: true

			});
			$('.js-filterJenis-basic-multiple').select2({
				placeholder: "Semua Status",
				allowClear: true
			});
			$('.js-filterTipe-basic-multiple').select2({
				placeholder: "Semua Tipe",
				allowClear: true
			});
			getTable(filterJenis, filterTipe, filterBiller);



			function getTable(filterJenis, filterTipe, filterBiller) {
				var oDataList = $('#product-category-table').DataTable({
					processing: true,
					serverSide: true,
					destroy: true,
					autoWidth: false,
					"bAutoWidth": false,
					ajax: {
						url: "{{ url('regex_list') }}",
						data: {
							"filterStatus": filterJenis,
							"filterTipe": filterTipe,
							"filterBiller": filterBiller
						},
					},
					"fnDrawCallback": function(data, e) {

					},


					dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

					columns: [


						{
							data: 'DT_RowIndex',
							name: 'DT_RowIndex',
							searchable: false,
							orderable: false
						},
						// {
						//     data: 'status',
						//     name: 'status',

						// }

						{
							data: 'billername',
							name: 'billername',

						},
						{
							data: 'groupname',
							name: 'groupname',

						},
						{
							data: 'groupcommand',
							name: 'groupcommand',

						},
						{
							data: 'groupprovider',
							name: 'groupprovider',

						},
						{
							data: 'sample_respond',
							name: 'sample_respond',

						},
						{
							data: 'tipe',
							name: 'tipe',

						},
					
						{
							data: 'parameter23',
							name: 'parameter23',

						},
						{
							data: 'review2',
							name: 'review2',

						},
						{
							data: 'statusText',
							name: 'statusText',

						},
						{
							data: 'tglUpdate',
							name: 'tglUpdate',

						},
						{
							data: 'updateOleh',
							name: 'updateOleh',

						},

						{
							data: 'action',
							name: 'Tindakan',
							searchable: false,
							orderable: false
						},
					],
					buttons: [

						{
							extend: 'csv',
							title: 'Export Menu Produk' + $.now(),
						},
					],
					columnDefs: [
						// {
						// 	render: function(data, type, full, meta) {
						// 		return "<div class='text-wrap width-100'>" + data + "</div>";
						// 	},
						// 	targets: 4
						// }, 
						{
							render: function(data, type, full, meta) {
								return "<div class='text-wrap width-100'>" + data + "</div>";
							},
							targets: 5
						},

						{
							render: function(data, type, full, meta) {
								return "<div class='text-wrap width-100'>" + data + "</div>";
							},
							targets: 7
						},
						{
							render: function(data, type, full, meta) {
								return "<div class='text-wrap width-100'>" + data + "</div>";
							},
							targets: 8
						},
						{
							render: function(data, type, full, meta) {
								return "<div class='text-wrap width-80'>" + data + "</div>";
							},
							targets: 10
						},
				


					],
					select: {
						style: 'single'
					},
				});
			}

			datePicker.on('apply.daterangepicker', function(ev, picker) {
				oDataList.draw(true);


			});



			$(document).on("click", ".exportCsv", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button('.buttons-csv').trigger();

					}
				}, '<span style="color: blue;">Export Menu Produk</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			});
			$(document).on("click", ".exportExcel", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button('.buttons-excel').trigger();

					}
				}, '<span style="color: blue;">Export Menu Produk</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			});
		});
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
					if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo(element.parent().parent().parent().parent());
					} else {
						error.appendTo(element.parent().parent().parent().parent().parent());
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo(element.parent().parent().parent());
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo(element.parent());
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent());
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"


		};

		function editData(id, url = "{{url("/regex/")}}") {
			$.ajax({
				url: url + '/' + id,
				type: 'GET',
				dataType: 'HTML',
				beforeSend: function() {
					$('#editForm').empty();
					$('.btnEdit').fadeOut();
					$('#loadBar').show();

				},
				success: function(data) {
					$('#addform').fadeOut();
					$('#loadBar').hide();
					$('#formWrapper').fadeOut();
					$('#editForm').append(data);
					$('#editForm').fadeIn();
					$("html, body").animate({
						scrollTop: 0
					}, "slow");
					$(".select2").select2({
						tags: true
					});
				},
				error: function(data) {
					$('#loadBar').hide();
					alert(url + '/' + id);
					alert(data);
					alert('500 : Internal server error');
					$('.btnEdit').fadeIn()
				}
			});
		}

		$('.btnEdit').click(function() {
			$('#addForm').fadeOut();
		});
		$('#addBtn').click(function() {
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
			$(".select2").select2({});
		});

		$('#cancelBtnAdd').click(function() {
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});



		$(document).on("click", ".addProduct", function(e) {
			e.preventDefault();

			if ($('#previewID').val() == '') {
				toastr.error('Preview tidak boleh kosong');
				return;
			}
			if ($('#billerID').val() == '') {
				toastr.error('Biller tidak boleh kosong');
				return;
			}
			console.log($('#statusID').children('option:selected').val(), $('#statusID').val())
			if ($('#statusID').children('option:selected').val() == "0") {
				if ($('#groupID').children('option:selected').val() != 'Regex Global') {
					if ($('#providerID').children('option:selected').val() == "") {
						toastr.error('Provider harus diisi');
						return;
					}
					if ($('#providerID').children('option:selected').val() == undefined) {
						toastr.error('Provider harus diisi');
						return;
					}
				}
			}

			if ($('#statusID').val() == "0") {
				if ($('#groupID').children('option:selected').val() != 'Regex Global') {
					if ($('#providerID').children('option:selected').val() == "") {
						toastr.error('Provider harus diisi');
						return;
					}
					if ($('#providerID').children('option:selected').val() == undefined) {
						toastr.error('Provider harus diisi');
						return;
					}
				}
			}


			$.each($('.ParamWajibID'), function(i, val) {
				if (val.value == "") {
					toastr.error('Mohon periksa kembali data inputan');
					return;
				}
			});
			showConfirm('Apakah anda Yakin ingin menambahkan data ? ', function(result) {
				if (result) {
					addProduct();
				}
			}, '<span style="color: blue;">Tambah Data</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		function addProduct() {
			// Setup validation

			var form = $("#frmCreate")[0];
			var formData = new FormData(form);
			var param2awal = [];
			var param2akhir = [];
			$.each($('.ParamWajibID1'), function(i, val) {
				param2awal.push(val.value);
			});
			$.each($('.ParamWajibID2'), function(i, val) {
				param2akhir.push(val.value);
			});

			formData.append("param2awal", param2awal);
			formData.append("param2akhir", param2akhir);

			$.ajax({
				url: _baseURL + "/regex",
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#product-category-table').DataTable().ajax.reload();
						$('#addForm').fadeOut();
						$('#addBtn').fadeIn();
						toastr.success("Produk Berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;

		}

		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini') {
			if ($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if (result) {
							console.log(url)
							$.ajax({
								url: url + '/' + $ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if (d.status == 200) {

										$('#product-category-table').DataTable().ajax.reload();
										toastr.success("data berhasil dihapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function() {},
								complete: function() {}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
			if (!$buttons) {
				$buttons = {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				};
			}
			bootbox.confirm({
				title: $title,
				message: $message,
				buttons: $buttons,
				callback: $callback
			});
		}
		var _baseURL = "<?php echo url(''); ?>";
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
					if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo(element.parent().parent().parent().parent());
					} else {
						error.appendTo(element.parent().parent().parent().parent().parent());
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo(element.parent().parent().parent());
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo(element.parent());
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent());
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"


		};

		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"regex-delete", [$id],
				true
			);
			return false;

		}

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
			if (!$buttons) {
				$buttons = {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				};
			}
			bootbox.confirm({
				title: $title,
				message: $message,
				buttons: $buttons,
				callback: $callback
			});
		}
	</script>