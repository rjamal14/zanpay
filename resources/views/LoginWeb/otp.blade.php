<head>
	<title>Zanpay Back-Office</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/zan.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>

<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="images/img-01.png" alt="IMG">
				</div>

				<form style ="width: 350px!important" class="login100-form validate-form">
					<span class="login100-form-title">
						ZanPay BackOffice - OTP
						
					</span>
					

					<input class="input100" type="hidden" id="_token" name="_token" placeholder="Email">
					<div class="wrap-input100 validate-input" data-validate = "OTP is required">
						<input class="input100" type="numeric" id="otp" name="OTP" maxlength="6" size="6">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-paper-plane" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn otpButton">
							Verifikasi OTP
						</button>
					</div>
					<div class="text-center p-t-12 otpSessionMessage" >
						<span>
							{{isset(Session::get('SESS_USERDATA')['otp_message'])  ? Session::get('SESS_USERDATA')['otp_message'] : ""}}
						</span>
					</div>
					<br>
					<br>
					<br>
					<br>


					<div class="text-left p-t-12">
						<span class="txt1">
							Tidak Menerima OTP?
						</span>
						<br>
						<div id="otp_timer">
						<p>Kirim Ulang OTP dalam <span id="count">60</span> detik...</p>
						<!-- <a class="txt2 resendOtp" href="#">
							Kirim Ulang OTP via email ke
						</a>
						<br>
						<a class="txt2 resendOtp" href="#">
							Kirim Ulang OTP via SMA ke
						</a> -->
						</div>
					</div>


					<div class="text-center p-t-136">
						<a class="txt2 " href="login?forgetOTP=1">
							Kembali ke halaman login
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
<x-package-footer />
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>


	var _baseURL = "<?php echo url(''); ?>";
	var $phoneUser =  `{{Session::get('SESS_USERDATA')['phone'] }}`;
	console.log($phoneUser !== "" );
	var divTimer = '<a class="txt2 flex resendOtp" onclick="resendOtp(0)" href="#"> Kirim Ulang OTP via email ke ' + `{{Session::get('SESS_USERDATA')['mask_email'] }}`+' </a>'
	if($phoneUser !== ""   ){
		 divTimer = '<a class="txt2  resendOtp" onclick="resendOtp(0)" href="#"> Kirim Ulang OTP via email ke ' + `{{Session::get('SESS_USERDATA')['mask_email'] }}`+'</a> <br> <a class="txt2  flex resendOtp" onclick="resendOtp(1)" href="#"> Kirim Ulang OTP via WhatsApp ke ' + `{{Session::get('SESS_USERDATA')['mask_phone'] }}`+'</a>'
	} 
window.onload = function(){

(function(){
  var counter = 60;

  setInterval(function() {
    counter--;
    if (counter >= 0) {
      span = document.getElementById("count");
      span.innerHTML = counter;
    }
    // Display 'counter' wherever you want to display it.
    if (counter === 0) {
    //    alert('this is where it happens');
	
					$('#otp_timer').html(divTimer);	
        clearInterval(counter);
    }

  }, 1000);

})();

}


	  	$('.otpButton').on('click', function (e) {
			e.stopImmediatePropagation();
			e.preventDefault();
      	  doLogin();
    	});
	  	$('.resendOtp').on('click', function (e) {
			e.stopImmediatePropagation();
			e.preventDefault();
      	  resendOtp();
    	});
		function resendOtp(type) {
			$.ajax({
				url: _baseURL + '/resendOtp',
				type: "post",
				data: {
					otp: $('#otp').val(),
					type: type,
					_token: $('#_token').val()
				},
				dataType: 'json',
				success: function (d) {
				if (d.status != 200){





					Swal.fire(
						'Error',
						d.message,
						'error'
						)
				} else {
					Swal.fire(
						'Berhasil',
						d.message,
						'success'
						)
						$('.otpSessionMessage').html('').html('<span>'+d.message+'</span>');	
						$('#otp_timer').html('<p>Kirim Ulang OTP dalam <span id="count">60</span> detik...</p>');
					var counter = 60;

					setInterval(function() {
						counter--;
						if (counter >= 0) {
						span = document.getElementById("count");
						span.innerHTML = counter;
						}
						// Display 'counter' wherever you want to display it.
						if (counter === 0) {
						//    alert('this is where it happens');
						
										$('#otp_timer').html(divTimer);	
									
							clearInterval(counter);
						}

					}, 1000);

										



				}

				},
				beforeSend: function () {
				},
				complete: function () {
					



				}
			});
    	}
		function doLogin() {
			$.ajax({
				url: _baseURL + '/doOtp',
				type: "post",
				data: {
					otp: $('#otp').val(),
					_token: $('#_token').val()
				},
				dataType: 'json',
				success: function (d) {
				if (d.status != 200){
					Swal.fire(
						'Error',
						d.message,
						'error'
						)
				} else {
					location.href =  _baseURL + '/dashboard'
				}

				},
				beforeSend: function () {
				},
				complete: function () {
				}
			});
    	}
</script>