<x-package-header />
<style>
	/* .table-responsive{
  height:auto;  
  overflow:scroll;
}
 thead tr:nth-child(1) th{
    background: white;
    position: sticky;
    top: 0;
    z-index: 10;
  } */
.fa.disabled,
.fa[disabled],
.disabled > .fa,
[disabled] > .fa {
  opacity: 0.5;
  /*optional*/ cursor: not-allowed !important;
  /*optional*/ pointer-events: none !important;
}	
a.disabled {
  cursor: not-allowed !important;
}
a.disabled btn {
  pointer-events: none !important;
}
div.disabled  {
	cursor: not-allowed !important;
}
a:disabled {
  cursor: not-allowed !important;
  pointer-events: all !important;
}
.disabled > .a,
[disabled] > .a {
  opacity: 0.5;
  /*optional*/ cursor: not-allowed !important;
  /*optional*/ pointer-events: none !important;
}	
.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		/* font-size: 10.5px; */
	}

	.table-custom td {
		font-size: 9px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 10px;
	}

	.table-custom-info tr {
		font-size: 10px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-6 col-sm-12 ">
				<h2 class="animate">Live Transaksi | <span id="txt_status_list" data-status=""></span></h2>
				<p class="animate">Tanggal & Jam : <strong>{{$curDate}}  &nbsp; <span id="time-part"></span></strong> </p>
				
			</div>
			<div class="col-md-6  col-sm-6 text-right">
				<button class="btn btn-sm btn-success mr-1 act_list_trx" data-txt="Semua Transaksi">Semua Transaksi</button>
				<button class="btn btn-sm btn-warning mr-1 act_list_trx" data-txt="Belum Selesai">Belum Selesai</button>
				<button class="liveTransaksiBtn btn btn-sm btn-primary mr-1">Kembali ke Transaksi</button>
		
			</div>
			
		</div>
	</div>
	
	
	<div id="mainContent" class>

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						<div class="body">
					
						<div class="table-responsive">
							<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom "  id="transaction-table">
								<thead class="thead-dark">
									<tr >
										<th>TRXID</th>
										<th>Tanggal Masuk</th>
										<th>Tanggal Update</th>
										<th>ID Agen</th>
										<th>Group</th>
										<th>Jenis</th>
										<!-- <th>Produk</th> -->
										<th>SKU</th>
										<th>No.Tujuan</th>
										<th>Harga Beli</th>
										<th>Harga Jual</th>
										<th>Laba</th>
										<th>Biller</th>
										<th>Saldo Biller</th>
										<th>SN</th>
										<th>Status</th>
										<!-- <th></th> -->
									</tr>
								</thead>
							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<x-package-footer />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://unpkg.com/floatthead"></script>
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script>
$('.liveTransaksiBtn').click(function(){
		window.location.href=_baseURL+'/transaksi';
});

var _baseURL = "<?php echo url(''); ?>";

$(document).ready(function() {
    var interval = setInterval(function() {
        var momentNow = moment();
      
        $('#time-part').html(momentNow.format("HH:mm:ss"));
    }, 100);

	$('#txt_status_list').html("Semua Transaksi")
});

$('.act_list_trx').click(function(){
	$('#txt_status_list').html($(this).attr('data-txt'));
	if($(this).attr('data-txt')=='Belum Selesai'){
		$('#txt_status_list').attr('data-status', 'belum_selesai')
	}else{
		$('#txt_status_list').attr('data-status', '')
	}
})
		
$.fn.dataTable.ext.errMode = 'throw';
var oDataList = $('#transaction-table').DataTable({
			processing: false,
			serverSide: true,
			autoWidth: false,
			ordering: false,
			"lengthMenu": [[ 25, 50, 100], [ 25, 50, 100]],
			"bAutoWidth" : false,
			"bLengthChange": true,
			"dom": '<"top"i>rt<"bottom"flp><"clear">',
			sDom: 'lrtip',
			ajax: {
				url: "{{ url('transaksi-live-list') }}",
				data: function(d){
					d.filterStatus = $('#txt_status_list').attr('data-status')
            	}
			},
		    "fnDrawCallback": function( data ,e) {

				$(() => $('table').floatThead({
						top: '61'
					}));
				},
		

			columns: [
				{
					data: 'trx_code',//0
					name: 'trx_code',
					width: "6%",
			
				},
				{
					data: 'tanggal_masuk',//1
					name: 'tanggal_masuk',
					width: "6%",
			
				},
				{
					data: 'tanggalUpdate',//1
					name: 'tanggalUpdate',
					width: "6%",
			
				},
				{
					data: 'reseller.reseller_code',//2
					name: 'reseller.reseller_code',
					width: "6%",
			
				},
				{
					data: 'jenis',//3
					name: 'jenis',
					width: "6%",
			
				},
				{
					data: 'group',//4
					name: 'group',
					width: "6%",
				},
			
				{
					data: 'productSKU',//5
					name: 'productSKU',
					width: "6%",
				},
				{
					data: 'custid',
					name: 'custid',//6
					width: "6%",
				},
				{
					data: 'price_biller',
					name: 'price_biller',//7
					width: "6%",
				},
				{
					data: 'price_reseller',//8
					name: 'price_reseller',
					width: "6%",
			
				},
				{
					data: 'laba',
					name: 'laba',//9
					width: "6%",
			
				},
				{
					data: 'biller',//10
					name: 'biller',
					width: "6%",
			
				},
				{
					data: 'saldoBiller',//11
					name: 'saldoBiller',//12
					width: "6%",
					render: function(data, type) {
						return 'Rp '+numeral(data).format('0,0')
					}
			
				},
				{
					data: 'SN',
					name: 'SN',//13
					width: "6%",
			
				},
			
				{
					data: 'statusText',//14
					name:'statusText',
					width: "6%",
				}
				,
				{
					data: 'action',//15
					name:'Tindakan',
					searchable: false,
					visible: false,
					orderable: false
				},
				{
					data: 'statusOrigin',
					name:'statusOrigin',//16
					visible: false,
						searchable: true,
				},
			],
		
			columnDefs: [
		
		
			],
			select: {
				style: 'single'
			},
		});
	

		setInterval( function () {
			oDataList.ajax.reload(  function ( json ) {
				var element =  document.querySelectorAll('.animate');
				$.each(element, function(i, val) {
					val.classList.add('animated', 'fadeIn');
				})
			}); // user paging is not reset on reload
		}, 2000 );

	</script>