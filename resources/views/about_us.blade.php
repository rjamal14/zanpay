<x-package-header />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>About Us</h2>
			
			</div>
			<div class="col-md-6 col-sm-12 text-right">
			
			</div>
		</div>
		
	</div>


        


	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
						<div class="d-flex" >
							<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							
							</div>
						
						</div>
					
					</div>
					<div class="body">
						
					<!-- form -->
						<form class="form-vertical" enctype="multipart/form-data">
							@csrf
							<div class="card-body">
								<strong>About Us</strong>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Image</label>
									<div class="col-sm-10 ">
										<div id="image_preview_edit1">   
											<img src ="{{$data[0]['image']}}" width="150px;" style="border-radius: 2px;">
										</div>
										<input type="file" id="fileEdit1" name="avatar" >
									</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Content</label>
										<div class="col-sm-10 ">
											<textarea type="text" row="5" id="aboutus" class="summernote form-control" placeholder="Isi Pesan Disini...">{{$data[0]['content']}}</textarea>
										</div>
								</div>
								<hr>
								<strong>Vision</strong>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Image</label>
									<div class="col-sm-10 ">
										<div id="image_preview_edit2">   
											<img src ="{{$data[1]['image']}}" width="150px;" style="border-radius: 2px;">
										</div>
										<input type="file" id="fileEdit2" name="visionImage">
									</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Content</label>
										<div class="col-sm-10 ">
											<textarea type="text" name="visionText" row="5" id="vision" class="form-control summernote" placeholder="Isi Pesan Disini...">{{$data[1]['content']}}</textarea>
										</div>
								</div>
								<hr>
								<strong>Mission</strong>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Image</label>
									<div class="col-sm-10 ">
										<div id="image_preview_edit3">   
											<img src ="{{$data[2]['image']}}" width="150px;" style="border-radius: 2px;">
										</div>
										<input type="file" id="fileEdit3" name="missionImage"  >
									</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Content</label>
										<div class="col-sm-10 ">
											<textarea type="text" row="5" id="mission" name="missionText"  class="form-control summernote" placeholder="Isi Pesan Disini...">{{$data[2]['content']}}</textarea>
										</div>
								</div>
								<hr>
							</div>
								
						
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="" i class="btn btnUpdate btn-info ">Submit</button>
							
							</div>
                 
						</form>
					<!-- end form -->
					</div>
				</div>


			
				
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
 $("#fileEdit1").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);

        };
        img.src = objectUrl;
        preview_image_edit1();
    }
});
function preview_image_edit1() {
      $('#image_preview_edit1').empty();
      $('#image_preview_edit1').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
	$("#fileEdit2").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit2').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit2();
    }
});
function preview_image_edit2() {
      $('#image_preview_edit2').empty();
      $('#image_preview_edit2').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }

	$("#fileEdit3").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit3').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit3();
    }
});
function preview_image_edit3() {
      $('#image_preview_edit3').empty();
      $('#image_preview_edit3').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }

	$( document ).ready(function() {
		$('.summernote').summernote();
		
	});

	var _baseURL = "<?php echo url(''); ?>";


	$(document).on("click", ".btnUpdate", function(e) {
        e.stopImmediatePropagation();
		e.preventDefault();
		// var $aboutusimage = $('#fileEdit1').prop('files')[0];
		// var $visionimage = $('#fileEdit2').prop('files')[0];
		// var $missionimage = $('#fileEdit3').prop('files')[0];

		var property1 = document.getElementById('fileEdit1').files[0];
		var property2 = document.getElementById('fileEdit2').files[0];
		var property3 = document.getElementById('fileEdit3').files[0];
    


        var form_data = new FormData();
        form_data.append("aboutusimage",property1)
        form_data.append("visionimage",property2)
        form_data.append("missionimage",property3)
        form_data.append("aboutus",$('#aboutus').summernote('code'))
        form_data.append("vision",$('#vision').summernote('code'))
        form_data.append("mission",$('#mission').summernote('code'))
		
		$.ajax({
					url: _baseURL + "/about_us",
					method:'POST',
					data:form_data,
					contentType:false,
					cache:false,
					processData:false,
					success: function(d) {
						console.log(d);
						// if (d.status == 200) {
							// location.reload();
						// } else {
						// 	toastr.error(d.message)
						// }
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
						// location.reload();
						toastr.success("Data Berhasil diupdate")
					},
				});
	});



		
		
	
    	

</script>