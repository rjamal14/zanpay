<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Produk</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Produk</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						<ul class="header-dropdown dropdown dropdown-animated scale-left">
							<li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
							<li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another Action</a></li>
									<li><a href="javascript:void(0);">Something else</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">
						<div class="col-lg-6 col-md-12" style="margin-bottom: 10px;">
							<label>Tanggal</label>
							<div class="input-daterange input-group" data-provide="datepicker">
								<input type="text" class="input-sm form-control" name="start">
								<span class="input-group-addon range-to">to</span>
								<input type="text" class="input-sm form-control" name="end">
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-hover js-basic-example dataTable table-custom">
								<thead>
									<tr>
										<th>Status Produk</th>
										<th>Kode Produk</th>
										<th>Nama Produk</th>
										<th>Harga Beli Produk / HPP</th>
										<th>Harga Jual Produk</th>
										<th>Provider / Header Produk</th>
										<th>Poin</th>
										<th>Catatan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-danger">Tidak Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-warning">Gangguan</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
									<tr>
										<td><span class="badge badge-danger">Tidak Aktif</span></td>
										<td>VCR100</td>
										<td>Voucher 100,000</td>
										<td>99,999</td>
										<td>100,500</td>
										<td>Telkomsel</td>
										<td>10</td>
										<td>-</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
