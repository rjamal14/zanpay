<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit News</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
					<div class="card-body">
 						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Icon</label>
							<div class="col-sm-10">
								<div id="image_preview_edit">   
									<img src ="{{$data['image']}}" width="150px;" style="border-radius: 2px;">
								</div>
									<input type="file" id="fileEdit" name="avatar" onclick="preview_image_edit()">
							</div>
                		</div>

						<div class="form-group row">
						<label class="col-sm-2 col-form-label">Kategori</label>
						<div class="col-sm-10">
							<select name="category" class="form-control" required>

							@foreach ($group as $g)
							<option value="{{$g['id']}}" {{$g['id'] == $data['category'] ? "selected":""}}>{{$g['name']}}</option>
							@endforeach
							
							</select>
						</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="title" value="{{$data['title']}}" required>
								<input type="hidden" id="descEdit"class="form-control"  value="{{$data['description']}}" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Deskripsi</label>
							<div class="col-sm-10">
						
								<textarea type="text" row="10" id="summernoteEdit"name="description" class="form-control" placeholder="Enter text here..."></textarea>
							</div>
						</div>
						
                	</div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editNews"  data-id='{{$data["id"]}}' class="btn btn-info editNews">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
	$(function() {
$val = $('#descEdit').val();
$("#summernoteEdit").summernote('code', $val);
	});

                 	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editNews", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateReseller($dataID);
				}
			}, '<span style="color: blue;">Edit Data</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function updateReseller(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/news-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
            	$('#news-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>