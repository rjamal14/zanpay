<meta name="csrf-token" content="{{ csrf_token() }}" />
<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>User Profile</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item">Pages</li>
					<li class="breadcrumb-item active">User Profile</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-4 col-md-12">
				<div class="card profile-header">
					<form  action="{{url('/profile-foto-update/').'/'.$data->id}}" method="post"role="form" id="frmImage" enctype="multipart/form-data">
					@csrf
						<div class="body text-center">
							<div class="profile-image mb-3 image_preview_edit12"><img src="{{$data->foto}}" style="width:200px;"class="rounded-circle" alt=""></div>
							<div>
								<h4 class="mb-0"><strong>{{$data->nama}}</strong></h4>
							</div>
							<br>
							<br>
							<span><input type="file" id="fileEdit" name="avatar"  accept="image/png, image/gif, image/jpeg, image/svg" required></span>
							<div class="mt-3">
							<input type="hidden" id="userId" value="{{$data->id}}" />
							<input class="input100" type="hidden" id="_token" name="_token" placeholder="Email">
								<button type="submit" class="btn btn-outline-secondary">Ubah Foto Profile</button>
							</div>
						</div>
					<form>
				</div>

				<!-- <div class="card">
					<div class="header">
						<h2>Info</h2>
						<ul class="header-dropdown dropdown dropdown-animated scale-left">
							<li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
							<li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another Action</a></li>
									<li><a href="javascript:void(0);">Something else</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">
						<small class="text-muted">Address: </small>
						<p>795 Folsom Ave, Suite 600 San Francisco, 94107</p>
						<div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1923731.7533500232!2d-120.39098936853455!3d37.63767091877441!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1522391841133" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<hr>
						<small class="text-muted">Email address: </small>
						<p>michael@gmail.com</p>
						<hr>
						<small class="text-muted">Mobile: </small>
						<p>+ 202-555-2828</p>
						<hr>
						<small class="text-muted">Birth Date: </small>
						<p class="m-b-0">October 22th, 1990</p>
						<hr>
						<small class="text-muted">Social: </small>
						<p><i class="fa fa-twitter m-r-5"></i> twitter.com/example</p>
						<p><i class="fa fa-facebook  m-r-5"></i> facebook.com/example</p>
						<p><i class="fa fa-github m-r-5"></i> github.com/example</p>
						<p><i class="fa fa-instagram m-r-5"></i> instagram.com/example</p>
					</div>
				</div> -->

				<!-- <div class="card">
					<div class="header bline">
						<h2>Who to follow</h2>
					</div>
					<div class="body">
						<ul class="right_chat list-unstyled mb-0">
							<li>
								<a href="javascript:void(0);">
									<div class="media">
										<img class="media-object " src="images/xs/avatar4.jpg" alt="">
										<div class="media-body">
											<span class="name">Chris Fox</span>
											<span class="message">Designer, Blogger</span>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="media">
										<img class="media-object " src="images/xs/avatar5.jpg" alt="">
										<div class="media-body">
											<span class="name">Joge Lucky</span>
											<span class="message">Java Developer</span>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="media">
										<img class="media-object " src="images/xs/avatar2.jpg" alt="">
										<div class="media-body">
											<span class="name">Isabella</span>
											<span class="message">CEO, Wraptheme</span>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="media">
										<img class="media-object " src="images/xs/avatar1.jpg" alt="">
										<div class="media-body">
											<span class="name">Folisise Chosielie</span>
											<span class="message">Art director, Movie Cut</span>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="media">
										<img class="media-object " src="images/xs/avatar3.jpg" alt="">
										<div class="media-body">
											<span class="name">Alexander</span>
											<span class="message">Writter, Mag Editor</span>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div> -->

			</div>

			<div class="col-lg-8 col-md-12">
				<div class="card">
					<div class="body">
						<ul class="nav nav-tabs-new">
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Overview">Overview</a></li>
							<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Settings">Settings</a></li>
						</ul>
					</div>
				</div>
				<div class="tab-content padding-0">
					<div class="tab-pane blog-page " id="Overview">
						<div class="card">
							<div class="body">
								<div class="row profile_state">
									<div class="col-lg-3 col-md-6">
										<i class="fa fa-camera"></i>
										<h5 class="m-b-0 number count-to" data-from="0" data-to="2365" data-speed="1000" data-fresh-interval="700">2365</h5>
										<small>Shots View</small>
									</div>
									<div class="col-lg-3 col-md-6">
										<i class="fa fa-thumbs-o-up"></i>
										<h5 class="m-b-0 number count-to" data-from="0" data-to="1203" data-speed="1000" data-fresh-interval="700">1203</h5>
										<small>Likes</small>
									</div>
									<div class="col-lg-3 col-md-6">
										<i class="fa fa-comments-o"></i>
										<h5 class="m-b-0 number count-to" data-from="0" data-to="324" data-speed="1000" data-fresh-interval="700">324</h5>
										<small>Comments</small>
									</div>
									<div class="col-lg-3 col-md-6">
										<i class="fa fa-user"></i>
										<h5 class="m-b-0 number count-to" data-from="0" data-to="1980" data-speed="1000" data-fresh-interval="700">1980</h5>
										<small>Profile Views</small>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="body">
								<div class="summernote">
									<h3 class="m-b-0">hi,</h3>
									<h4 class="m-t-0">we are Summernote</h4>
									<p></p>
								</div>
								<button class="btn btn-primary">Post</button>
							</div>
						</div>
						<div class="card single_post2">
							<img class="img-fluid" src="images/blog/blog-page-1.jpg" alt="img">
							<div class="body">
								<div class="content">
									<div class="actions_sidebar">
										<a href="javascript:void(0)"><i class="icon-share"></i></a>
										<a href="javascript:void(0)"><i class="icon-heart"></i> <span>5</span></a>
										<a href="javascript:void(0)"><i class="icon-bubble"></i> <span>8</span></a>
									</div>
									<h4 class="title">All photographs are accurate</h4>
									<p class="date">
										<small>July 27, 2018</small>
									</p>
									<p class="text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</p>
									<a class="btn btn-primary" href="javascript:void(0)">READ MORE</a>
								</div>
							</div>
						</div>
						<div class="card single_post2">
							<img class="img-fluid" src="images/blog/blog-page-2.jpg" alt="img">
							<div class="body">
								<div class="content">
									<div class="actions_sidebar">
										<a href="javascript:void(0)"><i class="icon-share"></i></a>
										<a href="javascript:void(0)"><i class="icon-heart"></i> <span>5</span></a>
										<a href="javascript:void(0)"><i class="icon-bubble"></i> <span>8</span></a>
									</div>
									<h4 class="title">All photographs are accurate</h4>
									<p class="date">
										<small>Aug 15, 2018</small>
									</p>
									<p class="text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</p>
									<a class="btn btn-primary" href="javascript:void(0)">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane active" id="Settings">

						<div class="card">
							<div class="header bline">
								<h2>Informasi Dasar</h2>
								
							</div>
							<div class="body">
								<div class="row clearfix">
									<div class="col-lg-12">
										<div class="form-group">
											<input type="text" class="form-control" id="nama" required value="{{$data->nama}}" name="nama" placeholder="Nama">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<input type="text" class="form-control" disabled  value="{{$data->email}}" placeholder="email">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<input type="text" class="form-control"  required name="phone" id="phone" value="{{$data->phone}}"placeholder="telepon">
										</div>
									</div>
									
								</div>
								<button type="button" class="btn btn-primary btnUpdate">Update</button> &nbsp;&nbsp;
								<button type="button" class="btn btn-default btnCancel">Cancel</button>
							</div>
						</div>

						<div class="card">
							<div class="header bline">
							<h6>Ganti Password</h6>
							</div>
							<form role="form" id="frmPassword" enctype="multipart/form-data">
								@csrf
							<div class="body">
								<div class="row clearfix">
								
							
									<div class="col-lg-12 col-md-12">
										
										<div class="form-group">
											<input type="password" class="form-control" id="cpassword" name='currentPassword'placeholder="">
										</div>
										<div class="form-group">
											<input type="password" class="form-control"name='newPassword' id="nPassword"  placeholder="Password Baru">
										</div>
										<div class="form-group">
											<input type="password" class="form-control" id="nPasswordC" name='newPassword_confirmation' placeholder="Konfirmasi Password Baru">
										</div>
									</div>
								</div>
								<button type="button" class="btn btn-primary btnUpdate1">Update</button> &nbsp;&nbsp;
								<button type="button" class="btn btn-default btnCancel1">Cancel</button>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
		var _baseURL = "<?php echo url(''); ?>";
	 $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('.image_preview_edit12').empty();
      $('.image_preview_edit12').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' class='rounded-circle'  style='width:200px;'> &nbsp");
    }



	$(document).on("click", ".btnUpdate", function(e) {
        e.stopImmediatePropagation();
		e.preventDefault();
        $dataID = $("#userId").val();
			showConfirm('Apakah anda Yakin ? ', function(result) {
				if (result) {
				updateProfile($dataID);
				}
			}, '<span style="color: blue;">Edit Profile</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
	  function updateProfile(idProduct) {
	

				var form = $("#frmProfile1")[0];
				var formData = new FormData(form);

				console.log($("#frmProfile1").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/profile-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: {
						nama: $('#nama').val(),
						phone: $('#phone').val(),
						_token: $('#_token').val()
					},
					processData: true,
					success: function(d) {
						console.log(d);
						if (d.status == 200) {
              					toastr.success(d.message)
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;

		}
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});
	$(document).on("click", ".btnUpdate1", function(e) {
	
        e.stopImmediatePropagation();
		e.preventDefault();
	
        $dataID = $("#userId").val();
			showConfirm('Apakah anda Yakin ? ', function(result) {
				if (result) {
				updatePhoto($dataID);
				}
			}, '<span style="color: blue;">Edit Profile</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
	  function updatePhoto(idProduct) {
	
		
				var form = $("#frmPassword")[0];
				$.ajax({
					url: _baseURL + "/profile-password-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: {
						'password' :  $("#cPassword").val(),
						'newPassword' :  $("#nPassword").val(),
						'newPasswordConfirmation' :  $("#nPasswordC").val(),
					},
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(d);
						if (d.status == 200) {
              					toastr.success(d.message)
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});
			}
				return false;

		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});
	
</script>