<x-package-header />
<style>
.width-200{
    width:200px;
}
.width-500{
    width:500px;
}
	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>FAQ</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<button id="addBtn" class="btn btn-sm btn-primary mr-1">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
		
	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
          <br>
          <center><i class="fa fa-spinner fa-spin"></i></center>
        </div>

        <div class="card-body" id="addForm" style="display: none;">
          	<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Tambah FAQ</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
			  @csrf
                <div class="card-body">
				<div class="form-group row">
					
                </div>
				
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pertanyaan</label>
                    <div class="col-sm-10">
						<textarea type="text" class="form-control" name="title" required></textarea>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jawaban</label>
                    <div class="col-sm-10">
						<textarea type="text" class="form-control" name="content" required></textarea>
                    </div>
                </div>
				
             
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="addFaqs"   class="btn btn-info addFaqs">Submit</button>
                  <button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
        	</div>
        </div>

        <div  class="card-body" id="editForm"></div>


	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
					</div>
					<div class="body">
						<div class="table-responsive">
							<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom faqs_table"  id="faqs_table">
								<thead>
									<tr>
										<th>No</th>
										<th>Pertanyaan</th>
										<th>Jawaban</th>
										<th></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>	
	$(document).on("click", ".img-init2", function() {
        $(".product_img2").click();
    });
    $(document).on("change", ".product_img2", function() {
        readURL2(this);
    });
    $(document).on("click", ".img-delete2", function() {
        $isImgEditable = false;
        $(".image-container-product2").removeClass("image-exist");
        $(".product_img2").val("");
        $(".image-container-product2").find("img").remove();
    });

	var _URL = window.URL || window.webkitURL;
	
	$("#file").change(function (e) {
    var file, img;
	if ((file = this.files[0])) 
		{
			img = new Image();
			var objectUrl = _URL.createObjectURL(file);
			img.onload = function () {
			
				_URL.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : '+this.width + " x " + this.height);
				$('#reomencedDimension').text(' Current Dimension : '+this.width + " x " + this.height);
			
				$('#file').removeClass('is-invalid');
				$('#btnCreateEvent').prop('disabled', false);
				$('#reomencedDimension').removeClass('error');
			};
			img.src = objectUrl;
			preview_image();
		}
	});
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if ($(".image-container-product2 img").length != 0) {
                    $(".image-container-product2 img").attr(
                        "src",
                        e.target.result
                    );
                } else {
                    $(".image-container-product2").append(
                        '<img src="' + e.target.result + '">'
                    );
                }
                $(".image-container-product2").addClass("image-exist");
                $isImgEditable = true;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	function editData(id,url ="{{url("/faqs-edit/")}}")
	{
		$.ajax({
			url:url+'/'+id,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
	
			},
			success:function(data){
				$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url+'/'+id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
		});	
	}
	function preview_image() {
		$('#image_preview').empty();
		$('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
	}
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
		{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
		}
		var _baseURL = "<?php echo url(''); ?>";
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
					if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo( element.parent().parent().parent().parent() );
					}
					else {
						error.appendTo( element.parent().parent().parent().parent().parent() );
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo( element.parent().parent().parent() );
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo( element.parent() );
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo( element.parent().parent() );
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo( element.parent().parent() );
				}

				else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"

		
		};
	

		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"delete-faqs", [$id],
				true
			);
			return false;
		}
		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini'){
			if($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if(result) {
							console.log(url)
							$.ajax({
								url: url+'/'+$ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if(d.status == 200) {
									
										$('#faqs_table').DataTable().ajax.reload();
										toastr.success("Faqs berhasil dihapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function () {
								},
								complete: function () {
								}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}	
	$(function() {
    	var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		var oDataList = $('#faqs_table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
		
			"bAutoWidth" : false,
			"lengthChange" : false,
			ajax: {
				url: "{{ url('faqs-list') }}",
				data: {
                  
             
                },
			},
		    "fnDrawCallback": function( data ,e) {
			
							
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
				
				{
					data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
					orderable: false},
				{
					data: 'title',
					name: 'title',
					orderable: false
			
				},
				{
					data: 'content',
					name: 'content',
					orderable: false
			
				},
			
				{
					data: 'action',
					name:'Tindakan',
					searchable: false,
					orderable: false
				},
				//13
			
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Faqs Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
				{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-200'>" + data + "</div>";
						},
						targets: 1
				},
				{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-500'>" + data + "</div>";
						},
						targets: 2
				},
			],
			select: {
				style: 'single'
			},
		});
		$('.btnEdit').click(function(){
				$('#addForm').fadeOut();
		});
		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
		{
			if (!$buttons) {
				$buttons = {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				};
			}
			bootbox.confirm({
				title: $title,
				message: $message,
				buttons: $buttons,
				callback: $callback
			});
		}
	
		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});
		$('#addBtn').click(function(){
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
     		$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
		});
		$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});
		$(document).on("click", ".addFaqs", function(e) {
			e.preventDefault();
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);
			if ($("#frmCreate").valid()) {
				showConfirm('Apakah anda Yakin ingin menambahkan Faqs ? ', function(result) {
					if (result) {
						addFaqs();
					}
				}, '<span style="color: blue;">Tambah Faqs</span>', {
					confirm: {
						label: 'Tambah',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			} else {
				return ;
			}
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
	


		function addFaqs() 
		{
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);
			if ($("#frmCreate").valid()) {
				var form = $("#frmCreate")[0];
				var formData = new FormData(form);
				console.log($("#frmCreate").serialize());
				$.ajax({
					url: _baseURL + "/add-faqs",
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
							$('#faqs_table').DataTable().ajax.reload();
							$('#addForm').fadeOut();
							// $('#addForm').empty();
							$('#addBtn').fadeIn();
							toastr.success("Data Berhasil ditambahkan")
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						toastr.error("Mohon cek data kembali")
					},
					beforeSend: function() {
					},
					complete: function() {
					},
				});
				return false;
			}
		}

		

	
	});

</script>