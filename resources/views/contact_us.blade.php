<x-package-header />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Contact Us</h2>
			
			</div>
			<div class="col-md-6 col-sm-12 text-right">
			
			</div>
		</div>
		
	</div>


        


	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
						<div class="d-flex" >
							<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							</div>
						
						</div>
					
					</div>
					<div class="body">
						
					<!-- form -->
						<form class="form-vertical" id="frmCreate" enctype="multipart/form-data">
							@csrf
							<div class="card-body">
								
								
					
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Phone & Fax</label>
										<div class="col-sm-10 ">
											<textarea id="phone" class="summernote" name="phone">{!!$data->phone!!}</textarea>
										</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Address</label>
										<div class="col-sm-10 ">
											<textarea id="address" class="summernote" name="address">{!!$data->address!!}</textarea>
										</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Email</label>
										<div class="col-sm-10 ">
											<textarea id="email" class="summernote" name="email">{!!$data->email!!}</textarea>
										</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">Latitude</label>
										<div class="col-sm-10 ">
											<input type="text" class="form-control" value='{{$data["lat"]}}' id="lat"  name="lat" required>
										</div>
								</div>
								<div class="form-group row">
										<label class="col-sm-2 col-form-label">longitude</label>
										<div class="col-sm-10 ">
											<input type="text" class="form-control" value='{{$data["lng"]}}'  name="lng" id="lng" required>
										</div>
								</div>




							</div>
								
						
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="" i class="btn btnUpdate btn-info ">Submit</button>
							
							</div>
                 
						</form>
					<!-- end form -->
					</div>
				</div>


			
				
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

	$( document ).ready(function() {
		$('.summernote').summernote();
		
	});

	var _baseURL = "<?php echo url(''); ?>";


	$(document).on("click", ".btnUpdate", function(e) {
		e.stopImmediatePropagation();
		e.preventDefault();
	

				$.ajax({
					url: _baseURL + "/contact_us",
					type: "post",
					dataType: "json",
					data: {
						email: $('#email').summernote('code'),
						phone: $('#phone').summernote('code'),
						address: $('#address').summernote('code'),
						lng: $('#lng').val(),
						lat: $('#lat').val(),
						"_token": "{{ csrf_token() }}",
					},
					success: function(d) {
						console.log(d);
						// if (d.status == 200) {
							// location.reload();
						// } else {
						// 	toastr.error(d.message)
						// }
						toastr.success("Data Berhasil diupdate")
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
						// location.reload();
					},
				});
				return false;
	});



		
		
	
    	

</script>