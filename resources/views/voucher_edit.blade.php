    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Edit Voucher</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			  @csrf
			  <div class="card-body">
				
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama Voucher</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" value="{{$data['title']}}" name="title" required>
                    </div>
                </div>

				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Group</label>
                      <div class="col-sm-10">
                        <select name="group" id="groupActivityEdit" class="voucherCodeEdit form-control" required>
						<option  data-id=" " value="">-- Pilih Group --</option>
						@foreach ($activityGroup as $g)
						<option  data-id="{{$g['text']}}" value="{{$g['id']}}" {{$data['activity_group'] == $g['id'] ? "selected='selected'":""}} >{{$g['name']}}</option>
                        @endforeach
						</select>
                      </div>
                </div>
				@if($data['activity_group'] == 0)
					<div class="form-group row AktivitasEdit">
						<label class="col-sm-2 col-form-label"> Aktivitas</label>
						<div class="col-sm-10">
							<select name="activityEdit" id="activityEdit" class="voucherCodeEdit form-control">

							@foreach ($payment as $p)
							<option  data-id="{{$p['name']}}" value="{{$p['id']}}"  data-txt="{{$p['name']}}" {{$data['activity'] == $p['id'] ? "selected='selected'":""}} >{{$p['text']}}</option>
							@endforeach
							
							</select>
						</div>
					</div>
					<div class="form-group row JnisEdit" style="display:none;">
						<label class="col-sm-2 col-form-label"> Aktivitas</label>
						<div class="col-sm-10">
							<select name="jenis" id="jnisEdit" class="voucherCodeEdit form-control">

							
							</select>
						</div>
					</div>
					<div class="form-group row PoviderEdit" style="display:none;">
						<label class="col-sm-2 col-form-label">Provider</label>
						<div class="col-sm-10">
							<select name="provider" id="poviderEdit" class="voucherCodeEdit form-control">

							
							</select>
						</div>
					</div>
					<div class="form-group row RefrensiIDEdit" style="">
						<label class="col-sm-2 col-form-label">Refrensi ID</label>
						<div class="col-sm-10">
							<select name="reference_id[]"  multiple="multiple" width="100%" id="RefrensiIDEdit" class="js-example-basic-multiple voucherCodeEdit form-control">
								
							@foreach ($ref as $r)
							<option  data-id="{{$r['name']}}" value="{{$r['id']}}"  data-txt="{{$r['name']}}" {{in_array( $r['id'], $refData) ? "selected='selected'":""}} >{{$r['text']}}</option>
							@endforeach
							
							</select>
						</div>
					</div>
				@elseif($data['activity_group'] == 1)
				<div class="form-group row AktivitasEdit"  style="display:none;">
                      <label class="col-sm-2 col-form-label">Aktivitas</label>
                      <div class="col-sm-10">
                        <select name="activityEdit" id="activityEdit" class="voucherCodeEdit form-control">

                        
						</select>
                      </div>
                </div>
				<div class="form-group row JnisEdit">
                      <label class="col-sm-2 col-form-label"> Aktivitas</label>
                      <div class="col-sm-10">
                        <select name="jenis" id="jnisEdit" class="voucherCodeEdit form-control">

							
							@foreach ($payment2 as $p2)
							<option  data-id="{{$p2['name']}}" value="{{$p2['id']}}"  data-txt="{{$p2['name']}}" {{$hKelp['command'] == $p2['text'] ? "selected='selected'":""}} >{{$p2['text']}}</option>
							@endforeach
                        
						</select>
                      </div>
                </div>
				<div class="form-group row PoviderEdit">
                      <label class="col-sm-2 col-form-label">Provider</label>
                      <div class="col-sm-10">
                        <select name="provider" id="poviderEdit" class="voucherCodeEdit form-control">
							@foreach ($provider as $pv2)
							<option  data-id="{{$pv2['name']}}" value="{{$pv2['id']}}"  data-txt="{{$pv2['name']}}" {{$data['activity'] == $pv2['id'] ? "selected='selected'":""}} >{{$pv2['text']}}</option>
							@endforeach
                        
						</select>
                      </div>
                </div>

				<div class="form-group row RefrensiIDEdit" >
                      <label class="col-sm-2 col-form-label">Refrensi ID</label>
                      <div class="col-sm-10">
                        <select name="reference_id[]"  multiple="multiple" width="100%" id="RefrensiIDEdit" class="js-example-basic-multiple voucherCodeEdit form-control">
							
						@foreach ($refData2 as $r)
						<option  data-id="{{$r['name']}}" value="{{$r['id']}}"  data-txt="{{$r['name']}}" {{in_array( $r['id'], $refData) ? "selected='selected'":""}} >{{$r['text']}}</option>
                        @endforeach
                        
						</select>
                      </div>
                </div>
				@else
				<div class="form-group row AktivitasEdit"  style="display:none;">
                      <label class="col-sm-2 col-form-label"> Aktivitas</label>
                      <div class="col-sm-10">
                        <select name="activityEdit" id="activityEdit" class="voucherCodeEdit form-control">

                        
						</select>
                      </div>
                </div>
				<div class="form-group row JnisEdit" style="display:none;">
                      <label class="col-sm-2 col-form-label"> Aktivitas</label>
                      <div class="col-sm-10">
                        <select name="jenis" id="jnisEdit" class="voucherCodeEdit form-control">

                        
						</select>
                      </div>
                </div>
				<div class="form-group row PoviderEdit" style="display:none;">
                      <label class="col-sm-2 col-form-label">Provider</label>
                      <div class="col-sm-10">
                        <select name="provider" id="poviderEdit" class="voucherCodeEdit form-control">
						
                        
						</select>
                      </div>
                </div>
				
				<div class="form-group row RefrensiIDEdit" style="display:none;">
                      <label class="col-sm-2 col-form-label">Refrensi ID</label>
                      <div class="col-sm-10">
                        <select name="reference_id[]"  multiple="multiple" width="100%" id="RefrensiIDEdit" class="js-example-basic-multiple voucherCodeEdit form-control">
				
                        
						</select>
                      </div>
                </div>
				@endif

			

				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" value="{{$data['description']}}"name="description" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Syarat</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control"  value="{{$data['requirement']}}" name="requirement" required>
                    </div>
                </div>
				

				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Tipe Voucher</label>
                      <div class="col-sm-10">
                        <select name="type" id="voucherTypeEdit"  class="voucherCodeEdit form-control" required>

						@foreach ($tipe as $ti)
						<option data-id="{{$ti['text']}}" value="{{$ti['id']}}" {{$ti['id'] == $data['type'] ? "selected='selected'" : ""}}>{{$ti['name']}}</option>
                        @endforeach
                        
						</select>
                      </div>
                </div>
				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Tipe Reward</label>
                      <div class="col-sm-10">
                        <select name="reward_type" id="reward_type" id="rewardType" class="form-control" required>

						@foreach ($rewardType as $rw)
						<option value="{{$rw['id']}}" {{$rw['id'] == $data['reward_type'] ? "selected='selected'" : ""}}>{{$rw['name']}}</option>
                        @endforeach
                        
						</select>
                      </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jumlah Reward</label>
                    <div class="col-sm-10">
						<input type="number" class="voucherCodeEdit form-control" value="{{$data['reward']}}" id="reward" name="reward" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Maksimal</label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" value="{{$data['maximum']}}" name="maximum" required>
                    </div>
                </div>
		
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Quota </label>
                    <div class="col-sm-10">
						<input type="number" class="form-control" value="{{$data['quota']}}" name="quota" required>
                    </div>
                </div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Pemakaian </label>
					<div class="col-sm-10">
						<select name="multiple"  id="pemakaianIDEdit" class="form-control" required>
						
						<option value="0" {{$data['multiple'] == 0 ? "selected='selected'" : ""}} >Hanya Sekali</option>
						<option value="1" {{$data['multiple'] == 1 ? "selected='selected'" : ""}}>Berulang Kali</option>
						</select>
					</div>
				</div>
				@if($data['multiple'] == 0)
				<div id='periodIDEdit' style="display:none;">
					<div class="form-group row" >
						<label class="col-sm-2 col-form-label">Periode</label>
						<div class="col-sm-10">
							<select name="periode" id="mIDEdit" class=" form-control" required>
							<option value="1" >Harian</option>
							<option value="2" >Mingguan</option>
							<option value="3" >Bulanan</option>
							</select>
						</div>
					</div>
					<div class="form-group row" >
						<label class="col-sm-2 col-form-label">Kuantitas</label>
						<div class="col-sm-10">
							<input type="number" maxlength="3" value="{{$data['many_times']}}" class="form-control" id="KuantitasIDEdit" name="many_times" required>
						</div>
					</div>
				</div>
				@else 
				<div id='periodIDEdit'>
					<div class="form-group row" >
						<label class="col-sm-2 col-form-label">Periode</label>
						<div class="col-sm-10">
							<select name="periode" id="mIDEdit" class=" form-control" required>
							<option value="1" {{$data['periode'] == 1 ? "selected='selected'" : ""}}>Harian</option>
							<option value="2" {{$data['periode'] == 2 ? "selected='selected'" : ""}}>Mingguan</option>
							<option value="3" {{$data['periode'] == 3 ? "selected='selected'" : ""}}>Bulanan</option>
							</select>
						</div>
					</div>
					<div class="form-group row" >
						<label class="col-sm-2 col-form-label">Kuantitas</label>
						<div class="col-sm-10">
							<input type="number" maxlength="3" value="{{$data['many_times']}}" class="form-control" id="KuantitasIDEdit" name="many_times" required>
						</div>
					</div>
				</div>
				@endif
			

				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tanggal Berlaku </label>
                    <div class="col-sm-2">
						<input type="date" class="form-control" placeholder="dd-mm-yyyy" value="{{date('Y-m-d',strtotime($data['created_at']))}}"
        min="1997-01-01" max="2030-12-31" name="created_at" required>
		<!-- <span class="flex"style="display:block;">&nbsp;s/d&nbsp;</span> -->
                    </div>
					<label style ="margin-left:-20px" class="col-sm-1 text-center col-form-label">s/d</label>
                    <div class="col-sm-2" style ="margin-left:-20px">
						<input type="date" class="form-control" placeholder="dd-mm-yyyy"  value="{{date('Y-m-d',strtotime($data['expired_at']))}}"
        min="1997-01-01" max="2030-12-31" name="expired_at" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kode Voucher </label>
                    <div class="col-sm-10">
						<input type="text"  class="form-control"  value="{{$data['code']}}"  id="voucherCodeEdit" name="code" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status Voucher </label>
                    <div class="col-sm-10">
						<select name="status" id="statusIDD" class=" form-control" required>
							<option value="1" {{$data['status'] == 1 ? "selected='selected'" : ""}}>Aktif</option>
							<option value="0" {{$data['status'] == 0 ? "selected='selected'" : ""}}>Tidak Aktif</option>
						</select>
                    </div>
                </div>
				

             
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="editVoucher"  data-id="{{$data['id']}}" class="btn btn-info editVoucher">Submit</button>
                  <button id="cancelBtnEdit" type="button" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
        	</div>
<script>
	
	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on("change", "#pemakaianIDEdit", function() {
		if($('#pemakaianIDEdit').children('option:selected').val()== 1){
		$("#periodIDEdit").show();
		$('#mIDEdit').prop('required', true);
		$('#KuantitasIDEdit').prop('required', true);
		} else {
			$("#periodIDEdit").hide();
		$('#mIDEdit').prop('required', false);
		$('#KuantitasIDEdit').prop('required', false);
		}
	
	});
$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
		"width":'100%'

	});


	reference_id
	var dataref = $('#RefrensiIDEdit').val();
	$.ajax({
		url:'{{url("/voucher-combobox-byref?")}}type='+$('#poviderEdit').children('option:selected').val()+'&group='+$('#groupActivityEdit').children('option:selected').val(),
		type:'GET',
		dataType:'json',
		beforeSend:function(){

		},
		success:function(data){
			console.log(data.data);
			var appendsEdit='<option data-id="" value="" disabled>-- Pilih --</option>';
			if(data.data.length > 0){
				$.each(data.data, function (i,val) {
					if(dataref.includes(val.id.toString())) {
						appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'" selected="selected">'+ val.text +'</option>';
					}else{
						appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';
					}

				});

				$("#RefrensiIDEdit").empty();
				$("#RefrensiIDEdit").append(appendsEdit);
			}
		},

	});



});



	$(document).on("change blur", ".voucherCodeEdit", function() {
		$type = $('#voucherTypeEdit').children('option:selected').data('id');
		$gc = $('#groupActivityEdit').children('option:selected').data('id');
		if($('#groupActivityEdit').children('option:selected').val() === "0"){
			$ac = $('#activityEdit').children('option:selected').data('txt');
		} else if($('#groupActivityEdit').children('option:selected').val() === "1"){ 
			let c = $('#poviderEdit').children('option:selected').data('txt')
			$ac = $('#jnisEdit').children('option:selected').data('txt');
		} else {
			$ac="";
		}
		if( $('#reward').val().length == 1){
		$rwd ="0"+$('#reward').val();
		}else{

		$rwd = $('#reward').val();
		}
		console.log($type,$gc,$ac,$rwd);
		// let $code ="V"+$type+$gc+$ac+$rwd;
		let $code ="V"+$type+$gc+$rwd;
        $("#voucherCodeEdit").val($code);
    });
	$(document).on("change", "#activityEdit", function() {
		$("#poviderEdit").empty();
		$("#jnisEdit").empty();
		$("#RefrensiIDEdit").empty();
		$thisId =$('#activityEdit').children('option:selected').val();
		$.ajax({
				url:'{{url("/voucher-combobox-byref?")}}type='+$thisId+'&group='+$('#groupActivityEdit').children('option:selected').val(),
				type:'GET',
				dataType:'json',
				beforeSend:function(){

				},
				success:function(data){
					console.log(data.data);
					var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

					$("#RefrensiIDEdit").empty();
					if(data.data.length > 0){
						$.each(data.data, function (i,val) {
							appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

						});
					$("#RefrensiIDEdit").append(appendsEdit);
					}
				},

		});	
	});
	$(document).on("change", "#poviderEdit", function() {
	
		$("#RefrensiIDEdit").empty();
		$.ajax({
				url:'{{url("/voucher-combobox-byref?")}}type='+$('#poviderEdit').children('option:selected').val()+'&group='+$('#groupActivityEdit').children('option:selected').val(),
				type:'GET',
				dataType:'json',
				beforeSend:function(){

				},
				success:function(data){
					console.log(data.data);
					var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

					$("#RefrensiIDEdit").empty();
					if(data.data.length > 0){
						$.each(data.data, function (i,val) {
							appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

						});
					$("#RefrensiIDEdit").append(appendsEdit);
					}
				},

		});	
	});

	$(document).on("change", "#groupActivityEdit", function() {
		$("#poviderEdit").empty();
		$("#jnisEdit").empty();
		$("#RefrensiIDEdit").empty();
		$("#activityEdit").empty();
		$thisId = $("#groupActivityEdit").children('option:selected').val();
		if ($thisId === "0" || $thisId === "1") {
			//	
			$('.RefrensiIDEdit').show();
			if ($thisId === "0") {
				$('#activityEdit').prop('required', true);
				$('#jnisEdit').prop('required', false);
				$('#poviderEdit').prop('required', false);
				$('.AktivitasEdit').show();
				$('.jnisEdit').hide();
				$('.PoviderEdit').hide();
				$.ajax({
					url: '{{url("/voucher-combobox-bygroup?")}}type=' + $thisId,
					type: 'GET',
					dataType: 'json',
					beforeSend: function() {

					},
					success: function(data) {
						console.log(data.data);
						var appendsEdit = '<option data-id="" value="">-- Pilih --</option>';

						$("#activityEdit").empty();
						if (data.data.length > 0) {
							$.each(data.data, function(i, val) {
								appendsEdit += '<option data-id="' + val.id + '" data-txt="' + val.name + '" value="' + val.id + '">' + val.text + '</option>';

							});
							$("#activityEdit").append(appendsEdit);
						}
					},

				});
				// $.ajax({
				// 	url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
				// 	type:'GET',
				// 	dataType:'json',
				// 	beforeSend:function(){

				// 	},
				// 	success:function(data){
				// 		console.log(data.data);
				// 		var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

				// 		$("#RefrensiIDEdit").empty();
				// 		if(data.data.length > 0){
				// 			$.each(data.data, function (i,val) {
				// 				appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

				// 			});
				// 		$("#RefrensiIDEdit").append(appendsEdit);
				// 		}
				// 	},

				// });	
			}
			if ($thisId === "1") {
				$('#activityEdit').prop('required', false);
				$('#jnisEdit').prop('required', true);
				$('#poviderEdit').prop('required', true);
				$('.AktivitasEdit').hide();
				$('.jnisEdit').show();
				$('.PoviderEdit').show();
				$.ajax({
					url: '{{url("/voucher-combobox-bygroup?")}}type=' + $thisId,
					type: 'GET',
					dataType: 'json',
					beforeSend: function() {

					},
					success: function(data) {
						console.log(data.data);
						var appendsEdit = '<option data-id="" value="">-- Pilih --</option>';

						$("#jnisEdit").empty();
						if (data.data.length > 0) {
							$.each(data.data, function(i, val) {
								appendsEdit += '<option  data-val="' + val.text + '" data-txt="' + val.name + '" data-id="' + val.id + '" value="' + val.id + '">' + val.text + '</option>';

							});
							$("#jnisEdit").append(appendsEdit);
						}
					},

				});
				// $.ajax({
				// 	url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
				// 	type:'GET',
				// 	dataType:'json',
				// 	beforeSend:function(){

				// 	},
				// 	success:function(data){
				// 		console.log(data.data);
				// 		var appendsEdit='<option data-id="" value="">-- Pilih --</option>';
				// 		$("#RefrensiIDEdit").empty();
				// 		if(data.data.length > 0){
				// 			$.each(data.data, function (i,val) {
				// 				appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

				// 			});
				// 		$("#RefrensiIDEdit").append(appendsEdit);
				// 		}
				// 	},

				// });	
			} else {
				$('#activityEdit').prop('required', false);
				$('#jnisEdit').prop('required', false);
				$('#poviderEdit').prop('required', false);
			}
			//


		} else {
			$('.AktivitasEdit').hide();
			$('.RefrensiIDEdit').hide();
			$('.jnisEdit').hide();
			$('.PoviderEdit').hide();
		}



	});
	// $(document).on("change", "#groupActivityEdit", function() {
	
	// 	console.log($(this).children('option:selected').val());
	// 	$thisId = $(this).children('option:selected').val();
	// 	if ($thisId === "0" || $thisId === "1"){
	// 			//	
	// 		$('.RefrensiIDEdit').show();
	// 			if($thisId === "0") {
	// 				$('#activityEdit').prop('required',true);
	// 				$('#jnisEdit').prop('required',false);
	// 				$('#poviderEdit').prop('required',false);
	// 				$('.AktivitasEdit').show();
	// 				$('.JnisEdit').hide();
	// 				$('.PoviderEdit').hide();
	// 				$.ajax({
	// 					url:'{{url("/voucher-combobox-bygroup?")}}type='+$thisId,
	// 					type:'GET',
	// 					dataType:'json',
	// 					beforeSend:function(){
						
	// 					},
	// 					success:function(data){
	// 						console.log(data.data);
	// 						var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

	// 						$("#activityEdit").empty();
	// 						if(data.data.length > 0){
	// 							$.each(data.data, function (i,val) {
	// 								appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';
							
	// 							});
	// 						$("#activityEdit").append(appendsEdit);
	// 						}
	// 					},
						
	// 				});	
	// 				$.ajax({
	// 					url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
	// 					type:'GET',
	// 					dataType:'json',
	// 					beforeSend:function(){
						
	// 					},
	// 					success:function(data){
	// 						console.log(data.data);
	// 						var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

	// 						$("#RefrensiIDEdit").empty();
	// 						if(data.data.length > 0){
	// 							$.each(data.data, function (i,val) {
	// 								appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';
							
	// 							});
	// 						$("#RefrensiIDEdit").append(appendsEdit);
	// 						}
	// 					},
						
	// 				});	
	// 			} 
	// 			if($thisId === "1"){
	// 				$('#activityEdit').prop('required',false);
	// 				$('#jnisEdit').prop('required',true);
	// 				$('#poviderEdit').prop('required',true);
	// 				$('.AktivitasEdit').hide();
	// 				$('.JnisEdit').show();
	// 				$('.PoviderEdit').show();
	// 				$.ajax({
	// 					url:'{{url("/voucher-combobox-bygroup?")}}type='+$thisId,
	// 					type:'GET',
	// 					dataType:'json',
	// 					beforeSend:function(){
						
	// 					},
	// 					success:function(data){
	// 						console.log(data.data);
	// 						var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

	// 						$("#jnisEdit").empty();
	// 						if(data.data.length > 0){
	// 							$.each(data.data, function (i,val) {
	// 								appendsEdit+='<option  data-val="'+val.text+'" data-txt="'+val.name+'" data-id="'+val.id+'" value="'+val.id+'">'+ val.text +'</option>';
							
	// 							});
	// 						$("#jnisEdit").append(appendsEdit);
	// 						}
	// 					},
						
	// 				});	
	// 				$.ajax({
	// 					url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
	// 					type:'GET',
	// 					dataType:'json',
	// 					beforeSend:function(){
						
	// 					},
	// 					success:function(data){
	// 						console.log(data.data);
	// 						var appendsEdit='<option data-id="" value="">-- Pilih --</option>';
	// 						$("#RefrensiIDEdit").empty();
	// 						if(data.data.length > 0){
	// 							$.each(data.data, function (i,val) {
	// 								appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';
							
	// 							});
	// 						$("#RefrensiIDEdit").append(appendsEdit);
	// 						}
	// 					},
						
	// 				});	
	// 			} else {
	// 				$('#activityEdit').prop('required',false);
	// 				$('#jnisEdit').prop('required',false);
	// 				$('#poviderEdit').prop('required',false);
	// 			}
	// 			//

		
	// 	} else {
	// 		$('.AktivitasEdit').hide();
	// 		$('.RefrensiIDEdit').hide();
	// 		$('.JnisEdit').hide();
	// 		$('.PoviderEdit').hide();
	// 	}
	


    // });

	$(document).on("change", "#jnisEdit", function() {
		$thisCommand = $('#jnisEdit').children('option:selected').data('val');
		$.ajax({
			url:'{{url("/voucher-combobox-bygroup2?")}}command='+$thisCommand,
			type:'POST',
			dataType:'json',
			data:{
				'command':$thisCommand
			},
			beforeSend:function(){
			
			},
			success:function(data){
				console.log(data.data);
				var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

				$("#poviderEdit").empty();
				if(data.data.length > 0){
					$.each(data.data, function (i,val) {
						appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.text+'" value="'+val.id+'">'+ val.text +'</option>';
				
					});
				$("#poviderEdit").append(appendsEdit);
				}
			},
			
		});	
    });
	



      $('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editVoucher", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Voucher ? ', function(result) {
				if (result) {
				updateVoucher($dataID);
				}
			}, '<span style="color: blue;">Edit Voucher</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function updateVoucher(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/voucher-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Voucher Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#btnEdit').fadeIn();
            	$('#voucher-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>