<div class="card card-primary">

	<div class="card-header">
		<h3 class="card-title"> Edit Regex</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->
	<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
		@csrf

		<div class="card-body">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Biller</label>
				<div class="col-sm-10">
					<select name="biller_id" id="billerIDEdit" class="form-control select2" required>
						<option value="">Pilih Biller</option>
						@foreach ($biller as $t)
						<option value="{{$t['id']}}" {{$data['biller_id'] == $t['id'] ? "selected":""}}>{{$t['name']}} </option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10">
					<select name="status" id="statusIDEdit" class="form-control" required>
						<option value='0' {{$data['status'] == 0 ? "selected":""}}>Sukses</option>
						<option value='1' {{$data['status'] == 1 ? "selected":""}}>Pending</option>
						<option value='2' {{$data['status'] == 2 ? "selected":""}}>Gagal</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Kata Kunci</label>
				<div class="col-sm-10">
					<textarea type="text" class="form-control" name="added_data" id="added_dataID" required>{{$data['added_data']}}</textarea>
				</div>
			</div>
			@if($data['group_id'] != 0 && $data['status']=="0")
			<div id="formHiddenEdit">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Group</label>
				<div class="col-sm-10">
					<select name="groupID" id="groupIDEdit" class="form-control select2" required>
						@foreach ($groupH as $g)
						<option value="{{$g['id']}}" {{$data['group']['group_name'] == $g['name'] ? "selected":""}}>{{$g['name']}} </option>
						@endforeach
					</select>
				</div>
			</div>
			
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tipe Produk</label>
					<div class="col-sm-10">
						<select name="tipeID" id="tipeIDEdit" class="form-control select2" required>
							@foreach ($tipe as $t2)
							<option value="{{$t2['id']}}" {{$data['group']['command'] == $t2['name'] ? "selected":""}}>{{$t2['name']}} </option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Provider</label>
					<div class="col-sm-10">
						<select name="provider_id" id="providerIDEdit" class="form-control select2" required>
							@foreach ($provider as $p)
							<option value="{{$p['id']}}" {{$data['group_id'] == $p['id'] ? "selected":""}}>{{$p['name']}} </option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			@elseif ($data['status']!=0)
			<div id="formHiddenEdit" style="display: none;">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Group</label>
					<div class="col-sm-10">
						<select name="groupID" id="groupIDEdit" class="form-control select2" required>
							<option value="">Pilih Group</option>
						@foreach ($groupH as $g)
						<option value="{{$g['id']}}">{{$g['name']}} </option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tipe Produk</label>
					<div class="col-sm-10">
						<select name="tipeID" id="tipeIDEdit" class="form-control select2">
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Provider</label>
					<div class="col-sm-10">
						<select name="provider_id" id="providerIDEdit" class="form-control select2">
						</select>
					</div>
				</div>
			</div>
			@else
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Group</label>
				<div class="col-sm-10">
					<select name="groupID" id="groupIDEdit" class="form-control select2" required>
						@foreach ($groupH as $g)
						<option value="{{$g['id']}}" {{$g['id']=="Regex Global" ? 'selected':''}} >{{$g['name']}} </option>
						@endforeach
					</select>
				</div>
			</div>
			<div id="formHiddenEdit" >
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Tipe Produk</label>
				<div class="col-sm-10">
					<select name="tipeID" id="tipeIDEdit" class="form-control select2" disabled>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Provider</label>
				<div class="col-sm-10">
					<select name="provider_id" id="providerIDEdit" class="form-control select2" disabled>
					</select>
				</div>
			</div>
			</div>
			<!-- <div class="form-group row">
					<label class="col-sm-2 col-form-label">Group</label>
					<div class="col-sm-10">
						<select name="groupID" id="groupIDEdit" class="form-control select2" required>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tipe Produk</label>
					<div class="col-sm-10">
						<select name="tipeID" id="tipeIDEdit" class="form-control select2" required>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Provider</label>
					<div class="col-sm-10">
						<select name="provider_id" id="providerIDEdit" class="form-control select2" required>
						</select>
					</div>
				</div> -->
			@endif

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Contoh Respon</label>
				<div class="col-sm-10">
					<textarea type="text" class="form-control" name="sample_respond" id="sampleRespondIDEdit" required>{{$data['sample_respond']}}</textarea>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Tipe Respon</label>
				<div class="col-sm-10">
					<select name="type_respond" id="" class="form-control" required>
						<option value='0' selected>Text</option>
					</select>
					<input type="hidden" id="DATAID" value='{{$data["id"]}}' />
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<label class="col-sm-6 col-form-label ">Parameter Wajib</label>
				<br>
				<br>
			</div>
			<div class="col-sm-12 form-group row">
				<div class="col-sm-12 row">
					<label class="col-sm-2 col-form-label">Refid Biller</label>
					<div class="col-sm-3">
						<input type="text" name="refid" value='{{$awal[0]?? ";"}}' id="ParamWajibIDEdit[]" class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[0]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Refid transaksi Anda "><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">Trxid</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[1]?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[1]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Refid Digiflazz transaksi Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">Tujuan</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[2]?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[2]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Tujuan Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">Kode Produk</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[3]?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[3]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah SKU Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">Harga</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[4]?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[4]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Harga Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">Sisa Saldo</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[5]?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[5]?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah Sisa Saldo Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
				<br>
				<div class="col-sm-12 row" style="margin-top:5px!important;">
					<label class="col-sm-2 col-form-label">SN</label>
					<div class="col-sm-3">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$awal[6] ?? ";"}}' class="ParamWajibID1Edit ParamWajibIDEdit form-control">
					</div>
					<div class="col-sm-1 text-center" style="margin-left:-30px!important;">
						-
					</div>
					<div class="col-sm-3" style="margin-left:-30px!important;">
						<input type="text" name="refid" id="ParamWajibIDEdit[]" value='{{$akhir[6] ?? ";"}}' class="ParamWajibID2Edit ParamWajibIDEdit form-control">

					</div>
					<div class="col-sm-1" style="margin-left:-25x!important;">
						<a style="border-radius:15px!important;" class="btn btn-sm  btnEdit btn-secondary" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cari kalimat apa yang terletak sebelum dan sesudah SN Anda"><i class="fa fa-question"></i></a>
					</div>
				</div>
			</div>



		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Parameter Opsional</label>
			<div class="col-sm-10">
				<textarea type="text" class="form-control" name="parameter" rows="5" id="parameterIDEdit" required>{!!$parameterParsed!!}</textarea>
			</div>
		</div>
		<hr>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Preview</label>
			<div class="col-sm-8">
				<textarea type="text" class="form-control" rows='8' name="preview" id="previewIDEdit" readonly required>{!!$reviewParsed!!}</textarea>
				<input type="hidden" class="form-control" rows='8' name="preview_wajib" value="{!!$data['preview']!!}" id="previewIDWEdit">
				<input type="hidden" class="form-control" rows='8' name="preview_opsi" value="{!!$data['preview_opsi']!!}" id="previewIDOEdit">
			</div>
			<div class="col-sm-2">
				<button type="submit" id="generatePreviewEdit" class="btn btn-success">Lihat Hasil</button>
			</div>
		</div>
</div>



<!-- /.card-body -->
<div class="card-footer">
	<button type="" id="editResellerGroup" data-id='{{$data["id"]}}' class="btn btn-info editResellerGroup">Submit</button>
	<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
</div>
<!-- /.card-footer -->
</form>
</div>
<script>
	$('#cancelBtnEdit').click(function() {
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
	});
	$("#fileEdit").change(function(e) {
		var _URL2 = window.URL || window.webkitURL;
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL2.createObjectURL(file);
			img.onload = function() {

				_URL2.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : ' + this.width + " x " + this.height);
				$('#dimRec').text(' Current Dimension : ' + this.width + " x " + this.height);

				$('#fileEdit').removeClass('is-invalid');
				$('#btnEditEvent').prop('disabled', false);
				$('#dimRec').removeClass('error');
			};
			img.src = objectUrl;
			preview_image_edit();
		}
	});

	function preview_image_edit() {
		$('#image_preview_edit').empty();
		$('#image_preview_edit').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
	}
	$(document).on("click", "#editResellerGroup", function(e) {
		e.stopImmediatePropagation();
		e.preventDefault();
		if ($('#previewIDEdit').val() == '') {
			toastr.error('Preview tidak boleh kosong');
			return;
		}
		if ($('#billerIDEdit').val() == '') {
			toastr.error('Biller tidak boleh kosong');
			return;
		}
		console.log($('#statusIDEdit').children('option:selected').val(), $('#statusIDEdit').val())
		if ($('#statusIDEdit').children('option:selected').val() == "0") {
			if ($('#groupIDEdit').children('option:selected').val() != 'Regex Global') {
				if ($('#providerIDEdit').children('option:selected').val() == "") {
					toastr.error('Provider harus diisi');
					return;
				}
				if ($('#providerIDEdit').children('option:selected').val() == undefined) {
					toastr.error('Provider harus diisi');
					return;
				}
			}
		}

		if ($('#statusIDEdit').val() == "0") {
			if ($('#groupIDEdit').children('option:selected').val() != 'Regex Global') {
				if ($('#providerIDEdit').children('option:selected').val() == "") {
					toastr.error('Provider harus diisi');
					return;
				}
				if ($('#providerIDEdit').children('option:selected').val() == undefined) {
					toastr.error('Provider harus diisi');
					return;
				}
			}
		}


		$.each($('.ParamWajibIDEdit'), function(i, val) {
			if (val.value == "") {
				toastr.error('Mohon periksa kembali data inputan');
				return;
			}
		});
		showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
			if (result) {
				updateResellerGroup($('#DATAID').val());
			}
		}, '<span style="color: blue;">Edit Data </span>', {
			confirm: {
				label: 'Update',
				className: 'btn-primary'
			},
			cancel: {
				label: 'Cancel',
				className: 'btn-default'
			}
		});



	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	function updateResellerGroup(idProduct) {
		// Setup validation
		var _baseURL = "<?php echo url(''); ?>";


		var form = $("#frmEdit")[0];
		var formData = new FormData(form);
		var param2awal = [];
		var param2akhir = [];
		$.each($('.ParamWajibID1Edit'), function(i, val) {
			param2awal.push(val.value);
		});
		$.each($('.ParamWajibID2Edit'), function(i, val) {
			param2akhir.push(val.value);
		});

		formData.append("param2awal", param2awal);
		formData.append("param2akhir", param2akhir);

		$.ajax({
			url: _baseURL + "/regex/" + idProduct,
			type: "post",
			dataType: "json",
			data: formData,
			processData: false,
			contentType: false,
			success: function(d) {
				console.log(toastr);
				if (d.status == 200) {
					toastr.success("Data Berhasil diupdate")
					$('#editForm').fadeOut();
					$('#addform').fadeOut();
					$('#editBtn').fadeIn();
					$('#product-category-table').DataTable().ajax.reload();


				} else {
					toastr.error(d.message)
				}
			},
			error: function(xhr) {
				console.log(xhr);
			},
			beforeSend: function() {},
			complete: function() {

			},
		});

		return false;

	}
</script>