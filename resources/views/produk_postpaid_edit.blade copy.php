<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Edit Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
                <div class="card-body">
              <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Group</label>
                      <div class="col-sm-10">
                        <select name="group_id" id="group_id" class="form-control" disabled>

						            @foreach ($group as $g)
						            <option value="{{$g['id']}}" {{$data['group_id'] == $g['id'] ? 'selected' : ""}}>{{$g['command'].' - '.$g['provider']}}</option>
                        @endforeach
                        
                </select>
                          </div>
                    </div>
                    @if($data['category_id'] != 0)
              <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kategori</label>
                      <div class="col-sm-10">
                        <select name="category_id" id="category_id" class="form-control" required>

                                @foreach ($cat as $ca)
                                <option value="{{$ca['id']}}" {{$data['category_id'] == $ca['id'] ? 'selected' : ""}}>{{$ca['name']}}</option>
                                @endforeach
                                
                        </select>
                      </div>
              </div>
              @endif
            <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Product SKU</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" value="{{$data['sku_code']}}" name="sku_code" required>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                <label class="fancy-radio custom-color-green"><input name="is_deleted" value="0" type="radio"><span><i></i>Aktif</span></label>
                <label class="fancy-radio custom-color-red"><input name="is_deleted" value="1" type="radio"><span><i></i>Tidak Aktif</span></label>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="name" value="{{$data['name']}}" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
              <input type="text" class="form-control" value="{{$data['description']}}" name="description" required>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="command" value="{{$data['productGroup']['command']}}" disabled>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Admin Fee</label>
                        <div class="col-sm-10">
                <input type="number" class="form-control" name="admin_fee" value="{{$data['admin_fee']}}"  required>
                        </div>
                      </div>

                      
                      <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Provider</label>
                        <div class="col-sm-10">
                <input type="text" class="form-control" name="provider" value="{{$data['productGroup']['command']}}" disabled>
                        </div>
                      </div> -->
                  
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cashback</label>
                        <div class="col-sm-10">
                <input type="number" class="form-control" name="cashback" value="{{$data['commission']}}" required>
                        </div>
                      </div>
                <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Biller</label>
                          <div class="col-sm-10">
                            <select name="biller_id" id="biller_id" class="form-control" required>
                      
                            @foreach ($biller as $bill)
                            <option value="{{$bill['id']}}" {{$data['productBiller']['biller_id'] == $g['id'] ? 'selected' : ""}}>{{$bill['name']}}</option>
                            @endforeach
                            </select>
                                      </div>
                                </div>
                                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">SKU Biller</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="sku_biller" value="{{$data['productBiller']['sku_biller']}}" required>
                    </div>
                  </div>
                                <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <label class="fancy-radio custom-color-green"><input name="is_trouble" value="0" type="radio"     {{$data['is_trouble'] == 0 ? "checked='checked'":"" }}><span><i></i>Normal</span></label>
                          <label class="fancy-radio custom-color-red"><input name="is_trouble" value="1" type="radio" 
                        {{$data['is_trouble'] == 1 ? "checked='checked'":"" }}><span><i></i>Ganguan</span></label>
                        </div>
                      </div>
                
                    </div>
                  
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editProduct"  data-id='{{$data["id"]}}' class="btn btn-info editProduct">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>


            


            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editProduct", function(e) {
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate produk ? ', function(result) {
				if (result) {
				updateProduct($dataID);
				}
			}, '<span style="color: blue;">Edit Produk</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function updateProduct(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
				formData.append("type", 'postpaid');
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/product-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
							toastr.success("Produk Berhasil diupdate")
              $('#product-postpaid-table').DataTable().ajax.reload();
							$('#editForm').fadeOut();
							$('#editBtn').fadeIn();
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
					},
				});

				return false;
			}

		}
          </script>