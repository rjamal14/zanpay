          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Detail</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			          @csrf
                <div class="card-body">
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Dikirim Pada</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$data['tanggal']}}" disabled>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Nama Pengirim</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" value="{{$data['sender_name']}}" disabled>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Email Pengirim</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$data['sender_email']}}" disabled>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">No. HP Pengirim</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$data['sender_phone']}}" disabled>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Subject</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$data['subject']}}" disabled>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Isi Pesan</label>
                      <div class="col-sm-10">
                        <textarea type="text" class="form-control"  rows="5"  name="title"  disabled>{{$data['content']}}</textarea>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Alamat IP</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$data['sender_ip']}}" disabled>
                      </div>
                  </div>
                 
                </div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer" style="height:60px;">
               
                      <div class="mr-auto">
                        <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                      </div>
                    </div>
                    <!-- /.card-footer -->
                </form>
          </div>
<script>
      $('#cancelBtnEdit').click(function(){
        $('#editForm').fadeOut();
        $('.btnEdit').fadeIn();
        $('#inbox-table').DataTable().ajax.reload();
		  });
          </script>