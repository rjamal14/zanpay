<x-package-header />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Terms and Condition</h2>
			
			</div>
			<div class="col-md-6 col-sm-12 text-right">
			
			</div>
		</div>
		
	</div>


        


	<div id= "pendahuluan" class="container-fluid ">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
						<div class="d-flex" >
							<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							
							</div>
						
						</div>
					
					</div>
					<div class="body">
						
					<!-- form -->
						<form class="form-vertical" enctype="multipart/form-data">
							@csrf
							<div class="card-body">
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Pendahuluan</label>
									<div class="col-sm-10 ">
										
									<div class="card">
										<div class="body">
											<!-- <div class="summernote" id="privacy">
												{!!$privacy->content!!}
											</div> -->
											<textarea id="term" class="summernote" name="editordata">{!!$term->content!!}</textarea>
											<!-- <textarea name="privacy" style="display:none;" id='privacyInput'>	{!!$term->content!!}</textarea> -->
										</div>
									</div>


									</div>
									
								</div>
								
							</div>
								
						
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="" i class="btn btnUpdate btn-info ">Submit</button>
							
							</div>
                 
						</form>
					<!-- end form -->
					</div>
				</div>

		</div>
	</div>



	<div class="card-body" id="addForm" style="display: none;">
          	<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Tambah </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
			  @csrf
                <div class="card-body">
				
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Judul</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" id="termTitle" name="title" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Isi</label>
                    <div class="col-sm-10">
						<textarea id="termContent" class="summernoteAdd" name="termContent"></textarea>
                    </div>
                </div>
                </div>
             
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="addTermDetail"   class="btn btn-info addTermDetail">Submit</button>
                  <button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
        	</div>
        </div>

        <div  class="card-body" id="editForm"></div>
	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
					<label class="col-sm-12 col-form-label">Detail</label>

						<div class="d-flex" >
							<div class="ml-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							<button id="addBtn" class="btn btn-sm btn-primary ml">
					<icon class="fa fa-plus"></icon>
				</button>
							</div>
						
						</div>
					
					</div>
					<div class="body">
						
					<!-- form -->
						<form class="form-vertical" enctype="multipart/form-data">
							@csrf
							<div class="card-body">
								<div class="form-group row">
										
										<div class="body">
											<div class="table-responsive">
												<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom privacy-table"  id="privacy-table">
													<thead>
														<tr>
															<th>No.</th>
															<th>Judul</th>
															<th>Isi</th>
															<th>Aksi</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>


									
								</div>
								
							</div>
								
						
							<!-- /.card-body -->
							<div class="card-footer">
							</div>
                 
						</form>
					<!-- end form -->
					</div>
				</div>

		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	var _baseURL = "<?php echo url(''); ?>";
	function editData(id,url ="{{url("/terms_privacy_list_edit/")}}"){
			$.ajax({
			url:url+'/'+id,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
     
			},
			success:function(data){
				$('#pendahuluan').hide();
        		$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('.summernoteEdit').summernote();
				
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url+'/'+id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
			});	
	}

		$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
		});


	$( document ).ready(function() {
		$('.summernote').summernote();
		$('.summernoteAdd').summernote({ height: 300});
		
	});
	$('#addBtn').click(function(){
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
     		$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
			$('#pendahuluan').hide();
	});
	$('#cancelBtnAdd').click(function(){
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
			$('#pendahuluan').show();
		});


		$(document).on("click", ".addTermDetail", function(e) {
			e.preventDefault();
				showConfirm('Apakah anda Yakin ingin menambahkan Isi Privacy Policy ? ', function(result) {
					if (result) {
						addTermDetail();
					}
				}, '<span style="color: blue;">Tambah Data</span>', {
					confirm: {
						label: 'Tambah',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"terms_privacy_list_delete", [$id],
				true
			);
			return false;

		}
		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini'){
			if($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if(result) {
							$.ajax({
								url: url+'/'+$ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if(d.status == 200) {
									
										$('#privacy-table').DataTable().ajax.reload();
										toastr.success("Data berhasil dihapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function () {
								},
								complete: function () {
								}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}	

		function addTermDetail() {
			$.ajax({
				url: _baseURL + "/terms_privacy_list_store",
				type: "post",
				dataType: "json",
				data: {
					content: $('#termContent').summernote('code'),
					title: $('#termTitle').val(),
					"_token": "{{ csrf_token() }}",
				},
				success: function(d) {
					if (d.status == 200) {
						$('#privacy-table').DataTable().ajax.reload();
						$('#addForm').fadeOut();
						$('#addBtn').fadeIn();
						toastr.success("Data berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {
				},
				complete: function() {
				},
			});

			return false;

		}
	var oDataList = $('#privacy-table').DataTable({
			processing: true,
			serverSide: true,
			paging: false,
			autoWidth: false,
		
			"bAutoWidth" : false,
			"lengthChange" : false,
			ajax: {
				url: "{{ url('terms_privacy_list') }}",
				data: {
                  
             
                },
			},
		    "fnDrawCallback": function( data ,e) {
							
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false},
			
				{
					data: 'judul',
					name: 'judul',
				},
				{
					data: 'content',
					name: 'content',
					orderable: false
			
				},
				{
					data: 'action',
					name:'Tindakan',
					searchable: false,
					orderable: false
				},
			
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Reseller Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
					{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-50'>" + data + "</div>";
						},
						targets: 0
					},
					{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-100'>" + data + "</div>";
						},
						targets: 1
					},
					{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-400'>" + data + "</div>";
						},
						targets: 2
					},
					{
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-80'>" + data + "</div>";
						},
						targets: 3
					},
		
		
			],
			select: {
				style: 'single'
			},
		});

		


	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});

	$(document).on("click", ".btnUpdate", function(e) {
        e.stopImmediatePropagation();
		e.preventDefault();
		showConfirm('Apakah anda Yakin ingin mengupdate Pendahuluan Privacy Policy ? ', function(result) {
					if (result) {
						$.ajax({
					url: _baseURL + "/terms_privacy",
					type: "post",
					dataType: "json",
					data: {
						term: $('#term').summernote('code'),
						"_token": "{{ csrf_token() }}",
					},
					success: function(d) {
						console.log(d);
						// if (d.status == 200) {
							// location.reload();
						// } else {
						// 	toastr.error(d.message)
						// }
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
						location.reload();
					},
				});
					}
				}, '<span style="color: blue;">Update Pendahuluan Privacy Policy </span>', {
					confirm: {
						label: 'Update',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
	
	});



		
		
	
    	

</script>