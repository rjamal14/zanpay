		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Edit Daftar Pekerjaan</h3>
			</div>
		
			<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
				@csrf
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Judul</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name" value="{{$data['name']}}" required>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Status</label>
					<div class="col-sm-10">
						<select class="form-control" name="status" required>
							<option value="0" {{$data['status'] == 0 ? "selected" :""}} >Belum Selesai</option>		<option value="1" {{$data['status'] == 1 ? "selected" :""}}>Selesai</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tanggal</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" value="{{$data['date']}}" name="date" required>
					</div>
				</div>

				
				<div class="card-footer">
					<button type="" id="editDaftarPekerjaan"  data-id='{{$data["id"]}}' class="btn btn-info editDaftarPekerjaan">Submit</button>
					<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
				</div>
			</form>
        </div>


          <script>


    $(document).on("click", "#editDaftarPekerjaan", function(e) {
        e.stopImmediatePropagation();
			e.preventDefault();
		console.log($(this).attr("data-id"));
		if ($("#frmEdit").valid()) {
		
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateReseller($dataID);
				}
			}, '<span style="color: blue;">Edit Data</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
	});
	function updateReseller(idProduct) {
			// Setup validation
	

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);
				var _baseURL = "<?php echo url(''); ?>";
				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/daftar-pekerjaan-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Reseller Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('.btnEdit ').fadeIn();
            	$('#daftar-pekerjaan-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
		{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
		}
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});
</script>