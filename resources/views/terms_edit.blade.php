		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Edit </h3>
			</div>
			<div class="card-body">
			<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
				@csrf
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Judul</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" id="termTitleEdit" name="title" value="{{$data['title']}}" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Isi</label>
                    <div class="col-sm-10">
						<textarea id="termContentEdit" class="summernoteEdit" name="Edit">{!!$data['content']!!}</textarea>
                    </div>
                </div>


				
				<div class="card-footer">
					<button type="" id="editDaftarPekerjaan"  data-id='{{$data["id"]}}' class="btn btn-info editDaftarPekerjaan">Submit</button>
					<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
				</div>
			</form>
        	</div>
        </div>


          <script>


    $(document).on("click", "#editDaftarPekerjaan", function(e) {
        e.stopImmediatePropagation();
			e.preventDefault();
	
		
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateReseller($dataID);
				}
			}, '<span style="color: blue;">Edit Data</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
	});
	function updateReseller(idProduct) {
			// Setup validation
				$.ajax({
					url: _baseURL + "/terms_list_update/"+idProduct,
					type: "post",
					dataType: "json",
					data: {
						content: $('#termContentEdit').summernote('code'),
						title: $('#termTitleEdit').val(),
						"_token": "{{ csrf_token() }}",
					},
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
            			  toastr.success("Data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('.btnEdit ').fadeIn();
							$('#pendahuluan ').show();
            				$('#term-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

		}
	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
		{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
		}
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('#pendahuluan').show();
			$('.btnEdit').fadeIn();
		});
</script>