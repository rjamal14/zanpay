<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Product Biller</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
					<div class="card-body">


					
					@if ($data['product'] == null)
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Biller</label>
						<div class="col-sm-10">
						<select name="biller_id" id="billerIDEdit" class="form-control productSelect" required>
							@foreach($biller as $b)
							<option value="{{ $b['id']}}" {{ $b['id']==$data['biller_id'] ? 'selected':''}} >{{$b['name']}}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe (parsing)</label>
						<div class="col-sm-10">
						<select name="parsingType" id="parsingTypeEdit" class="form-control">
							<option value="" >Pilih</option>
							@foreach($tipe as $typ)
							<option value="{{ $typ['id']}}">{{$typ['text']}}</option>
							@endforeach
						</select>
						</div>
					</div>
					@else
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Biller</label>
						<div class="col-sm-10">
						<select name="biller_id" id="billerIDEdit" class="form-control productSelect" disabled>
							@foreach($biller as $b)
							<option value="{{ $b['id']}}" {{ $b['id']==$data['biller_id'] ? 'selected':''}} >{{$b['name']}}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe (parsing)</label>
						<div class="col-sm-10">
						<select name="parsingType" id="parsingTypeEdit" class="form-control" disabled>
							<option value="" >Pilih</option>
							@if(isset($data['product']['group']))
							@foreach($tipe as $typ)
							<option value="{{ $typ['id']}}" {{ $data['product']['group']['group_name']== $typ['id'] ? 'selected':''}}>{{$typ['text']}}</option>
							@endforeach
							@endif
						</select>
						</div>
					</div>
					@endif
					<div class="form-group row">
								@if ($data['product'] == null)
								<label class="col-sm-2 col-form-label">Jenis</label>
						<div class="col-sm-10">
						<select name="group_id" id="groupIDEdit" class="form-control productSelectEdit" >
							<option value="">-- Pilih Group --</option>
							
						</select>
								@else
								<label class="col-sm-2 col-form-label">Jenis</label>
						<div class="col-sm-10">
						<select name="group_id" id="groupIDEdit" class="form-control productSelectEdit" disabled>
							<option value="">-- Pilih Group --</option>
							@if(isset($data['product']['group']))
							@foreach($group as $g)
							<option value="{{ $g['id']}}" {{( isset($data['product']) ? ($g['id']==$data['product']['group_id'] ? 'selected':''):'')}}>{{$g['command']." - ".$g['provider']}}</option>
							@endforeach
							@endif
						</select>
								@endif
					
						</div>
					</div>

			

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">SKU Biller</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="sku_biller" value="{{$data['sku_biller']}}" required>
				</div>
			</div>
			<!-- <div class="form-group row">
				<label class="col-sm-2 col-form-label">Description</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="description" value="{{$data['sku_description']}}" required>
				</div>
			</div> -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Harga Dasar</label>
				<div class="col-sm-10">
					<input type="number" class="form-control" name="price_basic" value="{{$data['price_basic']}}" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Admin Fee</label>
				<div class="col-sm-10">
					<input type="number" class="form-control" name="admin_fee" value="{{$data['admin_fee']}}"  required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Prioritas</label>
				<div class="col-sm-10">
				<select name="priority" id="priority" class="form-control" required>
					<option value="1" {{ $data['priority'] == 1 ? 'selected':''}} >Tinggi</option>
					<option value="2"  {{ $data['priority'] == 2 ? 'selected':''}} >Normal</option>
					<option value="3"  {{ $data['priority'] == 3 ? 'selected':''}} >Rendah</option>
				</select>
				</div>
			</div>
			@if($data['product_id'] == 0)
					<div class="form-group row">
				<label class="col-sm-2 col-form-label">Produk SKU</label>
				<div class="col-sm-10">
					<select name="product_id" id="productIDEdit" class="form-control productSelectEdit" >
						<option value="0" >--Silahkan Pilih Group--</option>
					</select>
				</div>
			</div>
			@else
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Produk SKU</label>
				<div class="col-sm-10">
					<select name="product_id" id="productIDEdit" class="form-control productSelectEdit" >
						<option value="0">Kosongkan</option>
							@foreach($products1 as $pr)
								<option value="{{$pr['id']}}" {{$pr['id'] == $data['product_id'] ? "selected='selected'" : "" }} >{{$pr['text']}}</option>
							@endforeach
					</select>
				</div>
			</div>
			@endif
			<div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <label class="fancy-radio custom-color-green"><input name="is_trouble" value="0" type="radio"     {{$data['is_trouble'] == 0 ? "checked='checked'":"" }}><span><i></i>Normal</span></label>
                          <label class="fancy-radio custom-color-red"><input name="is_trouble" value="1" type="radio" 
                        {{$data['is_trouble'] == 1 ? "checked='checked'":"" }}><span><i></i>Ganguan</span></label>
                        </div>
                      </div>
						
                	</div>
        
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editProductBiller"  data-id='{{$data["id"]}}' class="btn btn-info editProductBiller">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
$( document ).ready(function() {
      $('.productSelectEdit').select2({
        "width":'100%'
      });

	//   $thisIdEdit = $('#groupIDEdit').children('option:selected').val();
				
	// 				$.ajax({
	// 					url:'{{url("/product-combobox-bygroup?")}}group_id='+$thisIdEdit,
	// 					type:'GET',
	// 					dataType:'json',
	// 					beforeSend:function(){
	// 						$("#productIDEdit").empty();
	// 					},
	// 					success:function(data){
	// 						console.log(data.data);
	// 						var appendsEdit2='';
						
	// 						if(data.data.length > 0){
	// 							$.each(data.data, function (i,val) {
	// 								appendsEdit2+='<option data-id="'+val.id+'"  value="'+val.id+'">'+ val.text +'</option>';
							
	// 							});
	// 						$("#productIDEdit").append(appendsEdit2);
	// 						} else {
	// 							$("#productIDEdit").empty();
	// 							$("#productIDEdit").append('<option data-id=""  value="">Group tidak memiliki produk</option>');
	// 						}
	// 					},
	// 				});	

    });
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});
		$("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});
		function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
    $(document).on("click", "#editProductBiller", function(e) {
		e.stopImmediatePropagation();
			e.preventDefault();
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
    
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				UpdateProductBiller($dataID);
				}
			}, '<span style="color: blue;">Edit Data </span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function UpdateProductBiller(idProduct) {
			// Setup validation
			var _baseURL = "<?php echo url(''); ?>";
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/product_biller-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
              $('#product_biller-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>