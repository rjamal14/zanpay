<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Inbox</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Inbox</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						<ul class="header-dropdown dropdown dropdown-animated scale-left">
							<li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
							<li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another Action</a></li>
									<li><a href="javascript:void(0);">Something else</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">
						<div class="mail-inbox">
							<div class="mail-right check-all-parent">
								<div class="header d-flex align-center">
									<h2>Inbox</h2>
									<form class="ml-auto">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Search Inbox" aria-label="Search Mail" aria-describedby="search-mail">
											<div class="input-group-append">
												<span class="input-group-text" id="search-mail"><i class="icon-magnifier"></i></span>
											</div>
										</div>
									</form>
								</div>
								<div class="mail-action clearfix">
									<div class="pull-right ml-auto">
										<div class="pagination-email d-flex">
											<p>1-50/295</p>
											<div class="btn-group m-l-20">
												<button type="button" class="btn btn-default btn-sm"><i class="fa fa-angle-left"></i></button>
												<button type="button" class="btn btn-default btn-sm"><i class="fa fa-angle-right"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="mail-list">
									<ul class="list-unstyled">
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
										<li class="clearfix">
											<div class="mail-detail-right">
												<h6 class="sub"><a href="app-inbox-detail.html" class="mail-detail-expand">0812345678910</a></h6>
												<p class="dep"><span class="m-r-10">TRX ke:1.385 R# TDB3.081233696171 SUKSES SN:GUI96171202107231121475ST Ref Komisi:0/32.021.994-40.000=31.981.994 @23/07/2021 11.22.00/.</p>
												<span class="time">23 Juli 2021 11:24</span>
											</div>
											<div class="hover-action">
												<button class="btn btn-warning btn-sm mr-1" title="Delete" type="button"><i class="fa fa-archive"></i></button>
												<button class="btn btn-danger btn-sm js-sweetalert" title="Delete" type="button" data-type="confirm"><i class="fa fa-trash-o"></i></button>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
