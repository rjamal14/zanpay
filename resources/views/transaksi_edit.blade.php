          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Edit Transaksi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			          @csrf
                <div class="card-body">
                  <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
							<select name="status" class="form-control select request-status" >
							<option value="Q" {{$data->status  == "Q"  ? 'selected' : ""}}>ANTRIAN</option>
							<option value="S" {{$data->status  == "S"  ? 'selected' : ""}}>SUKSES</option>
							<option value="F" {{$data->status  == "F"  ? 'selected' : ""}}>GAGAL</option>
							<option value="P" {{$data->status  == "P"  ? 'selected' : ""}}>PROSES</option>
							</select>
                        </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">SN</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="sn" value="{{$sn}}" required>
                      </div>
                  </div>
                
                </div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editTeam"  data-id='{{$data["id"]}}' class="btn btn-info editTeam">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
          </div>
<script>
		$('#cancelBtnEdit').click(function(){
				$('#editForm').fadeOut();
				$('.btnEdit').fadeIn();
		});
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
					if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo( element.parent().parent().parent().parent() );
					}
					else {
						error.appendTo( element.parent().parent().parent().parent().parent() );
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo( element.parent().parent().parent() );
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo( element.parent() );
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo( element.parent().parent() );
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo( element.parent().parent() );
				}

				else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"

		
		};

		$(document).on("click", "#editTeam", function(e) {
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
			e.stopImmediatePropagation();
			e.preventDefault();
			console.log($(this).attr("data-id"));
			if ($("#frmEdit").valid()) {
				$dataID = $(this).attr("data-id");
				showConfirm('Apakah anda Yakin ingin mengupdate Transaksi ? ', function(result) {
					if (result) {
					updateTeam($dataID);
					}
				}, '<span style="color: blue;">Edit Trsansaksi</span>', {
					confirm: {
						label: 'Update',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			}
		});

		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
   

		function updateTeam(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/transaksi-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Transaksi Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
            	$('#transaction-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>