<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Reseller</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			  @csrf
                <div class="card-body">
				<div class="form-group row">
			  <label class="col-sm-2 col-form-label">Logo</label>
                      <div class="col-sm-10">
                        <div id="image_preview_edit">   
                        <img src ="{{$data['logo']}}" width="150px;" style="border-radius: 2px;">
                        </div>
                        <input type="file" id="fileEdit" name="logo" onclick="preview_image_edit()">
        </div>
                </div>
				<div class="form-group row">
                      <label class="col-sm-2 col-form-label">Tipe Pembayaran</label>
                      <div class="col-sm-10">
                        <select name="payment_id" id="payment_id" class="form-control" required>

						@foreach ($group as $g)
						<option value="{{$g['id']}}" {{$data['payment_id'] == $g['id'] ? "selected" : ""}}>{{$g['title']}}</option>
                        @endforeach
                        
						</select>
                      </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" value="{{$data['name']}}" name="name" required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Akun</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="account" value="{{$data['account']}}"  required>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-sm-2 col-form-label">Behalf</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="behalf" value="{{$data['behalf']}}" required>
                    </div>
                  </div>
		

				  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-10">
                        <select name="status" id="status" class="form-control" required>

						<option value="1" {{$data['status'] == 1 ? "selected" : ""}}>Active</option>
						<option value="0"  {{$data['status'] == 0 ? "selected" : ""}}>inactive</option>
			
                        
						</select>
                      </div>
                </div>
             
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="editPayment"  data-id='{{$data["id"]}}'  class="btn btn-info editPayment">Submit</button>
                  <button id="cancelBtnEdit" type="button" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
             
</div>
          <script>
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});


    $(document).on("click", "#editPayment", function(e) {
    
			e.preventDefault();
      console.log($(this).attr("data-id"));
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Payment Setting ? ', function(result) {
				if (result) {
				updatePayment($dataID);
				}
			}, '<span style="color: blue;">Edit Payment</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function updatePayment(idProduct) {
			// Setup validation
	
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/payment-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Reseller Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
							$('#btnEdit').fadeIn();
            	$('#payment-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;

		}
          </script>