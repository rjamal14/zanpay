<x-package-header />
<style>

	a.disabled {
    	pointer-events: none;
	}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-110{
    width:110px;
}
.width-150{
    width:150px;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}
	.table-custom th {
		font-size: 12px;
	}

	.table-custom td {
		font-size: 11.5px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Produk Terlaris</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<!-- <button class="btn btn-sm btn-primary mr-3">
					<i class="fa fa-download"></i> Export CSV
				</button>
				<button class="btn btn-sm btn-primary mr-3" data-toggle="modal" data-target="#filterModal">
					<icon class="fa fa-filter"></icon> Filter
				</button>
				<button class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-sort"></icon> Sortir
				</button> -->
				<button id="addBtn" class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
          <br>
          <center><i class="fa fa-spinner fa-spin"></i></center>
        </div>

        <div class="card-body" id="addForm" style="display: none;">
          	<div class="card card-primary">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
        	</div>
        </div>


	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
						<div class="d-flex" style="margin-bottom: 25.75px;margin-top: 25.75px;">
							<div class="mr-auto">
								<table border="0" class="table-custom-info" style="z-index:100;margin-bottom: -30.75px;margin-top: 16.75px;">
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Group:</label>
									<div class="col-lg-12">
									<select class="form-control js-filterGroup-basic-multiple" id="filterJenis" >
										<option value="">Semua Group</option>
										@foreach($group as $v)
										<option value="{{$v['group_name']}}">{{$v['group_name']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Jenis:</label>
									<div class="col-lg-12">
									<select  multiple="multiple" class="form-control js-filterJenis-basic-multiple" id="filterJenis" >
										@foreach($jenis as $v)
										<option value="{{$v['id']}}">{{$v['text']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Provider:</label>
									<div class="col-lg-12">
									<select  multiple="multiple" name="filterProvider[]" class="form-control js-filterProvider-basic-multiple" id="filterProvider" >
										@foreach($provider as $v)
										<option value="{{$v['id']}}">{{$v['text']}}</option>
										@endforeach 
										</select>
									</div>
									</th>
									
								
								</table>
							</div>
						</div>
					<div class="body">
						<div class="table-responsive check-all-parent">
						<font size="2"  >
							<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom "  id="product-terlaris-table">
								<thead>
									<tr>
										<th>No.
										
										</th>
										<th>Grop</th>
										<th width="100px;">Jenis</th>
										<th>Provider</th>
										<th>Kategori</th>
										<th>Nama Produk</th>
										<th>Produk SKU</th>
										<th>Biller</th>
										<th>Terjual</th>
										
									</tr>
								</thead>
							</table>
						</font>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.js"></script>
<script src="https://malsup.github.io/jquery.blockUI.js"></script>
	<script type="text/javascript">

	$(function() {
    var _baseURL = "<?php echo url(''); ?>";
    var filterJenis = [];
    var filterProvider = [];
    var filterGroup = [];
	$(document.body).on("change",".js-filterProvider-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterGroup = $('.js-filterGroup-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterGroup);
	});
	$(document.body).on("change",".js-filterGroup-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterGroup = $('.js-filterGroup-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterGroup);
	});
	$(document.body).on("change",".js-filterJenis-basic-multiple",function(){
		filterProvider = $('.js-filterProvider-basic-multiple').val()
		filterGroup = $('.js-filterGroup-basic-multiple').val()
		filterJenis = $('.js-filterJenis-basic-multiple').val()
		getTable(filterJenis,filterProvider,filterGroup);
	});

	$('.js-filterJenis-basic-multiple').select2({
		placeholder: "Semua Jenis",
	});
	$('.js-filterProvider-basic-multiple').select2({
		placeholder: "Semua Provider",
	});
	getTable(filterJenis,filterProvider,filterGroup);
	function getTable(filterJenis,filterProvider,filterGroup) {
		// if ( $.fn.dataTable.isDataTable( '#product-terlaris-table' ) ) {
		// 	table = $('#example').DataTable();
		// }
		var oDataList = $('#product-terlaris-table').DataTable({
        processing: true,
        serverSide: true,
        "bLengthChange": false,
        destroy: true,
        ordering: false,
        autoWidth: false,
		searchDelay: 711,
        "bAutoWidth" : false,
		pagingType: "full",
        ajax: {
          url: "{{ url('produk-terlaris-list') }}",
          data:  {
                "type": "prepaid",
                "filterJenis": filterJenis,
                "filterProvider": filterProvider,
                "filterGroup": filterGroup
                },

			beforeSend: function(){
				$(document.body).css({'cursor' : 'wait'});
				$('.js-filterGroup-basic-multiple').attr("disabled", "disabled");
				$('.js-filterProvider-basic-multiple').attr("disabled", "disabled");
				$('.js-filterJenis-basic-multiple').attr("disabled", "disabled");
				// $('.page-loader-wrapper').show();
				// $.blockUI({ overlayCSS: { backgroundColor: '#00f' } });
				},
			complete: function(){
				$(document.body).css({'cursor' : 'default'});
				$('.js-filterGroup-basic-multiple').removeAttr("disabled");
				$('.js-filterProvider-basic-multiple').removeAttr("disabled");
				$('.js-filterJenis-basic-multiple').removeAttr("disabled");
			},
		},
      
        columns: [
        
        
          {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
                orderable: false},
            // {
            //     data: 'status',
            //     name: 'status',
		
            // },
            {
                data: 'groupName',
                name: 'groupName',
		
            },
            {
				data: 'command',
                name: 'command',
		
            },
            {
                data: 'provider',
                name: 'provider',
		
            },
            {
                data: 'cat',
                name: 'cat',
		
            },
            {
				data: 'description',
                name: 'description',
		
            },
            {
				data: 'sku_code',
                name: 'sku_code',
            },
            {
                data: 'biller.name',
                name: 'biller.name',
            },
            {
                data: 'solds',
                name: 'solds',
            },
          
        ],
		    columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-110'>" + data + "</div>";
                    },
                    targets: 5
                },
     
        ],
        select: {
            style: 'single'
        },
	
   		});
    }
   
	
     
    	


	

	});
	




	</script>
