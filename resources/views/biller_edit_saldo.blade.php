<div class="card card-primary">
  <div class="card-header">
  <h3 class="card-title">Tambah Saldo Biller {{$data['name']}}</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form class="form-horizontal" role="form" id="frmEditSaldo" enctype="multipart/form-data">
  @csrf
  <div class="card-body">
    

    <div class="form-group row">
    <label class="col-sm-2 col-form-label">email</label>
    <div class="col-sm-10">
    <input type="numeric" class="form-control" name="numeric"  required>
    </div>
    </div>
  
  
   

  </div>


  <!-- /.card-body -->
  <div class="card-footer">
  <button type="" id="editBillerSaldo"  data-id='{{$data["id"]}}' class="btn btn-info editBillerSaldo">Submit</button>
  <button id="cancelBtnEditSaldo" type="button" class="btn btn-default cancelBtnEditSaldo float-right">Cancel</button>
  </div>
  <!-- /.card-footer -->
  </form>
</div>
          <script>
            	$('#cancelBtnEditSaldo').click(function(){
			$('#editForm').fadeOut();
			$('#editFormSaldo').fadeOut();
			$('.btnEdit').fadeIn();
			$('.btnEditSaldo').fadeIn();
		});


    $(document).on("click", "#editBillerSaldo", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin Menambahkan Saldo Biller ? ', function(result) {
				if (result) {
          UpdateBillerSaldo($dataID);
				}
			}, '<span style="color: blue;">Tambah Saldo Biller</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
  
});

		function UpdateBillerSaldo(idProduct) {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/biller-update-saldo/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Saldo Biller Berhasil ditambahkan")
							$('#editForm').fadeOut();
							$('#editFormSaldo').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
							$('#editBtnSaldo').fadeIn();
            	$('#biller-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>