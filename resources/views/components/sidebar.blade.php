<div>
	<!-- Simplicity is the ultimate sophistication. - Leonardo da Vinci -->
</div>
<div id="left-sidebar" class="sidebar">
	<div class="navbar-brand" >
		<a href="/backoffice"><img src="images/zanpay_logo_blue.png" alt="ZanPay Logo" style="height:40px!important;width:78px!important;" class="mx-auto d-block logo">
		<button type="button" class="btn-toggle-offcanvas btn btn-sm btn-default float-right"><i class="lnr lnr-menu fa fa-chevron-circle-left"></i></button>
	</div>
	<div class="sidebar-scroll">
		<div class="user-account">
			<div class="user_div">
				<img src="{{$user->foto}}" class="user-photo" alt="User Profile Picture">
			</div>
			<div class="dropdown">
				<span>Welcome,</span>
				<a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{$user->nama}}</strong></a>
				<ul class="dropdown-menu dropdown-menu-right account">
					<li><a href="{{url('/profile')}}"><i class="icon-user"></i>My Profile</a></li>
					<!-- <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
					<li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li> -->
					<li class="divider"></li>
					<li class="logoutBtn"><a><i class="icon-power logoutBtn"></i>Logout</a></li>
				</ul>
			</div>
		</div>
		<nav id="left-sidebar-nav" class="sidebar-nav">
			<ul id="main-menu" class="metismenu">
				<li @if(Request::is('dashboard')) class="active" @endif><a href="dashboard"><i class="icon-home"></i><span>Dashboard</span></a></li>
				<li @if( Request::is('transaksi') || Request::is('live-transaksi') )  class="active" @endif><a href="transaksi"><i class="icon-bar-chart"></i><span>Transaksi</span></a></li>
				<li @if(Request::is('produk_postpaid') || Request::is('produk_prepaid') || Request::is('product_menu') || Request::is('provider') || Request::is('product_group')|| Request::is('parsing')|| Request::is('product-category'))|| Request::is('produk_terlaris')|| Request::is('product_biller') || Request::is('regex') ) class="active" @endif>
					<a href="#reseler_dan_grup" class="has-arrow"><i class="icon-drawer"></i><span>Produk dan Grup</span></a>
					<ul>
						<li @if(Request::is('produk_postpaid')) class="active" @endif><a href="produk_postpaid">Produk Postpaid</a></li>
						<li @if(Request::is('produk_prepaid')) class="active" @endif><a href="produk_prepaid">Produk Prepaid</a></li>
						<li @if(Request::is('product_biller')) class="active" @endif><a href="product_biller">Produk Biller</a></li>
						<li @if(Request::is('produk_terlaris')) class="active" @endif><a href="produk_terlaris">Produk Terlaris</a></li>
						<li @if(Request::is('product_menu')) class="active" @endif><a href="product_menu">Menu Produk</a></li>
						<li @if(Request::is('product_group')) class="active" @endif><a href="product_group">Produk Grup</a></li>
						<li @if(Request::is('product-category')) class="active" @endif><a href="product-category">Produk Kategori</a></li>
						<li @if(Request::is('provider')) class="active" @endif><a href="provider">Prefix Header</a></li>
						<li @if(Request::is('produk_parsing')) class="active" @endif><a href="produk_parsing">Parsing Produk</a></li>
						<li @if(Request::is('regex')) class="active" @endif><a href="regex">Regex</a></li>
					</ul>
				</li>
				<li @if( Request::is('voucher') || Request::is('voucher_log') ) class="active" @endif>
					<a href="#reseler_dan_grup" class="has-arrow"><i class="icon-trophy"></i><span>Voucher</span></a>
					<ul>
						<li @if(Request::is('voucher')) class="active" @endif><a href="voucher">Voucher</a></li>
						<li @if(Request::is('voucher_log')) class="active" @endif><a href="voucher_log">Riwayat Voucher</a></li>
					
					</ul>
				</li>
				<li @if(Request::is('saldo_agen') || Request::is('mutasi_saldo_agen')  || Request::is('tiket_deposit') || Request::is('payment_method')) class="active" @endif>
				<!-- <li @if(Request::is('saldo_agen') || Request::is('mutasi_saldo_agen') || Request::is('saldo_biller') || Request::is('mutasi_saldo_biller') || Request::is('tiket_deposit') || Request::is('payment_method')) class="active" @endif> -->
					<a href="#mutasi" class="has-arrow"><i class="icon-wallet"></i><span>Saldo dan Mutasi</span></a>
					<ul>
						<!-- <li @if(Request::is('saldo_agen')) class="active" @endif><a href="saldo_agen">Reseller/Daftar Agen</a></li> -->
						<li @if(Request::is('mutasi_saldo_agen')) class="active" @endif><a href="mutasi_saldo_agen">Mutasi Saldo Agen</a></li>
						<!-- <li @if(Request::is('saldo_biller')) class="active" @endif><a href="saldo_biller">Saldo Biller</a></li>
						<li @if(Request::is('mutasi_saldo_biller')) class="active" @endif><a href="mutasi_saldo_biller">Mutasi Saldo Biller</a></li> -->
						<li @if(Request::is('tiket_deposit')) class="active" @endif><a href="tiket_deposit">Tiket Deposit</a></li>
						<li @if(Request::is('payment_method')) class="active" @endif><a href="payment_method">Payment Setting</a></li>
						<li @if(Request::is('transfer')) class="active" @endif><a href="transfer">Transfer</a></li>
					</ul>
				</li>
				<li @if(Request::is('news') || Request::is('news_category')) class="active" @endif>
					<a href="#news" class="has-arrow"><i class="icon-feed"></i><span>Berita</span></a>
					<ul>
						<li @if(Request::is('news')) class="active" @endif><a href="news">Berita</a></li>
						<li @if(Request::is('news_category')) class="active" @endif><a href="news_category">Kategori Berita</a></li>
					</ul>
				</li>
				<li @if(Request::is('banner')) class="active" @endif><a href="banner"><i class="icon-frame"></i><span>Banner</span></a></li>
				<li @if(Request::is('merchandise') || Request::is('merchandise_category')) class="active" @endif>
					<a href="#merchandise" class="has-arrow"><i class="icon-briefcase"></i><span>Merchandise</span></a>
					<ul>
						<li @if(Request::is('merchandise')) class="active" @endif><a href="merchandise">Merchandise</a></li>
						<li @if(Request::is('merchandise_category')) class="active" @endif><a href="merchandise_category">Kategori Merchandise</a></li>
					</ul>
				</li>
				<!-- <li @if(Request::is('message')) class="active" @endif><a href="message"><i class="icon-envelope"></i><span>Pesan</span></a></li> -->
				<!-- <li @if(Request::is('outbox')) class="active" @endif><a href="outbox"><i class="icon-call-out"></i><span>Outbox</span></a></li> -->
				<li @if(Request::is('reseller') || Request::is('reseller-commission') || Request::is('grup_reseller') || Request::is('document') || Request::is('notification') ) class="active" @endif>
					<a href="#reseler_dan_grup" class="has-arrow"><i class="icon-user"></i><span>Reseler dan Grup</span></a>
					<ul>
						<li @if(Request::is('reseller')) class="active" @endif><a href="reseller">Reseller</a></li>
						<li @if(Request::is('reseller-commission')) class="active" @endif><a href="reseller-commission">Downline dan Komisi</a></li>
						<li @if(Request::is('grup_reseller')) class="active" @endif><a href="grup_reseller">Grup Reseller</a></li>
						<li @if(Request::is('document')) class="active" @endif><a href="document">Dokumen</a></li>
						<li @if(Request::is('notification')) class="active" @endif><a href="notification">Notifikasi</a></li>
					</ul>
				</li>
				<li @if(Request::is('biller') || Request::is('mutasi_saldo_biller')) class="active" @endif>
					<a href="#biller" class="has-arrow"><i class="icon-arrow-up"></i><span>Biller H2H</span></a>
					<ul>
						<li @if(Request::is('biller')) class="active" @endif><a href="biller">Data Biller</a></li>
						<li @if(Request::is('mutasi_saldo_biller')) class="active" @endif><a href="mutasi_saldo_biller">Mutasi Saldo Biller</a></li>
					</ul>
				</li>
			
			
				<!-- <li @if(Request::is('produk_parsing')) class="active" @endif><a href="produk_parsing"><i class="icon-puzzle"></i><span>Parsing Produk</span></a></li> -->
				<li @if(Request::is('daftar-pekerjaan')) class="active" @endif><a href="daftar-pekerjaan"><i class="icon-calculator"></i><span>Daftar Pekerjaan</span></a></li>
				
				<li @if( Request::is('video_promosi') || Request::is('terms') ||  Request::is('feature') || Request::is('terms_privacy') || Request::is('about_us') || Request::is('social_media') ||Request::is('contact_us') || Request::is('team') || Request::is('contact_message') || Request::is('faqs') )  class="active" @endif>
					<a href="#web-config" class="has-arrow"><i class="icon-settings"></i><span>Web Config</span></a>
					<ul>
						<li @if(Request::is('video_promosi')) class="active" @endif><a href="video_promosi">Video Promosi</a></li>
						<li @if(Request::is('home_page')) class="active" @endif><a href="home_page">Home Page</a></li>
						<li @if(Request::is('feature')) class="active" @endif><a href="feature">Features</a></li>
						<li @if(Request::is('terms')) class="active" @endif><a href="terms">Syarat dan Ketentuan</a></li>
						<li @if(Request::is('terms_privacy')) class="active" @endif><a href="terms_privacy">Privacy Policy</a></li>
						<li @if(Request::is('about_us')) class="active" @endif><a href="about_us">About Us</a></li>
						<li @if(Request::is('contact_us')) class="active" @endif><a href="contact_us">Contact Us</a></li>
						<li @if(Request::is('social_media')) class="active" @endif><a href="social_media">Social Media</a></li>
						<li @if(Request::is('team')) class="active" @endif><a href="team">Team</a></li>
						<li @if(Request::is('contact_message')) class="active" @endif><a href="contact_message">Inbox</a></li>
						<li @if(Request::is('faqs')) class="active" @endif><a href="faqs">FAQs</a></li>
					</ul>
				</li>
				@if($user->role === 'super_admin')
					<li @if( Request::is('user') || Request::is('user') )  class="active" @endif><a href="user"><i class="icon-user"></i><span>User</span></a></li>
				@endif
			</ul>
		</nav>
	</div>
</div>
