<!doctype html>
<html lang="en">

<head>
	<title>zanpay</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="HexaBit Bootstrap 4x Admin Template">
	<meta name="author">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">

	<link rel="stylesheet" href="{{ asset('vendor/charts-c3/plugin.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/chartist/css/chartist.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/morrisjs/morris.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">

	<link rel="stylesheet" href="{{ asset('vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/sweetalert/sweetalert.css') }}" />
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">


	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('css/override.css') }}">
	<link rel="stylesheet" href="{{ asset('css/inbox.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/color_skins.css?v=').time() }}">
</head>

<body class="theme-blue">

	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img src="{{ asset('images/zanpay.png') }}" width="200" height="200" alt="HexaBit"></div>
			<p>Mohon Tunggu...</p>
		</div>
	</div>
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>

	<div id="wrapper">

		<nav class="navbar navbar-fixed-top">
			<div class="container-fluid">

				<div class="navbar-left">
					<div class="navbar-btn">
						<!-- <a href="index.html"><img src="{{ asset('images/icon-light.svg') }}" alt="HexaBit Logo" class="img-fluid logo"></a> -->
						<button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
					</div>
					<!-- <a href="{{ url()->previous() }}" class="icon-menu btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> -->
					<ul class="nav navbar-nav">
						<li><a href="calendar" class="icon-menu d-none d-sm-block d-md-none d-lg-block"><i class="icon-calendar"></i></a></li>
						<!-- <li><a href="chat" class="icon-menu d-none d-sm-block"><i class="icon-bubbles"></i></a>
						</li> -->
					</ul>
				</div>

				<div class="navbar-right">
					<div id="navbar-menu">
						<ul class="nav navbar-nav">
						<li class="dropdown dropdown-animated scale-left">
								<a href="whatsapp"  class="dropdown-toggle icon-menu">
									<i class="fa fa-whatsapp"></i>
									<span class="notification"></span>
								</a>
							</li>
						@if($data['unread_count'] == 0)
							<li class="dropdown dropdown-animated scale-left">
								<a href="chat" class="dropdown-toggle icon-menu">
									<i class="icon-envelope"></i>
									<span class="notification"></span>
								</a>
							</li>
						@else
							<li class="dropdown dropdown-animated scale-left">
								<a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
									<i class="icon-envelope"></i>
									<span class="notification-dot"></span>
								</a>
							
								<ul class="dropdown-menu right_chat email">
								
								@foreach ($data['data'] as $v)
									<li>
										<a href="{{url('/chat?message_id=').$v['message_id'].'&sender_id='. $v['sender']['id']}}">
											<div class="media">
												<img class="media-object " src="{{ $v['sender']['avatar'] }}" alt="">
												<div class="media-body">
													<span class="name">{{$v['sender']['name']}} <small class="float-right">{{$v['datetime']}}</small></span>
													<span class="message">{{$v['pesan']}}</span>
												</div>
											</div>
										</a>
									</li>
								@endforeach
								</ul>
							</li>
						@endif
							<!-- <li class="dropdown dropdown-animated scale-left">
								<a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
									<i class="icon-bell"></i>
									<span class="notification-dot"></span>
								</a>
								<ul class="dropdown-menu feeds_widget">
									<li class="header">You have 5 new Notifications</li>
									<li>
										<a href="javascript:void(0);">
											<div class="feeds-left"><i class="fa fa-thumbs-o-up text-success"></i></div>
											<div class="feeds-body">
												<h4 class="title text-success">7 New Feedback <small class="float-right text-muted">Today</small></h4>
												<small>It will give a smart finishing to your site</small>
											</div>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);">
											<div class="feeds-left"><i class="fa fa-user"></i></div>
											<div class="feeds-body">
												<h4 class="title">New User <small class="float-right text-muted">10:45</small></h4>
												<small>I feel great! Thanks team</small>
											</div>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);">
											<div class="feeds-left"><i class="fa fa-question-circle text-warning"></i>
											</div>
											<div class="feeds-body">
												<h4 class="title text-warning">Server Warning <small class="float-right text-muted">10:50</small></h4>
												<small>Your connection is not private</small>
											</div>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);">
											<div class="feeds-left"><i class="fa fa-check text-danger"></i></div>
											<div class="feeds-body">
												<h4 class="title text-danger">Issue Fixed <small class="float-right text-muted">11:05</small></h4>
												<small>WE have fix all Design bug with Responsive</small>
											</div>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);">
											<div class="feeds-left"><i class="fa fa-shopping-basket"></i></div>
											<div class="feeds-body">
												<h4 class="title">7 New Orders <small class="float-right text-muted">11:35</small></h4>
												<small>You received a new oder from Tina.</small>
											</div>
										</a>
									</li>
								</ul>
							</li> -->
							<li><a href="javascript:void(0);" class="icon-menu logoutBtn"><i class="icon-power"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>

		
