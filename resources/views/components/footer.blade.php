</div>

<!-- Javascript -->
<script src="{{ asset('bundles/libscripts.bundle.js') }}"></script>
<script src="{{ asset('bundles/vendorscripts.bundle.js') }}"></script>

<script src="{{ asset('bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('bundles/chartist.bundle.js') }}"></script>
<script src="{{ asset('bundles/morrisscripts.bundle.js') }}"></script>
<script src="{{ asset('vendor/toastr/toastr.js') }}"></script>

<script src="{{ asset('bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('js/index.js') }}"></script>
<script src="{{ asset('js/pages/blog.js') }}"></script>
<script src="{{ asset('bundles/jvectormap.bundle.js') }}"></script>

<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>

<script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>

<script src="{{ asset('bundles/fullcalendarscripts.bundle.js') }}"></script>
<!--/ calender javascripts -->

<script src="{{ asset('bundles/fullcalendarscripts.bundle.js') }}"></script>
<script src="{{ asset('vendor/fullcalendar/fullcalendar.js') }}"></script>
<script src="{{ asset('js/pages/calendar.js') }}"></script>
<!--/ calender javascripts -->
<script src="{{ asset('bundles/knob.bundle.js') }}"></script><!-- Jquery Knob-->
<script src="{{ asset('bundles/mainscripts.bundle.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js" defer></script> -->
<script src="{{ asset('summernote.js') }}" defer></script>
<script src="{{ asset('js/widgets/infobox/infobox-1.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script>
const base_URL = "{{ url('/') }}";
</script>
</body>

</html>
