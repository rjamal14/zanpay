<x-package-header />
<style>
.fa.disabled,
.fa[disabled],
.disabled > .fa,
[disabled] > .fa {
  opacity: 0.5;
  /*optional*/ cursor: not-allowed !important;
  /*optional*/ pointer-events: none !important;
}	
a.disabled {
  cursor: not-allowed !important;
}
a.disabled btn {
  pointer-events: none !important;
}
div.disabled  {
	cursor: not-allowed !important;
}
a:disabled {
  cursor: not-allowed !important;
  pointer-events: all !important;
}
.disabled > .a,
[disabled] > .a {
  opacity: 0.5;
  /*optional*/ cursor: not-allowed !important;
  /*optional*/ pointer-events: none !important;
}	
.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		/* font-size: 10.5px; */
	}

	.table-custom td {
		font-size: 9px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 10px;
	}

	.table-custom-info tr {
		font-size: 10px;
	}
	
	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-6 col-sm-12">
				<h2>Transaksi</h2>
			</div>
			<div class="col-md-6  col-sm-6 text-right">
				<button class="inboxBiller btn btn-sm btn-primary mr-1">Inbox & Outbox</button>
				<button class="liveTransaksi btn btn-sm btn-primary mr-1">Live Transaksi</button>
				<button class="btn btn-sm btn-primary mr-1 exportExcel">Export Excel</button>
				<!-- <button class="btn btn-sm btn-primary mr-1 exportCsv">Export CSV</button> -->
			</div>
		</div>
	</div>
	<div  class="card-body" id="editForm"></div>

	<div id="inboxBiller" class="container-fluid" style="display:none;">

	<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						
					<div class="col-md-12  col-sm-12 text-right">
								
									<button class="mainContent btn btn-sm btn-default mr-1"> <i class="fa fa-arrow-left">&nbsp; Kembali ke transaksi</i></button>

								
					
					</div>

					</div>
					<div class="body">

							<div class="form-group row">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-4">
											<label class="control-label col-lg-12 cursor-pointer startDate2" for="start_date2">Filter Tanggal:</label>
											<div class="col-sm-12">
												<button type="button" class="btn btn-primary daterange-ranges2">
													<i class="fa fa-calendar position-left"></i>
													<span>{{ date('d M Y') }} - {{ date('d M Y') }}</span>
													<b class="caret"></b>
												</button>
											</div>
										</div>
										<div class="col-lg-2">
											<label class="control-label col-lg-12 cursor-pointer startDate2" for="start_date2"></label>
										
										</div>
										<div class="col-lg-2">
											<label class="control-label col-lg-12 cursor-pointer startDate2" for="start_date2">Filter Order ID:</label>
											<div class="col-lg-12">
												<input type="search" class="form-control" id="filterOrderID" placeholder="Filter Order ID ..." >
											</div>
										</div>
										<div class="col-lg-2">
											<label class="control-label col-lg-12 cursor-pointer startDate2" for="start_date2">Filter Biller:</label>
											<div class="col-lg-12">
												<input type="search" class="form-control" id="filterBiller2" placeholder="Filter biller ..." >
											</div>
										</div>
										<div class="col-lg-2">
											<label class="control-label col-lg-12 cursor-pointer startDate2" for="start_date2">&nbsp;</label>
											<input type="text" class="form-control" placeholder="Cari..." name="searchInput searchInputInbox"  id="searchInput">
										</div>
									</div>
								</div>
							
							
							</div>
					<br>

						<div class="table-responsive">
						<table style="" class="table table-bordered table-hover c_list  table-custom "  id="inbox-biller-table">
								<thead>
									<tr>
										<th width="110px;">Order Id</th>
										<th width="90px;">Tanggal</th>
										<th width="90px;">Nama Biller</th>
										<th width="90px;">URL Biller</th>
										<th>Request</th>
										<th>Respond</th>
										<th>Callback</th>
										<th width="90px;">Status</th>
									</tr>
								</thead>
							
							</table>
						</div>
					</div>
					<!-- <div class="footer">
						
						<div class="col-md-12  col-sm-12 text-right">
							<button class="mainContent btn btn-sm btn-default mr-1"> <i class="fa fa-arrow-left">&nbsp; Kembali ke transaksi</i></button>
						</div>
	
					</div> -->
				</div>
			</div>
		</div>

	</div>
	<div id="mainContent" class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
					<div class="d-flex" style="margin-bottom: 25.75px;">
							<div class="mr-auto">
								<table border="0" class="table-custom-info">
									<tr>
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Tanggal:</label>
									<div class="col-lg-12">
										<button type="button" class="btn btn-primary daterange-ranges">
											<i class="fa fa-calendar position-left"></i>
											<span>{{ date('d M Y') }} - {{ date('d M Y') }}</span>
											<input type="hidden" id="startDate" />
											<input type="hidden" id="endDate" />
											<b class="caret"></b>
										</button>
									</div>
									</tr>
									<tr >
										<label style="margin-top:30px;" class="control-label col-lg-12 cursor-pointer">Status Modul: <span id="txt_status_mdl"></span></label>
										<div class="col-lg-12">
											<btn class="btn btn-success" id="btn_status_mdl"></btn>
										</div>
									</tr>
								
								</table>
							</div>
							<div class="ml-auto">
								<table border="0" class="table-custom-info">
								<tr>
										<td>Tanggal</td>
										<td style="padding: 0 10px;"> : </td>
										<td class="tanggaldipilih">{{$curDate}}</td>
									</tr>
	
									<tr>
										<td>Transaksi Sukses</td>
										<td style="padding: 0 10px;">:</td>
										<td  style="" class="jumlahTransaksiSukses">120.89 Trx</td>
									</tr>
									<tr>
										<td>Transaksi Gagal</td>
										<td style="padding: 0 10px;">:</td>
										<td  style="" class="jumlahTransaksiGagal">120.89 Trx</td>
									</tr>
									<tr>
										<td>Transaksi Proses</td>
										<td style="padding: 0 10px;">:</td>
										<td  style="" class="jumlahTransaksiProses">120.89 Trx</td>
									</tr>
									<tr>
										<td>Transaksi Antrian</td>
										<td style="padding: 0 10px;">:</td>
										<td  style="" class="jumlahTransaksiAntrian">120.89 Trx</td>
									</tr>
	
									<tr>
										<td>Jml TRX</td>
										<td style="padding: 0 10px;">:</td>
										<td  style="font-weight:bold;" class="jumlahTransaksi">120.89 Trx</td>
									</tr>
									<tr>
										<td>Omzet</td>
										<td style="padding: 0 10px;">:</td>
										<td style="font-weight:bold;"  class="omzet"><b>Rp. 1.520.000.000</b></td>
									</tr>
								
								</table>
							</div>
							<div class="ml-auto">
								<table border="0" class="table-custom-info detailSaldoH2h">
								</table>
								<table border="0" class="table-custom-info">
								
								
								</table>
							</div>
						</div>
					
					</div>

						<div class="d-flex" style="margin-bottom: 25.75px;margin-top: 25.75px;">
							<div class="mr-auto">
								<table border="0" class="table-custom-info">
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Biller:</label>
										<div class="col-lg-12">
									
											<input type="search" class="form-control" id="filterBiller" placeholder="Filter biller ..." >
										</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Produk:</label>
										<div class="col-lg-12">
											<input type="search" class="form-control" id="filterProduk" placeholder="Filter Produk ..." >
										</div>
									</th>
							
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Tujuan:</label>
										<div class="col-lg-12">
											<input type="search" class="form-control" id="filterTujuan" placeholder="Filter Tujuan ..." >
										</div>
									</th>

									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label>
									<div class="col-lg-12">
										<select  class="form-control " id="filterStatus" >
											<option value=""  selected="selected">Semua Status</option>
											<option value="Q">ANTRIAN</option>
											<option value="S">SUKSES</option>
											<option value="F">GAGAL</option>
											<option value="P">PROSES</option>
											<option value="qweqweqwe">BELUM SELESAI</option>
										</select>
									</div>
									</th>
									<th width="200px">
										<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Urutkan Berdasarkan:</label>
										<div class="col-lg-12">
											<select  class="form-control " id="sortType" >
												<option value="">Pilih</option>
												<option value="0">Produk Terlaris</option>
												<option value="1">Produk Terbaru</option>
												<option value="2">Nama A-Z</option>
												<option value="3">Nama Z-A</option>
											</select>
										</div>
									</th>
									<th >
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">&nbsp;</label>
									<div class="d-flex justify-content-center">
										<a class="btn btn-xl   btn-danger" onclick="cancelBulk()" id='batalkanSemua' href="javascript:void(0)"> <i class="fa fa-info"></i><small>&nbsp;&nbsp;Batalkan Semua Transaksi Pending</small></a>
									</div>
									</th>
									<th >
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">&nbsp;</label>
									<div style="margin-left:10px!important;margin-right:15px!important;" id='resendSemua' class="d-flex justify-content-center">
										<a  class="btn btn-xl   btn-success"  onclick="resendBulk()"href="javascript:void(0)"> <i class="fa fa-send"></i><small>&nbsp;&nbsp;Kirim Ulang Semua Transaksi Pending</small></a>
									</div>
									</th>
								
								</table>
							</div>
						</div>

					<div class="body">
					
						<div class="table-responsive">
						<!-- <div class="form-group" >
							<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Tanggal:</label>
							<div class="col-lg-12">
								<button type="button" class="btn btn-default daterange-ranges">
									<i class="icon-calendar22 position-left"></i>
									<span>{{ date('Y-m-d', strtotime('-1 year')) }} - {{ date('d M Y') }}</span>
									<b class="caret"></b>
								</button>
							</div>
						</div> -->
					
						<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom "  id="transaction-table">
								<thead>
									<tr>
										<th>TRXID</th>
										<th>Tanggal Masuk</th>
										<th>Tanggal Update</th>
										<th>ID Agen</th>
										<th>Nama Reseller</th>
										<th>Group</th>
										<th>Jenis</th>
										<!-- <th>Produk</th> -->
										<th>SKU</th>
										<th>No.Tujuan</th>
										<th>Harga Beli</th>
										<th>Harga Jual</th>
										<th>Laba</th>
										<th>Biller</th>
										<th>Saldo Biller</th>
										<th>SN</th>
										<th>Status</th>
										<th></th>
									</tr>
								</thead>
							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<label>Tanggal</label>
				<div class="input-daterange input-group" data-provide="datepicker">
					<input type="text" class="input-sm form-control" name="start">
					<span class="input-group-addon range-to">to</span>
					<input type="text" class="input-sm form-control" name="end">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Apply Filter</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="dateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<label>Tanggal</label>
				<div class="input-daterange input-group" data-provide="datepicker">
					<input type="text" class="input-sm form-control" name="start">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Apply Filter</button>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script>
var _baseURL = "<?php echo url(''); ?>";
var startDate;
var endDate;

		$('.mainContent').click(function(){
				$('#mainContent').fadeIn();
				$('#inboxBiller').fadeOut();
				$('#transaction-table').DataTable().ajax.reload();
		});


$(document).ready(()=>{
	$.ajax({
		url:'{{url("/transaksi-module-status/")}}',
		type:'GET',
		dataType:'json',
		success:function(data){
			if(data.data.status == 'R'){
				$("#btn_status_mdl").attr('data', 'active');
				$("#btn_status_mdl").removeClass('btn-success');
				$("#btn_status_mdl").addClass('btn-danger');
				$("#btn_status_mdl").html('Matikan Module');
				$("#txt_status_mdl").removeClass('text-danger');
				$("#txt_status_mdl").addClass('text-success');
				$("#txt_status_mdl").html('Aktif');
			}else{
				$("#btn_status_mdl").attr('data', 'non-active');
				$("#btn_status_mdl").removeClass('btn-danger');
				$("#btn_status_mdl").addClass('btn-success');
				$("#btn_status_mdl").html('Hidupkan Module');
				$("#txt_status_mdl").removeClass('text-success');
				$("#txt_status_mdl").addClass('text-danger');
				$("#txt_status_mdl").html('Tidak aktif');
			}
		},
		error:function(data) {
			console.log(data)
		}
	});	
})

$(document).on("click", "#btn_status_mdl", ()=>{
	if($("#btn_status_mdl").attr('data')=="active"){
		
		showConfirm('Apakah anda Yakin ingin matikan module? ', function(result) {
			if (result) {
				$.ajax({
					url:'https://zanpay.co.id/api/module.php',
					type:'POST',
					dataType:'json',
					data: {
						"admin_id": "{{$data['user']}}",
						"module": "flag_trx",
						"action": "stop"
					},
					success:function(data){
						$("#btn_status_mdl").attr('data', 'non-active');
						$("#btn_status_mdl").removeClass('btn-danger');
						$("#btn_status_mdl").addClass('btn-success');
						$("#btn_status_mdl").html('Matikan Module');
						$("#txt_status_mdl").removeClass('text-success');
						$("#txt_status_mdl").addClass('text-danger');
						$("#txt_status_mdl").html('Tidak aktif');
						console.log(data)
						bootbox.alert("Modul dimatikan");
					},
					error:function(data) {
						bootbox.alert("Gagal matikan module");
						console.log(data)
					}
				});	
			
			}
		}, '<span style="color: blue;">Matikan module</span>', {
			confirm: {
				label: 'Matikan',
				className: 'btn-danger'
			},
			cancel: {
				label: 'Cancel',
				className: 'btn-default'
			}
		});
	}else{

		$.ajax({
			url:'https://zanpay.co.id/api/module.php',
			type:'POST',
			dataType:'json',
			data: {
				"admin_id": "{{$data['user']}}",
				"module": "flag_trx",
				"action": "run"
			},
			success:function(data){
				$("#btn_status_mdl").attr('data', 'active');
				$("#btn_status_mdl").removeClass('btn-success');
				$("#btn_status_mdl").addClass('btn-danger');
				$("#btn_status_mdl").html('Hidupkan Module');
				$("#txt_status_mdl").removeClass('text-danger');
				$("#txt_status_mdl").addClass('text-success');
				$("#txt_status_mdl").html('Aktif');
			},
			error:function(data) {
				bootbox.alert("Berhasil aktifkan module");
				console.log(data)
			}
		});	
	}

})
	var trx_code, sku_code, custid
		function resendRequest(trx_code,sku_code,custid){
			showConfirm('Apakah anda Yakin ingin Mengirim ulang request ? ', function(result) {
					if (result) {
						$.ajax({
						url:'{{url("/transaksi-pending-resend/")}}',
						type:'POST',
						dataType:'json',
						data: {
							"_token": "{{ csrf_token() }}",
							"trx_code": trx_code,
							"sku_code": sku_code,
							"custid": custid,
						},
						beforeSend:function(){
				
						},
						success:function(data){
							console.log(data)
							bootbox.alert(data.message);
							$('#transaction-table').DataTable().ajax.reload();
						},
						error:function(data) {
							$('#transaction-table').DataTable().ajax.reload();
							console.log(data)
						}
						});	
					
					}
				}, '<span style="color: blue;">Resend Request</span>', {
					confirm: {
						label: 'Resend',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			
		}
		function cancelRequest(trx_code,sku_code,custid){
			showConfirm('Apakah anda Yakin ingin me-refund  transaksi ? ', function(result) {
					if (result) {
						$.ajax({
						url:'{{url("/transaksi-pending-cancel/")}}',
						type:'POST',
						dataType:'json',
						data: {
							"_token": "{{ csrf_token() }}",
							"trx_code": trx_code,
							"sku_code": sku_code,
							"custid": custid,
						},
						beforeSend:function(){
				
						},
						success:function(data){
							bootbox.alert(data.message)
							$('#transaction-table').DataTable().ajax.reload();
							console.log(data)
						},
						error:function(data) {
							$('#transaction-table').DataTable().ajax.reload();
							console.log(data)
						}
						});	
					
					}
				}, '<span style="color: blue;">Refund Transaksi</span>', {
					confirm: {
						label: 'Refund',
						className: 'btn-danger'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		}
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": true,
			"progressBar": true,
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function cancelRequest2(trx_code,sku_code,custid){
			bootbox.prompt({
            title: "Apakah anda Yakin ingin me-refund  transaksi ?",
            inputType: 'textarea',
            buttons: {
                confirm: {
                    label: 'Batalkan Transaksi',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'kembali',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                console.log(result);
					if(result != null){
						$reason = $('textarea.bootbox-input-textarea').val();
						quoteidforcustomer = $(this).attr("data-id")
						if (!$reason){ 
							toastr.error('Mohon isi keterangan.')
							$('.bootbox-input-textarea').focus()
							return false; 
						} else {
							$.ajax({
						url:'{{url("/transaksi-pending-cancel/")}}',
						type:'POST',
						dataType:'json',
						data: {
							"_token": "{{ csrf_token() }}",
							"trx_code": trx_code,
							"sku_code": sku_code,
							"custid": custid,
							"reason": $reason,
						},
						beforeSend:function(){
				
						},
						success:function(data){
							$('#transaction-table').DataTable().ajax.reload();
							console.log(data)
						},
						error:function(data) {
							$('#transaction-table').DataTable().ajax.reload();
							console.log(data)
						}
						});	
						}
					}
				}
			});
			
		}
	
	function cancelBulk(){
		showConfirm('Apakah anda Yakin ingin me-refund  semua transaksi "antrian" ? ', function(result) {
					if (result) {
						
					
					}
				}, '<span style="color: blue;">Refund Transaksi</span>', {
					confirm: {
						label: 'Refund',
						className: 'btn-danger'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
	}
	function resendBulk(){
		showConfirm('Apakah anda Yakin ingin mengirim ulang semua transaksi "antrian" ? ', function(result) {
					if (result) {
						$.ajax({
						url:'{{url("/transaksi-bulk-resend/")}}',
						type:'POST',
						dataType:'json',
						data: {
							"_token": "{{ csrf_token() }}",
							
						},
						beforeSend:function(){
				
						},
						success:function(data){
							$('#transaction-table').DataTable().ajax.reload();
						bootbox.alert(data.message)
						},
						error:function(data) {
							$('#transaction-table').DataTable().ajax.reload();
						bootbox.alert(data.message)
						}
						});	
					
					}
				}, '<span style="color: blue;">Kirim Ulang Transaksi</span>', {
					confirm: {
						label: 'Resend',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
	}


	$(function() {


    var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		
    var datePicker = $('.daterange-ranges').daterangepicker({
        startDate:moment(),
        endDate: moment(),
        ranges: {
            'Hari ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
            'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],        
            '2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
        },
        locale: { 
            cancelLabel: 'Batal', 
            applyLabel: 'Proses',
			daysOfWeek: [
							"Sen",
							"Sel",
							"Rab",
							"Kam",
							"Jum",
							"Sab",
							"Min"
						],
			monthNames: [
				"Januari",
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
            	'Desember'
        	],
        },
        opens: 'right',
        showCustomRangeLabel: false,
        alwaysShowCalendars: true,
        applyButtonClasses: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default',
		maxDate: new Date(),
		
		},
		// Datepickerrange callback
		datepickerRangeCallback =  function(start, end) {
			$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
		}, datepickerRangeCallback);

		// $('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' +  moment().format('DD MMM YYYY'));
		
		var oDataList = $('#transaction-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: true,
			ordering: false,
			"lengthMenu": [[ 25, 50, -1], [ '25 rows', '50 rows', 'Show all']],
			"bAutoWidth" : false,
			"bLengthChange": true,
			"dom": '<"top"i>rt<"bottom"flp><"clear">',
			sDom: 'lrtip',
			ajax: {
				url: "{{ url('transaksi-list') }}",
				data: function (d) {
                d.startDate = datePicker.data('daterangepicker').startDate.format('YYYY-M-DD')
                d.endDate = datePicker.data('daterangepicker').endDate.format('YYYY-M-DD')
                d.sortType = $('#sortType').val()
                d.filterBiller = $('#filterBiller').val()
                d.filterProduk = $('#filterProduk').val()
                d.filterTujuan = $('#filterTujuan').val()
                d.filterStatus = $('#filterStatus').val()
            	}
			},
		    "fnDrawCallback": function( data ,e) {

		
				$('.tanggaldipilih').empty().html(data.json.tanggalTerpilih);
				$('.jumlahTransaksi').empty().html(data.json.jumlahTransaksi);
				$('.jumlahTransaksiSukses').empty().html(data.json.jumlahTransaksiSukses);
				$('.jumlahTransaksiGagal').empty().html(data.json.jumlahTransaksiGagal);
				$('.jumlahTransaksiProses').empty().html(data.json.jumlahTransaksiProses);
				$('.jumlahTransaksiAntrian').empty().html(data.json.jumlahTransaksiAntrian);
				$('.omzet').empty().html(data.json.omzet);
				$('.saldoH2h').empty().html(data.json.saldoH2h);
				$('.detailSaldoH2h').empty().html(data.json.c);
				$('.saldoAgen').empty().html(data.json.saldoAgen);
				},
		

			columns: [
			
		
				// {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
				// 	orderable: false},
				// {
				//     data: 'status',
				//     name: 'status',
			
				// },
				{
					data: 'trx_code',//0
					name: 'trx_code',
					width: "6%",
			
				},
				{
					data: 'tanggal_masuk',//1
					name: 'tanggal_masuk',
					width: "6%",
			
				},
				{
					data: 'tanggalUpdate',//1
					name: 'tanggalUpdate',
					width: "6%",
			
				},
				{
					data: 'reseller.reseller_code',//2
					name: 'reseller.reseller_code',
					width: "6%",
			
				},
				{
					data: 'resellername',//2
					name: 'resellername',
					width: "6%",
			
				},
				{
					data: 'jenis',//3
					name: 'jenis',
					width: "6%",
			
				},
				{
					data: 'group',//4
					name: 'group',
					width: "6%",
			
				},
				
				// {
				//     data: 'produk.description',
				//     name: 'produk.description',
			
				// },
			
				{
					data: 'sku_code',//5
					name: 'produk.sku_code',
					width: "6%",
			
				},
				{
					data: 'custid',
					name: 'custid',//6
					width: "6%",
			
				},
				{
					data: 'price_biller',
					name: 'price_biller',//7
					width: "6%",
			
				},
				{
					data: 'price_reseller',//8
					name: 'price_reseller',
					width: "6%",
			
				},
				{
					data: 'laba',
					name: 'laba',//9
					width: "6%",
			
				},
				{
					data: 'biller',//10
					name: 'biller',
					width: "6%",
			
				},
				{
					data: 'saldoBiller',//11
					name: 'saldoBiller',//12
					render: function(data, type) {
						return 'Rp '+numeral(data).format('0,0')
					},
					width: "6%",
			
				},
				{
					data: 'SN',
					name: 'SN',//13
					width: "6%",
			
				},
			
				{
					data: 'statusText',//14
					name:'statusText',
					width: "6%",
				}
				,
				{
					data: 'action',//15
					name:'Tindakan',
					searchable: false,
					orderable: false
				},
				{
					data: 'statusOrigin',
					name:'statusOrigin',//16
					visible: false,
						searchable: true,
				},
			],
			buttons: [
				{ 
					extend: 'excel',
					title: function() {
						return 'Export Transaksi Tanggal ' + datePicker.data('daterangepicker').startDate.format('YYYY-M-DD') +" s.d "+ datePicker.data('daterangepicker').endDate.format('YYYY-M-DD')
					},
					action: function ( e, dt, node, config ) {
						// dt.one('preXhr', function (e, s, data)
						// {
						// 	data.length = -1;
						// // }).one('draw', function (e, settings, json, xhr)
						// // {
							// // 	var excelButtonConfig = $.fn.DataTable.ext.buttons.excelHtml5;
							// // 	var addOptions = { exportOptions: { 'columns': ':all'} };
				
						// // 	$.extend(true, excelButtonConfig, addOptions);
						// // 	excelButtonConfig.action.call(this, e, dt, node, config);
						// // }).draw();
						// }).draw();
						
						dt.page.len(-1).draw(true);
						var _this = this;
						setTimeout(function() {
							$.fn.DataTable.ext.buttons.excelHtml5.action.call(_this, e, dt, node, config)
						}, 3000)

					},
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
					// {
					// 	render: function (data, type, full, meta) {
					// 		return "<div class='text-wrap width-50'>" + data + "</div>";
					// 	},
					// 	targets: 5
					// },
		
		
			],
			select: {
				style: 'single'
			},
		});

		$('.inboxBiller').click(function(){
				$('#mainContent').fadeOut();
				$('#inboxBiller').fadeIn();
				oDataList.ajax.reload();
		});
		$('.liveTransaksi').click(function(){
				window.location.href=_baseURL+'/live-transaksi';
		});
		$('#searchInput').on( 'keyup', function () {
			$('#inbox-biller-table').DataTable()
        .columns( 8 )
        .search( this.value )
        .draw();
		} );
		$('#filterBiller').on('keyup', function(){
			oDataList.draw(true);
			});
		$('#filterProduk').on('keyup', function(){
			oDataList.draw(true);
			});
		$('#filterTujuan').on('keyup', function(){
			oDataList.draw(true);
			});
		$('#filterStatus').on('change', function(){
			oDataList.draw(true);
			});
		$('#sortType').on('change', function(){
			oDataList.draw(true);
		});

		datePicker.on('apply.daterangepicker', function(ev, picker) {
			var startDate = picker.startDate.format('YYYY-M-DD');
			var endDate = picker.endDate.format('YYYY-M-DD');
			$("#startDate").val(startDate);
			$("#endDate").val(endDate);
			console.log(startDate);
			console.log(endDate);
			oDataList.draw(true);
		});
	

		var datePicker2 = $('.daterange-ranges2').daterangepicker({
        startDate:moment(),
        endDate: moment(),
        ranges: {
            'Hari ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
            'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],        
            '2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
        },
        locale: { 
            cancelLabel: 'Batal', 
            applyLabel: 'Proses',
			daysOfWeek: [
							"Sen",
							"Sel",
							"Rab",
							"Kam",
							"Jum",
							"Sab",
							"Min"
						],
			monthNames: [
				"Januari",
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
            	'Desember'
        	],
        },
        opens: 'right',
        showCustomRangeLabel: false,
        alwaysShowCalendars: true,
        applyButtonClasses: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default',
		maxDate: new Date(),
		
		},
		// Datepickerrange callback
		datepickerRangeCallback =  function(start, end) {
		$('.daterange-ranges2 span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
		}, datepickerRangeCallback);
		$('.daterange-ranges2 span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' +  moment().format('DD MMM YYYY'));

	
		var oDataList2 = $('#inbox-biller-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ordering: false,
			"bAutoWidth" : false,
			"bLengthChange": false,
			"dom": '<"top"i>rt<"bottom"flp><"clear">',
			sDom: 'lrtip',
			ajax: {
				url: "{{ url('inbox-biller-list') }}",
				data: function (d) {
					d.startDate = datePicker2.data('daterangepicker').startDate.format('YYYY-M-DD')
                	d.endDate = datePicker2.data('daterangepicker').endDate.format('YYYY-M-DD')
					d.filterBiller = $('#filterBiller2').val()
					d.filterOrderID = $('#filterOrderID').val()
					// d.searchInputInbox = $('#searchInputInbox').val()
            	}
			},
		    "fnDrawCallback": function( data ,e) {
	
			},
		

			columns: [
			
				{
					data: 'trx_code',//0
					name: 'trx_code',
			
				},
				{
					data: 'tanggal_masuk',
					name: 'tanggal_masuk',//1
			
				},
				{
					data: 'billerName',//2
					name: 'billerName',
			
				},
				{
					data: 'billerUrl',
					name: 'billerUrl',//3
			
				},
				{
					data: 'reqd',
					name: 'reqd',//4
			
				},
				
				{
				    data: 'respon',
				    name: 'respon',//5
			
				},
				{
				    data: 'callback',
				    name: 'callback',//6
			
				},
			
				{
					data: 'status',
					name: 'status',//7
			
				},
				{
					data: 'helper',//8
					name: 'helper',
					visible: false,
			
				},
			],
			buttons: [
			
			],
			columnDefs: [
					{	
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-200'>" + data + "</div>";
						},
						targets: 4
					},
					{	
						render: function (data, type, full, meta) {
							return "<div class='text-wrap' style=''>" + data + "</div>";
						},
						targets: 0
					},
					{	
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-200'>" + data + "</div>";
						},
						targets: 5
					},
					{	
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-200'>" + data + "</div>";
						},
						targets: 6
					},
				
			],
			select: {
				style: 'single'
			},
		});
		datePicker2.on('apply.daterangepicker', function(ev, picker) {
			oDataList2.draw(true);
		});
	
		
		$('#filterBiller2').on('keyup', function(){
			oDataList2.draw(true);
			});
		$('#filterOrderID').on('keyup', function(){
			oDataList2.draw(true);
			});
		
		$('#searchInputInbox').on('keyup', function(){
			oDataList2.draw(true);
			});
		
	
		$(document).on("click", ".exportCsv", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-csv' ).trigger();
					}
				}, '<span style="color: blue;">Export Transaksi</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
		$(document).on("click", ".exportExcel", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button( '.buttons-excel' ).trigger();
					
					}
				}, '<span style="color: blue;">Export Transaksi</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
		});
});
function editData(id,url ="{{url("/transaksi-edit/")}}"){
			$.ajax({
			url:url+'/'+id,
			type:'GET',
			dataType:'HTML',
			beforeSend:function(){
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();
     
			},
			success:function(data){
        		$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
			error:function(data) {
				$('#loadBar').hide();
				alert(url+'/'+id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
			});	
	}
function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}
</script>