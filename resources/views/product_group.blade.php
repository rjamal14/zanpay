<x-package-header />
<style>
	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 200px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {}

	.table-custom td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-6 col-sm-12">
				<h2>Group Produk</h2>
			</div>
			<div class="col-md-6  col-sm-6 text-right">
				<button id="addBtn" class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
				<!-- <button class="btn btn-sm btn-primary mr-1 exportCsv">Export CSV</button> -->
			</div>
		</div>
	</div>

	<div class="card-body" id="editForm"></div>
	<div class="card-body" id="addForm" style="display: none;">
		<div class="card card-primary">

			<div class="card-header">
				<h3 class="card-title"> Tambah Produk Grup</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col-sm-10">
							<div id="image_preview_create"></div>
							<input type="file" id="fileCreate" required name="avatar">
						</div>
					</div>




					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe</label>
						<div class="col-sm-10">
							<select name="group_name" style="width: 100%" id="type" class="form-control js-example-tags" required>
								@foreach ($type as $t)
								<option value="{{$t}}">{{$t}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Jenis</label>
						<div class="col-sm-10">

							<select name="command" style="width: 100%" id="command" class="form-control js-example-tags" required>
								@foreach($jenis as $j)
								<option value="{{$j['text']}}">{{$j['text']}}</option>
								@endforeach
							</select>

						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Provider</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="provider" value required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Prefix HLR</label>
						<div class="col-sm-10">

							<select name="prefix" style="width: 100%" id="prefix" class="form-control js-example-tags" required>
								<option value="0">Pilih</option>
								@foreach($prefix as $p)
								<option value="{{$p['no']}}">{{$p['provider']}} - {{$p['prefix_header']}}</option>
								@endforeach
							</select>

						</div>
					</div>


				</div>

				<!-- /.card-body -->
				<div class="card-footer">
					<button type="" id="addProduct" data-id='' class="btn btn-info addProduct">Submit</button>
					<button id="cancelBtnAdd" type="button" class="btn btn-default cancelBtnAdd float-right">Cancel</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>

	</div>
	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">

						<div class="body">

							<div class="table-responsive">


								<table class="table table-bordered table-hover c_list  table-custom " id="product_menu-table">
									<thead>
										<tr>
											<th>No.</th>
											<th>Tipe</th>
											<th>Jenis</th>
											<th>prefix </th>
											<th>Gambar</th>
											<th>Jumlah Produk</th>
											<th></th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<x-package-footer />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.js"></script>
	<script>
		$(document).ready(function() {
			$(".js-example-tags").select2({
				tags: true
			});
		});


		$("#fileCreate").change(function(e) {
			var _URL2 = window.URL || window.webkitURL;
			var file, img;
			if ((file = this.files[0])) {
				img = new Image();
				var objectUrl = _URL2.createObjectURL(file);
				$('#image_preview_create').empty();
				$('#image_preview_create').append("<img src='" + objectUrl + "' width='150px' style='border-radius: 2px;'> &nbsp");
			}
		});
		$(document).on("click", ".addProduct", function(e) {
			e.preventDefault();
			showConfirm('Apakah anda Yakin ingin menambahkan produk ? ', function(result) {
				if (result) {
					addProduct();
				}
			}, '<span style="color: blue;">Tambah Produk</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});


		function addProduct() {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmCreate").validate(optValidate);

			if ($("#frmCreate").valid()) {
				var form = $("#frmCreate")[0];
				var formData = new FormData(form);

				console.log($("#frmCreate").serialize());
				formData.append("type", 'prepaid');


				// formData.append("destinationDescription", $content.getData());
				var _baseURL = "<?php echo url(''); ?>";
				$.ajax({
					url: _baseURL + "/add-product-group",
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
							$('#product_menu-table').DataTable().ajax.reload();
							$('#addForm').fadeOut();
							$('#addBtn').fadeIn();
							toastr.success("Produk Berhasil ditambahkan")
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {},
					complete: function() {},
				});

				return false;
			}

		}

		$(function() {
			var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,
				$isImgEditable = false,
				$choosenBahanBaku = [],
				$variants, $choosenVariant = [],
				$status_filter = "",
				$outlet_filter = "",
				$is_paid = "",
				$paid_approval = "";

			var datePicker = $('.daterange-ranges').daterangepicker({
					startDate: moment(),
					endDate: moment(),
					ranges: {
						'Hari ini': [moment(), moment()],
						'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
						'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
						'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
						'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
						'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
						'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
						'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
					},
					locale: {
						cancelLabel: 'Batal',
						applyLabel: 'Proses',
						daysOfWeek: [
							"Sen",
							"Sel",
							"Rab",
							"Kam",
							"Jum",
							"Sab",
							"Min"
						],
						monthNames: [
							"Januari",
							'Februari',
							'Maret',
							'April',
							'Mei',
							'Juni',
							'Juli',
							'Agustus',
							'September',
							'Oktober',
							'November',
							'Desember'
						],
					},
					opens: 'right',
					showCustomRangeLabel: false,
					alwaysShowCalendars: true,
					applyButtonClasses: 'btn-small btn-primary',
					cancelClass: 'btn-small btn-default',
					maxDate: new Date(),

				},
				// Datepickerrange callback
				datepickerRangeCallback = function(start, end) {
					$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
				}, datepickerRangeCallback);
			$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + moment().format('DD MMM YYYY'));

			var oDataList = $('#product_menu-table').DataTable({
				processing: true,
				serverSide: true,
				autoWidth: false,

				"bAutoWidth": false,
				ajax: {
					url: "{{ url('product_group-list') }}",

				},
				"fnDrawCallback": function(data, e) {

				},


				dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

				columns: [


					{
						data: 'DT_RowIndex',
						name: 'DT_RowIndex',
						searchable: false,
						orderable: false
					},
					// {
					//     data: 'status',
					//     name: 'status',

					// }

					{
						data: 'group_name',
						name: 'group_name',

					},
					{
						data: 'nama',
						name: 'nama',

					},
					{
						data: 'prefiX',
						name: 'prefiX',

					},
					{
						data: 'gambar',
						name: 'gambar',

					},
					{
						data: 'products_count',
						name: 'products_count',

					},
					{
						data: 'action',
						name: 'Tindakan',
						searchable: false,
						orderable: false
					},
				],
				buttons: [

					{
						extend: 'csv',
						title: 'Export Menu Produk' + $.now(),
					},
				],
				columnDefs: [{

					},


				],
				select: {
					style: 'single'
				},
			});
			datePicker.on('apply.daterangepicker', function(ev, picker) {
				oDataList.draw(true);


			});



			$(document).on("click", ".exportCsv", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button('.buttons-csv').trigger();

					}
				}, '<span style="color: blue;">Export Menu Produk</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			});
			$(document).on("click", ".exportExcel", function(e) {
				showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
					if (result) {
						oDataList.button('.buttons-excel').trigger();

					}
				}, '<span style="color: blue;">Export Menu Produk</span>', {
					confirm: {
						label: 'Export',
						className: 'btn-primary'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				});
			});
		});
		var optValidate = {
			ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
			errorClass: 'validation-error-label',
			successClass: 'validation-valid-label',
			highlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},
			unhighlight: function(element, errorClass) {
				$(element).removeClass(errorClass);
			},

			// Different components require proper error label placement
			errorPlacement: function(error, element) {

				// Styled checkboxes, radios, bootstrap switch
				if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
					if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
						error.appendTo(element.parent().parent().parent().parent());
					} else {
						error.appendTo(element.parent().parent().parent().parent().parent());
					}
				}

				// Unstyled checkboxes, radios
				else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
					error.appendTo(element.parent().parent().parent());
				}

				// Input with icons and Select2
				else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
					error.appendTo(element.parent());
				}

				// Inline checkboxes, radios
				else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent());
				}

				// Input group, styled file input
				else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			},
			validClass: "validation-valid-label"


		};
		function deleteProduct($id) {
			// Setup validation
			deleteTableList(
				"product-group-delete", [$id],
				true
			);
			return false;

		}
		function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini'){
			if($ids) {
				bootbox.confirm({
					title: 'Delete Data',
					message: message,
					buttons: {
						confirm: {
							label: 'Delete',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if(result) {
							console.log(url)
							$.ajax({
								url: url+'/'+$ids,
								data: {
									"_token": "{{ csrf_token() }}",
								},
								dataType: 'json',
								type: 'post',
								success: function(d) {
									if(d.status == 200) {
									
										$('#product_menu-table').DataTable().ajax.reload();
										toastr.success("data berhasil dihapus")
									} else {
										toastr.error(d.message)
									}
								},
								error: function(d, statusText, xhr) {
									bootbox.alert('Oops. Error when processing your request. ' + xhr);
								},
								beforeSend: function () {
								},
								complete: function () {
								}
							});
						}
					}
				});
			} else {
				bootbox.alert("error");
			}

			return false;
		}	
		function editData(id, url = "{{url("/product_group-edit/")}}") {
			$.ajax({
				url: url + '/' + id,
				type: 'GET',
				dataType: 'HTML',
				beforeSend: function() {
					$('#editForm').empty();
					$('.btnEdit').fadeOut();
					$('#loadBar').show();

				},
				success: function(data) {
					$('#addform').fadeOut();
					$('#loadBar').hide();
					$('#formWrapper').fadeOut();
					$('#editForm').append(data);
					$('#editForm').fadeIn();
					$("html, body").animate({
						scrollTop: 0
					}, "slow");

				},
				error: function(data) {
					$('#loadBar').hide();
					alert(url + '/' + id);
					alert(data);
					alert('500 : Internal server error');
					$('.btnEdit').fadeIn()
				},
				complete: function() {
					$("#typeEdit").select2({
						tags: true
					});
					console.log('ok')
				}
			});
		}

		$('.btnEdit').click(function() {
			$('#addForm').fadeOut();
		});
		$('#addBtn').click(function() {
			$('#addBtn').fadeOut();
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
			$('#addForm').fadeIn();
		});

		$('#cancelBtnAdd').click(function() {
			$('#addForm').fadeOut();
			$('#addBtn').fadeIn();
		});


		function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
			if (!$buttons) {
				$buttons = {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				};
			}
			bootbox.confirm({
				title: $title,
				message: $message,
				buttons: $buttons,
				callback: $callback
			});
		}
	</script>