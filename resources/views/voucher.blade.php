<meta name="csrf-token" content="{{ csrf_token() }}" />
<x-package-header />
<style>
	.image-container-product2 {
		border-radius: 8px;
		background-color: #7d85c9;
		width: 195px;
		height: 195px;
		position: relative;
		overflow: hidden;
	}

	.image-container-product2 a {
		width: 100%;
		text-align: center;
		padding: 5px 0;
		background-color: #5671c9;
		color: #FFF;
		display: inline-block;
	}

	.image-container-product2.image-exist a {
		width: 49%;
	}

	.image-container-product2 .overlay {
		position: absolute;
		bottom: 0;
		left: 0;
		right: 0;
		overflow: hidden;
		width: 100%;
		height: 0;
		transition: .5s ease;
		z-index: 100;
	}

	.image-container-product2:hover .overlay,
	.image-container-product2 img:hover .overlay {
		height: 30px;
	}

	.image-container-product2 img {
		position: absolute;
		left: 50%;
		top: 50%;
		height: 100%;
		width: auto;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}



	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 200px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Voucher</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<!-- <button class="btn btn-sm btn-primary mr-1 exportExcel">
					<i class="fa fa-download"></i> Export Excel
				</button> -->

				<button id="addBtn" class="btn btn-sm btn-primary mr-1">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>

	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
		<br>
		<center><i class="fa fa-spinner fa-spin"></i></center>
	</div>

	<div class="card-body" id="addForm" style="display: none;">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Tambah Voucher</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
				@csrf
				<div class="card-body">

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama Voucher</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="title" required>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Group</label>
						<div class="col-sm-10">
							<select name="group" id="groupActivity" class="voucherCodeEdit form-control" required>
								<option data-id=" " value="">-- Pilih Group --</option>
								@foreach ($activityGroup as $g)
								<option data-id="{{$g['text']}}" value="{{$g['id']}}">{{$g['name']}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row Aktivitas" style="display:none;">
						<label class="col-sm-2 col-form-label">Aktivitas</label>
						<div class="col-sm-10">
							<select name="activity" id="activity" class="voucherCode form-control">


							</select>
						</div>
					</div>

					<div class="form-group row Jnis" style="display:none;">
						<label class="col-sm-2 col-form-label">Aktivitas</label>
						<div class="col-sm-10">
							<select name="jenis" id="jnis" class="voucherCode form-control">


							</select>
						</div>
					</div>
					<div class="form-group row Povider" style="display:none;">
						<label class="col-sm-2 col-form-label">Provider</label>
						<div class="col-sm-10">
							<select name="provider" id="povider" class="voucherCode form-control">


							</select>
						</div>
					</div>

					<div class="form-group row RefrensiID" style="display:none;">
						<label class="col-sm-2 col-form-label">Refrensi ID</label>
						<div class="col-sm-10">
							<select name="reference_id[]" multiple="multiple" width="100%" id="RefrensiID" class="js-example-basic-multiple voucherCode form-control">
							</select>
						</div>
					</div>


					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Deskripsi</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="description" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Syarat</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="requirement" required>
						</div>
					</div>


					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe Voucher</label>
						<div class="col-sm-10">
							<select name="type" id="voucherType" class="voucherCode form-control" required>

								@foreach ($tipe as $ti)
								<option data-id="{{$ti['text']}}" value="{{$ti['id']}}">{{$ti['name']}}</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tipe Reward</label>
						<div class="col-sm-10">
							<select name="reward_type" id="reward_type" id="rewardType" class="form-control" required>

								@foreach ($rewardType as $rw)
								<option value="{{$rw['id']}}">{{$rw['name']}}</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Jumlah Reward</label>
						<div class="col-sm-10">
							<input type="number" class="voucherCode form-control" id="rewardEdit" name="reward" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Maksimal</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="maximum" required>
						</div>
					</div>
				
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Quota </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="quota" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Pemakaian </label>
						<div class="col-sm-10">
							<select name="multiple"  id="pemakaianID" class="form-control" required>
							<option value="">--Pilih--</option>
							<option value="0">Hanya Sekali</option>
							<option value="1">Berulang Kali</option>
							</select>
						</div>
					</div>
					<div id='periodID' style="display:none;">
						<div class="form-group row" >
							<label class="col-sm-2 col-form-label">Periode</label>
							<div class="col-sm-10">
								<select name="periode" id="mID" class=" form-control" required>
								<option value="1">Harian</option>
								<option value="2">Mingguan</option>
								<option value="3">Bulanan</option>
								</select>
							</div>
						</div>
						<div class="form-group row" >
							<label class="col-sm-2 col-form-label">Kuantitas</label>
							<div class="col-sm-10">
								<input type="number" maxlength="3" class="form-control" id="KuantitasID" name="many_times" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tanggal Berlaku </label>
						<div class="col-sm-2">
							<input type="date" id="fDate" onBlur="getMinDate()" class="form-control" placeholder="dd-mm-yyyy" value="{{date('Y-m-d')}}" min="1997-01-01" max="2030-12-31" name="created_at" required>
							<!-- <span class="flex"style="display:block;">&nbsp;s/d&nbsp;</span> -->
						</div>
						<label style="margin-left:-20px" class="col-sm-1 text-center col-form-label">s/d</label>
						<div class="col-sm-2" style="margin-left:-20px">
							<input type="date" id="tDate" class="form-control" placeholder="dd-mm-yyyy" value="{{date('Y-m-d')}}" min="1997-01-01" max="2030-12-31" name="expired_at" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Kode Voucher </label>
						<div class="col-sm-10">
							<input type="text"  class="form-control" id="voucherCode" name="code" required>
						</div>
					</div>



				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" id="addVoucher" class="btn btn-info addVoucher">Submit</button>
					<button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>

	<div class="card-body" id="editForm"></div>


	<div class="container-fluid">
		<div class="row clearfix">

			<div class="card">

				<div class="body">
					<div class="table-responsive">
						<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom voucher-table" id="voucher-table">
							<thead>
								<tr>
									<th>No</th>
									<th>Kode Voucher</th>
									<th>Nama Voucher</th>
									<th>Deskripsi</th>
									<th>Aktivitas</th>
									<th>Tipe Voucher</th>
									<th>Syarat</th>
									<th>Reward</th>
									<th>Maksimal</th>
									<th>Quota</th>
									<th>Sisa Quota</th>
									<th>Pemakaian</th>
									<th>Edit Terakhir</th>
									<th>Oleh</th>
									<th>Aksi</th>
								</tr>
							</thead>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



</div>
<x-package-footer />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	// var today = new Date();
	// var dd = today.getDate();
	// var mm = today.getMonth() + 1; //January is 0!
	// var yyyy = today.getFullYear();

	// if (dd < 10) {
	//   dd = '0' + dd
	// }

	// if (mm < 10) {
	//   mm = '0' + mm
	// }

	// today = yyyy + '-' + mm + '-' + dd;
	// // document.getElementById("fDate").setAttribute("min", today);

	// function getMinDate() {
	//   var minToDate = document.getElementById("fDate").value;
	//   document.getElementById("tDate").setAttribute("min", minToDate);
	// }



	$(document).ready(function() {
		$('.js-example-basic-multiple').select2({
			"width": '100%'

		});



		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	});
	$(document).on("change blur", ".voucherCode", function() {
		$type = $('#voucherType').children('option:selected').data('id');
		$gc = $('#groupActivity').children('option:selected').data('id');
		if ($('#groupActivity').children('option:selected').val() === "0") {
			$ac = $('#activity').children('option:selected').data('txt');
		} else if ($('#groupActivity').children('option:selected').val() === "1") {
			let c = $('#povider').children('option:selected').data('txt')
			$ac = $('#jnis').children('option:selected').data('txt');
		} else {
			$ac = "";
		}
		if ($('#rewardEdit').val().length == 1) {
			$rwd = "0" + $('#rewardEdit').val();
		} else {

			$rwd = $('#rewardEdit').val();
		}
		console.log($type, $gc, $ac, $rwd);
		let $code = "V" + $type + $gc + $rwd;
		// let $code ="V"+$type+$gc+$ac+$rwd;
		$("#voucherCode").val($code);
	});
	$(document).on("change", "#pemakaianID", function() {
		if($('#pemakaianID').children('option:selected').val()== 1){
		$("#periodID").show();
		$('#mID').prop('required', true);
		$('#KuantitasID').prop('required', true);
		} else {
			$("#periodID").hide();
		$('#mID').prop('required', false);
		$('#KuantitasID').prop('required', false);
		}
	
	});
	$(document).on("change", "#activity", function() {
		$("#povider").empty();
		$("#jnis").empty();
		$("#RefrensiID").empty();
		$thisId =$('#activity').children('option:selected').val();
		$.ajax({
				url:'{{url("/voucher-combobox-byref?")}}type='+$thisId+'&group='+$('#groupActivity').children('option:selected').val(),
				type:'GET',
				dataType:'json',
				beforeSend:function(){

				},
				success:function(data){
					console.log(data.data);
					var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

					$("#RefrensiID").empty();
					if(data.data.length > 0){
						$.each(data.data, function (i,val) {
							appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

						});
					$("#RefrensiID").append(appendsEdit);
					}
				},

		});	
	});
	$(document).on("change", "#povider", function() {
	
		$("#RefrensiID").empty();
		$.ajax({
				url:'{{url("/voucher-combobox-byref?")}}type='+$('#povider').children('option:selected').val()+'&group='+$('#groupActivity').children('option:selected').val(),
				type:'GET',
				dataType:'json',
				beforeSend:function(){

				},
				success:function(data){
					console.log(data.data);
					var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

					$("#RefrensiID").empty();
					if(data.data.length > 0){
						$.each(data.data, function (i,val) {
							appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

						});
					$("#RefrensiID").append(appendsEdit);
					}
				},

		});	
	});
	$(document).on("change", "#groupActivity", function() {
		$("#povider").empty();
		$("#jnis").empty();
		$("#RefrensiID").empty();
		$("#activity").empty();
		console.log($(this).children('option:selected').val());
		$thisId = $(this).children('option:selected').val();
		if ($thisId === "0" || $thisId === "1") {
			//	
			$('.RefrensiID').show();
			if ($thisId === "0") {
				$('#activity').prop('required', true);
				$('#jnis').prop('required', false);
				$('#povider').prop('required', false);
				$('.Aktivitas').show();
				$('.Jnis').hide();
				$('.Povider').hide();
				$.ajax({
					url: '{{url("/voucher-combobox-bygroup?")}}type=' + $thisId,
					type: 'GET',
					dataType: 'json',
					beforeSend: function() {

					},
					success: function(data) {
						console.log(data.data);
						var appendsEdit = '<option data-id="" value="">-- Pilih --</option>';

						$("#activity").empty();
						if (data.data.length > 0) {
							$.each(data.data, function(i, val) {
								appendsEdit += '<option data-id="' + val.id + '" data-txt="' + val.name + '" value="' + val.id + '">' + val.text + '</option>';

							});
							$("#activity").append(appendsEdit);
						}
					},

				});
				// $.ajax({
				// 	url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
				// 	type:'GET',
				// 	dataType:'json',
				// 	beforeSend:function(){

				// 	},
				// 	success:function(data){
				// 		console.log(data.data);
				// 		var appendsEdit='<option data-id="" value="">-- Pilih --</option>';

				// 		$("#RefrensiID").empty();
				// 		if(data.data.length > 0){
				// 			$.each(data.data, function (i,val) {
				// 				appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

				// 			});
				// 		$("#RefrensiID").append(appendsEdit);
				// 		}
				// 	},

				// });	
			}
			if ($thisId === "1") {
				$('#activity').prop('required', false);
				$('#jnis').prop('required', true);
				$('#povider').prop('required', true);
				$('.Aktivitas').hide();
				$('.Jnis').show();
				$('.Povider').show();
				$.ajax({
					url: '{{url("/voucher-combobox-bygroup?")}}type=' + $thisId,
					type: 'GET',
					dataType: 'json',
					beforeSend: function() {

					},
					success: function(data) {
						console.log(data.data);
						var appendsEdit = '<option data-id="" value="">-- Pilih --</option>';

						$("#jnis").empty();
						if (data.data.length > 0) {
							$.each(data.data, function(i, val) {
								appendsEdit += '<option  data-val="' + val.text + '" data-txt="' + val.name + '" data-id="' + val.id + '" value="' + val.id + '">' + val.text + '</option>';

							});
							$("#jnis").append(appendsEdit);
						}
					},

				});
				// $.ajax({
				// 	url:'{{url("/voucher-combobox-byref?")}}type='+$thisId,
				// 	type:'GET',
				// 	dataType:'json',
				// 	beforeSend:function(){

				// 	},
				// 	success:function(data){
				// 		console.log(data.data);
				// 		var appendsEdit='<option data-id="" value="">-- Pilih --</option>';
				// 		$("#RefrensiID").empty();
				// 		if(data.data.length > 0){
				// 			$.each(data.data, function (i,val) {
				// 				appendsEdit+='<option data-id="'+val.id+'" data-txt="'+val.name+'" value="'+val.id+'">'+ val.text +'</option>';

				// 			});
				// 		$("#RefrensiID").append(appendsEdit);
				// 		}
				// 	},

				// });	
			} else {
				$('#activity').prop('required', false);
				$('#jnis').prop('required', false);
				$('#povider').prop('required', false);
			}
			//


		} else {
			$('.Aktivitas').hide();
			$('.RefrensiID').hide();
			$('.Jnis').hide();
			$('.Povider').hide();
		}



	});

	$(document).on("change", "#jnis", function() {
		$thisCommand = $(this).children('option:selected').data('val');
		$.ajax({
			url: '{{url("/voucher-combobox-bygroup2?")}}',
			type: 'POST',
			dataType: 'json',
			data: {
				'command': $thisCommand
			},
			beforeSend: function() {

			},
			success: function(data) {
				console.log(data.data);
				var appendsEdit = '<option data-id="" value="">-- Pilih --</option>';

				$("#povider").empty();
				if (data.data.length > 0) {
					$.each(data.data, function(i, val) {
						appendsEdit += '<option data-id="' + val.id + '" data-txt="' + val.text + '" value="' + val.id + '">' + val.text + '</option>';

					});
					$("#povider").append(appendsEdit);
				}
			},

		});
	});


	$(function() {
		var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,
			$isImgEditable = false,
			$choosenBahanBaku = [],
			$variants, $choosenVariant = [],
			$status_filter = "",
			$outlet_filter = "",
			$is_paid = "",
			$paid_approval = "";

		var datePicker = $('.daterange-ranges').daterangepicker({
				startDate: moment(),
				endDate: moment(),
				ranges: {
					'Hari ini': [moment(), moment()],
					'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
					'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
					'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
					'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
					'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
					'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
				},
				locale: {
					cancelLabel: 'Batal',
					applyLabel: 'Proses',
					daysOfWeek: [
						"Sen",
						"Sel",
						"Rab",
						"Kam",
						"Jum",
						"Sab",
						"Min"
					],
					monthNames: [
						"Januari",
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					],
				},
				opens: 'right',
				showCustomRangeLabel: false,
				alwaysShowCalendars: true,
				applyButtonClasses: 'btn-small btn-primary',
				cancelClass: 'btn-small btn-default',
				maxDate: new Date(),

			},
			// Datepickerrange callback
			datepickerRangeCallback = function(start, end) {
				$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
			}, datepickerRangeCallback);
		$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + moment().format('DD MMM YYYY'));




		var oDataList = $('#voucher-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,

			"bAutoWidth": false,
			"lengthChange": false,
			ajax: {
				url: "{{ url('voucher-list') }}",
				data: {


				},
			},
			"fnDrawCallback": function(data, e) {


			},


			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

			columns: [


				{
					data: 'DT_RowIndex',
					name: 'DT_RowIndex',
					searchable: false,
					orderable: false
				},
				// {
				//     data: 'status',
				//     name: 'status',

				// },
				{
					data: 'code',
					name: 'code',
				},
				{
					data: 'title',
					name: 'title',
				},
				{
					data: 'description',
					name: 'description',
				},
				{
					data: 'aktivitas',
					name: 'aktivitas',
				},
				{
					data: 'tipe',
					name: 'tipe',
				},

				{
					data: 'syarat',
					name: 'syarat',
				},
				{
					data: 'nominal',
					name: 'nominal',
				},
				{
					data: 'maxim',
					name: 'maxim',
				},
				{
					data: 'quota',
					name: 'quota',
				},
				{
					data: 'quota_left',
					name: 'quota_left',
				},
				{
					data: 'pemakaian',
					name: 'pemakaian',
				},
				{
					data: 'updatedAt',
					name: 'updatedAt',
				},
				{
					data: 'updatedBy',
					name: 'updatedBy',

				},
				{
					data: 'action',
					name: 'Tindakan',
					searchable: false,
					orderable: false
				},

			],
			buttons: [{
					extend: 'excel',
					title: 'Export voucher Tanggal: ',
				},
				{
					extend: 'csv',
					title: 'Export Transaksi' + $.now(),
				},
			],
			columnDefs: [{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-80'>" + data + "</div>";
					},
					targets: 2
				},
				{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-100'>" + data + "</div>";
					},
					targets: 3
				},
				{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-80'>" + data + "</div>";
					},
					targets: 10
				},
				{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-50'>" + data + "</div>";
					},
					targets: 11
				},


			],
			select: {
				style: 'single'
			},
		});





		$(document).on("click", ".exportCsv", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-csv').trigger();

				}
			}, '<span style="color: blue;">Export Transaksi</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		$(document).on("click", ".exportExcel", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-excel').trigger();

				}
			}, '<span style="color: blue;">Export Transaksi</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
	});

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}



	$('.btnEdit').click(function() {
		$('#addForm').fadeOut();
	});




	function editData(id, url = "{{url("/voucher-edit/")}}") {
		$.ajax({
			url: url + '/' + id,
			type: 'GET',
			dataType: 'HTML',
			beforeSend: function() {
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();

			},
			success: function(data) {
				$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({
					scrollTop: 0
				}, "slow");
			},
			error: function(data) {
				$('#loadBar').hide();
				alert(url + '/' + id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
		});
	}

	$('.btnEdit').click(function() {
		$('#addForm').fadeOut();
	});
	$('#addBtn').click(function() {
		$('#addBtn').fadeOut();
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
		$('#addForm').fadeIn();

	});

	$('#cancelBtnAdd').click(function() {
		$('#addForm').fadeOut();
		$('#addBtn').fadeIn();
	});

	$(document).on("click", ".addVoucher", function(e) {
		e.preventDefault();
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreate").validate(optValidate);
		if ($("#frmCreate").valid()) {
			showConfirm('Apakah anda Yakin ingin menambahkan voucher ? ', function(result) {
				if (result) {
					addVoucher();
				}
			}, '<span style="color: blue;">Tambah voucher</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		} else {
			return;
		}
	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}


	function preview_image() {
		$('#image_preview').empty();
		$('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
	}


	function addVoucher() {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreate").validate(optValidate);

		if ($("#frmCreate").valid()) {
			var form = $("#frmCreate")[0];
			var formData = new FormData(form);

			console.log($("#frmCreate").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/add-voucher",
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#voucher-table').DataTable().ajax.reload();
						$('#addForm').fadeOut();
						// $('#addForm').empty();
						$('#addBtn').fadeIn();
						toastr.success("voucher Berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;
		}

	}

	function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini') {
		if ($ids) {
			bootbox.confirm({
				title: 'Delete Data',
				message: message,
				buttons: {
					confirm: {
						label: 'Delete',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				},
				callback: function(result) {
					if (result) {
						console.log(url)
						$.ajax({
							url: url + '/' + $ids,
							data: {
								"_token": "{{ csrf_token() }}",
							},
							dataType: 'json',
							type: 'post',
							success: function(d) {
								if (d.status == 200) {

									$('#voucher-table').DataTable().ajax.reload();
									toastr.success("voucher berhasil dihapus")
								} else {
									toastr.error(d.message)
								}
							},
							error: function(d, statusText, xhr) {
								bootbox.alert('Oops. Error when processing your request. ' + xhr);
							},
							beforeSend: function() {},
							complete: function() {}
						});
					}
				}
			});
		} else {
			bootbox.alert("error");
		}

		return false;
	}

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}
	var _baseURL = "<?php echo url(''); ?>";
	var optValidate = {
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		errorClass: 'validation-error-label',
		successClass: 'validation-valid-label',
		highlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		// Different components require proper error label placement
		errorPlacement: function(error, element) {

			// Styled checkboxes, radios, bootstrap switch
			if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
				if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent().parent().parent());
				} else {
					error.appendTo(element.parent().parent().parent().parent().parent());
				}
			}

			// Unstyled checkboxes, radios
			else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
				error.appendTo(element.parent().parent().parent());
			}

			// Input with icons and Select2
			else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
				error.appendTo(element.parent());
			}

			// Inline checkboxes, radios
			else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
				error.appendTo(element.parent().parent());
			}

			// Input group, styled file input
			else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
				error.appendTo(element.parent().parent());
			} else {
				error.insertAfter(element);
			}
		},
		validClass: "validation-valid-label"


	};

	function deleteProduct($id) {
		// Setup validation
		deleteTableList(
			"delete-product", [$id],
			true
		);
		return false;

	}

	function deleteProduct($id) {
		// Setup validation
		deleteTableList(
			"delete-voucher", [$id],
			true
		);
		return false;
	}
</script>