<div class="card card-primary">

  <div class="card-header">
    <h3 class="card-title"> Edit Reseller</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Avatar</label>
        <div class="col-sm-10">
          <div id="image_preview_edit">
            <img src="{{$data['avatar']}}" width="150px;" style="border-radius: 2px;">
          </div>
          <input type="file" id="fileEdit" name="avatar" onclick="preview_image_edit()">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Group Reseller</label>
        <div class="col-sm-10">
          <select name="reseller_group" id="reseller_group" class="form-control" required>

            @foreach ($group as $g)
            <option value="{{$g['id']}}" {{$data['reseller_group'] == $g['id '] ? "selected":""}}>{{$g['name']}}</option>
            @endforeach

          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Reseller Code</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="reseller_code" value="{{$data['reseller_code']}}" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Virtual Account</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="virtual_account" value="{{$data['virtual_account']}}" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="name" value="{{$data['name']}}" required>
        </div>
      </div>

      <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
						<label class="fancy-radio custom-color-green"><input name="is_deleted" value="0" type="radio"><span><i></i>Aktif</span></label>
						<label class="fancy-radio custom-color-red"><input name="is_deleted" value="1" type="radio"><span><i></i>Tidak Aktif</span></label>
                    </div>
                  </div> -->
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Nomor HP</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="phone" value="{{$data['phone']}}" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Pin</label>
        <div class="col-sm-10">
          <input type="number" class="form-control" name="pin" value="{{$data['pin']}}" required>
        </div>
      </div>
      <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="command" required>
                    </div>
                  </div> -->
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" name="email" value="{{$data['email']}}" required>
        </div>
      </div>
      <!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Provider</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="provider" required>
                    </div>
                  </div> -->
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Platform</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="platform" value="{{$data['platform']}}" required>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
          <select name="status" id="status" class="form-control" required>

            <option value="0" {{$data['status'] == 0 ? "selected":""}}>Verifikasi</option>
            <option value="1" {{$data['status'] == 1 ? "selected":""}}>Kyc</option>
            <option value="2" {{$data['status'] == 2 ? "selected":""}}>Verify</option>
            <option value="3" {{$data['status'] == 3 ? "selected":""}}>Pending</option>

          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Keagenan</label>
        <div class="col-sm-10">
          <select name="is_active" id="is_actives" class="form-control" required>

            <option value="1" {{$data['is_active'] == 1 ? "selected":""}}>Aktif</option>
            <option value="0" {{$data['is_active'] == 0 ? "selected":""}}>Non Aktif</option>


          </select>
        </div>
      </div>


    </div>


    <!-- /.card-body -->
    <div class="card-footer">
      <button type="" id="editReseller" data-id='{{$data["id"]}}' class="btn btn-info editReseller">Submit</button>
      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
    </div>
    <!-- /.card-footer -->
  </form>
</div>
<script>
  $('#cancelBtnEdit').click(function() {
    $('#editForm').fadeOut();
    $('.btnEdit').fadeIn();
  });


  $(document).on("click", "#editReseller", function(e) {
    optValidate.rules = {};
    optValidate.message = {};
    $("#frmEdit").validate(optValidate);
    e.stopImmediatePropagation();
    e.preventDefault();
    console.log($(this).attr("data-id"));
    if ($("#frmEdit").valid()) {

      $dataID = $(this).attr("data-id");
      showConfirm('Apakah anda Yakin ingin mengupdate Reseller ? ', function(result) {
        if (result) {
          updateReseller($dataID);
        }
      }, '<span style="color: blue;">Edit Reseller</span>', {
        confirm: {
          label: 'Update',
          className: 'btn-primary'
        },
        cancel: {
          label: 'Cancel',
          className: 'btn-default'
        }
      });


    }

  });
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  $("#fileEdit").change(function(e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      var objectUrl = _URL2.createObjectURL(file);
      img.onload = function() {

        _URL2.revokeObjectURL(objectUrl);
        console.log(' Current Dimension : ' + this.width + " x " + this.height);
        $('#dimRec').text(' Current Dimension : ' + this.width + " x " + this.height);

        $('#fileEdit').removeClass('is-invalid');
        $('#btnEditEvent').prop('disabled', false);
        $('#dimRec').removeClass('error');
      };
      img.src = objectUrl;
      preview_image_edit();
    }
  });

  function preview_image_edit() {
    $('#image_preview_edit').empty();
    $('#image_preview_edit').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
  }

  function updateReseller(idProduct) {
    // Setup validation
    optValidate.rules = {};
    optValidate.message = {};
    $("#frmEdit").validate(optValidate);

    if ($("#frmEdit").valid()) {
      var form = $("#frmEdit")[0];
      var formData = new FormData(form);

      console.log($("#frmEdit").serialize());


      // formData.append("destinationDescription", $content.getData());

      $.ajax({
        url: _baseURL + "/reseller-update/" + idProduct,
        type: "post",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function(d) {
          console.log(toastr);
          if (d.status == 200) {
            toastr.success("Reseller Berhasil diupdate")
            $('#editForm').fadeOut();
            $('#addform').fadeOut();
            $('#editBtn').fadeIn();
            $('#reseller-table').DataTable().ajax.reload();


          } else {
            toastr.error(d.message)
          }
        },
        error: function(xhr) {
          console.log(xhr);
        },
        beforeSend: function() {},
        complete: function() {

        },
      });

      return false;
    }

  }
</script>