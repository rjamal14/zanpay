<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Edit Menu Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
			        @csrf
					<div class="card-body">
					<div class="form-group row">
							<label class="col-sm-2 col-form-label">Icon</label>
							<div class="col-sm-10">
								<div id="image_preview_edit">   
									<img src ="{{$data['image']}}" width="150px;" style="border-radius: 2px;">
								</div>
									<input type="file" id="fileEdit" name="avatar" onclick="preview_image_edit()">
							</div>
                		</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Nama</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="title" value ="{{$data['title']}}" required>
							</div>
						</div>


						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Tipe</label>
							<div class="col-sm-10">
							<select name="type" id="typeIDEdit" class="form-control" required>
								@foreach ($type as $t)
								<option value="{{$t}}" {{$data['type'] == $t ? "selected":""}}>{{$t}}</option>
								@endforeach
							</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Jenis</label>
							<div class="col-sm-10">
							<select name="command" id="commandEdit" class="form-control" required>
								@foreach ($jenis as $t)
								<option value="{{$t}}" {{$data['command'] == $t ? "selected":""}}>{{$t}}</option>
								@endforeach
							</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label"> Sub Jenis </label>
							<div class="col-sm-10">
							<select name="subcommand" id="subcommandEdit" class="form-control" >
								<option value="">Pilih Semua</option>
								@foreach ($jenisSub as $t)
								<option value="{{$t}}" {{$data['subcommand'] == $t ? "selected":""}}>{{$t}}</option>
								@endforeach
							</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label"> Group Menu </label>
							<div class="col-sm-10">
							<select name="group_menu" class="form-control" required>
								@foreach ($groupMenu as $t)
								<option value="{{$t}}" {{$data['group_menu'] == $t ? "selected":""}}>{{$t}}</option>
								@endforeach
							</select>
							</div>
						</div>

						<!-- <div class="form-group row">
							<label class="col-sm-2 col-form-label"> Status </label>
							<div class="col-sm-10">
							<select name="status"  class="form-control" required>
								@foreach ($status as $t)
								<option value="{{$t['id']}}" {{$data['status'] == $t['id'] ? "selected":""}}>{{$t['name']}}</option>
								@endforeach
							</select>
							</div>
						</div> -->
						
                	</div>
                  
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="editResellerGroup"  data-id='{{$data["id"]}}' class="btn btn-info editResellerGroup">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
$(document).on("change", "#typeIDEdit", function() {
					$thisId = $(this).children('option:selected').val();
					$.ajax({
						url:'{{url("/group-combobox-bygroup?")}}group_name='+$thisId,
						type:'GET',
						dataType:'json',
						beforeSend:function(){
							$("#commandEdit").empty();
							$("#subcommandEdit").empty();
						},
						success:function(data){
							console.log(data.data);
							var appendsEdit='<option data-id=""  value="">Pilih</option>';
						
							if(data.data.length > 0){
								$.each(data.data, function (i,val) {
									appendsEdit+='<option data-id="'+val.id+'"  value="'+val.text+'">'+ val.text +'</option>';
							
								});
							$("#commandEdit").append(appendsEdit);
							} else {
								$("#commandEdit").empty();
								$("#commandEdit").append('<option data-id=""  value="">tidak ada data</option>');
							}
						},
					});	
    });
$(document).on("change", "#commandEdit", function() {
					$thisId = $(this).children('option:selected').val();
					$.ajax({
						url:'{{url("/group-combobox-bygroup?")}}command='+$thisId,
						type:'GET',
						dataType:'json',
						beforeSend:function(){
							$("#subcommandEdit").empty();
						},
						success:function(data){
							console.log(data.data);
							var appendsEdit='<option data-id=""  value="">Pilih Semua</option>';
						
							if(data.data.length > 0){
								$.each(data.data, function (i,val) {
									appendsEdit+='<option data-id="'+val.id+'"  value="'+val.text+'">'+ val.text +'</option>';
							
								});
							$("#subcommandEdit").append(appendsEdit);
							} else {
								$("#subcommandEdit").empty();
								$("#subcommandEdit").append('<option data-id=""  value="">tidak ada data</option>');
							}
						},
					});	
    });


			  
            	$('#cancelBtnEdit').click(function(){
			$('#editForm').fadeOut();
			$('.btnEdit').fadeIn();
		});
		$("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});
		function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
    $(document).on("click", "#editResellerGroup", function(e) {
		e.stopImmediatePropagation();
			e.preventDefault();
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);
    
      console.log($(this).attr("data-id"));
      if ($("#frmEdit").valid()) {
      
        $dataID = $(this).attr("data-id");
			showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
				if (result) {
				updateResellerGroup($dataID);
				}
			}, '<span style="color: blue;">Edit Data </span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		function updateResellerGroup(idProduct) {
			// Setup validation
			var _baseURL = "<?php echo url(''); ?>";
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmEdit").validate(optValidate);

			if ($("#frmEdit").valid()) {
				var form = $("#frmEdit")[0];
				var formData = new FormData(form);

				console.log($("#frmEdit").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/product_menu-update/"+idProduct,
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Data Berhasil diupdate")
							$('#editForm').fadeOut();
							$('#addform').fadeOut();
							$('#editBtn').fadeIn();
              $('#product_menu-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>