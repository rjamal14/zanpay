<x-package-header />
<style>
	.image-container-product2 {
		border-radius: 8px;
		background-color: #7d85c9;
		width: 195px;
		height: 195px;
		position: relative;
		overflow: hidden;
	}

	.image-container-product2 a {
		width: 100%;
		text-align: center;
		padding: 5px 0;
		background-color: #5671c9;
		color: #FFF;
		display: inline-block;
	}

	.image-container-product2.image-exist a {
		width: 49%;
	}

	.image-container-product2 .overlay {
		position: absolute;
		bottom: 0;
		left: 0;
		right: 0;
		overflow: hidden;
		width: 100%;
		height: 0;
		transition: .5s ease;
		z-index: 100;
	}

	.image-container-product2:hover .overlay,
	.image-container-product2 img:hover .overlay {
		height: 30px;
	}

	.image-container-product2 img {
		position: absolute;
		left: 50%;
		top: 50%;
		height: 100%;
		width: auto;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}



	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 200px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {
		font-size: 10px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Reseller</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<button class="btn btn-sm btn-primary mr-1 exportExcel">
					<i class="fa fa-download"></i> Export Excel
				</button>

				<button id="addBtn" class="btn btn-sm btn-primary mr-1">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>

	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
		<br>
		<center><i class="fa fa-spinner fa-spin"></i></center>
	</div>

	<div class="card-body" id="addForm" style="display: none;">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title"> Tambah Reseller</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" role="form" id="frmCreate" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Avatar</label>
						<div class="col-sm-10">
							<div id="image_preview">

							</div>
							<input type="file" id="file" name="avatar">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Group Reseller</label>
						<div class="col-sm-10">
							<select name="reseller_group" id="reseller_group" class="form-control" required>

								@foreach ($group as $g)
								<option value="{{$g['id']}}">{{$g['name']}}</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Reseller Code</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="reseller_code" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Virtual Account</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="virtual_account" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="name" required>
						</div>
					</div>

					<!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
						<label class="fancy-radio custom-color-green"><input name="is_deleted" value="0" type="radio"><span><i></i>Aktif</span></label>
						<label class="fancy-radio custom-color-red"><input name="is_deleted" value="1" type="radio"><span><i></i>Tidak Aktif</span></label>
                    </div>
                  </div> -->
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nomor HP</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="phone" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Pin</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="pin" required>
						</div>
					</div>
					<!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="command" required>
                    </div>
                  </div> -->
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" name="email" required>
						</div>
					</div>
					<!-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Provider</label>
                    <div class="col-sm-10">
						<input type="text" class="form-control" name="provider" required>
                    </div>
                  </div> -->
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Platform</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="platform" required>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Status</label>
						<div class="col-sm-10">
							<select name="status" id="status" class="form-control" required>

								<option value="0" selected>Verifikasi</option>
								<option value="1">Kyc</option>
								<option value="2">Verify</option>
								<option value="3">Pending</option>

							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Keagenan</label>
						<div class="col-sm-10">
							<select name="is_active" id="is_actives" class="form-control" required>

								<option value="1" selected>Aktif</option>
								<option value="0" >Non Aktif</option>
							

							</select>
						</div>
					</div>

				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" id="addReseller" class="btn btn-info addReseller">Submit</button>
					<button id="cancelBtnAdd" type="button" class="btn btn-default float-right">Cancel</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>

	<div class="card-body" id="editForm"></div>


	<div class="container-fluid">
		<div class="row clearfix">
			<!-- <div class="col-lg-12">
			<div class="row clearfix">
					<div class="col-lg-3 col-md-6">
						<div class="card info-box-2">
							<div class="body">
								<div class="icon">
									<div class="chart chart-bar">6,4,8,6,8,10,5,6,7,9,5</div>
								</div>
								<div class="content">
									<div class="text">Populasi</div>
									<div class="number populasi">4,254</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="card info-box-2">
							<div class="body">
								<div class="icon">
									<div class="chart chart-pie" chartPopulasi>30, 35, 25, 8</div>
								</div>
								<div class="content">
									<div class="text">Member Aktif</div>
									<div class="number memberAktif">98%</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="card info-box-2">
							<div class="body">
								<div class="icon">
									<div class="chart chart-bar kunjunganChart">4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
								</div>
								<div class="content">
									<div class="text">Kunjungan</div>
									<div class="number kunjungan">1,195</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="card info-box-2">
							<div class="body">
								<div class="icon">
									<span class="chart chart-line pertumbuhanChar">9,4,6,5,6,4,7,3</span>
								</div>
								<div class="content">
									<div class="text">Pertumbuhan</div>
									<div class="number pertumbuhan">$1,243</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			<div class="card">
				<div class="header">
					<div class="d-flex">
						<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							<table border="0.1">
								<!-- <th width="200px" height="100px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Group:</label>
									<div class="col-lg-12">
										<select name="filterGroup" id="filterGroup" class="form-control select2" required>
										<option value=""> Semua Group</option>
										</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Platform:</label>
									<div class="col-lg-12">
									<select name="filterPlatform" id="filterPlatform" class="form-control select2" required>
									<option value=""> Semua Platform</option>
									</select>
									</div>
									</th>
									<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label>
									<div class="col-lg-12">
									<select name="filterStatus" id="filterStatus" class="form-control select2" required>
									<option value=""> Semua Status</option>
										</select>
									</div>
									</th> -->


								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Group:</label>
									<div class="col-lg-12">

										<input type="search" class="form-control" id="filterGroup" placeholder="Filter Group ...">
									</div>
								</th>
								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Platform:</label>
									<div class="col-lg-12">
										<input type="search" class="form-control" id="filterPlatform" placeholder="Filter Platform ...">
									</div>
								</th>

								<th width="200px">
									<label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label>
									<div class="col-lg-12">
										<select name="filterStatus" id="filterStatus" class="form-control select2" required>
											<option value=""> Semua Status</option>
											<option value="0"> Belum Verifikasi Email</option>
											<option value="1"> Belum Upload Dokumen</option>
											<option value="2"> Verifikasi</option>
											<option value="3"> Suspend</option>
										</select>
									</div>
								</th>




							</table>
						</div>

					</div>

				</div>
				<div class="body">
					<div class="table-responsive">
						<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom reseller-table" id="reseller-table">
							<thead>
								<tr>
									<th>Avatar</th>
									<th>ID</th>
									<th>Refferal</th>
									<th>Virtual Account</th>
									<th>Nama</th>
									<th>Nomor HP</th>
									<!-- <th>PIN</th> -->
									<th>Email</th>
									<th>Platform</th>
									<th>Grup</th>
									<th>Keagenan</th>
									<th>Saldo</th>
									<th>Tanggal Join</th>
									<th>Status</th>
									<th>Aktivitas Terakhir</th>
									<th>Aksi</th>
								</tr>
							</thead>

						</table>
					</div>
				</div>
			</div>


			<div class="card" id="history" style="display:none;">
				<div class="header">
					<div class="d-flex">
						<div class="mr-auto"> <b>History Cashback:</b>
							<div class="number namaReseller">-</div>


						</div>

					</div>

				</div>
				<div class="body">
					<div class="table-responsive">
						<table class="table" id="reseller-history-table">
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Deskripsi</th>
									<th>Debit</th>
									<th>Kredit</th>
									<th>Cashback</th>
								</tr>
							</thead>

						</table>
					</div>
				</div>

			</div>
			<div class="card">
				<div class="header">
					<div class="d-flex">
						<div class="mr-auto"> <b>Mapping Reseller</b>

						</div>

					</div>

				</div>
				<div class="body">
					<div class="col-lg-12 col-md-12">
						<div id="vmap" class="text-center" style="height: 380px; width: auto;"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	let colors = {
		"path01": "blue",
		"path32": "red",
		"path22": "green",

	}
	jQuery('#vmap').vectorMap({
		map: 'indonesia_id',
		backgroundColor: '#fff',
		borderColor: 'black',
		borderOpacity: 0.25,
		borderWidth: 1,
		color: 'grey',
		enableZoom: true,
		hoverColor: '#c9dfaf',
		hoverOpacity: null,
		normalizeFunction: 'linear',
		scaleColors: ['#b6d6ff', '#005ace'],
		// selectedColor: '#c9dfaf',
		selectedRegions: null,
		showTooltip: true,
		onRegionClick: function(element, code, region) {
			var message = 'You clicked "' +
				region +
				'" which has the code: ' +
				code.toUpperCase();

			alert(message);
		}
	});

	jQuery('#vmap').vectorMap('set', 'colors', colors);



	$(document).on("click", ".img-init2", function() {
		$(".product_img2").click();
	});

	$(document).on("change", ".product_img2", function() {
		readURL2(this);
	});

	$(document).on("click", ".img-delete2", function() {
		$isImgEditable = false;
		$(".image-container-product2").removeClass("image-exist");
		$(".product_img2").val("");
		$(".image-container-product2").find("img").remove();

	});
	var _URL = window.URL || window.webkitURL;
	$("#file").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL.createObjectURL(file);
			img.onload = function() {

				_URL.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : ' + this.width + " x " + this.height);
				$('#reomencedDimension').text(' Current Dimension : ' + this.width + " x " + this.height);

				$('#file').removeClass('is-invalid');
				$('#btnCreateEvent').prop('disabled', false);
				$('#reomencedDimension').removeClass('error');
			};
			img.src = objectUrl;
			preview_image();
		}
	});

	function readURL2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				if ($(".image-container-product2 img").length != 0) {
					$(".image-container-product2 img").attr(
						"src",
						e.target.result
					);
				} else {
					$(".image-container-product2").append(
						'<img src="' + e.target.result + '">'
					);
				}
				$(".image-container-product2").addClass("image-exist");
				$isImgEditable = true;
			};

			reader.readAsDataURL(input.files[0]);
		}
	}



	$(function() {
		var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,
			$isImgEditable = false,
			$choosenBahanBaku = [],
			$variants, $choosenVariant = [],
			$status_filter = "",
			$outlet_filter = "",
			$is_paid = "",
			$paid_approval = "";

		var datePicker = $('.daterange-ranges').daterangepicker({
				startDate: moment(),
				endDate: moment(),
				ranges: {
					'Hari ini': [moment(), moment()],
					'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
					'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
					'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
					'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
					'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
					'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
				},
				locale: {
					cancelLabel: 'Batal',
					applyLabel: 'Proses',
					daysOfWeek: [
						"Sen",
						"Sel",
						"Rab",
						"Kam",
						"Jum",
						"Sab",
						"Min"
					],
					monthNames: [
						"Januari",
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					],
				},
				opens: 'right',
				showCustomRangeLabel: false,
				alwaysShowCalendars: true,
				applyButtonClasses: 'btn-small btn-primary',
				cancelClass: 'btn-small btn-default',
				maxDate: new Date(),

			},
			// Datepickerrange callback
			datepickerRangeCallback = function(start, end) {
				$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
			}, datepickerRangeCallback);
		$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + moment().format('DD MMM YYYY'));




		var filterStatus = $('#filterStatus').val();
		var filterGroup = $('#filterGroup').val();
		var filterPlatform = $('#filterPlatform').val();;

		var oDataList = $('#reseller-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,

			"bAutoWidth": false,
			"lengthChange": false,
			ajax: {
				url: "{{ url('reseller-list') }}",
				data: {
					filterStatus: filterStatus,
					filterPlatform: filterPlatform,
					filterGroup: filterGroup,

				},
			},
			"fnDrawCallback": function(data, e) {
				// $("#filterGroup").select2({
				// 		data: data.json.group,
				// 		search: true,
				// 		selectionCssClass: 'bg-indigo-400',
				// });
				// $("#filterStatus").select2({
				// 		data: data.json.status,
				// 		search: true,
				// 		selectionCssClass: 'bg-indigo-400',
				// });
				// $("#filterPlatform").select2({
				// 		data: data.json.platform,
				// 		search: true,
				// 		selectionCssClass: 'bg-indigo-400',
				// });

				// $('.populasi').empty().html(data.json.populasi);
				// $('.memberAktif').empty().html(data.json.memberAktif);
				// $('.chartPopulasi').empty().html(data.json.chartPopulasi);
				// $('.kunjungan').empty().html(data.json.kunjungan);
				// $('.pertumbuhan').empty().html(data.json.pertumbuhan);
				// if ($('.dt-category').length == 0) {
				// 	var c = '<div class="mr-auto" style=" display:block;margin-bottom:-34px!important;"><table border="0.1" ><th width="200px"><label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Group:</label><div class="col-lg-12"><input type="search" class="form-control" id="filterGroup" placeholder="Filter Group ..." ></div></th><th width="200px"><label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Platform:</label><div class="col-lg-12"><input type="search" class="form-control" id="filterPlatform" placeholder="Filter Platform ..." ></div></th><th width="200px"><label class="control-label col-lg-12 cursor-pointer startDate" for="start_date">Filter Status:</label><div class="col-lg-12"><select name="filterStatus" id="filterStatus" class="form-control select2" required><option value=""> Semua Status</option><option value="0"> Belum Verifikasi Email</option><option value="1"> Belum Upload Dokumen</option><option value="2"> Verifikasi</option><option value="3"> Suspend</option></select></div></th></table></div>';
				// 	$('.toolbar-item-datatable').append(c);
				// }

			},


			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

			columns: [


				// {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
				// 	orderable: false},
				// {
				//     data: 'status',
				//     name: 'status',

				// },
				{
					data: 'avatar',
					name: 'avatar',
					searchable: false,
					orderable: false,

				},
				{
					data: 'reseller_code',
					name: 'reseller_code',
					orderable: false,

				},
				{
					data: 'custom_code',
					name: 'custom_code',
					orderable: false,

				},
				{
					data: 'virtual_account',
					name: 'virtual_account',
					orderable: false,

				},
				{
					data: 'name',
					name: 'name',
					orderable: false,

				},
				{
					data: 'phone',
					name: 'phone',
					orderable: false,

				},
				// {
				// 	data: 'pinP',
				// 	name: 'pinP',

				// },

				{
					data: 'email',
					name: 'email',
					orderable: false,

				},
				//6
				{
					data: 'platform',
					name: 'platform',
					orderable: false,

				},
				//7
				{
					data: 'groupName',
					name: 'groupName',
					orderable: false,

				},
				{
					data: 'keagenan',
					name: 'keagenan',
					orderable: false,

				},
				{
					data: 'Balance',
					name: 'Balance',

				},
				//9
				{
					data: 'tanggalJoin',
					name: 'tanggalJoin',
					width: "1%",

				},
				{
					data: 'statusReseller',
					name: 'statusReseller',
					orderable: false,

				},
				
				{
					data: 'lastActivity',
					name: 'lastActivity',
					searchable: false,
					orderable: false,

				},
				//12
				{
					data: 'action',
					name: 'Tindakan',
					searchable: false,
					orderable: false,
				},
				//13
				{
					data: 'status',
					name: 'status',
					visible: false,
				},
			],
			buttons: [{
					extend: 'excel',
					title: 'Export Reseller Tanggal: ',
				},
				{
					extend: 'csv',
					title: 'Export Transaksi' + $.now(),
				},
			],
			columnDefs: [{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-80'>" + data + "</div>";
					},
					targets: 11
				},
				{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-60'>" + data + "</div>";
					},
					targets: 12
				},
				{
					render: function(data, type, full, meta) {
						return "<div class='text-wrap width-10'>" + data + "</div>";
					},
					targets: 13
				},


			],
			select: {
				style: 'single'
			},
		});

		$('#filterGroup').on('keyup', function() {
			oDataList
				.column(7)
				.search(this.value)
				.draw();
		});
		$('#filterPlatform').on('keyup', function() {
			oDataList
				.column(6)
				.search(this.value)
				.draw();
		});

		$('#filterStatus').on('change', function() {
			oDataList
				.column(13)
				.search(this.value)
				.draw();
		});



		getTable(filterStatus, filterGroup, filterPlatform);


		// $(document).on('change', '#filterStatus', function() {
		//         filterStatus=$(this).val();
		// 		console.log(filterStatus);
		//         filterGroup = $('#filterGroup').val();
		//         filterPlatform = $('#filterPlatform option:selected').text();
		//         $('.reseller-table').DataTable().clear().destroy();
		// 		getTable(filterStatus,filterGroup,filterPlatform);
		// 	 })
		// 	$(document).on('change', '#filterGroup', function() {
		//         filterGroup=$(this).val();
		//         filterStatus = $('#filterStatus').val();
		//         filterPlatform = $('#filterPlatform option:selected').text();
		//         $('.reseller-table').DataTable().clear().destroy();
		// 		getTable(filterStatus,filterGroup,filterPlatform);
		// 	 })
		// 	$(document).on('change', '#filterPlatform', function() {
		//         filterPlatform = $('#filterPlatform option:selected').text();
		//         filterGroup = $('#filterGroup').val();
		//         filterStatus = $('#filterStatus').val();
		//         $('.reseller-table').DataTable().clear().destroy();
		// 		getTable(filterStatus,filterGroup,filterPlatform);
		// 	 })

		$(document).on("click", ".exportCsv", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-csv').trigger();

				}
			}, '<span style="color: blue;">Export Transaksi</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
		$(document).on("click", ".exportExcel", function(e) {
			showConfirm('Apakah anda Yakin ingin mengexport data ? ', function(result) {
				if (result) {
					oDataList.button('.buttons-excel').trigger();

				}
			}, '<span style="color: blue;">Export Transaksi</span>', {
				confirm: {
					label: 'Export',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		});
	});

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}



	$('.btnEdit').click(function() {
		$('#addForm').fadeOut();
	});

	function cashbackHistory(id, nama) {

		$('#history').fadeIn();
		$('#reseller-history-table').DataTable().destroy();
		getTableHistory(id);
		$('.namaReseller').empty().html(nama);
		document.querySelector('#history').scrollIntoView({
			behavior: 'smooth'
		})


	}

	function getTableHistory(id) {
		$('#reseller-history-table').DataTable({
			"paging": false,
			ajax: {
				url: "{{ url('reseller-history-list') }}" + '/' + id,

			},
			"fnDrawCallback": function(data, e) {


			},


			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

			columns: [


				{
					data: 'tanggal',
					name: 'tanggal',
					searchable: false,
					orderable: false

				},
				{
					data: 'description',
					name: 'description',
					searchable: false,
					orderable: false

				},
				{
					data: 'debit',
					name: 'debit',
					searchable: false,
					orderable: false

				},
				{
					data: 'kredit',
					name: 'kredit',
					searchable: false,
					orderable: false
				},
				{
					data: 'point_after',
					name: 'point_after',
					searchable: false,
					orderable: false
				},

			],
			buttons: [

			],
			columnDefs: [

			],
			select: {
				style: 'single'
			},
		});
	}

	function editData(id, url = "{{url("/reseller-edit/")}}") {
		$.ajax({
			url: url + '/' + id,
			type: 'GET',
			dataType: 'HTML',
			beforeSend: function() {
				$('#editForm').empty();
				$('.btnEdit').fadeOut();
				$('#loadBar').show();

			},
			success: function(data) {
				$('#addform').fadeOut();
				$('#loadBar').hide();
				$('#formWrapper').fadeOut();
				$('#editForm').append(data);
				$('#editForm').fadeIn();
				$("html, body").animate({
					scrollTop: 0
				}, "slow");
			},
			error: function(data) {
				$('#loadBar').hide();
				alert(url + '/' + id);
				alert(data);
				alert('500 : Internal server error');
				$('.btnEdit').fadeIn()
			}
		});
	}

	$('.btnEdit').click(function() {
		$('#addForm').fadeOut();
	});
	$('#addBtn').click(function() {
		$('#addBtn').fadeOut();
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
		$('#addForm').fadeIn();
	});

	$('#cancelBtnAdd').click(function() {
		$('#addForm').fadeOut();
		$('#addBtn').fadeIn();
	});

	$(document).on("click", ".addReseller", function(e) {
		e.preventDefault();
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreate").validate(optValidate);
		if ($("#frmCreate").valid()) {
			showConfirm('Apakah anda Yakin ingin menambahkan Reseller ? ', function(result) {
				if (result) {
					addReseller();
				}
			}, '<span style="color: blue;">Tambah Reseller</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		} else {
			return;
		}
	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}


	function preview_image() {
		$('#image_preview').empty();
		$('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
	}


	function addReseller() {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreate").validate(optValidate);

		if ($("#frmCreate").valid()) {
			var form = $("#frmCreate")[0];
			var formData = new FormData(form);

			console.log($("#frmCreate").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/add-reseller",
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#reseller-table').DataTable().ajax.reload();
						$('#addForm').fadeOut();
						$('#addForm').empty();
						$('#addBtn').fadeIn();
						toastr.success("reseller Berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;
		}

	}

	function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini') {
		if ($ids) {
			bootbox.confirm({
				title: 'Delete Data',
				message: message,
				buttons: {
					confirm: {
						label: 'Delete',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				},
				callback: function(result) {
					if (result) {
						console.log(url)
						$.ajax({
							url: url + '/' + $ids,
							data: {
								"_token": "{{ csrf_token() }}",
							},
							dataType: 'json',
							type: 'post',
							success: function(d) {
								if (d.status == 200) {

									$('#reseller-table').DataTable().ajax.reload();
									toastr.success("Data Berhasil di Hapus")
								} else {
									toastr.error(d.message)
								}
							},
							error: function(d, statusText, xhr) {
								bootbox.alert('Oops. Error when processing your request. ' + xhr);
							},
							beforeSend: function() {},
							complete: function() {}
						});
					}
				}
			});
		} else {
			bootbox.alert("error");
		}

		return false;
	}

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}
	var _baseURL = "<?php echo url(''); ?>";
	var optValidate = {
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		errorClass: 'validation-error-label',
		successClass: 'validation-valid-label',
		highlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		// Different components require proper error label placement
		errorPlacement: function(error, element) {

			// Styled checkboxes, radios, bootstrap switch
			if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
				if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent().parent().parent());
				} else {
					error.appendTo(element.parent().parent().parent().parent().parent());
				}
			}

			// Unstyled checkboxes, radios
			else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
				error.appendTo(element.parent().parent().parent());
			}

			// Input with icons and Select2
			else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
				error.appendTo(element.parent());
			}

			// Inline checkboxes, radios
			else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
				error.appendTo(element.parent().parent());
			}

			// Input group, styled file input
			else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
				error.appendTo(element.parent().parent());
			} else {
				error.insertAfter(element);
			}
		},
		validClass: "validation-valid-label"


	};

	function deleteProduct($id) {
		// Setup validation
		deleteTableList(
			"delete-product", [$id],
			true
		);
		return false;

	}

	function deleteProduct($id) {
		// Setup validation
		deleteTableList(
			"delete-reseller", [$id],
			true
		);
		return false;
	}
</script>