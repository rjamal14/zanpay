<x-package-header />
<style>

	.image-container-product2{border-radius:8px;background-color:#7d85c9;width:195px;height:195px;position:relative;overflow:hidden;}
.image-container-product2 a{width:100%;text-align:center;padding:5px 0;background-color:#5671c9;color:#FFF;display:inline-block;}
.image-container-product2.image-exist a{width:49%;}
.image-container-product2 .overlay{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
    width: 100%;
    height: 0;
    transition: .5s ease;
    z-index:100;
}
.image-container-product2:hover .overlay, .image-container-product2 img:hover .overlay{height:30px;}
.image-container-product2 img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}



.validation-error-label,
.validation-valid-label {
  margin-top: 7px;
  margin-bottom: 7px;
  display: block;
  color: #F44336;
  position: relative;
  padding-left: 26px;
}
.validation-valid-label {
  color: #4CAF50;
}
.validation-error-label:before,
.validation-valid-label:before {
  position: absolute;
  top: 2px;
  left: 0;
  display: inline-block;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 9px;
}
.validation-error-label:empty,
.validation-valid-label:empty {
  display: none;
}

label.validation-error-label{font-weight: normal;}
	.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
.width-180{
    width:180px;
}
.width-100{
    width:100px;
}
.width-80{
    width:80px;
}
.width-50{
    width:50px;
}
.width-30{
    width:30px;
}
.form-control{
	font-size: 11.5px;
}
.col-form-label{
	font-size: 11.5px!important;
}

	table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
    	vertical-align: top;
	}
	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Home Page</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
			
			</div>
		</div>
		
	</div>


        


	<div class="container-fluid">
		<div class="row clearfix">
				<div class="card">
					<div class="header">
						<div class="d-flex" >
							<div class="mr-auto" style=" display:inline-block;margin-bottom:-150px!important;">
							
							</div>
						
						</div>
					
					</div>
					<div class="body">
						
					<!-- form -->
						<form class="form-vertical" method="POST" action=""  role="form" id="frmEdit" enctype="multipart/form-data">
							@csrf
							<div class="card-body">
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Screenshoot Utama</label>
									<div class="col-sm-6 center-block">
										<div id="image_preview_edit">   
											<img src ="{{$data['main_image']}}" width="30%;" style="border-radius: 2px;">
										</div>
									</div>
									<div class="col-sm-4 center-block" >
											<input type="file" style="margin-right:300px!important;" id="fileEdit" name="main_image" >
									</div>
								</div>
								<hr>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Screenshoot Kedua</label>
									<div class="col-sm-6">

										<div id="image_preview_edit2">   
											<img src ="{{$data['secondary_image']}}"width="30%;" style="border-radius: 2px;">
										</div>

									</div>
									<div class="col-sm-4 center-block" >
									<input type="file" style="margin-right:300px!important;" id="fileEdit2" name="secondary_image" >
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Judul</label>
									<div class="col-sm-8">
											<input class="form-control" name="title" value='{{$data["title"]}}'>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Deskripsi</label>
									<div class="col-sm-8">
											<textarea class="form-control" name="description" >{{$data["description"]}}</textarea>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Play Store URL</label>
									<div class="col-sm-8">
											<input class="form-control" name="playstore_url" value='{{$data["playstore_url"]}}'>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Play Store URL</label>
									<div class="col-sm-8">
											<input class="form-control" name="app_store_url" value='{{$data["app_store_url"]}}'>
									</div>
								</div>

							</div>
								
						
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="submit" i class="btn btn-info ">Submit</button>
							
							</div>
                 
						</form>
					<!-- end form -->
					</div>
				</div>


			
				
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>


	$(document).on("change", ".file_multi_video", function(evt) {
		var $source = $('#video_here');
		$source[0].src = URL.createObjectURL(this.files[0]);
		$source.parent()[0].load();
		console.log($source);
	});

	var _baseURL = "<?php echo url(''); ?>";
	$(document).on("click", ".img-init2", function() {
        $(".product_img2").click();
    });
    $(document).on("change", ".product_img2", function() {
        readURL2(this);
    });
    $(document).on("click", ".img-delete2", function() {
        $isImgEditable = false;
        $(".image-container-product2").removeClass("image-exist");
        $(".product_img2").val("");
        $(".image-container-product2").find("img").remove();

    });
	var _URL = window.URL || window.webkitURL;
	
	$("#fileEdit").change(function (e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL.createObjectURL(file);
			img.onload = function () {
			
				_URL.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : '+this.width + " x " + this.height);
				$('#reomencedDimension').text(' Current Dimension : '+this.width + " x " + this.height);
			
				$('#file').removeClass('is-invalid');
				$('#btnCreateEvent').prop('disabled', false);
				$('#reomencedDimension').removeClass('error');
			};
			img.src = objectUrl;
			preview_image();
    	}
	});
	$("#fileEdit2").change(function (e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL.createObjectURL(file);
			img.onload = function () {
			
				_URL.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : '+this.width + " x " + this.height);
				$('#reomencedDimension').text(' Current Dimension : '+this.width + " x " + this.height);
			
				$('#file2').removeClass('is-invalid');
				$('#btnCreateEvent').prop('disabled', false);
				$('#reomencedDimension').removeClass('error');
			};
			img.src = objectUrl;
			preview_image2();
    	}
	});
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if ($(".image-container-product2 img").length != 0) {
                    $(".image-container-product2 img").attr(
                        "src",
                        e.target.result
                    );
                } else {
                    $(".image-container-product2").append(
                        '<img src="' + e.target.result + '">'
                    );
                }
                $(".image-container-product2").addClass("image-exist");
                $isImgEditable = true;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

	
function preview_image() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='50%' style='border-radius: 2px;'> &nbsp");
    }
function preview_image2() {
      $('#image_preview_edit2').empty();
      $('#image_preview_edit2').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='50%' style='border-radius: 2px;'> &nbsp");
    }

   

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false)
			{
				if (!$buttons) {
					$buttons = {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-danger'
						}
					};
				}
				bootbox.confirm({
					title: $title,
					message: $message,
					buttons: $buttons,
					callback: $callback
				});
			}



			$('.btnEdit').click(function(){
			$('#addForm').fadeOut();
	});




		
		
	
    	

</script>