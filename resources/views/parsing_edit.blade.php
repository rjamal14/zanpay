<div class="card card-primary">

	<div class="card-header">
		<h3 class="card-title"> Edit Produk Parsing</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->
	<form class="form-horizontal" role="form" id="frmEdit" enctype="multipart/form-data">
		@csrf
		<div class="card-body">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Biller</label>
				<div class="col-sm-10">
					<select name="group_id" id="type" class="form-control" required>
						@foreach ($biller as $t)
						<option value="{{$t['id']}}" {{$data['biller_id'] == $t['id'] ? "selected":""}}>{{$t['name']}} </option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Tipe Request</label>
				<div class="col-sm-10">
					<select name="parsingType" id="parsingTypeEdit" class="form-control" required>
						<option value="0" {{$data['type'] == 0 ? "selected='selected'":''}}>Transaksi</option>
						<option value="1" {{$data['type'] == 1 ? "selected='selected'":''}}>Bukan Transaksi</option>

					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Request</label>
				<div class="col-sm-10">
					<select name="holderId" id="holderIdEdit" class="form-control select2" required>
						<option value="0" {{$data['holder_id'] == 0 ? "selected='selected'":''}}>Tidak ada group holder</option>
						@foreach ($groupH as $t )
						<option value="{{$t['id']}}" {{$data['holder_id'] ==$t['id'] ? "selected='selected'":''}}>{{$t['text']}}</option>
						@endforeach
					</select>
				</div>
			</div>
	
	
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Parsing</label>
				<div class="col-sm-10">
					<textarea type="text" class="form-control" name="parsing" required>{{$data['parsing']}}</textarea>
				</div>
			</div>
		</div>

		<!-- /.card-body -->
		<div class="card-footer">
			<button type="" id="editResellerGroup" data-id='{{$data["id"]}}' class="btn btn-info editResellerGroup">Submit</button>
			<button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
		</div>
		<!-- /.card-footer -->
	</form>
</div>
<script>
	$('#cancelBtnEdit').click(function() {
		$('#editForm').fadeOut();
		$('.btnEdit').fadeIn();
	});
	$("#fileEdit").change(function(e) {
		var _URL2 = window.URL || window.webkitURL;
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			var objectUrl = _URL2.createObjectURL(file);
			img.onload = function() {

				_URL2.revokeObjectURL(objectUrl);
				console.log(' Current Dimension : ' + this.width + " x " + this.height);
				$('#dimRec').text(' Current Dimension : ' + this.width + " x " + this.height);

				$('#fileEdit').removeClass('is-invalid');
				$('#btnEditEvent').prop('disabled', false);
				$('#dimRec').removeClass('error');
			};
			img.src = objectUrl;
			preview_image_edit();
		}
	});

	function preview_image_edit() {
		$('#image_preview_edit').empty();
		$('#image_preview_edit').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "' width='150px' style='border-radius: 2px;'> &nbsp");
	}
	$(document).on("click", "#editResellerGroup", function(e) {
		e.stopImmediatePropagation();
		e.preventDefault();


		console.log($(this).attr("data-id"));

		$dataID = $(this).attr("data-id");
		showConfirm('Apakah anda Yakin ingin mengupdate Data ? ', function(result) {
			if (result) {
				updateResellerGroup($dataID);
			}
		}, '<span style="color: blue;">Edit Data </span>', {
			confirm: {
				label: 'Update',
				className: 'btn-primary'
			},
			cancel: {
				label: 'Cancel',
				className: 'btn-default'
			}
		});



	});
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	function updateResellerGroup(idProduct) {
		// Setup validation
		var _baseURL = "<?php echo url(''); ?>";


		var form = $("#frmEdit")[0];
		var formData = new FormData(form);

		console.log($("#frmEdit").serialize());


		// formData.append("destinationDescription", $content.getData());

		$.ajax({
			url: _baseURL + "/produk_parsing/" + idProduct,
			type: "post",
			dataType: "json",
			data: formData,
			processData: false,
			contentType: false,
			success: function(d) {
				console.log(toastr);
				if (d.status == 200) {
					toastr.success("Data Berhasil diupdate")
					$('#editForm').fadeOut();
					$('#addform').fadeOut();
					$('#editBtn').fadeIn();
					$('#product-category-table').DataTable().ajax.reload();


				} else {
					toastr.error(d.message)
				}
			},
			error: function(xhr) {
				console.log(xhr);
			},
			beforeSend: function() {},
			complete: function() {

			},
		});

		return false;

	}
</script>