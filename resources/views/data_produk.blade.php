<x-package-header />
<style>
	.table-custom th {
		font-size: 12px;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		display: inline-block;
		margin-right: 1em;
		white-space: nowrap;
		line-height: 1;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Produk</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<button class="btn btn-sm btn-primary mr-3">
					<i class="fa fa-download"></i> Export CSV
				</button>
				<button class="btn btn-sm btn-primary mr-3" data-toggle="modal" data-target="#filterModal">
					<icon class="fa fa-filter"></icon> Filter
				</button>
				<button class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-sort"></icon> Sortir
				</button>
				<button class="btn btn-sm btn-primary mr-3">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="header">
						<ul class="header-dropdown dropdown dropdown-animated scale-left">
							<li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another Action</a></li>
									<li><a href="javascript:void(0);">Something else</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover js-basic-example dataTable table-custom">
								<thead>
									<tr>
										<th>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</th>
										<th>Status</th>
										<th>Produk SKU</th>
										<th>Nama</th>
										<th>Grup</th>
										<th>Jenis</th>
										<th>Provider</th>
										<th>Harga Dasar</th>
										<th>Harga Jual</th>
										<th>Cashback</th>
										<th>Biller</th>
										<th>Gangguan</th>
										<th>Deskripsi</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="pretty-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td><span class="badge badge-success">Aktif</span></td>
										<td>SKU12345</td>
										<td>VBM100</td>
										<td>Voucher</td>
										<td>Telkomsel Data</td>
										<td>Telkomsel</td>
										<td>Rp. 95,000</td>
										<td>Rp. 105,000</td>
										<td>Rp. 5,000</td>
										<td>Biller 1</td>
										<td>
											<label class="pretty-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>Lorem Ipsum</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
