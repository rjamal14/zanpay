<x-package-header />
<style>
.width-180{
    width:180px;
}
.alpha-danger {
  background-color: #FFEBEE;
  border-color: #E53935;
}
.alpha-success {
  background-color: #E8F5E9;
  border-color: #43A047;
}
table tbody tr.alpha-danger{background-color: #fd9baa !important;}
table tbody tr.alpha-success{background-color: #e8f5e9 !important;}

table.dataTable td {
    padding: 10px;
}
	.jvectormap-container {
		height: 100%;
		width: 100%;
	}

	.table-custom td {
		margin-bottom: 0px;
	}

	.table-deposit th {
		font-size: 12px;
	}

	.table-deposit tr {
		font-size: 12px;
	}

	.table-deposit td {
		padding: 3px;
	}
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Dashboard</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
									<div class="col-lg-12">
										<button type="button" class="btn btn-primary daterange-ranges">
											<i class="fa fa-calendar position-left"></i>
											<span>{{ date('d M Y')  }} - {{ date('d M Y') }}</span>
											<b class="caret"></b>
										</button>
									</div>
				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-12">
				<div class="card top_report">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="body">
								<div class="clearfix">
									<div class="float-left">
										<i class="fa fa-2x fa-users text-col-blue"></i>
									</div>
									<div class="number float-right text-right">
										<h6>Members</h6>
										<span class="font700">{{$jumlahMember}}</span>
									</div>
								</div>
								<div class="progress progress-xs progress-transparent mb-0 mt-3 ">
									<div class="progress-bar bg-info" data-transitiongoal="80"></div>
								</div>
								<small class="text-muted">Jumlah reseller yang terdaftar di zanpay</small>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="body">
								<div class="clearfix">
									<div class="float-left">
										<i class="fa fa-2x fa-bar-chart-o text-col-green"></i>
									</div>
									<div class="number float-right text-right">
										<h6>Total Saldo Reseller</h6>
										<span class="font700">Rp. {{$jumlahSaldoReseller}}</span>
									</div>
								</div>
								<div class="progress progress-xs progress-transparent  mb-0 mt-3">
									<!-- <div class="progress-bar" data-transitiongoal="{{$jumlahSaldoResellerP}}"></div> -->
									<div class="progress-bar bg-danger" data-transitiongoal="70"></div>
								</div>
								<small class="text-muted">Total saldo reseller yang terkumpul</small>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="body">
								<div class="clearfix">
									<div class="float-left">
										<i class="fa fa-2x fa-usd text-col-green"></i>
									</div>
									<div class="number float-right text-right">
										<h6>Total Deposit Member</h6>
										<span class="font700 depositMember">Rp. {{$jumlahSaldoBiller}}</span>
									</div>
								</div>
								<div class="progress progress-xs progress-transparent  mb-0 mt-3">
									<div class="progress-bar bg-warning" data-transitiongoal="70"></div>
								</div>
								<small class="text-muted">Deposit saldo yang sudah sukses</small>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="body">
								<div class="clearfix">
									<div class="float-left">
										<i class="fa fa-2x fa-bar-chart-o text-col-green"></i>
									</div>
									<div class="number float-right text-right">
										<h6>Omzet Hari Ini</h6>
										<span class="font700">Rp. {{$jumlahOmzet}}</span>
									</div>
								</div>
								<div class="progress progress-xs progress-transparent mb-0 mt-3">
									<div class="progress-bar  bg-success" data-transitiongoal="70"></div>
								</div>
								<small class="text-muted">Total keuntungan yang didapat</small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-8">
				<div class="card">
					<div class="header bline">
						<h2 ><a  style="color:rgb(0,0,0);" href="{{url('/transaksi')}}">Rekap Transaksi</a></h2>
						<ul class="header-dropdown dropdown dropdown-animated scale-left">
							<li><a id="bulanan" class="btn btn-default btn-sm" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Bulanan" aria-describedby="tooltip159055">Bulanan</a></li>
							<li><a id="tahunan" class="btn btn-outline-primary btn-sm" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tahunan">Tahunan</a></li>
						</ul>
					</div>
					<div class="body">
						<div id="transaction-chart"class="mt-2" >
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card"  style="height:430px;">
					<div class="header bline">
						<h2>Produk Terlaris</h2>
					</div>
					<div class="body">
						<div id="product-sold-chart"  class="mt-2">
						
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">

			<div class="col-md-7 col-sm-12" style="display:none;">
				<div class="card" style="height:570px;">
					<div class="header">
						<h2>Saldo Reseller</h2>
					</div>
					<div class="body">
						<table class="table table-hover js-basic-example dataTable table-custom mb-0">
							<tbody>
								@foreach($jumlahSaldoResellerD as $v)
								<tr>
									<td style="width: 100px;">{{$v['tanggal']}}</td>
									<td>
									<div id="progress progress-xs progress-transparent {{$v['class']}} mb-0 mt-3" style="height: 7px;" class="progress">
											<div class="progress-bar " data-transitiongoal="{{abs($v['percentage'])}}">
											</div>

										</div>
										<span style="font-size: 12px;">
										 {{$v['saldoRp']}}
										</span>
									</td>
								</tr>
								@endforeach
						
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="col-md-7 col-sm-12" >
			<div class="card" style="height:795px;">
					<div class="header">
						<h2>Deposit Terbaru</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table id="deposit-table" class="table row-margin table-hover js-basic-example dataTable table-custom table-deposit mb-0">
								<thead class="thead-dark">
									<tr>
									<th>Jam&Tanggal</th>
										<th>Nama</th>
										<th>Kode Unik</th>
										<th>Channel</th>
										<th>Jumlah</th>
										<th>Status</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-5 col-sm-12">
				<div class="card"  style="">
					<div class="header">
						<h2>Saldo Biller & H2H</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-hover js-basic-example table-custom mb-0 biller-table">
								<thead style="font-size:13px!important;" class="thead-dark">
									<tr >
										<!-- <th><i class="icon-user"></th> -->
										<th>Nama</th>
										<th>Avatar</th>
										<th>Saldo</th>
										<th>Transaksi Terakhir</th>
									</tr>
								</thead>
								<tbody style="font-size:12px!important;">

								</tbody>
							
							</table>
						</div>
						<!-- <div class="row clearfix">
							<div class="col-5">
								<div class="sparkline-pie2">6,4,8</div>
							</div>
							<div class="col-7 legendPie">
								<icon class="fa fa-square" style="color:#20c997"></icon> <span>Biller 1 <span class="float-right">Rp. 6,000,000</span></span><br>
								<icon class="fa fa-square" style="color:#6610f2"></icon> <span>Biller 2 <span class="float-right">Rp. 4,000,000</span></span><br>
								<icon class="fa fa-square" style="color:#ffc107"></icon> <span>Biller 3 <span class="float-right">Rp. 8,000,000</span></span>
							</div>
						</div> -->
					</div>
				</div>
			
				<div class="card" style="height:506px;">
					<div class="header">
						<h2>Daftar Pekerjaan</h2>
						<small style="color: gray;">list pekerjaan minggu ini</small>
					</div>
					<div class="body todo_list">
						<ul class="list-unstyled mb-0 daftarPekerjaan">
							<!-- <li>
								<label class="fancy-checkbox mb-0">
									<input type="checkbox" name="checkbox" checked="">
									<span>Tembak Saldo</span>
								</label>
								<hr>
							</li>
							<li>
								<label class="fancy-checkbox mb-0">
									<input type="checkbox" name="checkbox">
									<span>Tembak Saldo</span>
								</label>
								<hr>
							</li>
							<li>
								<label class="fancy-checkbox mb-0">
									<input type="checkbox" name="checkbox">
									<span>Tembak Saldo</span>
								</label>
								<hr>
							</li>
							<li>
								<label class="fancy-checkbox mb-0">
									<input type="checkbox" name="checkbox">
									<span>Tembak Saldo</span>
								</label>
							</li> -->
						</ul>
					</div>
				</div>

				
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card">
					<div class="header">
						<h2>Reseller Sultan</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-hover js-basic-example dataTable table-custom mb-0 reseller-sultan-table">
								<thead style="font-size:12px!important;" class="thead-dark">
									<tr>
										<th><i class="icon-user"></th>
										<th>Nama</th>
										<th>Tipe Agen</th>
										<th>Jml Transaksi</th>
										<th>Point</th>
										<th>Downline</th>
										<th>Saldo</th>
										<th>Aktivitas</th>
									</tr>
								</thead>
								<tbody style="font-size:11px!important;">
								</tbody>
							
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-xl-12 col-lg-12 col-md-12">
				<div class="card">
					<div class="header">
						<h2>Maping Reseller</h2>
					</div>
					<div class="body">
					<div class="row clearfix">
							<div class="col-6">
								<small>Page Views</small>
								<h4 class="m-b-0">32,211,536</h4>
							</div>
							<div class="col-6">
								<small>Site Visitors</small>
								<h4 class="m-b-0">23,516</h4>
							</div>
						</div>
						<div class="row clearfix">
							<div class="col-lg-6 col-md-12">
								<div id="vmap" class="text-center"style="height: 380px; width: auto;"></div>
							</div>
							
							<div class="col-lg-6 col-md-12  text-center">
								<div id="donut_chart" class="donut_chart m-b-30" style="height: 250px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>
</div>
<x-package-footer />

<script src="{{ asset('jqvmap-master/dist/jquery.vmap.js')}}" type="text/javascript"></script>
<script src="{{ asset('jqvmap-master/dist/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	var oDataList1123d = $('.biller-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			"bLengthChange": false,
			searching: false,
			"searching": false,
			"paging":   false,
			"info":     false,
		
			"bAutoWidth" : false,
			ajax: {
				url: "{{ url('biller-list') }}",
				data: function (d) {
                
            	}
			},
		    "fnDrawCallback": function( data ,e) {
		
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
		
				{
					data: 'name',
					name: 'name',
			
				},
				{
					data: 'avatar3',
					name: 'avatar3',
			
				},
				{
					data: 'saldo',
					name: 'balance',
			
				},
			
				{
				    data: 'latest',
				    name: 'latest',
			
				},
			
			
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export biller Tanggal: '+ $.now() ,
				},
				{ 
					extend: 'csv',
					title: 'Export Biller'+ $.now(),
				},
			],
			columnDefs: [
				
		
		
			],
			select: {
				style: 'single'
			},
		});

	getDaftarPekerjaan();
	function getDaftarPekerjaan() {
		$.ajax({
			url: "{{ url('dashboard-pekerjaan-list') }}",
			dataType: 'json',
			data:{
			},
			success: function (d) {
				c ="";
				$.each(d, function (i,val) {
				console.log(val.status);
					if(val.status == 0){
						c += `
						<li>
							<label class="fancy-checkbox mb-0">
								<input class="inputDaftarPekerjaan" data-id="`+val.id+`" data-status="1" type="checkbox" name="checkbox">
								<span>`+val.name+`</span>
							</label>
							<hr>
						</li>
					`;
					} else {
						c += `
					<li>
								<label class="fancy-checkbox mb-0">
									<input   class="inputDaftarPekerjaan"  data-id="`+val.id+`"  type="checkbox" data-status="0" name="checkbox" checked="">
									<span>`+val.name+`</span>
								</label>
								<hr>
							</li>
					`;
					}
			
				})		
				$('.daftarPekerjaan').empty().append(c);
			},
			beforeSend: function () {
				$(document.body).css({'cursor' : 'wait'});
			},
			complete: function () {
				$(document.body).css({'cursor' : 'default'});
			}
		});
	}

	$(document).on('click','.inputDaftarPekerjaan', function() {
		$id = $(this).attr('data-id');
		$status = $(this).attr('data-status');
		console.log($id);
		updateDaftarPekerjaan($id,$status);	
    });


	function updateDaftarPekerjaan($id,$status) {
		$.ajax({
			url: "{{ url('update-dashboard-pekerjaan-list') }}?id=" +$id+"&status="+$status,
			dataType: 'json',
			data:{
				// 'id':$id,
				// 'status':$status,
			},
			success: function (d) {
				getDaftarPekerjaan() 
			},
			beforeSend: function () {
				$(document.body).css({'cursor' : 'wait'});
			},
			complete: function () {
				$(document.body).css({'cursor' : 'default'});
			}
		});
	}


	  var $stateID, switcheryCek, switchery, $editState, $allOutletState = false,$isImgEditable = false, $choosenBahanBaku = [], $variants, $choosenVariant = [],$status_filter="", $outlet_filter="", $is_paid="" ,$paid_approval="";
		
		var datePicker = $('.daterange-ranges').daterangepicker({
			startDate:moment(),
			endDate: moment(),
			ranges: {
				'Hari ini': [moment(), moment()],
				'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
				'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
				'Minggu Lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
				'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
				'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],        
				'2 Bulan Terakhir': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
			},
			locale: { 
				cancelLabel: 'Batal', 
				applyLabel: 'Proses',
				daysOfWeek: [
								"Sen",
								"Sel",
								"Rab",
								"Kam",
								"Jum",
								"Sab",
								"Min"
							],
				monthNames: [
					"Januari",
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				],
			},
			opens: 'right',
			showCustomRangeLabel: false,
			alwaysShowCalendars: true,
			applyButtonClasses: 'btn-small btn-primary',
			cancelClass: 'btn-small btn-default',
			maxDate: new Date(),
			
		},
			// Datepickerrange callback
			datepickerRangeCallback =  function(start, end) {
			$('.daterange-ranges span').html(start.format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' + end.format('DD MMM YYYY'));
			}, datepickerRangeCallback);
			$('.daterange-ranges span').html(moment().format('DD MMM YYYY') + ' &nbsp; - &nbsp; ' +  moment().format('DD MMM YYYY'));
			
			datePicker.on('apply.daterangepicker', function(ev, picker) {
				$.ajax({
					url: "{{ url('dashboard-badge') }}",
					dataType: 'json',
					type: 'post',
					data: {
						"_token": "{{ csrf_token() }}",
						startDate : datePicker.data('daterangepicker').startDate.format('YYYY-M-DD'),
						endDate : datePicker.data('daterangepicker').endDate.format('YYYY-M-DD')
					},
					success: function (d) {
						$('.depositMember').html(d);
						console.log(d);
					},
					beforeSend: function () {
					},
					complete: function (d) {
						$('.depositMember').html(d.responseText);
						console.log(d);
					}
				});
			});

			function initDonutChart2() {
	
				$.ajax({
					url: "{{ url('platform-chart') }}",
					dataType: 'json',
					success: function (d) {
						var data = d.data;
						Morris.Donut({
							element: 'donut_chart',
							data: [{
								label: 'Web',
								value: data.web
							}, {
								label: 'Android',
								value: data.android
							}, {
								label: 'IoS',
								value: data.ios
							}
							],
							colors: ['#f15a24', '#f7931e', '#ffb83b'],
							formatter: function (y) {
								return y + '%'
							}
						});
					},
					beforeSend: function () {
					},
					complete: function () {
					}
				});
			}	// console.log($('#transaction-chart').getContext("2d"));
let colors = {
    "path01": "blue",
    "path32": "red",
    "path22": "green",

}
jQuery('#vmap').vectorMap(
{
    map: 'indonesia_id',
    backgroundColor: '#fff',
    borderColor: 'black',
    borderOpacity: 0.25,
    borderWidth: 1,
    color: 'grey',
    enableZoom: true,
    hoverColor: '#c9dfaf',
    hoverOpacity: null,
    normalizeFunction: 'linear',
    scaleColors: ['#b6d6ff', '#005ace'],
    // selectedColor: '#c9dfaf',
    selectedRegions: null,
    showTooltip: true,
    onRegionClick: function(element, code, region)
    {
        var message = 'You clicked "'
            + region
            + '" which has the code: '
            + code.toUpperCase();

        alert(message);
    }
});

jQuery('#vmap').vectorMap('set', 'colors',colors);

	

	var oDataList = $('#deposit-table').DataTable({
			processing: true,
			serverSide: true,
			"searching": false,
			"bLengthChange":   false,
			"pagingType":   'full',
        "ordering": false,
        "info":     false,
			autoWidth: false,
		
			"bAutoWidth" : false,
			ajax: {
				url: "{{ url('tiket_deposit-list') }}",
			},
		    "fnDrawCallback": function(row, data, index) {
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
		
				// {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
				// 	orderable: false},
				// {	
				//     data: 'status',
				//     name: 'status',
			
				// },
				{
					data: 'jamTanggal2',
					name: 'jamTanggal2',
			
				},
				{
					data: 'nama',
					name: 'nama',
			
				},
				{
					data: 'unique_code',
					name: 'unique_code',
			
				},
				{
					data: 'payment2',
					name: 'payment2',
			
				},
				{
					data: 'amount',
					name: 'amount',
			
				},
				{
					data: 'action3',
					name: 'action3',
			
				},
		
				
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Tiket Deposit Saldo Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
				
				{
						"targets": 2, // your case first column
						"className": "text-center",
				},
				{
						"targets": 3, // your case first column
						"className": "text-center",
				},
				{
						"targets": 0, // your case first column
						"className": "text-center",
				},
		
			],
			select: {
				style: 'single'
			},

			"createdRow": function( row, data, dataIndex ) {
                // if (data.status2 == 1 || data.status2 == 3) {
                //     $(row).addClass('alpha-success');
                // } else if (data.status2 == 2 || data.status2 == 4 ) {
                //     $(row).addClass('alpha-danger');
                // }
				// console.log(data.status2);
            },
		
	});

	$('#deposit-table').on('change', '.request-status', function () {
			var tr = $(this).closest('tr');
			var data = oDataList.row(tr).data();
			updateStatus(data.no, $(this).val());
	});
	var _baseURL = "<?php echo url(''); ?>";
		function updateStatus($dataID, $status) {
			$.ajax({
				url: _baseURL + '/tiket_deposit-update-status/' + $dataID,
				dataType: 'json',
				data: {
					stat: 1,
					id: $dataID,
					status: $status
				},
				success: function (d) {
					if (d.status == 200) {
						toastr.success("deposit Berhasil diupdate")
						$('#deposit-table').DataTable().ajax.reload();
					} else {
						toastr.success(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr)
				},
				beforeSend: function () {
				},
				complete: function () {
				}
			});
    	}
	var oDataList2 = $('.reseller-sultan-table').DataTable({
			processing: true,
			serverSide: true,
			"searching": false,
			"paging":   false,
			"info":     false,
			autoWidth: false,
		
			"bAutoWidth" : false,
			ajax: {
				url: "{{ url('reseller_sultan-list') }}",
			},
		    "fnDrawCallback": function( data ,e) {
		
			},
		

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',
	
			columns: [
			
		
				// {data: 'DT_RowIndex', name: 'DT_RowIndex' ,  searchable: false,
				// 	orderable: false},
				// {	
				//     data: 'status',
				//     name: 'status',
			
				// },
				{
					data: 'image2',
					name: 'image2',
			
				},
				{
					data: 'nama',
					name: 'nama',
			
				},
				{
					data: 'groups',
					name: 'groups',
			
				},
				{
					data: 'count',
					name: 'count',
			
				},
				{
					data: 'point',
					name: 'point',
			
				},
				{
					data: 'downline_count',
					name: 'downline_count',
			
				},
				{
					data: 'balance',
					name: 'balance',
			
				},
		
				{
					data: 'activity',
					name: 'activity',
			
				},
				{
					data: 'balances',
					name: 'balances',
			
				},
				{
					data: 'qty',
					name: 'qty',
			
				},
		
				
			],
			buttons: [
				{ 
					extend: 'excel',
					title: 'Export Tiket Deposit Saldo Tanggal: ' ,
				},
				{ 
					extend: 'csv',
					title: 'Export Transaksi'+ $.now(),
				},
			],
			columnDefs: [
               { targets: 8, visible: false },
               { targets: 9, visible: false },
			   {
						render: function (data, type, full, meta) {
							return "<div class='text-wrap width-180'>" + data + "</div>";
						},
						targets: 5
				},
            ],
			select: {
				style: 'single'
			},
			order: [[9, "desc"]]
	});
	initDonutChart2();
	// <li><a id="bulanan" class="btn btn-default btn-sm" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Bulanan" aria-describedby="tooltip159055">Bulanan</a></li>
	// 						<li><a id="tahunan" class="btn btn-outline-primary btn-sm" href="javascript:void(0);" data-toggle="tooltip" ]
	let bulanan =false;
	$('#bulanan').on('click', function(){
         $(this).removeClass( "btn-default" ).addClass( "btn-outline-primary" );
         $('#tahunan').removeClass( "btn-outline-primary" ).addClass( "btn-default" );
		 bulanan =true;
		 getTransaksiChart(bulanan);

    });
	$('#tahunan').on('click', function(){
         $(this).removeClass( "btn-default" ).addClass( "btn-outline-primary" );
         $('#bulanan').removeClass( "btn-outline-primary" ).addClass( "btn-default" );
		 bulanan =false;
		 getTransaksiChart(bulanan);

    });
    getTransaksiChart(bulanan);

    function getTransaksiChart(bulanan = false)
    {
        $.ajax({
        	url: "{{ url('transaksi-chart') }}",
            data: {
                bulanan: bulanan,
                // start_date: datePicker.data('daterangepicker').startDate.format('YYYY-M-DD'),
                // end_date: datePicker.data('daterangepicker').endDate.format('YYYY-M-DD'),
                // days_type: $('.days_filter:checked').val(),
                // product_filter: $productFilter
            },
            dataType: 'json',
            type: 'get',
            success: function (d) {
				var chart = c3.generate({
						bindto: '#transaction-chart', // id of chart wrapper
						data: {
							columns:d.data,

							labels: true,
							type: 'line', // default type of chart
							colors: {
								'data1': hexabit.colors["green"],
								'data2': hexabit.colors["yellow"],
												'data3': hexabit.colors["red"],
							},
							names: {
								// name of each serie
								'data1': 'Sukses',
								'data2': 'Proses',
												'data3': 'Gagal',
							}
						},

						axis: {
							x: {
								type: 'category',
								// name of each category
								categories:d.label
							},
						},

						legend: {
							show: true, //hide legend
						},

						padding: {
							bottom: 10,
							top: 0
						},
						});
            },
            beforeSend: function () {
				$(document.body).css({'cursor' : 'wait'});
            },
            complete: function () {
				$(document.body).css({'cursor' : 'default'});
            }
        });
    }
    productSoldChart();

    function productSoldChart()
    {
        $.ajax({
        	url: "{{ url('product-sold-chart') }}",
            data: {
                // outlet_id: $('#outlet').val(),
                // start_date: datePicker.data('daterangepicker').startDate.format('YYYY-M-DD'),
                // end_date: datePicker.data('daterangepicker').endDate.format('YYYY-M-DD'),
                // days_type: $('.days_filter:checked').val(),
                // product_filter: $productFilter
            },
            dataType: 'json',
            type: 'get',
            success: function (d) {
				  // top products
				var dataStackedBar = {
							labels: d.legend,
							series: d.series
					};
					new Chartist.Bar('#product-sold-chart', dataStackedBar, {
							height: "255px",
							stackBars: true,
							axisX: {
								showGrid: false
							},
							axisY: {
								labelInterpolationFnc: function(value) {
									if (Math.floor(value) === value) {
										return value;
									}
								}
							},
							plugins: [
								Chartist.plugins.tooltip({
									appendToBody: true
								}),
								Chartist.plugins.legend({
									legendNames: d.label
								})
							]
					}).on('draw', function(data) {
							if (data.type === 'bar') {
								data.element.attr({
									style: 'stroke-width: 35px'
								});
							}
					});
            },
            beforeSend: function () {
              
            },
            complete: function () {
               
            }
        });
    }
    getpieChart();

    function getpieChart()
    {
        $.ajax({
        	url: "{{ url('saldo-biller-chart2') }}",
            data: {
                // outlet_id: $('#outlet').val(),
                // start_date: datePicker.data('daterangepicker').startDate.format('YYYY-M-DD'),
                // end_date: datePicker.data('daterangepicker').endDate.format('YYYY-M-DD'),
                // days_type: $('.days_filter:checked').val(),
                // product_filter: $productFilter
            },
            dataType: 'json',
            type: 'get',
            success: function (d) {
				$('.sparkline-pie2').empty().prepend(d.data)
				$('.sparkline-pie2').sparkline('html', {
					type: 'pie',
					offset: 90,
					width: '100%',
					height: '100%',
					sliceColors: d.color
				});
				$('.legendPie').empty();
				$.each(d.helper, function( index, value ) {
					$('.legendPie').prepend(`<icon class="fa fa-square" style="`+value.color+`"></icon> <span>`+value.biller+`<span class="float-right">`+value.balance+`</span></span><br>`);
				});	
            },
            beforeSend: function () {
              
            },
            complete: function () {
               
            }
        });
    }

</script>