<meta name="csrf-token" content="{{ csrf_token() }}" />
<x-package-header />
<style>
	.image-container-product2 {
		border-radius: 8px;
		background-color: #7d85c9;
		width: 195px;
		height: 195px;
		position: relative;
		overflow: hidden;
	}

	.image-container-product2 a {
		width: 100%;
		text-align: center;
		padding: 5px 0;
		background-color: #5671c9;
		color: #FFF;
		display: inline-block;
	}

	.image-container-product2.image-exist a {
		width: 49%;
	}

	.image-container-product2 .overlay {
		position: absolute;
		bottom: 0;
		left: 0;
		right: 0;
		overflow: hidden;
		width: 100%;
		height: 0;
		transition: .5s ease;
		z-index: 100;
	}

	.image-container-product2:hover .overlay,
	.image-container-product2 img:hover .overlay {
		height: 30px;
	}

	.image-container-product2 img {
		position: absolute;
		left: 50%;
		top: 50%;
		height: 100%;
		width: auto;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}



	.validation-error-label,
	.validation-valid-label {
		margin-top: 7px;
		margin-bottom: 7px;
		display: block;
		color: #F44336;
		position: relative;
		padding-left: 26px;
	}

	.validation-valid-label {
		color: #4CAF50;
	}

	.validation-error-label:before,
	.validation-valid-label:before {
		position: absolute;
		top: 2px;
		left: 0;
		display: inline-block;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		font-size: 9px;
	}

	.validation-error-label:empty,
	.validation-valid-label:empty {
		display: none;
	}

	label.validation-error-label {
		font-weight: normal;
	}

	.text-wrap {
		white-space: normal;
	}

	.width-200 {
		width: 200px;
	}

	.width-180 {
		width: 180px;
	}

	.width-100 {
		width: 100px;
	}

	.width-80 {
		width: 80px;
	}

	.width-50 {
		width: 50px;
	}

	.width-30 {
		width: 30px;
	}

	.form-control {
		font-size: 11.5px;
	}

	.col-form-label {
		font-size: 11.5px !important;
	}

	table.dataTable tbody td {
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom th {
		font-size: 10.5px;
	}

	.table-custom td {
		font-size: 10px;
		word-break: break-word;
		vertical-align: top;
	}

	.table-custom tr {
		font-size: 12px;
	}

	.table-custom-info tr {
		font-size: 13px;
	}

	.pretty-checkbox {
		position: relative;
		margin-right: 1em;
		line-height: 1;
	}

    .zoom {
        padding: 0px;
        transition: transform .2s; /* Animation */
        width: 60px;
        height: 60px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(3.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }
</style>
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>User</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<!-- <button class="btn btn-sm btn-primary mr-1 exportExcel">
					<i class="fa fa-download"></i> Export Excel
				</button> -->

				<button id="addBtn" class="btn btn-sm btn-primary mr-1" data-toggle="modal" data-target="#ModalUser">
					<icon class="fa fa-plus"></icon>
				</button>
			</div>
		</div>

	</div>

	<div class="col-md-12" id="loadBar" style="display: none;">
		<br>
		<center><i class="fa fa-spinner fa-spin"></i></center>
	</div>


	<div class="container-fluid">
		<div class="row clearfix">

			<div class="card">

				<div class="body">
					<div class="table-responsive">
						<table style="font-size: 8px;" class="table table-bordered table-hover c_list  table-custom User-table" id="User-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Foto</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Role</th>
									<th>Tanggal Perubahan</th>
								</tr>
							</thead>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalUser" tabindex="-1" role="dialog" aria-labelledby="ModalUser" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" id="frmCreateUser" enctype="multipart/form-data">
			<center>
				<label for="foto">Foto</label><br>
				<img id="prevImg" scr="" style="width: 200px; height: 200px;">
				<div class="input-group mb-2" style="width: 200px; text-align: left;">
					<div class="">
						<input type="file" accept="image/*" class="custom-file-input" id="foto_input" name="foto" required>
						<label class="custom-file-label" for="foto_input">Choose file</label>
					</div>
				</div>
			</center>
			<div class="form-group">
				<label for="nama_input">Name</label>
				<input type="text" name="nama" class="form-control" id="nama_input" aria-describedby="namaHelp" placeholder="Masukan Nama" required>
				<small id="nama_txt" class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<label for="nama_input">Email</label>
				<input type="email" name="email" class="form-control" id="email_input" aria-describedby="namaHelp" placeholder="Masukan Nama" required>
				<small id="nama_txt" class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<label for="phone_input">Phone</label>
				<input type="phone" name="phone" class="form-control" id="phone_input" placeholder="Masukan Phone" required>
			</div>
			<div class="form-group">
				<label for="password_input">Password</label>
				<input type="password" name="password" class="form-control" id="password_input" placeholder="Masukan Password" required>
			</div>
			<div class="form-group">
				<label for="role_input">Role</label>
				<select id="role_input" name="role" class="form-control" required>
					<option selected disabled>-- Role --</option>
					<option value="super_admin">Superadmin</option>
					<option value="staff">Staff</option>
				</select>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary addUser" >Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal edit -->
<div class="modal fade" id="EditModalUser" tabindex="-1" role="dialog" aria-labelledby="EditModalUser" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" id="frmUpdateUser" enctype="multipart/form-data">
			<center>
				<label for="foto">Foto</label><br>
				<img id="edit_prevImg" scr="" style="width: 200px; height: 200px;">
				<div class="input-group mb-2" style="width: 200px; text-align: left;">
					<div class="">
						<input type="file" accept="image/*" class="custom-file-input" id="edit_foto_input" name="foto" required>
						<label class="custom-file-label" for="foto">Choose file</label>
					</div>
				</div>
			</center>
			<div class="form-group">
				<label for="nama_input">Name</label>
				<input type="text" name="nama" class="form-control" id="edit_nama_input" aria-describedby="namaHelp" placeholder="Masukan Nama" required>
				<small id="edit_nama_txt" class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<label for="nama_input">Email</label>
				<input type="email" name="email" class="form-control" id="edit_email_input" aria-describedby="namaHelp" placeholder="Masukan Nama" required>
				<small id="edit_email_txt" class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<label for="phone_input">Phone</label>
				<input type="phone" name="phone" class="form-control" id="edit_phone_input" placeholder="Masukan Phone" required>
			</div>
			<div class="form-group">
				<label for="password_input">Password</label>
				<input type="password" name="password" class="form-control" id="edit_password_input" placeholder="Masukan Password" required>
			</div>
			<div class="form-group">
				<label for="role">Role</label>
				<select id="edit_role_input" name="role" class="form-control" required>
					<option selected disabled>-- Role --</option>
					<option value="super_admin">Superadmin</option>
					<option value="staff">Staff</option>
				</select>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary UpdateUser" >Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>
<x-package-footer />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	var _baseURL = "<?php echo url(''); ?>";
	var IdActive = null;
	var optValidate = {
		ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
		errorClass: 'validation-error-label',
		successClass: 'validation-valid-label',
		highlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		// Different components require proper error label placement
		errorPlacement: function(error, element) {

			// Styled checkboxes, radios, bootstrap switch
			if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
				if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
					error.appendTo(element.parent().parent().parent().parent());
				} else {
					error.appendTo(element.parent().parent().parent().parent().parent());
				}
			}

			// Unstyled checkboxes, radios
			else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
				error.appendTo(element.parent().parent().parent());
			}

			// Input with icons and Select2
			else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
				error.appendTo(element.parent());
			}

			// Inline checkboxes, radios
			else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
				error.appendTo(element.parent().parent());
			}

			// Input group, styled file input
			else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
				error.appendTo(element.parent().parent());
			} else {
				error.insertAfter(element);
			}
		},
		validClass: "validation-valid-label"


	};

	foto_input.onchange = evt => {
		const [file] = foto_input.files
		if (file) {
			prevImg.src = URL.createObjectURL(file)
		}
	}

	edit_foto_input.onchange = evt => {
		const [file] = edit_foto_input.files
		if (file) {
			edit_prevImg.src = URL.createObjectURL(file)
		}
	}

	// var today = new Date();
	// var dd = today.getDate();
	// var mm = today.getMonth() + 1; //January is 0!
	// var yyyy = today.getFullYear();

	// if (dd < 10) {
	//   dd = '0' + dd
	// }

	// if (mm < 10) {
	//   mm = '0' + mm
	// }

	// today = yyyy + '-' + mm + '-' + dd;
	// // document.getElementById("fDate").setAttribute("min", today);

	// function getMinDate() {
	//   var minToDate = document.getElementById("fDate").value;
	//   document.getElementById("tDate").setAttribute("min", minToDate);
	// }



	$(document).ready(function() {
		$('.js-example-basic-multiple').select2({
			"width": '100%'

		});



		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	});


	$(function() {
		

		var oDataList = $('#User-table').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
            order: [[0, 'desc']],
			ajax: {
				url: "{{ url('user-list') }}",
				data: {
				},
			},

			dom: '<"toolbar-item-datatable"><"datatable-header"fl><"datatable-scroll-wrap"tr><"datatable-footer"p>',

			columns: [
                {
                    data: 'id',
					name: 'id',
                    width: "4%",
				},
                {
					data: 'foto',
					name: 'toto',
                    width: "1%",
                    render: function(data, type) {
                        return '<img src="'+data+'" class="img-rounded zoom"/>'
                    }
				},
				
				{
					data: 'nama',
					name: 'nama',
				},
				{
					data: 'email',
					name: 'email',
				},
				{
					data: 'phone',
					name: 'phone',
				},
				{
					data: 'role',
					name: 'role',
					render: function(data, type) {
                        return '<span class="'+(data === 'super_admin' ? 'text-danger' : 'text-primary')+'">'+ (data === 'super_admin' ? 'Super Admin' : 'Staff') +'</span>'
                    }
				},
				{
					data: 'updated_at',
					name: 'updated_at',
				},
				{
					data: 'action',
					name: 'Tindakan',
					searchable: false,
					orderable: false
				},

			],
		});

	});

	function showConfirm($message, $callback = function(result) {}, $title = '', $buttons = false) {
		if (!$buttons) {
			$buttons = {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-danger'
				}
			};
		}
		bootbox.confirm({
			title: $title,
			message: $message,
			buttons: $buttons,
			callback: $callback
		});
	}

	$(document).on("click", ".addUser", function(e) {
		e.preventDefault();
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreateUser").validate(optValidate);
		if ($("#frmCreateUser").valid()) {
			showConfirm('Apakah anda Yakin ingin menambahkan user ? ', function(result) {
				if (result) {
					addUser();
				}
			}, '<span style="color: blue;">Tambah User</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		} else {
			return;
		}
	});

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	function addUser() {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmCreateUser").validate(optValidate);

		if ($("#frmCreateUser").valid()) {
			var form = $("#frmCreateUser")[0];
			var formData = new FormData(form);

			console.log($("#frmCreateUser").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/user-create",
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#User-table').DataTable().ajax.reload();
						$('#ModalUser').modal('hide');
						toastr.success("User Berhasil ditambahkan")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;
		}

	}

	function deleteUser($id) {
		// Setup validation
		deleteTableList(
			"user-delete", [$id],
			true
		);
		return false;

	}

	function deleteTableList(url, $ids, closeLayer = false, message = 'Apakah anda Yakin untuk menghapus data ini') {
		if ($ids) {
			bootbox.confirm({
				title: 'Delete Data',
				message: message,
				buttons: {
					confirm: {
						label: 'Delete',
						className: 'btn-success'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-danger'
					}
				},
				callback: function(result) {
					if (result) {
						console.log(url)
						$.ajax({
							url: url + '/' + $ids,
							data: {
								"_token": "{{ csrf_token() }}",
							},
							dataType: 'json',
							type: 'delete',
							success: function(d) {
								if (d.status == 200) {

									$('#User-table').DataTable().ajax.reload();
									toastr.success("User berhasil dihapus")
								} else {
									toastr.error(d.message)
								}
							},
							error: function(d, statusText, xhr) {
								bootbox.alert('Oops. Error when processing your request. ' + xhr);
							},
							beforeSend: function() {},
							complete: function() {}
						});
					}
				}
			});
		} else {
			bootbox.alert("error");
		}

		return false;
	}

	$(document).on("click", ".btnEdit", function(e) {
		dataID = $(this).attr("data-id");
		IdActive = dataID; 
		$.ajax({
		url: _baseURL+'/user-detail/'+dataID,
		type:'GET',
		dataType:'HTML',
			success:function(res){
				var data = $.parseJSON(res);
				if(data.status === 'success'){
					$("#edit_nama_input").val(data.data.nama);
					$("#edit_email_input").val(data.data.email);
					$("#edit_phone_input").val(data.data.phone);
					$("#edit_role_input").val(data.data.role);
					edit_prevImg.src = data.data.foto
					$('#edit_password_input').attr('placeholder','Masukan Jika Ingin Merubah Password');
					$('#edit_password_input').attr('required', false); 
					$('#edit_foto_input').attr('required', false);
				}else{
					toastr.error("User tidak tersedia")
				}
			}
		});
	});

	$(document).on("click", ".UpdateUser", function(e) {
		e.preventDefault();
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmUpdateUser").validate(optValidate);
		if ($("#frmUpdateUser").valid()) {
			showConfirm('Apakah anda Yakin ingin merubah user ? ', function(result) {
				if (result) {
					UpdateUser();
				}
			}, '<span style="color: blue;">Update User</span>', {
				confirm: {
					label: 'Update',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
		} else {
			return;
		}
	});

	function UpdateUser() {
		// Setup validation
		optValidate.rules = {};
		optValidate.message = {};
		$("#frmUpdateUser").validate(optValidate);

		if ($("#frmUpdateUser").valid()) {
			var form = $("#frmUpdateUser")[0];
			var formData = new FormData(form);

			console.log($("#frmUpdateUser").serialize());


			// formData.append("destinationDescription", $content.getData());

			$.ajax({
				url: _baseURL + "/user-update/"+ IdActive,
				type: "post",
				dataType: "json",
				data: formData,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(toastr);
					if (d.status == 200) {
						$('#User-table').DataTable().ajax.reload();
						$('#EditModalUser').modal('hide');
						toastr.success("User Berhasil diubah")
					} else {
						toastr.error(d.message)
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
				beforeSend: function() {},
				complete: function() {},
			});

			return false;
		}

	}

</script>