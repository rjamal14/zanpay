<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Saldo Agen</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Saldo Agen</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="body">
						<div class="table-responsive check-all-parent">
							<table class="table table-custom table-hover m-b-0 c_list">
								<thead>
									<tr>
										<th>
											<label class="fancy-checkbox">
												<input class="check-all" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</th>
										<th>Name</th>
										<th>Jenis Payment</th>
										<th>Rekening</th>
										<th>Atas Nama</th>
										<th>Saldo</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td style="width: 50px;">
											<label class="fancy-checkbox">
												<input class="checkbox-tick" type="checkbox" name="checkbox">
												<span></span>
											</label>
										</td>
										<td>
											<img src="images/xs/avatar1.jpg" class="rounded-circle avatar" alt="">
											<p class="c_name">Biller</p>
										</td>
										<td><i class="fa fa-bank"></i> E-Money</td>
										<td>
											<span class="phone"><i class="fa fa-credit-card m-r-10"></i> 264-625-2583</span>
										</td>
										<td>
											<address><i class="fa fa-thumb-tack"></i> Sutijem</address>
										</td>
										<td>
											<i class="fa fa-money"></i> Rp. 2,000,000
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
