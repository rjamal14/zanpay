<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Calendar</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Calendar</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="body">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
