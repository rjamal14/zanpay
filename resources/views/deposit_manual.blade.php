<div class="card card-primary">
  
              <div class="card-header">
                <h3 class="card-title"> Tambah Deposit Manual</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" role="form" id="frmDepositManual" enctype="multipart/form-data">
			        @csrf
                <div class="card-body">
               
					<div class="form-group row">
					<label class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<select name="name" id ="selectReseller" class="js-example-basic-single form-control" required>
								<option  data-id ="Pillih Reseller" value="" disabled selected>-- Pilih Reseller --</option>
								@foreach ($group as $g)
								<option data-id ="{{$g['phone']}}" value="{{$g['id']}}">{{$g['reseller_code']}} - {{strtoupper($g['name'])}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
					<label class="col-sm-2 col-form-label">No HP</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="phoneInput" name="phone" placeholder ="Pilih Reseller" value=""  required readonly>
						</div>
					</div>
					<div class="form-group row">
					<label class="col-sm-2 col-form-label">Nominal</label>
						<div class="col-sm-10">
							<input type="number" required class="form-control" id="phoneInput" autocomplete="off" name="nominal" value="">
						</div>
					</div>

					<div class="form-group row">
					<label class="col-sm-2 col-form-label">Metode Pembayaran</label>
						<div class="col-sm-10">
							<select name="payment_method"  class="form-control" required>
							@foreach ($payment as $p)
							<option value="{{$p['id']}}">{{$p['name']}}  - {{$p['behalf']}} - {{$p['account']}}</option>
							@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
					<label class="col-sm-2 col-form-label">Pin Admin</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="phoneInput" name="pin" required autocomplete="off" />
						</div>
					</div>
				
				</div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="" id="tambahDepositManual"   class="btn btn-info tambahDepositManual">Submit</button>
                      <button id="cancelBtnEdit" type="button" class="btn btn-default cancelBtnEdit float-right">Cancel</button>
                    </div>
                    <!-- /.card-footer -->
                  </form>
        	</div>
          <script>
            	$('#cancelBtnEdit').click(function(){
			$('#depositManualForm').fadeOut();
			$('.btnEdit').fadeIn();
		});

        $(document).on("change", '#selectReseller', function(){
			console.log($('#selectReseller').find(':selected').attr('data-id'));
			$('#phoneInput').val($('#selectReseller').find(':selected').attr('data-id'));
		});

	$(document).ready(function() {
		$('.js-example-basic-single').select2();
	});


    $(document).on("click", "#tambahDepositManual", function(e) {
      optValidate.rules = {};
			optValidate.message = {};
			$("#frmDepositManual").validate(optValidate);
        e.stopImmediatePropagation();
			e.preventDefault();
      console.log($(this).attr("data-id"));
      if ($("#frmDepositManual").valid()) {
      
			showConfirm('Apakah anda yakin ingin menambahkan deposit manual ? ', function(result) {
				if (result) {
				tambahDeposit();
				}
			}, '<span style="color: blue;">Tambah deposit manual</span>', {
				confirm: {
					label: 'Tambah',
					className: 'btn-primary'
				},
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				}
			});
      
      
      }
     
		});
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
    $("#fileEdit").change(function (e) {
    var _URL2 = window.URL || window.webkitURL;
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL2.createObjectURL(file);
        img.onload = function () {
        
            _URL2.revokeObjectURL(objectUrl);
            console.log(' Current Dimension : '+this.width + " x " + this.height);
            $('#dimRec').text(' Current Dimension : '+this.width + " x " + this.height);
         
              $('#fileEdit').removeClass('is-invalid');
              $('#btnEditEvent').prop('disabled', false);
              $('#dimRec').removeClass('error');
        };
        img.src = objectUrl;
        preview_image_edit();
    }
});

function preview_image_edit() {
      $('#image_preview_edit').empty();
      $('#image_preview_edit').append("<img src='"+URL.createObjectURL(event.target.files[0])+"' width='150px' style='border-radius: 2px;'> &nbsp");
    }
		function tambahDeposit() {
			// Setup validation
			optValidate.rules = {};
			optValidate.message = {};
			$("#frmDepositManual").validate(optValidate);

			if ($("#frmDepositManual").valid()) {
				var form = $("#frmDepositManual")[0];
				var formData = new FormData(form);

				console.log($("#frmDepositManual").serialize());
			

				// formData.append("destinationDescription", $content.getData());

				$.ajax({
					url: _baseURL + "/tiket_deposit-manual",
					type: "post",
					dataType: "json",
					data: formData,
					processData: false,
					contentType: false,
					success: function(d) {
						console.log(toastr);
						if (d.status == 200) {
              toastr.success("Manual deposit Berhasil ditambahkan")
							$('#depositManualForm').fadeOut();
							$('#addform').fadeOut();
							$('#btnEdit').fadeIn();
            	$('#deposit-table').DataTable().ajax.reload();
						
           
						} else {
							toastr.error(d.message)
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					beforeSend: function() {
					},
					complete: function() {
     
					},
				});

				return false;
			}

		}
          </script>