<x-package-header />
<x-package-sidebar />
<div id="main-content">
	<div class="block-header">
		<div class="row clearfix">
			<div class="col-md-6 col-sm-12">
				<h2>Grup Reseller</h2>
			</div>
			<div class="col-md-6 col-sm-12 text-right">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
					<li class="breadcrumb-item active">Grup Reseller</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<div class="body">
						<div class="table-responsive check-all-parent">
							<table class="table table-custom table-hover m-b-0 c_list">
								<thead>
									<tr>
										<th>Nama Grup</th>
										<th>Deskripsi</th>
										<th>Markup</th>
										<th>Cashback</th>
										<th>Jumlah Reseller</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
									<tr>
										<td><i class="fa fa-group m-r-10"></i> Reseller</td>
										<td>
											<i class="fa fa-info-circle m-r-10"></i> -
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-money"></i> RP. 25
										</td>
										<td>
											<i class="fa fa-male"></i> 234,123
										</td>
										<td>
											<a href='profile' class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger btn-sm js-sweetalert" data-type="confirm" title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<x-package-footer />
