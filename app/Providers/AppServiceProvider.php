<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\View\Components\Header;
use App\View\Components\Sidebar;
use App\View\Components\Footer;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
		require_once __DIR__ . '/../Helpers/NumericHelper.php';
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Blade::component('package-header', Header::class);
		Blade::component('package-sidebar', Sidebar::class);
		Blade::component('package-footer', Footer::class);
	}
}
