<?php

namespace App\Http\Controllers\Voucher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Voucher;
use App\Models\VoucherLog;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\PaymentMethod;
use App\Models\Payment;

use DataTables;
use Session;
use DB;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class VoucherLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    public function fnData (Request $request)
    {


        $query = VoucherLog::with(['reseller','voucher'])->orderBy('created_at','desc')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('nominal', function ($q)  {
                return formatCurrency($q->reward);
        })
        ->addColumn('resellerCode', function ($q)  {
            if($q->reseller != null){
                return $q->reseller->reseller_code;
            }
        })
        ->addColumn('resellerName', function ($q)  {
            if($q->reseller != null){
                return $q->reseller->name;
            }
        })
        ->addColumn('resellerPhone', function ($q)  {
            if($q->reseller != null){
                return $q->reseller->phone;
            }
        })
        ->addColumn('referenceID', function ($q)  {
            if($q->voucher != null){
                $x= $q->voucher->reference_id;
                $y= $q->voucher->activity_group;
                if($x == null){
                    return "Semua Produk";
                } else{
                  
                    $x=explode(',',$x);
                    if($y == 0)
                    {

                        $data = PaymentMethod::whereIn('id',$x)->get();
                        $res=collect($data)->pluck('name')->toArray();
                        return implode(', ',collect($data)->pluck('name')->toArray());
                    }
                    else {
                        $data = Product::whereIn('id',$x)->get();
                        $res=collect($data)->pluck('fullname')->toArray();
                        return implode(', ',$res);
                    }
                }
            }
        })
        ->addColumn('grouP', function ($q)  {
            if($q->voucher != null){
                $x= $q->voucher->activity_group;
                if($x == 0){
                    return "Deposit";
                } else if($x == 1){
                    return "Transaksi";
                    
                } else {
                    
                    return "Merchandise";
                }
            }
        })
        ->addColumn('aktivitas', function ($q)  {
            if($q->voucher != null){
                $x= $q->voucher->activity_group;
                if($x == 0){
                    return Payment::where('id',$q->voucher->activity)->first()->title ?? "";
                } else if($x == 1){
                    return ProductGroup::where('id',$q->voucher->activity)->first()->fullname ?? "";
                } else {
                    return "Merchandise";
                }
            }
            // if($q->activity == 1){
            //     return 'DEPOSIT';
            // } else if($q->activity == 2){
            //     return 'Virtual Account & Modern Store';
            // } else if($q->activity == 3){
            //     return 'PULSA';
            // } else if($q->activity == 4){
            //     return 'DATA';
            // } else if($q->activity == 5){
            //     return 'TOKEN';
            // } else if($q->activity == 6){
            //     return 'E-MONEY';
            // } else if($q->activity == 7){
            //     return 'TAGIHAN';
            // } else if($q->activity == 7){
            //     return 'MERCHANDISE';
            // }
        })
        ->addColumn('tipe', function ($q)  {
            if($q->type == 0){
                return 'Cashback';
            } else if($q->type == 1){
                return 'Diskon';
            } 
        })
        ->addColumn('syarat', function ($q)  {
                return formatCurrency($q->requirement);
        })
        ->addColumn('jamTanggal', function ($q)  {
            return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
        })
    
        ->rawColumns( ['aktivitas','action'])
        ->make(true);
    }
    public function index (Request $request)
    {
        return view('voucher_log');
    }

}