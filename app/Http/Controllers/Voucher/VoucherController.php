<?php

namespace App\Http\Controllers\Voucher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Voucher;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\ProductGroup;
use App\Models\VoucherLog;
use App\Models\Product;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = Voucher::with('log')->where('is_deleted',0)->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('nominal', function ($q)  {
            if($q->reward_type == 0){
                return $q->reward.'%';
            } else {
                return formatCurrency($q->reward);
            }
        })
        ->addColumn('maxim', function ($q)  {
                return formatCurrency($q->maximum);
        })
        // 1=deposit, 2=va, 3=pulsa, 4=data, 5=token, 6=emoney, 7=tagihan, 8=merchandise
        ->addColumn('aktivitas', function ($q)  {
            if($q->activity_group == 0){
                $a = Payment::where('id',$q->activity)->first();
                if($a){

                    return $a->title; 
                } else {
                    return ""; 
                }
            } else if($q->activity_group == 1){
                $a= ProductGroup::where('id',$q->activity)->first();
                if($a){
                    return $a->command.' '.$a->provider;
                } else {
                    return "";
                }
            } else{
                return "Merchandise";
            }
        })
        ->addColumn('tipe', function ($q)  {
            if($q->type == 0){
                return 'Cashback';
            } else if($q->type == 1){
                return 'Diskon';
            } 
        })
        ->addColumn('pemakaian', function ($q)  {
            if($q->multiple == 0){
                return 'Hanya Sekali';
            } else{
                 if($q->periode == 1){
                    return 'Berulang kali, '.$q->many_times.'x dalam sehari';
                } else if($q->periode == 2){
                    return 'Berulang kali, '.$q->many_times.'x dalam seminggu';
                } else {
                    return 'Berulang kali, '.$q->many_times.'x dalam sebulan';
                }
            }
        })
    
        ->addColumn('syarat', function ($q)  {
        
                return formatCurrency($q->requirement);
        })
        ->addColumn('updatedAt', function ($q)  {
            if($q->updated_at == null){
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            } else {
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->updated_at)))." " . date('H:i:s',strtotime($q->updated_at));
            }
        })
        ->addColumn('updatedBy', function ($q)  {
            if($q->updated_by == null){
                return ($q->created_by);
            } else {
                return $q->updated_by;
            }
        })
    
        ->rawColumns( ['aktivitas','action'])
        ->make(true);
    }

  
    public function groupCombobox (Request $request)
    {
        if($request->type == 0){
            $data = Payment::all();
            $res = [];
            foreach($data as $k=>$d){
                $name = explode(' ',$d['title']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama = substr($name[0],0,2).substr($name[0],0,1);
                }
                $res[$k]['id']=$d['id'];
                $res[$k]['text']=$d['title'];
                $res[$k]['name']=strtoupper($nama);
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        } else  if($request->type == 1){
            $data = ProductGroup::groupBy('command')->get();
            $res = [];
            foreach($data as $k=>$d){
                $name = explode(' ',$d['command']);
                if (count($name) > 1){
                    if($name[1] == "&"){
                        $name2 = $name[2];
                    } else {
                        $name2 = $name[1];
                    }
                    $nama = substr($name[0],0,2).substr($name2,-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $res[$k]['id']=$d['id'];
                $res[$k]['text']=$d['command'];
                $res[$k]['name']=strtoupper($nama);
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        } else {
            return response()->json([
                'data'=>[],
                'message'=>'success'
            ]);
        }
    }
    public function groupCombobox2 (Request $request)
    {
       if(isset($request->command)) {

            $data = ProductGroup::groupBy('provider')->where('command',$request->command)->get();
            $res = [];
            foreach($data as $k=>$d){
                $name = explode(' ',$d['provider']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $res[$k]['id']=$d['id'];
                $res[$k]['text']=$d['provider'];
                $res[$k]['name']=strtoupper($nama);
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        } else {
            return response()->json([
                'data'=>[],
                'message'=>'success'
            ]);
        }
    }
    public function groupComboboxRef (Request $request)
    {
    //    dd($request->all());
       if($request->group==0) {
            $data = PaymentMethod::where('status',1)->where('payment_id',$request->type)->get();
            $res = [];
            foreach($data as $k=>$d){
                $name = explode(' ',$d['name']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $res[$k]['id']=$d['id'];
                $res[$k]['text']=$d['name'];
                $res[$k]['name']=strtoupper($nama);
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        }  else if($request->group==1) {
            $data = Product::where('is_deleted',0)->where('group_id',$request->type)->get();
            $res = [];
            foreach($data as $k=>$d){
                $name = explode(' ',$d['name']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $res[$k]['id']=$d['id'];
                $res[$k]['text']=$d['name']." (".$d['sku_code'].") ";
                $res[$k]['name']=strtoupper($nama);
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        } else {
            return response()->json([
                'data'=>[],
                'message'=>'success'
            ]);
        }
    }
    public function index (Request $request)
    {
       
        $data = Voucher::where('id',$request->id)->first();
        $tipe =[
            [
                'id'=>0,
                'name'=>'Cashback',
                'text'=>'CB',
            ],
            [
                'id'=>1,
                'name'=>'Diskon',
                'text'=>'DS',
            ]
        ];
        $activityGroup =[
            [
                'id'=>0,
                'name'=>'Deposit',
                'text'=>'DEP'
            ],
            [
                'id'=>1,
                'name'=>'Transaksi',
                'text'=>'TRS',
            ],
            [
                'id'=>2,
                'name'=>'Merchandise',
                'text'=>'MCD',
            ]
        ];
        $activity =[
            [
                'id'=>0,
                'name'=>'Deposit',
                'text'=>'DEP'
            ],
            [
                'id'=>1,
                'name'=>'Transaksi',
                'text'=>'TRS',
            ],
            [
                'id'=>2,
                'name'=>'Merchandise',
                'text'=>'MCD',
            ]
        ];


        $rewardType =[
            [
                'id'=>0,
                'name'=>'Percent'
            ],
            [
                'id'=>1,
                'name'=>'Nominal'
            ]
        ];
        $status =[
          [
              'id'=>1,
              'name'=>'Aktif'
          ],
          [
              'id'=>0,
              'name'=>'Non Aktif'
          ]
        ];
        return view('voucher')
        ->withStatus($status)
        ->withRewardType($rewardType)
        ->withAktivitas($activity)
        ->withActivityGroup($activityGroup)
        ->withTipe($tipe);

    }
   
    public function edit (Request $request,$id)
    {
        $data = Voucher::where('id',$request->id)->first();
        $hkelp=null;
        if($data->activity_group == 1 ){
            $hkelp=ProductGroup::where('id',$data->activity)->first();
        }

        $tipe =[
            [
                'id'=>0,
                'name'=>'Cashback',
                'text'=>'CB',
            ],
            [
                'id'=>1,
                'name'=>'Diskon',
                'text'=>'DS',
            ]
        ];
        $activityGroup =[
            [
                'id'=>0,
                'name'=>'Deposit',
                'text'=>'DEP'
            ],
            [
                'id'=>1,
                'name'=>'Transaksi',
                'text'=>'TRS',
            ],
            [
                'id'=>2,
                'name'=>'Merchandise',
                'text'=>'MCD',
            ]
        ];
        $activity =[
            [
                'id'=>0,
                'name'=>'Deposit',
                'text'=>'DEP'
            ],
            [
                'id'=>1,
                'name'=>'Transaksi',
                'text'=>'TRS',
            ],
            [
                'id'=>2,
                'name'=>'Merchandise',
                'text'=>'MCD',
            ]
        ];


        $rewardType =[
            [
                'id'=>0,
                'name'=>'Percent'
            ],
            [
                'id'=>1,
                'name'=>'Nominal'
            ]
        ];
        $status =[
          [
              'id'=>1,
              'name'=>'Aktif'
          ],
          [
              'id'=>0,
              'name'=>'Non Aktif'
          ]
        ];
        $payment = Payment::all();
        $respayment = [];
        foreach($payment as $k=>$d){
            $name = explode(' ',$d['title']);
            if (count($name) > 1){
                $nama = substr($name[0],0,2).substr($name[1],-1,1);
            } else {
                $nama = substr($name[0],0,2).substr($name[0],0,1);
            }
            $respayment[$k]['id']=$d['id'];
            $respayment[$k]['text']=$d['title'];
            $respayment[$k]['name']=strtoupper($nama);
        }


        $refre = PaymentMethod::where('status',1)->get();
        $resRef = [];
        foreach($refre as $k=>$d){
            $name = explode(' ',$d['name']);
            if (count($name) > 1){
                $nama = substr($name[0],0,2).substr($name[1],-1,1);
            } else {
                $nama2 = $name[0];
                $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
            }
            $resRef[$k]['id']=$d['id'];
            $resRef[$k]['text']=$d['name'];
            $resRef[$k]['name']=strtoupper($nama);
        }
        $RefData=explode(',',$data->reference_id);

            $dataPr = Product::where('is_deleted',0)->with('group')->get();
         
            $RefData2 = [];
            foreach($dataPr as $k=>$d){
                $name = explode(' ',$d['name']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $RefData2[$k]['id']=$d['id'];
                $RefData2[$k]['text']=$d['name']." (".$d['sku_code'].") ";
                $RefData2[$k]['name']=strtoupper($nama);
            }

            $datapayment2 = ProductGroup::groupBy('command')->get();
            $payment2 = [];
            foreach($datapayment2 as $k=>$d){
                $name = explode(' ',$d['command']);
                if (count($name) > 1){
                    if($name[1] == "&"){
                        $name2 = $name[2];
                    } else {
                        $name2 = $name[1];
                    }
                    $nama = substr($name[0],0,2).substr($name2,-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $payment2[$k]['id']=$d['id'];
                $payment2[$k]['text']=$d['command'];
                $payment2[$k]['name']=strtoupper($nama);
            }

            $dataProvid = ProductGroup::groupBy('provider')->get();
            $resProvider = [];
            foreach($dataProvid as $k=>$d){
                $name = explode(' ',$d['provider']);
                if (count($name) > 1){
                    $nama = substr($name[0],0,2).substr($name[1],-1,1);
                } else {
                    $nama2 = $name[0];
                    $nama = substr($name[0],0,2).$nama2[strlen($nama2)-1];
                }
                $resProvider[$k]['id']=$d['id'];
                $resProvider[$k]['text']=$d['provider'];
                $resProvider[$k]['name']=strtoupper($nama);
            }
        return view('voucher_edit')
        ->withStatus($status)
        ->withRewardType($rewardType)
        ->withAktivitas($activity)
        ->withActivityGroup($activityGroup)
        ->withData($data)
        ->withPayment($respayment)
        ->withHKelp($hkelp)
        ->withPayment2($payment2)
        ->withProvider($resProvider)
        ->withRef($resRef)
        ->withRefData($RefData)
        ->withRefData2($RefData2)
        ->withTipe($tipe)
        ->withActivityGroupData($data->activity_group);
    }
    public function update (Request $request,$id)
    {
        // validasi Kode
        // dd($request->all());
            $cek = Voucher::where('id','!=',$id)->where('code',$request->code)->count();
            if($cek > 0){
                return response()->json([
                    'status' => 400,
                    'message' => "Code sudah Terdaftar di voucher lain",
                    'data'=>[]
                ]);
            }

            $productNew =  Voucher::where('id',$id)->first();



            if($request->group == 0) {
                $activity=$request->activity;
            } else {
                $activity=$request->provider;
            }
          

            $productNew->activity = $activity;
            if(!empty($request->reference_id)) {
                $ref=implode(',',$request->reference_id);
            } else {
                $ref=null;
            }


            $productNew->code = $request->code;
            $productNew->title = $request->title;
            $productNew->description = $request->description;
            $productNew->activity_group = $request->group;
            if($request->group == 0) {
                $activity=$request->activity;
            } else  if($request->group == 1)  {
                $activity=$request->provider;
            } else {
                $activity=0;
            }
          

            $productNew->activity = $activity;
            if(!empty($request->reference_id)) {
                $ref=implode(',',$request->reference_id);
            } else {
                $ref=null;
            }
            if($request->multiple == 0){
                $many_time = 0;
                $periode = 0;
            } else {
                $many_time = $request->many_times;
                $periode = $request->periode;
            }
            // dd($ref);
            $productNew->reference_id = $ref;
            $productNew->requirement = $request->requirement;
            $productNew->type = $request->type;
            $productNew->reward = $request->reward;
            $productNew->reward_type = $request->reward_type;
            $productNew->quota = $request->quota;
            $productNew->maximum = $request->maximum;
            $productNew->quota_left = $request->quota;
            $productNew->status = $request->status;
            $productNew->created_at = $request->created_at;
            $productNew->expired_at = $request->expired_at;
            $productNew->updated_by =  $request->session()->get('SESS_USERDATA')['nama'];
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->multiple =$request->multiple ;
            $productNew->many_times =$many_time;
            $productNew->periode =$periode;
           
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Voucher berhasil diupdate",
                        'data'=>$request->all()
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (Voucher)',
                    ]);
            }
    }


    public function store (Request $request)
    {
        // validasi Kode
      
         
            $cek = Voucher::where('code',$request->code)->count();
            if($cek > 0){
                return response()->json([
                    'status' => 400,
                    'message' => "Code sudah Terdaftar di voucher lain",
                    'data'=>[]
                ]);
            }
            $productNew =  new Voucher();

            $productNew->code = $request->code;
            $productNew->title = $request->title;
            $productNew->description = $request->description;
            $productNew->activity_group = $request->group;
            if($request->group == 0) {
                $activity=$request->activity;
            } else  if($request->group == 1)  {
                $activity=$request->provider;
            } else {
                $activity=0;
            }
            // dd($activity);

            $productNew->activity = $activity;
            if(!empty($request->reference_id)) {
                $ref=implode(',',$request->reference_id);
            } else {
                $ref=null;
            }
            // dd($ref);
            if($request->multiple == 0){
                $many_time = 0;
                $periode = 0;
            } else {
                $many_time = $request->many_times;
                $periode = $request->periode;
            }
            $productNew->reference_id = $ref;
            $productNew->requirement = $request->requirement;
            $productNew->type = $request->type;
            $productNew->reward = $request->reward;
            $productNew->reward_type = $request->reward_type;
            $productNew->quota = $request->quota;
            $productNew->maximum = $request->maximum;
            $productNew->quota_left = $request->quota;
            $productNew->status =1;
            $productNew->multiple =$request->multiple ;
            $productNew->many_times =$many_time;
            $productNew->periode =$periode;
            $productNew->created_by =  $request->session()->get('SESS_USERDATA')['nama'];
            $productNew->created_at = $request->created_at;
            $productNew->expired_at = $request->expired_at;

           
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Voucher berhasil ditambahkan",
                        'data'=>$request->all()
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (Voucher)',
                    ]);
            }
    }
    public function delete (Request $request,$id)
    {
        Voucher::where('id',$id)->delete();

        return response()->json([
            'status' => 200,
            'message' => "Voucher berhasil dihapus"
        ]);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}