<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api', ['except' => ['register', 'login']]);
	}

	public function register(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'name' => 'required|max:45',
			'email' => 'required|unique:users|max:45',
			'phone' => 'required|unique:users|max:15',
			'password' => 'required|min:6',
		]);
		$email = $request->input('email');
		$phone = $request->input('phone');
		$password = $request->input('password');
		$name = $request->input('name');
		$created_at = Carbon::now();
		$id_users = null;
		$data_user = array(
			'email' => $email,
			'phone' => $phone,
			'password' => $password,
			'name' => $name,
			'created_at' => $created_at,
		);
		if (!$validator->fails()) {
			try {
				$insert_users = Users::create($data_user);
				$id_users = $insert_users->id;
				try {
					DB::table('user_setting')->insert([
						'id_users' => $id_users,
						'languange' => 'en',
					]);
					$data = array('Please check your email for verification code.');
				} catch (QueryException $exception) {
					$error[] = $exception->getMessage();
				}
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => 'Register']), 'data' => $error];
			$status = 200;
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => 'Register']), 'data' => $data];
			$status = 201;
		}
		return response()->json($content, $status);
	}

	public function login(Request $request)
	{
		$error = array();
		$data = array();
		$status = 200;
		$validator = Validator::make($request->all(), [
			'email' => 'email|required',
			'password' => 'required',
		]);
		$email = $request->input('email');
		$password = $request->input('password');
		$remember = $request->input('remember');
		$id_user_groups = $request->input('id_user_groups');
		$exp = $remember ? 1051200 : 1440;
		$logedin = false;
		if (!$validator->fails()) {
			try {
				$users = DB::table('users')
					->select('id', 'email', 'password', 'is_active')
					->where('email', '=', $email)
					->where('is_deleted', '=', false);
				if ($users->count() === 0) {
					$error[] = __('Email not found.');
				} else {
					if ($users->first()->is_active === false) {
						$error[] = __('Account blocked.');
					} else {
						if (Hash::check($password, $users->first()->password) === false) {
							$error[] = __('Password incorect.');
						} else {
							try {
								$user_group_users = DB::table('user_group_users AS a')
									->leftJoin('user_groups AS b', function ($join) {
										$join->on('b.id', '=', 'a.id_user_groups');
										$join->on('b.is_deleted', '=', false);
									})
									->leftJoin('user_group_privileges AS c', 'c.id_user_groups', '=', 'a.id_user_groups')
									->select('a.id_user_groups', 'b.name')
									->distinct()
									->where('a.id_users', '=', $users->first()->id)
									->where('c.privileged', '=', true);
								if ($user_group_users->count('a', 'b', 'c') === 0) {
									$error[] = __('User has no group privileges.');
								} else {
									if ($user_group_users->count('a', 'b', 'c') === 1) {
										$id_user_groups = $user_group_users->first()->id_user_groups;
									}
									if ($user_group_users->count('a', 'b', 'c') > 1) {
										$data = $user_group_users->get();
									}
									if ($id_user_groups) {
										$credentials = request(['email', 'password']);
										$claims = array('id_user_groups' => $id_user_groups);
										if (!$token = auth()->setTTL($exp)->claims($claims)->attempt($credentials)) {
											$error[] = __('Invalid credentials.');
										} else {
											$logedin = true;
											$data = ['jwt' => $this->getToken($token), 'user' => auth()->user()];
										}
									}
								}
							} catch (QueryException $exception) {
								$error[] = $exception->getMessage();
							}
						}
					}
				}
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => 'Login']), 'data' => $error];
			$status = 401;
		} else {
			$content = ['success' => true, 'logedin' => $logedin, 'message' => __('messages.success', ['Attribute' => 'Login']), 'data' => $data];
			$status = 200;
		}
		return response()->json($content, $status);
	}

	public function logout()
	{
		auth()->logout();
		$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => 'Logout']), 'data' => []];
		return response()->json($content);
	}

	public function refreshToken()
	{
		$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => 'Refresh token']), 'data' => $this->getToken(auth()->refresh())];
		return response()->json($content);
	}

	protected function getToken($token)
	{
		return [
			'token' => $token,
			'typ' => 'Bearer',
			'exp' => auth('api')->factory()->getTTL() * 60
		];
	}

	public function user()
	{
		$error = array();
		$data = array();
		$status = 200;
		if (auth()->check()) {
			try {
				$user = DB::table('users AS a')
					->leftJoin('user_group_users AS b', 'b.id_users', '=', 'a.id')
					->leftJoin('user_groups AS c', function ($join) {
						$join->on('c.id', '=', 'b.id_user_groups');
						$join->where('c.is_deleted', '=', false);
					})
					->select('a.name as name_user', 'a.email', 'a.email_verified_at', 'a.phone', 'a.phone_verified_at', 'a.is_active', 'c.id AS id_user_groups', 'c.name AS name_user_groups')
					->where('a.id', '=', auth()->user()->id)
					->where('c.id', '=', auth()->payload()->get('id_user_groups'));
				$data = $user->first();
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			$error[] = 'Error';
			$status = 403;
		}
		if ($error) {
			$content = array('success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error);
		} else {
			$content = array('success' => true, 'message' =>  __('messages.success', ['Attribute' => null]), 'data' => $data);
		}
		return response()->json($content, $status);
	}

	public function userSetting(Request $request)
	{
		$error = array();
		$data = array();
		$status = 200;
		if (auth()->check()) {
			switch ($request->method()) {
				case 'GET':
					try {
						$user_setings = DB::table('user_setting AS a')
							->leftJoin('users AS b', 'b.id', '=', 'a.id_users')
							->select('a.languange', 'a.dark_mode')
							->where('a.id', '=', auth()->user()->id);
						$data = $user_setings->first();
					} catch (QueryException $exception) {
						$error[] = $exception->getMessage();
					}
					break;
				case 'PUT':
					try {
						$user_setings = DB::table('user_setting');
						$user_setings->where('id', '=', auth()->user()->id)
							->update($request->all());
						$data = $user_setings->first();
					} catch (QueryException $exception) {
						$error[] = $exception->getMessage();
					}
					break;
				default:
					$error[] = __('messages.error');
			}
			if ($error) {
				$content = array('success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error);
			} else {
				session()->put('locale', $data->languange);
				$content = array('success' => true, 'message' =>  __('messages.success', ['Attribute' => null]), 'data' => $data);
			}
		} else {
			$error[] = 'Error';
			$status = 403;
		}
		return response()->json($content, $status);
	}

	public function modules()
	{
		$error = array();
		$data = array();
		$status = 200;
		if (auth()->check()) {
			try {
				$modules = DB::table('modules AS a')
					->join('module_actions AS b', function ($join) {
						$join->on('b.id_modules', '=', 'a.id');
						$join->where('b.name', '=', 'read');
						$join->where('b.is_deleted', '=', false);
					})
					->join('user_group_privileges AS c', 'c.id_module_actions', '=', 'b.id')
					->join('user_groups AS d', function ($join) {
						$join->on('d.id', '=', 'c.id_user_groups');
						$join->where('d.is_deleted', '=', false);
					})
					->select('a.id', 'a.code', 'a.name', 'a.icon', 'a.class', 'a.path')
					->where('a.is_active', '=', true)
					->where('a.is_deleted', '=', false)
					->where('c.privileged', '=', true)
					->where('c.id_user_groups', '=', auth()->payload()->get('id_user_groups'))
					->orderBy('index', 'ASC')
					->orderBy('id', 'ASC');
				$data = $modules->get();
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			$error[] = 'Error';
			$status = 403;
		}
		if ($error) {
			$content = array('success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error);
		} else {
			$content = array('success' => true, 'message' =>  __('messages.success', ['Attribute' => null]), 'data' => $data);
		}
		return response()->json($content, $status);
	}
}
