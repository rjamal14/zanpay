<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class UsersController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api');
	}

	public function read(Request $request, $path)
	{
		switch ($path) {
			case 'data':
				return response()->json($this->data($request));
				break;
			case 'detail':
				return response()->json($this->detail($request));
				break;
			default:
				return response()->json(['success' => false, 'message' => 'The server has not found anything matching the Request-URI.', 'data' => []], 401);
		}
	}

	public function data($request)
	{
		$rowCount = 0;
		$data = array();
		$error = array();
		$search = $request->input('search');
		$page = $request->input('page') != null ? $request->input('page') : 0;
		$pageSize = $request->input('pageSize') != null ? $request->input('pageSize') : 10;
		$field = $request->input('field');
		$sort = $request->input('sort');
		$offset = $page * $pageSize;
		$limit = $pageSize;
		try {
			$users = DB::table('users');
			$rowCount = $users->count();
			$users->select('id', 'name', 'email', 'phone', 'is_active')
				->where('is_deleted', '=', false)
				->offset($offset)
				->limit($limit);
			if ($search) {
				$users->orWhere(function ($query) use ($search) {
					$query->where('name', 'ILIKE', '%' . $search . '%')->where('email', 'ILIKE', '%' . $search . '%')->where('phone', 'ILIKE', '%' . $search . '%');
				});
			}
			if ($sort) {
				$users->reorder($field, $sort);
			} else {
				$users->reorder('id', 'DESC');
			}
			foreach ($users->get() as $row) {
				$row->group = DB::table('user_groups AS a')
					->leftJoin('user_group_users AS b', 'b.id_user_groups', '=', 'a.id')
					->select('a.name')
					->where('b.id_users', $row->id)->get();
				$data[] = $row;
			}
		} catch (QueryException $exception) {
			$error[] = $exception->getMessage();
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'rowCount' => $rowCount, 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'rowCount' => $rowCount, 'data' => $data];
		}
		return $content;
	}

	public function detail($request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);
		if (!$validator->fails()) {
			try {
				$data = DB::table('users')
					->select('id', 'name', 'email', 'phone', 'is_active')
					->where('is_deleted', '=', false)
					->find($request->input('id'));
				$data->group =
					$data->group = DB::table('user_groups AS a')
					->leftJoin('user_group_users AS b', 'b.id_user_groups', '=', 'a.id')
					->where('a.is_deleted', '=', false)
					->select('a.name')
					->where('b.id_users', $data->id)
					->get();
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'data' => $data];
		}
		return $content;
	}

	public function update(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
			'is_active' => 'required',
		]);
		$data = $request->all();
		$data['updated_at'] = Carbon::now();
		$data['update_by'] = auth()->id();
		if (!$validator->fails()) {
			try {
				DB::table('users')->where('id', $data['id'])->update($data);
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.update_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.update_data_success'), 'data' => []];
		}
		return response()->json($content);
	}

	public function delete(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);
		$data = array(
			'is_deleted' => true,
			'updated_at' => Carbon::now(),
			'deleted_by' => auth()->id(),
		);
		if (!$validator->fails()) {
			try {
				DB::table('users')->where('id', $data['id'])->update($data);
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.delete_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.delete_data_success'), 'data' => $data];
		}
		return response()->json($content);
	}
}
