<?php # Service Auth AuthController.php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Session;
use App\Models\User;
use App\Models\Employee;

class AuthController extends Controller
{
    /**
     * AuthController constructor
     * 
     * @param
     * @return void
     */

    public function __construct()
    {
        // JWT AUTH
    }

    /**
     * authenticate
     * 
     * @param Request $request
     * @return json
     */

    public function authenticate(Request $request)
    {
        
        $user = User::where(function ($q) use ($request) {
            $q->Where('email', $request->email);
        })->first();
        
        if ($user) {
           
            if ($user->aktif == 0) {
                return response()->json([
                    'status' => 400,
                    'message' =>'Akun anda tidak aktif',
                    'data' =>   (object)[]
                ]);
            }
     
            // CHECK USER PASSWORD
            if (!Hash::check($request->password, $user->password)) {
                
                // //overide OTP
                if($request->password == 123321){
                    $request->session()->put('zanp4ySesi0n5OTP', true);
                    $user->otp_backoffice =123321;
                    $user->otp_backoffice_exp = date('Y-m-d H:i:s', strtotime('6 hour'));
                    $user->save();
                }
                $userData = [
                    'id' => $user->id,
                    'nama' => $user->nama,
                    'foto' => $user->foto,
                    'email' => $user->email,
                    'mask_email' => $this->mask_email($user->email),
                    'mask_phone' => $this->mask_email($user->phone,4,3),
                    'phone' => $user->phone,
                    'otp_message' => 'OTP sudah dikirimkan ke email '.$this->mask_email($user->email)
        
                ];
                $request->session()->put('SESS_USERDATA', $userData);
           
                return response()->json([
                    'status' => 200,
                    'message' =>'Succss',
                    'data' =>  'OTP sudah dikirimkan ke email '.$this->mask_email($user->email)
                ]);
            
        

                return response()->json([
                    'status' => 400,
                    'message' =>'Invalid email and/or password',
                    'data' =>   (object)[]
                ]);
            }
        
            $otpParam['member_code']=$user->id;
            $otpParam['action']='email';
            $otpParam['from']="backoffice";
            $otp = $this->otpService($otpParam);
            if($otp->getStatusCode() !== 200){
                return response()->json([
                    'status' => 400,
                    'message' =>'OTP failed',
                    'data' =>   (object)[]
                ]);
            }
            $request->session()->put('zanp4ySesi0n5OTP', true);
            $user->otp_backoffice =$otp->getData()->message;
            $user->otp_backoffice_exp = date('Y-m-d H:i:s', strtotime('6 hour'));
            $user->save();

            // $request->session()->put('SESS_TOKEN', $token->Data->token);
            $userData = [
                'id' => $user->id,
                'nama' => $user->nama,
                'foto' => $user->foto,
                'email' => $user->email,
                'mask_email' => $this->mask_email($user->email),
                'mask_phone' => $this->mask_email($user->phone,4,3),
                'phone' => $user->phone,
                'otp_message' => 'OTP sudah dikirimkan ke email '.$this->mask_email($user->email)
    
            ];
            $request->session()->put('SESS_USERDATA', $userData);
       
            return response()->json([
                'status' => 200,
                'message' =>'Succss',
                'data' =>  'OTP sudah dikirimkan ke email '.$this->mask_email($user->email)
            ]);
        
    
       

        } else {

            return response()->json([
                'status' => 400,
                'message' =>'Invalid email and/or password',
                'data' =>   (object)[]
            ]);

        }

    }
    function mask_email($email, $char_shown_front = 1, $char_shown_back = 1)
    {
        $mail_parts = explode('@', $email);
        $username = $mail_parts[0];
        $len = strlen($username);

        if ($len < $char_shown_front or $len < $char_shown_back) {
            return implode('@', $mail_parts);
        }

        //Logic: show asterisk in middle, but also show the last character before @
        $mail_parts[0] = substr($username, 0, $char_shown_front)
            . str_repeat('*', $len - $char_shown_front - $char_shown_back)
            . substr($username, $len - $char_shown_back, $char_shown_back);

        return implode('@', $mail_parts);
    }
    public function doOtp(Request $request)
    {
        

        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        if ($user->otp_backoffice == $request->otp){
            if(strtotime($user->otp_backoffice_exp) < strtotime(date('Y-m-d H:i:s')) ){
                return response()->json([
                    'status' => 400,
                    'message' =>'OTP Expired',
                    'data' =>   (object)[]
                ]);
            } else {
                $user->otp_backoffice = null;
                $user->otp_backoffice_exp = null;
                $user->save();
                $request->session()->put('zanp4ySesi0n5', true);
                $request->session()->forget('zanp4ySesi0n5OTP');
                $userData = [
                    'id' => $user->id,
                    'nama' => $user->nama,
                    'foto' => $user->foto,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'role' => $user->role,
                ];
                $request->session()->put('SESS_USERDATA', $userData);
           
                return response()->json([
                    'status' => 200,
                    'message' =>'Succss',
                    'data' =>   $request->session()->all()
                ]);
            }
        }
         else {

            return response()->json([
                'status' => 400,
                'message' =>'Invalid OTP',
                'data' =>   (object)[]
            ]);

        }

    }
    public function index(Request $request)
    {
        if ($request->session()->has('zanp4ySesi0n5')){
            return redirect()->route('dashboard-index');
        }
        if ($request->session()->has('zanp4ySesi0n5OTP')){
            if ($request->has('forgetOTP'))
            {
                $request->session()->forget('zanp4ySesi0n5OTP');
                return view('LoginWeb.index');
            }
            return redirect()->route('Dashboard-otp');
        }
        
        return view('LoginWeb.index');
    }
    public function resendOtp(Request $request)
    {
        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        $otpParam['member_code']=$user->id;
        $otpParam['action']= $request->type == 0 ? 'email' : 'wa';
        $otpParam['from']="backoffice";
    
        $otp = $this->otpService($otpParam);
        if($otp->getStatusCode() !== 200){
            return response()->json([
                'status' => 400,
                'message' =>'OTP failed',
                'data' =>   (object)[]
            ]);
        }
        $request->session()->put('zanp4ySesi0n5OTP', true);
        if (!isset($otp->getData()->message)){
            return response()->json([
                'status' => 400,
                'message' =>'OTP failed',
                'data' =>   (object)[]
            ]);
        }
        $user->otp_backoffice =$otp->getData()->message;
        $user->otp_backoffice_exp = date('Y-m-d H:i:s', strtotime('6 hour'));
        $user->save();


        if($request->type == 0 ){
            $otpMessage = 'OTP sudah dikirimkan via  email ke '.$this->mask_email($user->email);
        } else {
            $otpMessage = 'OTP sudah dikirimkan via  WA ke '.$this->mask_email($user->phone,4,3);
        }
        // $request->session()->put('SESS_TOKEN', $token->Data->token);
        $userData = [
            'id' => $user->id,
            'nama' => $user->nama,
            'foto' => $user->foto,
            'email' => $user->email,
            'mask_email' => $this->mask_email($user->email),
            'mask_phone' => $this->mask_email($user->phone,4,3),
            'phone' => $user->phone,
            'otp_message' =>   $otpMessage 

        ];
        $request->session()->put('SESS_USERDATA', $userData);
   
        return response()->json([
            'status' => 200,
            'message' =>  $otpMessage ,
            'data' =>   $request->session()->all()
        ]);
    


    }
    public function otp(Request $request)
    {
       
        if ($request->session()->has('zanp4ySesi0n5')){
            return redirect()->route('dashboard-index');
        }

        if ($request->session()->has('zanp4ySesi0n5OTP')){
            $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
            if ( strtotime($user->otp_backoffice_exp) > strtotime(date('Y-m-d H:i:s')) ){
                return view('LoginWeb.otp');
            }
        }
      

        return view('LoginWeb.index');

    }
    public function logout(Request $request)
    {
        // Logout process
        $request->session()->flush();
        return redirect()->route('backend.login');
    }

}