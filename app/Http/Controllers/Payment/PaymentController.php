<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\PaymentMethod;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

  
    public function master (Request $request)
    {
        return response()->json([
            'status' => 200,
            'message' => "User berhasil diupdate",
            'data'=>Payment::all()
        ]);
    
    }
  
    public function index (Request $request)
    {
        $data = Payment::all();
        return view('payment')->withData($data);
    }
    public function edit (Request $request,$id)
    {
        $data = PaymentMethod::where('id',$request->id)->first();
        $group = Payment::all();
        return view('payment_edit')->withGroup($group)->withData($data);
    }
 
   
    public function update (Request $request,$id)
    {

            $data = PaymentMethod::where('id',$id)->first();
            if(!$data){
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Payment Methods)',
                ]);
            }
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $data->logo = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }

            }

            if($request->has('payment_id') && $request->payment_id != ""){
               $data->payment_id = $request->payment_id;
            }

            if($request->has('name') && $request->name != ""){
               $data->name = $request->name;
            }
            if($request->has('account') && $request->account != "" ){
               $data->account = $request->account;
            }
            if($request->has('behalf') && $request->behalf != "" ){
               $data->behalf = $request->behalf;
            }
            if($request->has('status') && $request->status != "" ){
               $data->status = $request->status;
            }
            $save =$data->save();
            if($save){
                return response()->json([
                    'status' => 200,
                    'message' => "payment Method berhasil diupdate",
                    'data'=>$request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (payment method)',
                ]);
            }
    }
    public function create (Request $request)
    {
            $data = new PaymentMethod();
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $data->logo = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }

            }

            if($request->has('payment_id') && $request->payment_id != ""){
               $data->payment_id = $request->payment_id;
            }

            if($request->has('name') && $request->name != ""){
               $data->name = $request->name;
            }
            if($request->has('account') && $request->account != "" ){
               $data->account = $request->account;
            }
            if($request->has('behalf') && $request->behalf != "" ){
               $data->behalf = $request->behalf;
            }
            if($request->has('status') && $request->status != "" ){
               $data->status = $request->status;
            }
            $save =$data->save();
            if($save){
                return response()->json([
                    'status' => 200,
                    'message' => "payment Method berhasil dibuat",
                    'data'=>$request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (payment method)',
                ]);
            }
    }
    public function delete (Request $request,$id)
    {


    

        PaymentMethod::where('id',$id)->delete();
   
        return response()->json([
            'status' => 200,
            'message' => "Produk berhasil dihapus"
        ]);


    }

    public function fnData(Request $request)
    {
        $query = PaymentMethod::with('payment')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('tipe', function ($q)  {
            if(isset($q->payment)){
                return $q->payment->title;
            } else {
                return "";
            }
        })
        ->addColumn('LOGO', function ($q)  {
        
                return '<img src="'.$q->logo.'" class=" avatar" ">';
         
        })
        ->addColumn('status', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-danger">Inactive</span>';
            } else  {
                return '<span class="badge badge-success">Active</span>';
            } 
        })

        ->rawColumns( ['LOGO','action','status','tipe'])
        ->make(true);
    }

}