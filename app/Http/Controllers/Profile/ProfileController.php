<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

  
    public function index (Request $request)
    {
        $userId= ($request->session()->get('SESS_USERDATA')['id']);
        $data = User::where('id',$userId)->first();
        // return $user;
        return view('profile')->withData($data);
    
    }
 
   
    public function update (Request $request,$id)
    {
        
            $user =  User::where('id',$id)->first();
            if($request->has('nama') && $request->nama != ""){
                $user->nama = $request->nama;
            }
            if($request->has('phone') && $request->phone != "" ){
                $user->phone = $request->phone;
            }
            $saveProvider = $user->save();
            if($saveProvider){
                return response()->json([
                    'status' => 200,
                    'message' => "User berhasil diupdate",
                    'data'=>$request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product Biller)',
                ]);
            }
    }
    public function updateFoto (Request $request,$id)
    {   

            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['avatar'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            $user = User::where('id',$id)->update([
                'foto' => $param['avatar'] 
            ]);
  
            $data = User::where('id',$id)->first();
            return redirect('profile')->withData($data);
        

    }
    public function updatePassword (Request $request,$id)
    {   
        return $request->all();
        return $request->file('avatar');

            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['avatar'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            $user = $user::where('id',$id)->update([
                'foto' => $param['avatar'] 
            ]);
            if($user){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
        

    }

}