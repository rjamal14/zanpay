<?php

namespace App\Http\Controllers\Merchandise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchandise;
use App\Models\MerchandiseCategory;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class MerchandiseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = MerchandiseCategory::withCount('merchandise')->where("is_deleted",0);
        
        $query = $query->get();
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('nama', function ($q)  {
            return '<img src="'.$q->icon.'" class=" avatar" ">'. '&nbsp;';
        })
        ->addColumn('jumlah', function ($q)  {
            return  $q->merchandise_count;
        })
        ->rawColumns( ['action','nama'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        return view('merchandise_category');
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 

    public function add (Request $request)
    {   
          
            $param=$request->except('avatar');
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['icon'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading icon',
                ]);
            }


            $data = new MerchandiseCategory();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $create = $data::create($param);
            if($create){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {

        MerchandiseCategory::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = MerchandiseCategory::where('id',$request->id)->first();
        return view('merchandise_category_edit')->withData($data);
    }
    public function update (Request $request,$id)
    {
        $param = $request->except('avatar');
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['icon'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }
            $data =  MerchandiseCategory::where('id',$id)->first();

            $update = $data->update($param);
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
    }

}