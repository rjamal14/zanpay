<?php

namespace App\Http\Controllers\Merchandise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchandise;
use App\Models\MerchandiseCategory;
use App\Models\MerchandiseImage;
use App\Models\MerchandiseVariant;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class MerchandiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Merchandise::withCount('view')
        ->withCount('like')
        ->withCount('view')
        ->with('category')
        ->with('image')
        ->with('variant')
        ->where("is_deleted",0);
        $query = $query->get();
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('gambar', function ($q)  {
            if(isset($q->image)){
                return '<img src="'.$q->image->image_url.'" style="width:120px;" ">'. '&nbsp;';
            } else {
                return "";
            }
        })
        ->addColumn('Variants', function ($q)  {
            if(isset($q->variant)){
                $x ="";
                foreach($q->variant as $v){
                    $x.=$v->name.', '; 
                }
                $x = rtrim($x, ", ");
                return $x;
            } else {
                return "";
            }
        })
        ->addColumn('tanggal', function ($q)  {
             
            return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
    
        })
        ->rawColumns( ['action','nama','gambar'])
        ->make(true);
    }
    public function fnImageData(Request $request,$id)
    {

        $query = MerchandiseImage::where('image_prio',1)->where('merchandise_id',$id)->where('is_deleted',0);
        $query = $query->get();
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a  style="text-align:left;!important" class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="hapus_gambar('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('gambar', function ($q)  {
                return '<img src="'.$q->image_url.'" style="width:200px;" ">'. '&nbsp;';
        
        })
        ->rawColumns( ['action','nama','gambar'])
        ->make(true);
    }
    public function getImage(Request $request,$id)
    {

        $query = MerchandiseImage::selectRaw('id, image_url AS src')->where('image_prio',1)->where('merchandise_id',$id)->where('is_deleted',0);
        $query = $query->get();
        return response()->json([
            'status' => 200,
            'message' => 'SUKSES',
            'data'=>[]
        ]);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        // $biller = Biller::all();
        $group = MerchandiseCategory::where('is_deleted',0)->get();
     	return view('merchandise')->withGroup($group);
    }
 

    public function add (Request $request)
    {   
          
            $param=$request->except('avatar','variant','images');
          


            $data = new Merchandise();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $create = $data::create($param);
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
           
            if($urlAvatar->getData()->status == 200){
                MerchandiseImage::where('merchandise_id', $create->id)->where('image_prio',0)->create([
                    'image_url' => $urlAvatar->getData()->filepath,
                    'merchandise_id' => $create->id,
                    'image_prio' => 0,
                    'created_by' => $request->session()->get('SESS_USERDATA')['nama'],
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
            if($request->has('variant') && $request->variant != "") {
                $variant = explode(',',$request->variant);
                if(isset($variant)){
                    foreach($variant as $v){
                        MerchandiseVariant::create([
                            'merchandise_id' =>$create->id,
                            'name' =>$v,
                            'created_by' =>$request->session()->get('SESS_USERDATA')['nama']
                        ]);
                    }
                }
            }
            $images = $request->images;
            if(!empty($images)){
                foreach($images as $val){
                    $urlImages = $this->upload(
                        $val
                    ); 
                    if($urlImages->getData()->status == 200){
                        $newImage = new MerchandiseImage();
                        $newImage->merchandise_id =$create->id;
                        $newImage->image_url = $urlImages->getData()->filepath;
                        $newImage->image_prio = 1;
                        $newImage->created_at = date('Y-m-d H:i:s');
                        $newImage->created_by =$request->session()->get('SESS_USERDATA')['nama'];
                        $newImage->save();
                    } else {
                        return response()->json([
                            'status' => 500,
                            'message' => 'error uploading avatar',
                        ]);
                    }
                }
            }
            if($create){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {

        Merchandise::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);
        MerchandiseVariant::where('merchandise_id',$id)->delete();
        MerchandiseImage::where('merchandise_id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }
    public function deleteImage (Request $request,$id)
    {

        MerchandiseImage::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = Merchandise::where('id',$request->id)->with(['image','images','variant'])->first();
        $group = MerchandiseCategory::where('is_deleted',0)->get();
        $variant ='';
        if(isset ($data->variant)){
            $var = collect($data->variant)->pluck('name')->toArray();
            $variant = implode(',',$var);

        }
        $galery = MerchandiseImage::where('merchandise_id',$id)->selectRaw('id, image_url AS src')->where('is_deleted',0)->where('image_prio',1)->get();
        return view('merchandise_edit')
        ->withData($data)
        ->withGroup($group)
        ->withGalery($galery)
        ->withVariant($variant);
    }
    public function update (Request $request,$id)
    {
        $param = $request->except('avatar','id=','_token','variant','images');
        // dd($param);
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                MerchandiseImage::where('merchandise_id',$id)->where('image_prio',0)->update([
                    'image_url' => $urlAvatar->getData()->filepath
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }
        $images = $request->images;
        if(!empty($images)){
            foreach($images as $val){
                $urlImages = $this->upload(
                    $val
                ); 
                if($urlImages->getData()->status == 200){
                    $newImage = new MerchandiseImage();
                    $newImage->merchandise_id =$id;
                    $newImage->image_url = $urlImages->getData()->filepath;
                    $newImage->image_prio = 1;
                    $newImage->created_at = date('Y-m-d H:i:s');
                    $newImage->created_by = $request->session()->get('SESS_USERDATA')['nama'];
                    $newImage->save();
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
        }
        if($request->has('variant') && $request->variant != "") {
            $variants = explode(',',$request->variant);
            if(isset($variants)){
                MerchandiseVariant::where('merchandise_id',$id)->delete();
                foreach($variants as $v){
                    MerchandiseVariant::create([
                        'merchandise_id' =>$id,
                        'name' =>$v,
                        'created_by' =>$request->session()->get('SESS_USERDATA')['nama']
                    ]);
                }
            }
        }
            $update =  Merchandise::where('id',$id)->update($param);

            // $update = $data;
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
    }

}