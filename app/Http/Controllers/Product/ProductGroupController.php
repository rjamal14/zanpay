<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductGroup;
use App\Models\Prefix;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ProductGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = ProductGroup::where('is_deleted',0)->withCount('products')->with('prefix')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
                if($q->products_count > 0){
                    return '<a    class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn  btn-sm btn-default" href="javascript:void(0)"title="Hapus" disabled><i class="fa fa-trash-o"></i></a>';
                 
                } else {
                    return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
                }
        })
        ->addColumn('nama', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return $q->command.' - '.$q->provider;
        })
        ->addColumn('gambar', function ($q)  {
            if(isset($q->provider_icon)){
                return '<img src="'.$q->provider_icon.'" style="width:60px;" ">'. '&nbsp;';
            } else {
                return "";
            }
        })
        ->addColumn('prefiX', function ($q)  {
            if(isset($q->prefix)){
                return $q->prefix->prefix_header;
            } else {
                return '<span class="badge badge-danger">Belum di set</span>';
            }
        })
    
    
        ->rawColumns( ['gambar','action','prefiX'])
        ->make(true);
    }

  
    public function index (Request $request)
    {
        $group = ProductGroup::all();
        $prefix = Prefix::all();
       
        $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        $jenis=[];
        foreach($jenis1 as $k=>$v){
            $jenis[$k]['id']=$v;
            $jenis[$k]['text']=$v;
        }
        $type =collect($group)->pluck('group_name')->unique()->toArray();
        return view('product_group')
        ->withPrefix($prefix)
        ->withJenis($jenis)->withType($type);

    }

    public function edit (Request $request,$id)
    {
            $data = ProductGroup::where('id',$id)->first();
            $group = ProductGroup::all();
            $type =collect($group)->pluck('group_name')->unique()->toArray();
            $prefix = Prefix::all();
            $jenis1 = collect($group)->pluck(['command'])->unique()->toArray();
            $jenis=[];
            foreach($jenis1 as $k=>$v){
                $jenis[$k]['id']=$v;
                $jenis[$k]['text']=$v;
            }
        //  dd($data,$prefix);
            return view('product_group_edit')
            ->withData($data)
            ->withPrefix($prefix)
            ->withJenis($jenis)
            ->withType($type);
    }
    public function delete (Request $request,$id)
    {
            $data = ProductGroup::where('id',$id)->withCount('products')->first();
            if($data->products_count > 0){
                return response()->json([
                    'status'=>400,
                    'message'=>'group has product',
                    'data'=>(Object)[],
                ]);
            } else {
                $data = ProductGroup::where('id',$id)->delete();
                if ($data){
                    
                    return response()->json([
                        'status'=>200,
                        'message'=>'success',
                        'data'=>(Object)[],
                    ]);
                } else {
                    return response()->json([
                        'status'=>400,
                        'message'=>'failed',
                        'data'=>(Object)[],
                    ]);

                }
            }
         
    }
    public function update (Request $request,$id)
    {
            $productNew =  ProductGroup::where('id',$id)->first();
            $productNew->group_name = $request->group_name;
            $productNew->command = $request->command;
            $productNew->provider = $request->provider;
            $productNew->hlr_prefix = $request->prefix;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->provider_icon = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
    }
    public function store (Request $request)
    {
       
            $productNew =  new ProductGroup();
            $productNew->group_name = $request->group_name;
            $productNew->command = $request->command;
            $productNew->provider = $request->provider;
            $productNew->hlr_prefix =$request->prefix;
            // $productNew->status = $request->status;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->provider_icon = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil ditambahkan",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
   
    }
    public function updateOld (Request $request,$id)
    {
        if ($request->filled('prefix')){
            $cekPrefix = Prefix::where('no',$request->prefix)->count();
            if ($request->prefix != 0){
                if($cekPrefix === 0){
                    $newPrefix = new Prefix();
                    $newPrefix->prefix_header = str_replace(' ', '', $request->prefix);
                    $newPrefix->provider = $request->provider;
                    $newPrefix->date_inserted =date("Y-m-d H:i:s");
                    $saveP = $newPrefix->save();
                    $prefixId = $newPrefix->no;
                } else {
                    $prefixId = $request->prefix;
                }
            } else {
                $prefixId = $request->prefix;
            }
        }
            $productNew =  ProductGroup::where('id',$id)->first();
            $productNew->group_name = $request->group_name;
            $productNew->command = $request->command;
            $productNew->provider = $request->provider;
            $productNew->hlr_prefix = $prefixId;
            // $productNew->status = $request->status;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->provider_icon = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
    }
    public function storeold (Request $request)
    {
        //cek prefix
        if ($request->filled('prefix')){
            $cekPrefix = Prefix::where('no',$request->prefix)->count();
            if ($request->prefix != 0){
                if($cekPrefix === 0){
                    $newPrefix = new Prefix();
                    $newPrefix->prefix_header = str_replace(' ', '', $request->prefix);
                    $newPrefix->provider = $request->provider;
                    $newPrefix->date_inserted =date("Y-m-d H:i:s");
                    $saveP = $newPrefix->save();
                    $prefixId = $newPrefix->no;
                } else {
                    $prefixId = $request->prefix;
                }
            } else {
                $prefixId = $request->prefix;
            }

        if($request->has('prefix') && $request->prefix != "pilih")
            $productNew =  new ProductGroup();
            $productNew->group_name = $request->group_name;
            $productNew->command = $request->command;
            $productNew->provider = $request->provider;
            $productNew->hlr_prefix = $prefixId;
            // $productNew->status = $request->status;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->provider_icon = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil ditambahkan",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
        }
        return response()->json([
            'status' => 500,
            'message' => 'error ',
        ]);
    }
}