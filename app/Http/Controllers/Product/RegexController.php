<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductParsing;
use App\Models\Biller;
use App\Models\ProductGroupHolder;
use App\Models\Regex;
use App\Models\ProductGroup;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class RegexController extends Controller
{
    public function tanggalIndonesia($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array(
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }
    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Regex::with(['group', 'biller']);
        // return $query;
        if ($request->has("filterTipe") && ($request->filterTipe != "")) {
            $grou = ProductGroup::where('command', $request->filterTipe)->get();
            $idGro = collect($grou)->pluck('id')->toArray();
            $query = $query->wherein('group_id', $idGro);
        }
        if ($request->has("filterBiller") && ($request->filterBiller != "")) {
            $query = $query->where('biller_id', $request->filterBiller);
        }
        if ($request->has("filterStatus") && ($request->filterStatus != "")) {
            $query = $query->where('status', $request->filterStatus);
        }

        $query = $query->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($q) {

                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData(' . $q->id . ')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct(' . $q->id . ')"><i class="fa fa-trash-o"></i></a>';
            })
            ->addColumn('billername', function ($q) {
                if (isset($q->biller)) {
                    return $q->biller->name;
                } else {
                    return 'Belum ada biller';
                }
            })
            ->addColumn('groupname', function ($q) {
                if (isset($q->group)) {
                    return $q->group->group_name;
                } else {
                    return 'Regex Global';
                }
            })
            ->addColumn('tipe', function ($q) {
                if ($q->type_respond == 0) {
                    return "Text";
                } else {
                    return 'Json';
                }
            })
            ->addColumn('statusText', function ($q) {
                if ($q->status == 0) {
                    return '<span class="badge badge-success">Sukses</span>';
                } else if ($q->status == 1) {
                    return '<span class="badge badge-warning">Pending</span>';
                } else {
                    return '<span class="badge badge-danger">Gagal</span>';
                }
            })
            ->addColumn('tglUpdate', function ($q) {
                if ($q->updated_at == null) {
                    return 'Belum ada update';
                } else {
                    return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->updated_at))) . " " . date('H:i:s', strtotime($q->updated_at));
                }
            })
            ->addColumn('updateOleh', function ($q) {
                if ($q->updated_by == null) {
                    return 'Belum ada update';
                } else {
                    return $q->updated_by;
                }
            })
            ->addColumn('parameter22', function ($q) {
                if ($q->type_respond == 0) {
                    $param = $q->parameter;
                    $param = explode('|', $param);
                    foreach ($param as $p) {
                        $insideBracket[] = $this->get_string_between($p, '[', ']');
                        $params[] = strtok($p, '[');
                    }
                    foreach ($insideBracket as $k => $pr) {
                        $explodeee = explode('-', $pr);
                        if($explodeee[0] != ";" && $explodeee[1] != ";"){
                            $result[] = $params[$k] . '[awal ' . $explodeee[0] . ', akhir ' . $explodeee[1] . ']';
                        }
                    }
                    $res = "";
                    foreach ($result as $r) {
                        $res .= $r . '<br>';
                    }
                    return $res;
                } else {
                    return $q->parameter;
                }
            })
            ->addColumn('parameter2', function ($q) {
                if ($q->type_respond == 0) {
                    if ($q->parameter_opsi != null || $q->parameter_opsi != "") {
                        $param = $q->parameter_opsi;
                        $param = explode('|', $param);
                        foreach ($param as $p) {
                            $insideBracket[] = $this->get_string_between($p, '[', ']');
                            $params[] = strtok($p, '[');
                        }
                        foreach ($insideBracket as $k => $pr) {
                            $explodeee = explode('-', $pr);
                            $result[] = $params[$k] . '[awal ' . $explodeee[0] . ', akhir ' . $explodeee[1] . ']';
                        }
                        $res = "";
                        foreach ($result as $r) {
                            $res .= $r . '<br>';
                        }
                        return $res;
                    }
                    return "";
                } else {
                    return $q->parameter_opsi;
                }
            })
            ->addColumn('parameter23', function ($q) {
                $res = "";
                $res2 = "";
                if ($q->type_respond == 0) {
                    if ($q->parameter_opsi != null || $q->parameter_opsi != "") {
                        $param = $q->parameter_opsi;
                        $param = explode('|', $param);
                        foreach ($param as $p) {
                            $insideBracket[] = $this->get_string_between($p, '[', ']');
                            $params[] = strtok($p, '[');
                        }
                        foreach ($insideBracket as $k => $pr) {
                            $explodeee = explode('-', $pr);
                            if($explodeee[0] != ";" && $explodeee[1] != ";"){
                                $result[] = $params[$k] . '[awal ' . $explodeee[0] . ', akhir ' . $explodeee[1] . ']';
                            } else {
                                $result=[];
                            }
                        }
                        $res = "";
                        foreach ($result as $r) {
                            $res .= $r . '<br>';
                        }
                    }
                } else {
                    $res = $q->parameter_opsi;
                }
                if ($q->type_respond == 0) {
                    $param2 = $q->parameter;
                    $param2 = explode('|', $param2);
                    foreach ($param2 as $p) {
                        $insideBracket2[] = $this->get_string_between($p, '[', ']');
                        $params2[] = strtok($p, '[');
                    }
                    foreach ($insideBracket2 as $k2 => $pr2) {
                        $explodeee2 = explode('-', $pr2);
                        if($explodeee2[0] != ";" && $explodeee2[1] != ";"){
                            $result2[] = $params2[$k2] . '[awal ' . $explodeee2[0] . ', akhir ' . $explodeee2[1] . ']';
                        }
                    }
                    $res2 = "";
                    foreach ($result2 as $r2) {
                        $res2 .= $r2 . '<br>';
                    }
                } else {
                    $res2= $q->parameter;
                }
                if($res == ""){
                    return "<b>Parameter Wajib:</b><br>".$res2;
                } else {
                    return "<b>Parameter Wajib:</b><br>".$res2.'<br><b>Parameter Opsi:</b><br>'.$res;
                }




            })
            ->addColumn('review2', function ($q) {
                if ($q->preview_opsi != null) {
                    $text = "<b>Preview Wajib:</b> <br>" . $q->preview . "<br> <b>Preview Opsional:</b> <br>" . $q->preview_opsi;
                } else {
                    $text = "<b>Preview Wajib:</b> <br>" . $q->preview;
                }
                return $text;
            })
            ->addColumn('groupcommand', function ($q) {
                if ($q->group != null) {
                    $text = $q->group->command;
                } else {
                    $text = "Regex Global";
                }
                return $text;
            })
            ->addColumn('groupprovider', function ($q) {
                if ($q->group != null) {
                    $text = $q->group->provider;
                } else {
                    $text = "Regex Global";
                }
                return $text;
            })

            ->rawColumns(['action', 'statusText', 'parameter22','parameter23','parameter2', 'review2'])
            ->make(true);
    }


    public function index(Request $request)
    {
        $biller = Biller::where('is_deleted', 0)->get();
        // dd($biller);
        $groupH = ProductGroupHolder::where('type', 0)->get();
        $res = [];
        foreach ($groupH as $k => $d) {
            $res[$k]['id'] = $d['id'];
            $res[$k]['text'] = $d['name'];
        }

        $group = ProductGroup::where('is_deleted', 0)->groupBy('command')->get();
        foreach ($group as $k => $v) {
            $jenis[$k]['id'] = $v['command'];
            $jenis[$k]['text'] = $v['command'];
        }
        $jenis[] = [
            "id" => 'Regex Global',
            "text" => 'Regex Global',
        ];
        // dd($jenis);
        $biller = Biller::where('is_deleted', 0)->get();
        return view('regex')->withBiller($biller)->withGroupH($res)->withTipe($jenis)->withGroup($group);
    }
    public function generate(Request $request)
    {
        // dd($request->all());
        $params = [];
        $res = "";
        $res2 = "";
        $res22 = "";
        foreach($request->param2awal as $kk=>$pp){
            $x = true;
            if ($pp == ";" && $request->param2akhir[$kk] == ";"){
            // $x = false;
            $x = false;
            }
            if ($x){
                $a = $this->get_string_between($request->sample, $pp,$request->param2akhir[$kk]); 
                if ($kk == 0){
                    $ttt = 'Refid';
                    $res2 .= 'Refid=['.$a.'] ';
                } else if($kk == 1){
                    $ttt = 'Trxid';
                    $res2 .= 'Trxid=['.$a.'] ';
                } else if($kk == 2){
                    $ttt = 'Custid';
                    $res2 .= 'Custid=['.$a.'] ';
                } else if($kk == 3){
                    $ttt = 'SKU';
                    $res2 .= 'SKU=['.$a.'] ';
                } else if($kk == 4){
                    $ttt = 'Harga';
                    $res2 .= 'Harga=['.$a.'] ';
                } else if($kk == 5){
                    $ttt = 'Saldo';
                    $res2 .= 'Saldo=['.$a.'] ';
                } else if($kk == 6){
                    $ttt = 'SN';
                    $res2 .= 'SN=['.$a.']';
                }
                if($a==""){
                    return response()->json(
                        [
                            "status" => 400,
                            "message" => $ttt. ' tidak di temukan',
                            'data' => (object)[]
                        ]
                    );
                }
            }
              
        }
        // dd($request->param,$request->all());

        if ($request->param != "") {
            $param = (nl2br(htmlspecialchars($request->param)));
            $param = explode('<br />', $param);

            foreach ($param as $p) {
                $params[] = "####44123" . trim($p);
            }
            foreach ($params as $p) {
                // $res[]=trim($this->get_string_between($p,'####44123','[').$this->get_string_between($request->sample,$this->get_string_between($p,"awal ",","),$this->get_string_between($p,"akhir ","]")));
                $a = $this->get_string_between($p, '####44123', 'awal') ?? null;
                $b = $this->get_string_between($p, "awal ", ",") ?? null;
                $c = $this->get_string_between($p, "akhir ", "]") ?? null;
                if ($a == null || $b == null || $c == null) {
                    return response()->json(
                        [
                            "status" => 400,
                            "message" => 'Mohon Periksa Kembali Inputan',
                            'data' => (object)[]
                        ]
                    );
                } else if($b == ";" && $c == ";"){
                    $check="";
                } else{
                    $d = trim($this->get_string_between($request->sample, $b, $c)) ?? null;
                    if ($d == null) {
                        return response()->json(
                            [
                                "status" => 400,
                                "message" => 'Mohon Periksa Kembali Inputan',
                                'data' => (object)[]
                            ]
                        );
                    } else {
                        $check = ($a . $d) . ']';
                    }
                }

                $res .= ($check) . " ";
                // $res .=trim($this->get_string_between($p,'####44123','awal').trim($this->get_string_between($request->sample,$this->get_string_between($p,"awal ",","),$this->get_string_between($p,"akhir ","]")))).']'." ";
            }
            $res = trim($res);
            if($res == ""){
                $final = "Preview Wajib: \n". $res2;
            } else {
                $final = "Preview Wajib: \n". $res2."\n\n Preview Opsi:\n ". $res;
            }
        }
        else {
            $final = "Preview Wajib: \n". $res2;
        }
        return response()->json(
            [
                "status" => 200,
                "message" => 'Berhasil!',
                'data' => $final,
                'data1' => $res,
                'data2' => $res2,
                'data22' => $res22,
            ]
        );
        //    foreach($1)
    }


    public function detail(Request $request, $id)
    {
        $data = Regex::where('id', $id)->with('group')->first();
        $paramz = explode('|',$data->parameter);
        $awal=[];
        $akhir=[];
        foreach($paramz as $ka=>$vi){
            $awal[] = $this->get_string_between($vi, '[', '-');
            $akhir[] = $this->get_string_between($vi, '-', ']');
        }
        $biller = Biller::where('is_deleted', 0)->get();
        // dd($biller);
        $pParsing = ProductParsing::where('type', 0)->where('biller_id', $data->biller_id)->get();
        $pParsing = collect($pParsing)->pluck('holder_id')->toArray();
        
        $groupH = ProductGroupHolder::where('type', 0)->whereIn('id', $pParsing)->get();
        $res = [];
        foreach ($groupH as $k => $d) {
            $res[$k]['id'] = $d['id'];
            $res[$k]['name'] = $d['name'];
        }
        $res[count($groupH)]['id'] = 'Regex Global';
        $res[count($groupH)]['name'] = 'Regex Global';
        if($data->group_id != 0){
            $data2 = ProductGroup::where('holder_id', $data->group->holder_id)->groupBy('command')->get();
            $res2 = [];
            foreach ($data2 as $k => $d) {
                $res2[$k]['id'] = $d['command'];
                $res2[$k]['name'] = $d['command'];
            }
    
            $data3 = ProductGroup::where('command', $data->group->command)->groupBy('provider')->get();
            $res3 = [];
            foreach ($data3 as $k => $d) {
                $res3[$k]['id'] = $d['id'];
                $res3[$k]['name'] = $d['provider'];
            }
        } else {
            $data2 = ProductGroup::groupBy('command')->get();
            $res2 = [];
            foreach ($data2 as $k => $d) {
                $res2[$k]['id'] = $d['command'];
                $res2[$k]['name'] = $d['command'];
            }
    
            $data3 = ProductGroup::groupBy('provider')->get();
            $res3 = [];
            foreach ($data3 as $k => $d) {
                $res3[$k]['id'] = $d['id'];
                $res3[$k]['name'] = $d['provider'];
            }
        }
       
        $parameterParsed = "";
        $reviewParsed = "";
        $resultRaw = [];
        if ($data->type_respond == 0) {
            if($data->parameter_opsi != null || $data->parameter_opsi != ""){
                $paramRaws = $data->parameter_opsi;
                $paramRaws = explode('|', $paramRaws);
                foreach ($paramRaws as $p) {
                    $insideBracket[] = $this->get_string_between($p, '[', ']');
                    $paramRawss[] = strtok($p, '[');
                }
                foreach ($insideBracket as $k => $pr) {
                    $explodeee = explode('-', $pr);
                    $resultRaw[] = $paramRawss[$k] . '[awal ' . $explodeee[0] . ', akhir ' . $explodeee[1] . ']';
                }
                $resRaw = "";
                foreach ($resultRaw as $r) {
                    $resRaw .= $r . "\n";
                }
                $parameterParsed = $resRaw;
            }
           
        } else {
            $parameterParsed = $data->parameter_opsi;
        }
        if($data->parameter_opsi != null || $data->parameter_opsi != ""){
            $reviewParsed = 'Preview Wajib:  '.$data->preview .'  Preview Opsi:  '.$data->preview_opsi;
        } else {
            $reviewParsed = 'Preview Wajib:'.$data->preview;
        }
        return view('regex_edit')->withData($data)->withBiller($biller)->withGroupH($res)->withTipe($res2)->withProvider($res3)->withParameterParsed($parameterParsed)->withReviewParsed($reviewParsed)->withAwal($awal)->withAkhir($akhir);
    }
    public function update(Request $request, $id)
    {

        $params = "";
        $res = "";
        if($request->parameter != ""){
            $param = (nl2br(htmlspecialchars($request->parameter)));
            $param = explode('<br />', $param);
    
            foreach ($param as $k => $p) {
                $cek = trim(str_replace(array('awal', ' akhir', ' '), '', $p));
                if (count($param) == $k + 1) {
                    $params .= str_replace(',', '-', $cek);
                } else {
                    $params .= str_replace(',', '-', $cek) . "|";
                }
            }
        }
       else {
           $param = null;
       }

        //wajib
        $paramW1 = explode(',',$request->param2awal);
        $paramW2 = explode(',',$request->param2akhir);
        $result2="";

        foreach($paramW1 as $kw=>$pw){
            $x = true;
            if ($pw == ";" && $paramW2[$kw] == ";"){
            // $x = false;
                $x = true;
            }
            if ($x){
                if ($kw == 0){
                    $ttt = 'Refid';
                   $result2 .= 'Refid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 1){
                    $ttt = 'Trxid';
                   $result2 .= 'Trxid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 2){
                    $ttt = 'Custid';
                   $result2 .= 'Custid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 3){
                    $ttt = 'SKU';
                   $result2 .= 'SKU=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 4){
                    $ttt = 'Harga';
                   $result2 .= 'Harga=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 5){
                    $ttt = 'Saldo';
                   $result2 .= 'Saldo=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 6){
                    $ttt = 'SN';
                   $result2 .= 'SN=['.$pw.'-'.$paramW2[$kw].']';
                }
            } 
        }

        $productNew =  Regex::where('id',$id)->first();
        $productNew->biller_id = $request->biller_id;
        $productNew->group_id = ($request->status == 0 && isset($request->provider_id) )? $request->provider_id : 0;
        $productNew->sample_respond = $request->sample_respond;
        $productNew->added_data = $request->added_data;
        $productNew->parameter = $result2;
        $productNew->parameter_opsi = $params;
        $productNew->type_respond = $request->type_respond;
        $productNew->preview_opsi= $request->preview_opsi;
        $productNew->preview = $request->preview_wajib;
        $productNew->status = $request->status;
        $productNew->updated_at = date("Y-m-d H:i:s");
        $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
        $saveProduct = $productNew->save();
        if ($saveProduct) {
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil diupdate",
                'data' => $saveProduct
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
        }
    }
    public function store(Request $request)
    {
        $params = "";
        $res = "";
        if($request->parameter != ""){
            $param = (nl2br(htmlspecialchars($request->parameter)));
            $param = explode('<br />', $param);
    
            foreach ($param as $k => $p) {
                $cek = trim(str_replace(array('awal', ' akhir', ' '), '', $p));
                if (count($param) == $k + 1) {
                    $params .= str_replace(',', '-', $cek);
                } else {
                    $params .= str_replace(',', '-', $cek) . "|";
                }
            }
        }
       else {
           $param = null;
       }

        //wajib
        $paramW1 = explode(',',$request->param2awal);
        $paramW2 = explode(',',$request->param2akhir);
        $result2="";

        foreach($paramW1 as $kw=>$pw){
            $x = true;
            if ($pw == ";" && $paramW2[$kw] == ";"){
            // $x = false;
                $x = true;
            }
            if ($x){
                if ($kw == 0){
                    $ttt = 'Refid';
                   $result2 .= 'Refid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 1){
                    $ttt = 'Trxid';
                   $result2 .= 'Trxid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 2){
                    $ttt = 'Custid';
                   $result2 .= 'Custid=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 3){
                    $ttt = 'SKU';
                   $result2 .= 'SKU=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 4){
                    $ttt = 'Harga';
                   $result2 .= 'Harga=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 5){
                    $ttt = 'Saldo';
                   $result2 .= 'Saldo=['.$pw.'-'.$paramW2[$kw].']|';
                } else if($kw == 6){
                    $ttt = 'SN';
                   $result2 .= 'SN=['.$pw.'-'.$paramW2[$kw].']';
                }
            } 
        }

        $productNew =  new Regex();
        $productNew->biller_id = $request->biller_id;
        $productNew->group_id = ($request->status == 0 && isset($request->provider_id) )? $request->provider_id : 0;
        $productNew->sample_respond = $request->sample_respond;
        $productNew->added_data = $request->added_data;
        $productNew->parameter = $result2;
        $productNew->parameter_opsi = $params;
        $productNew->type_respond = $request->type_respond;
        $productNew->preview_opsi= $request->preview_opsi;
        $productNew->preview = $request->preview_wajib;
        $productNew->status = $request->status;
        $productNew->created_at = date("Y-m-d H:i:s");
        $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
        $saveProduct = $productNew->save();
        if ($saveProduct) {
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil diisi",
                'data' => $saveProduct
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
        }
    }
    public function delete(Request $request, $id)
    {

        Regex::where('id', $id)->delete();
        return response()->json([
            'status' => 200,
            'message' => "sukses"
        ]);
    }

    // groupByBiller

    public function groupByBiller(Request $request)
    {
        if ($request->biller_id != "") {
            $data = ProductParsing::where('type', 0)->where('biller_id', $request->biller_id)->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['holder_id'];
                $res[$k]['text'] = $d['title'];
            }
            $res[] = [
                "id" => 'Regex Global',
                "text" => 'Regex Global',
            ];
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }

    public function tipeByGroup(Request $request)
    {
        if ($request->holder_id != "") {
            $data = ProductGroup::where('holder_id', $request->holder_id)->groupBy('command')->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['command'];
                $res[$k]['text'] = $d['command'];
            }
       
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }
    public function providerByTipe(Request $request)
    {
        if ($request->command != "") {
            $data = ProductGroup::where('command', $request->command)->groupBy('provider')->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['id'];
                $res[$k]['text'] = $d['provider'];
            }
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }
}
