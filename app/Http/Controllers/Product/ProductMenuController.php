<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductMenu;
use App\Models\ProductGroup;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ProductMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = ProductMenu::where('status',1)->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> ';
        })
        ->addColumn('gambar', function ($q)  {
            if(isset($q->image)){
                return '<img src="'.$q->image.'" style="width:60px;" ">'. '&nbsp;';
            } else {
                return "";
            }
        })
        ->addColumn('tipe', function ($q)  {
            return $q->command .' - '. $q->type;
        })
    
        ->rawColumns( ['gambar','action'])
        ->make(true);
    }

  
    public function index (Request $request)
    {
        $type =[
            'Prabayar','Pascabayar'
        ];
        $jenis = ProductMenu::groupBy('command')->get();
        $jenis = collect($jenis)->pluck('command')->toArray();
        $jenisSub = ProductMenu::groupBy('subcommand')->get();
        $jenisSub = collect($jenisSub)->pluck('subcommand')->toArray();
        $groupMenu = ProductMenu::groupBy('group_menu')->get();
        $groupMenu = collect($groupMenu)->pluck('group_menu')->toArray();
        $status =[
          [
              'id'=>1,
              'name'=>'Tampilkan'
          ],
          [
              'id'=>0,
              'name'=>'Sembunyikan'
          ]
        ];
        return view('product_menu')->withStatus($status)
        ->withJenisSub($jenisSub)
        ->withGroupMenu($groupMenu)
        ->withType($type)
        ->withJenis($jenis);

    }

    public function edit (Request $request,$id)
    {
            $data = ProductMenu::where('id',$request->id)->first();
            $type =[
                'Prabayar','Pascabayar'
            ];
            $jenis = ProductGroup::groupBy('command')->where('group_name',$data->type)->get();
            $jenis = collect($jenis)->pluck('command')->toArray();
            $jenisSub = ProductGroup::groupBy('provider')->where('command',$data->command)->get();
            $jenisSub = collect($jenisSub)->pluck('provider')->toArray();
            $groupMenu = ProductMenu::groupBy('group_menu')->get();
            $groupMenu = collect($groupMenu)->pluck('group_menu')->toArray();
            $status =[
              [
                  'id'=>1,
                  'name'=>'Tampilkan'
              ],
              [
                  'id'=>0,
                  'name'=>'Sembunyikan'
              ]
            ];
            return view('product_menu_edit')
            ->withData($data)
            ->withStatus($status)
            ->withJenisSub($jenisSub)
            ->withGroupMenu($groupMenu)
            ->withType($type)
            ->withJenis($jenis);
    }
    public function update (Request $request,$id)
    {
            $productNew =  ProductMenu::where('id',$id)->first();
            $productNew->title = $request->title;
            $productNew->type = $request->type;
            $productNew->command = $request->command;
            $productNew->subcommand = $request->subcommand;
            $productNew->group_menu = $request->group_menu;
            $productNew->updated = date("Y-m-d H:i:s");

            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->image = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data'=>$request->all()
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
            }
    }

    public function store (Request $request)
    {
            $productNew =  new ProductMenu();
            $productNew->title = $request->title;
            $productNew->type = $request->type;
            $productNew->command = $request->command;
            $productNew->subcommand = $request->subcommand;
            $productNew->group_menu = $request->group_menu;
            $productNew->updated = date("Y-m-d H:i:s");

            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $productNew->image = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data'=>$request->all()
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
            }
    }


    
    public function groupCombobox (Request $request)
    {
        if(isset($request->group_name)){
            $data = ProductGroup::where('group_name',$request->group_name)->groupBy('command')->get();
            foreach($data as $k=>$d){
                $res[$k]['id']=$d['command'];
                $res[$k]['text']=$d['command'];
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
            
        } else if(isset($request->command)){
            $data = ProductGroup::where('command',$request->command)->groupBy('provider')->get();
            foreach($data as $k=>$d){
                $res[$k]['id']=$d['provider'];
                $res[$k]['text']=$d['provider'];
            }
            return response()->json([
                'data'=>$res,
                'message'=>'success'
            ]);
        }
  
    }
}