<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductGroup;
use App\Models\ProductGroupHolder;
use App\Models\ProductBiller;
use App\Models\Product;
use App\Models\Biller;
use App\Models\ProductParsing;
use App\Imports\ProductBillerImport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductBillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = ProductBiller::where('is_deleted', 0);
        if (!empty($request->filterTipe)) {
            if (in_array('Belum Di Pilih', $request->filterTipe)) {
                $query = $query->whereDoesntHave('product');
                if ($request->has("filterBiller") && !empty($request->filterBiller)) {
                    $query = $query->whereIn('product_biller.biller_id', $request->filterBiller);
                }


                $query->with(['product.productGroup', 'biller'])->get();
            } else {
                if ($request->has("filterTipe") && !empty($request->filterTipe) && !empty($request->filterJenis)) {
                    $productG = ProductGroup::whereIn('group_name', $request->filterTipe)->where('is_deleted', 0)->get();
                    $idG = collect($productG)->pluck('id')->toArray();
                    $query = $query->whereHas('product', function ($q) use ($idG, $request) {
                        $q = $q->whereIn('products.group_id', $idG);
                        $q = $q->whereHas('productGroup', function ($q) use ($request) {
                            $q->whereIn('product_group.command', $request->filterJenis);
                        });
                    });
                }
                if ($request->has("filterTipe") && !empty($request->filterTipe) && empty($request->filterJenis)) {
                    $productG = ProductGroup::whereIn('group_name', $request->filterTipe)->where('is_deleted', 0)->get();
                    $idG = collect($productG)->pluck('id')->toArray();
                    $query = $query->whereHas('product', function ($q) use ($idG) {
                        $q = $q->whereIn('products.group_id', $idG);
                    });
                }
                if ($request->has("filterJenis") && !empty($request->filterJenis) && !empty($request->filterJenis)) {

                    $query = $query->whereHas('product', function ($q) use ($request) {
                        $q = $q->whereHas('productGroup', function ($q) use ($request) {
                            $q->whereIn('product_group.command', $request->filterJenis);
                        });
                    });
                }

                if ($request->has("filterBiller") && !empty($request->filterBiller)) {
                    $query = $query->whereIn('product_biller.biller_id', $request->filterBiller);
                }


                $query->with(['product.productGroup', 'biller'])->get();
            }
        } else {
            if ($request->has("filterTipe") && !empty($request->filterTipe) && !empty($request->filterJenis)) {
                $productG = ProductGroup::whereIn('group_name', $request->filterTipe)->where('is_deleted', 0)->get();
                $idG = collect($productG)->pluck('id')->toArray();
                $query = $query->whereHas('product', function ($q) use ($idG, $request) {
                    $q = $q->whereIn('products.group_id', $idG);
                    $q = $q->whereHas('productGroup', function ($q) use ($request) {
                        $q->whereIn('product_group.command', $request->filterJenis);
                    });
                });
            }
            if ($request->has("filterTipe") && !empty($request->filterTipe) && empty($request->filterJenis)) {
                $productG = ProductGroup::whereIn('group_name', $request->filterTipe)->where('is_deleted', 0)->get();
                $idG = collect($productG)->pluck('id')->toArray();
                $query = $query->whereHas('product', function ($q) use ($idG) {
                    $q = $q->whereIn('products.group_id', $idG);
                });
            }
            if ($request->has("filterJenis") && !empty($request->filterJenis) && !empty($request->filterJenis)) {

                $query = $query->whereHas('product', function ($q) use ($request) {
                    $q = $q->whereHas('productGroup', function ($q) use ($request) {
                        $q->whereIn('product_group.command', $request->filterJenis);
                    });
                });
            }

            if ($request->has("filterBiller") && !empty($request->filterBiller)) {
                $query = $query->whereIn('product_biller.biller_id', $request->filterBiller);
            }


            $query->with(['product', 'product.productGroup', 'biller'])->get();
        }

        // $query = $query->whereHas('productBiller');


        // dd($query);


        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($q) {

                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData(' . $q->id . ')"><i class="fa fa-edit"></i></a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="deleteProduct(' . $q->id . ')"><i class="fa fa-trash"></i></a>';
            })
            ->addColumn('nama', function ($q) {
                if (isset($q->biller)) {
                    return $q->biller->name ?? "Belum Ada Biller" ;
                } else {
                    return "Belum Ada Biller";
                }
            })
            ->addColumn('desc', function ($q) {

                if (isset($q->product)) {

                    return $q->product->fullname;
                } else {
                    return "Belum dipilih";
                }
            })
            ->addColumn('tipe', function ($q) {
                if ($q->product_id == 0) {
                    return "Belum dipilih";
                }
                if (isset($q->product)) {
                    if(isset($q->product->productGroup)){

                        return $q->product->productGroup->group_name;
                    }
                } else {

                    return "Belum dipilih";
                }
            })
            ->addColumn('jenis', function ($q) {
                if (isset($q->product)) {
                    if(isset($q->product->productGroup)){

                        return $q->product->productGroup->command;
                    }
                  
                } else {
                    return "Belum dipilih";
                }
            })
            ->addColumn('skuProduct', function ($q) {
                if (isset($q->product)) {
                  
                    return $q->product->sku_code;
                  
                } else {
                    return "Belum dipilih";
                }
            })
            ->addColumn('tanggal', function ($q) {
                if ($q->updated_at != null) {
                    $ddate = $q->updated_at;
                } else {
                    $ddate = $q->created_at;
                }
                return $this->tanggalIndonesia(date('Y-m-d', strtotime($ddate))) . " " . date('H:i:s', strtotime($ddate));
            })
            ->addColumn('updatedBy', function ($q) {
                if ($q->updated_by != null) {
                    $ddate = $q->updated_by;
                } else {
                    $ddate = 'Sistem';
                }
                return $ddate;
            })
            ->addColumn('prioritas', function ($q) {
                if ($q->priority == 1) {
                    return '<span class="badge badge-success">Tinggi</span>';
                } else  if ($q->priority == 2) {
                    return '<span class="badge badge-warning">Medium</span>';
                } else {
                    return '<span class="badge badge-danger">Rendah</span>';
                }
            })
            ->addColumn('status', function ($q) {
                if ($q->is_trouble == 1) {
                    return '<span class="badge badge-danger">Gangguan</span>';
                } else {
                    return '<span class="badge badge-success">Normal</span>';
                }
            })

            ->rawColumns(['nama', 'action', 'prioritas', 'status'])
            ->make(true);
    }

    public function delete(Request $request, $id)
    {
        $data = ProductBiller::where('id', $id)->delete();
        // return $data;
        return response()->json([
            'status' => 200,
            'message' => 'sukses'
        ]);
    }

    public function tanggalIndonesia($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array(
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }
    public function index(Request $request)
    {

        // $provider1 = collect($group)->pluck('provider')->unique()->toArray();
        // $provider=[];
        // foreach($provider1 as $k=>$v){
        //     $provider[$k]['id']=$v;
        //     $provider[$k]['text']=$v;
        // }
        $tipePar = ProductGroupHolder::all();
        $tipePars = collect($tipePar)->pluck(['name'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }
        $countX = count($tipe);
        $tipe[$countX]['id'] = 'Belum Di Pilih';
        $tipe[$countX]['text'] = 'Belum Di Pilih';
        // dd($tipe);
        // $tipe=[
        //         [
        //         'id'=>'Pascabayar',
        //         'text'=>'Pascabayar'
        //         ],
        //         [
        //         'id'=>'Prabayar',
        //         'text'=>'Prabayar'
        //         ],
        //         [
        //         'id'=>'Belum Di Pilih',
        //         'text'=>'Belum Di Pilih'
        //         ],
        //     ];
        $group = ProductGroup::where('is_deleted', 0)->get();
        $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        $jenis = [];
        foreach ($jenis1 as $k => $v) {
            $jenis[$k]['id'] = $v;
            $jenis[$k]['text'] = $v;
        }
        $biller = Biller::where('is_deleted', 0)->get();
        return view('product_biller')->withBiller($biller)
            ->withJenis($jenis)
            ->withTipe($tipe)
            ->withGroup($group);
    }

    public function edit(Request $request, $id)
    {

   


        $data = ProductBiller::where('id', $id)->with('product.group')->first();
        // return $data;

        
        $dataP = ProductParsing::where('is_deleted', 0)->where('biller_id', $data->biller_id)->get();
        $getHolder=collect($dataP)->pluck('holder_id')->toArray();

        $tipePar = ProductGroupHolder::whereIn('id',$getHolder)->get();
        $tipePars = collect($tipePar)->pluck(['name'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }
        $countX = count($tipe);
        $tipe[$countX]['id'] = 'Belum Di Pilih';
        $tipe[$countX]['text'] = 'Belum Di Pilih';
        // dd($tipe);
        $group = ProductGroup::where('is_deleted', 0)->get();
        $biller = Biller::where('is_deleted', 0)->get();
        $pr = [];
        if ($data->product_id != 0) {
            $product = Product::where('id', $data->product_id)->first();
            if ($product) {
                $products = $product->group_id;
                $products1 = Product::where('group_id', $products)->where('is_deleted', 0)->get();
                $pr = [];
                foreach ($products1 as $k => $d) {
                    $pr[$k]['id'] = $d['id'];
                    $pr[$k]['text'] = $d['fullname'];
                }
            }
        }
        // dd( $data['product']);
        return view('product_biller_edit')
            ->withData($data)->withProducts1($pr)->withTipe($tipe)
            ->withBiller($biller)->withGroup($group);
    }

    public function update(Request $request, $id)
    {
        $idProduct =  (isset($request->product_id) ? ($request->product_id != "" ? $request->product_id : 0) : 0);
        $productNew =  ProductBiller::where('id', $id)->first();
        if (isset( $request->biller_id)){
            $productNew->biller_id = $request->biller_id;
        }
        // $productNew->biller_id = $request->biller_id;
        $productNew->sku_biller = $request->sku_biller;
        $productNew->price_basic = $request->price_basic;
        $productNew->admin_fee = $request->admin_fee;
        $productNew->priority = $request->priority;
        $productNew->is_trouble = $request->is_trouble;
        $productNew->product_id = $idProduct;
        $productNew->description = "";
        $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
        $productNew->updated_at = date("Y-m-d H:i:s");
        $saveProduct = $productNew->save();
        if ($saveProduct) {
           
            if( $request->priority == 1 && $idProduct != 0) {
                ProductBiller::where('product_id', $idProduct)->where('id','!=',$id)->update([
                    'priority'=>2
                ]);
            }
            if ($idProduct != 0) {
                $productNew1 =  Product::where('id', $idProduct)->first();
                // $productNew1->biller_id = $request->biller_id;
                // $productNew1->price = $request->price_basic;
                $saveProduct1 = $productNew1->save();
            }
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil diupdate",
                'data' => $saveProduct
            ]);
        }
        return response()->json([
            'status' => 400,
            'message' => "Data beGagalrhasil diupdate",
            'data' => $saveProduct
        ]);
    }
    // public function updatev2 (Request $request,$id)
    // {
    //     $idProduct =  ( isset($request->product_id) ? ($request->product_id != "" ? $request->product_id : 0) : 0) ;
    //         $productNew =  ProductBiller::where('id',$id)->first();
    //         $productNew->biller_id = $request->biller_id;
    //         $productNew->sku_biller = $request->sku_biller;
    //         $productNew->price_basic = $request->price_basic;
    //         $productNew->admin_fee = $request->admin_fee;
    //         $productNew->priority = $request->priority;
    //         $productNew->is_trouble = $request->is_trouble;
    //         $productNew->product_id = $idProduct;
    //         $productNew->description ="";
    //         $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
    //         $productNew->updated_at = date("Y-m-d H:i:s");
    //         $saveProduct = $productNew->save();
    //         if($saveProduct){
    //             if($idProduct != 0 ){

    //                     $productNew1 =  new Product();
    //                     $productNew1->biller_id = $request->biller_id;
    //                     $productNew1->group_id = $request->group_id;
    //                     // $productNew1->category = 0;
    //                     $productNew1->sku_code = $request->sku_biller.time();
    //                     $productNew1->name = $request->description;
    //                     $productNew1->price = $request->price_basic;
    //                     $productNew1->nominal = ceil((int)$request->price_basic / 1000) * 1000; ;
    //                     $productNew1->admin_fee = $request->admin_fee;
    //                     $productNew1->commission = 0;
    //                     $productNew1->biller_id = $request->biller_id;
    //                     $productNew1->description = $request->description;
    //                     $productNew1->is_trouble = 0;
    //                     $productNew1->created_by = $request->session()->get('SESS_USERDATA')['nama'];
    //                     $productNew1->created_at = date("Y-m-d H:i:s");
    //                     $saveProduct1 = $productNew1->save();
    //                     $productNew =  ProductBiller::where('id',$id)->update([
    //                         'product_id'=>$productNew1->id
    //                     ]);
    //                 }
    //                 return response()->json([
    //                     'status' => 200,
    //                     'message' => "Data berhasil diupdate",
    //                     'data'=>$saveProduct
    //                 ]);
    //             } else {
    //                     $productNew2 =  new Product();
    //                     $productNew2->biller_id = $request->biller_id;
    //                     $productNew2->group_id = $request->group_id;
    //                     // $productNew2->category = 0;
    //                     $productNew2->sku_code = $request->sku_biller.time();
    //                     $productNew2->name = $request->description;
    //                     $productNew2->price = $request->price_basic;
    //                     $productNew2->nominal = ceil((int)$request->price_basic / 1000) * 1000; ;
    //                     $productNew2->admin_fee = $request->admin_fee;
    //                     $productNew2->commission = 0;
    //                     $productNew2->biller_id = $request->biller_id;
    //                     $productNew2->description = $request->description;
    //                     $productNew2->is_trouble = 0;
    //                     $productNew2->created_by = $request->session()->get('SESS_USERDATA')['nama'];
    //                     $productNew2->created_at = date("Y-m-d H:i:s");
    //                     $saveProduct1 = $productNew2->save();
    //                     $productNew =  ProductBiller::where('id',$id)->update([
    //                         'product_id'=>$productNew2->id
    //                     ]);
    //                     return response()->json([
    //                         'status' => 200,
    //                         'message' => "Data berhasil diupdate",
    //                         'data'=>$saveProduct
    //                     ]);
    //             }

    //         } else {
    //                 return response()->json([
    //                     'status' => 500,
    //                     'message' => 'error ',
    //                 ]);
    //         }
    // }
    public function store(Request $request)
    {
        $idProduct =  (isset($request->product_id) ? ($request->product_id != "" ? $request->product_id : 0) : 0);
        $productNewBiller =  new ProductBiller();
        $productNewBiller->product_id =  $idProduct;
        $productNewBiller->biller_id = $request->biller_id;
        $productNewBiller->sku_biller = $request->sku_biller;
        $productNewBiller->price_basic = $request->price_basic;
        $productNewBiller->admin_fee = $request->admin_fee;
        $productNewBiller->priority = $request->priority;
        $productNewBiller->is_trouble = 0;
        $productNewBiller->description = $request->description;
        $productNewBiller->created_by = $request->session()->get('SESS_USERDATA')['nama'];
        $productNewBiller->created_at = date("Y-m-d H:i:s");
        $saveProduct = $productNewBiller->save();
        if ($saveProduct) {
       
            if( $request->priority == 1 && $idProduct != 0) {
                ProductBiller::where('product_id', $idProduct)->where('id','!=',$productNewBiller->id)->update([
                    'priority'=>2
                ]);
            }
            if ($idProduct != 0) {
                $productNew1 =  Product::where('id', $idProduct)->first();
                $productNew1->biller_id = $request->biller_id;
                // $productNew1->price = $request->price_basic;
                $saveProduct1 = $productNew1->save();
            }
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil ditambahkan",
                'data' => $saveProduct
            ]);
        }
        return response()->json([
            'status' => 400,
            'message' => "gagal",
        ]);
    }
    public function storebackupold(Request $request)
    {

        $productNew =  new Product();
        $productNew->biller_id = $request->biller_id;
        $productNew->group_id = $request->group_id;
        // $productNew->category = 0;
        $productNew->sku_code = $request->sku_biller;
        $productNew->name = $request->description;
        $productNew->price = $request->price_basic;
        $productNew->nominal = ceil((int)$request->price_basic / 1000) * 1000;;
        $productNew->admin_fee = $request->admin_fee;
        $productNew->commission = 0;
        $productNew->biller_id = $request->biller_id;
        $productNew->description = "";
        $productNew->is_trouble = 0;
        $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
        $productNew->created_at = date("Y-m-d H:i:s");

        $saveProduct = $productNew->save();
        if ($saveProduct) {
            $productNewBiller =  new ProductBiller();
            $productNewBiller->product_id = $productNew->id;
            $productNewBiller->biller_id = $request->biller_id;
            $productNewBiller->sku_biller = $request->sku_biller;
            $productNewBiller->price_basic = $request->price_basic;
            $productNewBiller->admin_fee = $request->admin_fee;
            $productNewBiller->priority = $request->priority;
            $productNewBiller->is_trouble = 0;
            $productNewBiller->description = $request->description;
            $productNewBiller->created_by = $request->session()->get('SESS_USERDATA')['nama'];
            $productNewBiller->created_at = date("Y-m-d H:i:s");
            $productNewBiller->save();

            return response()->json([
                'status' => 200,
                'message' => "Data berhasil ditambahkan",
                'data' => $saveProduct
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
        }
    }

    public function import(Request $request)
    {

        $data = Excel::toArray(new ProductBillerImport(), $request->file);

        return response()->json([
            'status' => 200,
            'message' => "Apakah anda yakin untuk import " . (count($data[0]) - 1) . ' Produk Biller',
            'data' => ($data[0])
        ]);
    }
    public function doimport(Request $request)
    {
        $data = Excel::toArray(new ProductBillerImport(), $request->file);
        $countSuccess = 0;
        $countFail = 0;
        $nameUser = 'Import Oleh ' . $request->session()->get('SESS_USERDATA')['nama'];
        $today =  date('Y-m-d H:i:s');
        $all = count($data[0]) - 1;
        $messageError = [];
        DB::beginTransaction();
        foreach ($data[0] as $k => $v) {
            if ($k != 0) {
                $idBiller =  Biller::where('name', $v['1'])->first();
                if ($idBiller) {

                    $cekSku = Product::where('sku_code', trim($v['3']))->first();

                    if($cekSku){
                        $idBiller = $idBiller->id;
                        $params['biller_id'] = $idBiller;
                        $params['product_id'] = $cekSku->id;
                        $params['is_trouble'] = 0;
                        $params['sku_biller'] = $v['2']  != ""  ? $v['2'] : "";
                        $params['price_basic'] = $v['5']  != ""  ? $v['5'] : 0;
                        $params['admin_fee'] = $v['6'] != ""  ? $v['6'] : 0;
                        $params['priority'] = $v['7'] == 'Tinggi' ? 1 : ($v['7'] == "Normal" ? 2 : 3);
                        $params['description'] = $v['4']  != ""  ? $v['4'] : "";
                        $params['created_at'] = $today;
                        $params['updated_at'] = $today;
                        $params['created_by'] = $nameUser;
                        try {
                            $result = DB::table('product_biller')->insert($params);
                            $countSuccess++;
                        } catch (\Exception $e) {
                            return response()->json([
                                'status' => 400,
                                'message' => "import Gagal di row ke-" . $k,
                                'data' => ($e)
                            ]);
                            $countFail++;
                        }
                    }else{
                        $countFail++;
                        array_push($messageError, "- row {$k} sku {$v['3']} tidak dikenal");
                    }
                    

                    
                } else {
                    $countFail++;
                }
            }
        }
        if ($countFail > 0) {
            DB::rollback();
        } else {
            DB::commit();
        }


        return response()->json([
            'status' => 200,
            'message' => "Berhasil Import <b>" . $countSuccess . "</b> data!",
            'data' => [
                'all' => $countSuccess,
                'sukses' => $countSuccess,
                'gagal' => $countFail,
                'message_error' => $messageError,
            ]
        ]);
    }


    public function groupCombobox(Request $request)
    {
        if ($request->group_id != "") {
            $data = Product::where('is_deleted', 0)->where('group_id', $request->group_id)->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['id'];
                $res[$k]['text'] = $d['fullname'];
            }
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }

    public function groupCombobox2(Request $request)
    {
        if ($request->group_name != "") {
            $data = ProductGroup::where('is_deleted', 0)->where('group_name', $request->group_name)->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['id'];
                $res[$k]['text'] = $d['command'] . " - " . $d['provider'];
            }
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }
    public function groupCombobox3(Request $request)
    {
        // dd($request->all());
        if ($request->biller != "") {
            $data = ProductParsing::where('is_deleted', 0)->where('biller_id', $request->biller)->get();
            $getHolder=collect($data)->pluck('holder_id')->toArray();
            $tipePar = ProductGroupHolder::whereIn('id',$getHolder)->get();
            $tipePars = collect($tipePar)->pluck(['name'])->unique()->toArray();
            $tipe = [];
            foreach ($tipePars as $k => $v) {
                $tipe[$k]['id'] = $v;
                $tipe[$k]['text'] = $v;
            }
            $countX = count($tipe);
            $tipe[$countX]['id'] = 'Belum Di Pilih';
            $tipe[$countX]['text'] = 'Belum Di Pilih';
           
            return response()->json([
                'data' => $tipe,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }
}
