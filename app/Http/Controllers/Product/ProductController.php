<?php

namespace App\Http\Controllers\Product;

use App\Exports\ProductExport;
use App\Exports\ProductExportPostpaid;
use App\Exports\ProductExportBiller;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductBiller;
use App\Models\Biller;
use App\Models\ProductBin;
use App\Imports\ProductBillerImport;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function fnDataTerlaris(Request $request)
    {

        // if($request->has("filterGroup") && $request->filterGroup == 0){
        //     $query = Product::with(['productGroup','productBiller.biller','biller','category'])->where("is_deleted",0)->withCount('sold')->whereHas(
        //         'productGroup', function ($query) use($request){
        //             if($request->has("filterJenis") && !empty($request->filterJenis)){
        //                 $query->whereIn('product_group.command',$request->filterJenis);
        //             }
        //             if($request->has("filterProvider") && !empty($request->filterProvider)){
        //                 $query->whereIn('product_group.provider',$request->filterProvider);
        //             }

        //         }
        //     );
        // } else {
        $query = Product::with(['productGroup', 'productBiller.biller', 'biller', 'category'])->where("is_deleted", 0)->withCount('sold')->whereHas(
            'productGroup',
            function ($query) use ($request) {
                if ($request->has("filterJenis") && !empty($request->filterJenis)) {
                    $query->whereIn('product_group.command', $request->filterJenis);
                }
                if ($request->has("filterProvider") && !empty($request->filterProvider)) {
                    $query->whereIn('product_group.provider', $request->filterProvider);
                }
                if ($request->has("filterGroup") && $request->filterGroup != "") {
                    $query->where('product_group.group_name', $request->filterGroup);
                }
            }
        );
        // }

        $query = $query->whereHas('productBiller');
        if ($request->has("filterBiller") && !empty($request->filterBiller)) {
            $query = $query->whereIn('biller_id', $request->filterBiller);
        }

        $query = $query->orderBy('sold_count', 'desc')->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($q) {

                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData(' . $q->id . ')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct(' . $q->id . ')"><i class="fa fa-trash-o"></i></a>';
            })
            ->addColumn('status', function ($q) {

                if ($q->is_deleted == 1) {
                    return '<span class="badge badge-danger">Tidak Aktif</span>';
                } else {
                    return '<span class="badge badge-primary">Aktif</span>';
                }
            })
            ->addColumn('billerText', function ($q) {

                if (isset($q->biller)) {
                    return $q->biller->name ?? "Biller belum ada";
                } else {
                    return 'Biller belum ada';
                }
            })
            ->addColumn('isTrouble', function ($q) {
                if ($q->is_trouble == 1) {
                    return '<span class="badge badge-warning">Gangguan</span>';
                } else {
                    return '<span class="badge badge-success">Normal</span>';
                }
            })
            ->addColumn('jenis', function ($q) {
                // return $q->productGroup->provider." ".$q->productGroup->command;
                return $q->productGroup->command;
            })
            ->addColumn('deskripsi', function ($q) {
                return "";
            })
            ->addColumn('solds', function ($q) {
                return $q->sold_count . ' Trx';
            })
            ->addColumn('groupName', function ($q) {
                if ($q->productGroup == null) {
                    return 'Belum ada Group';
                } else {
                    return $q->productGroup->group_name;
                }
            })
            ->addColumn('command', function ($q) {
                if ($q->productGroup == null) {
                    return 'Belum ada Data';
                } else {
                    return $q->productGroup->command;
                }
            })
            ->addColumn('provider', function ($q) {
                if ($q->productGroup == null) {
                    return 'Belum ada Data';
                } else {
                    return $q->productGroup->provider;
                }
            })
            ->addColumn('cat', function ($q) {
                if ($q->category == null) {
                    return 'Belum ada Data';
                } else {
                    return $q->category->name;
                }
            })

            ->rawColumns(['action', 'status', 'isTrouble'])
            ->make(true);
    }

    public function fnDataPostpaid(Request $request)
    {

        // $posts = Post::whereHas('comments', function (Builder $query) {
        //     $query->where('content', 'like', 'code%');
        // })->get();
        if ($request->has("type") && $request->type == "prepaid") {
            $query = Product::with(['productGroup', 'productBiller.biller', 'biller', 'category'])->where("is_deleted", 0)->whereHas(
                'productGroup',
                function ($query) use ($request) {
                    if ($request->has("filterJenis") && !empty($request->filterJenis)) {
                        $query->whereIn('product_group.command', $request->filterJenis);
                    }
                    if ($request->has("filterProvider") && !empty($request->filterProvider)) {
                        $query->whereIn('product_group.provider', $request->filterProvider);
                    }
                    $query->where('product_group.group_name', "Prabayar");
                }
            );
        } else {
            $query = Product::with(['productGroup', 'productBiller.biller', 'biller', 'category'])->where("is_deleted", 0)->whereHas(
                'productGroup',
                function ($query) use ($request) {
                    if ($request->has("filterJenis") && !empty($request->filterJenis)) {
                        $query->whereIn('product_group.command', $request->filterJenis);
                    }
                    if ($request->has("filterProvider") && !empty($request->filterProvider)) {
                        $query->whereIn('product_group.provider', $request->filterProvider);
                    }
                    $query->where('product_group.group_name', "Pascabayar");
                }
            );
        }
        // $query = $query->whereHas('productBiller');
        if ($request->has("filterBiller") && !empty($request->filterBiller)) {
            $query = $query->whereIn('biller_id', $request->filterBiller);
        }

        $query = $query->get();
        // return($query);
        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($q) {

                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData(' . $q->id . ')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct(' . $q->id . ')"><i class="fa fa-trash-o"></i></a>';
            })
            ->addColumn('status', function ($q) {

                if ($q->is_deleted == 1) {
                    return '<span class="badge badge-danger">Tidak Aktif</span>';
                } else {
                    return '<span class="badge badge-primary">Aktif</span>';
                }
            })
            ->addColumn('isTrouble', function ($q) {
                if ($q->is_trouble == 1) {
                    return '<span class="badge badge-warning">Gangguan</span>';
                } else {
                    return '<span class="badge badge-success">Normal</span>';
                }
            })
            ->addColumn('isUpdate', function ($q) {
                if ($q->is_updated == 1) {
                    return '<span class="badge badge-success">Ya</span>';
                } else {
                    return '<span class="badge badge-default">Tidak</span>';
                }
            })
            ->addColumn('billerText', function ($q) {


                if (isset($q->productBiller)) {
                    if (isset($q->productBiller->biller)) {
                        return $q->productBiller->biller->name;
                    }else {
                        return 'Biller belum ada';
                    }
                } else {
                    return 'Biller belum ada';
                }
            })
            ->addColumn('jenis', function ($q) {
                // return $q->productGroup->provider." ".$q->productGroup->command;
                return $q->productGroup->command;
            })
            ->addColumn('deskripsi', function ($q) {
                return "";
            })
            ->addColumn('category', function ($q) {
                if ($q->category == null) {
                    return 'tidak ada kategori';
                } else {
                    return $q->category->name;
                }
            })
            ->addColumn('adminFee', function ($q) {

                return formatCurrency($q->admin_fee);
            })
            ->addColumn('hargaDasar', function ($q) {
                if (isset($q->productBiller)) {
                    return formatCurrency($q->productBiller->price_basic);
                } else {
                    return "<p style='color:red;'>SKU Biller belum di set</p>";
                }
            })
            ->addColumn('hargaJual', function ($q) {
                if (isset($q->price)) {
                    return $q->price;
                } else {
                    return "";
                }
            })

            ->rawColumns(['action', 'status', 'isTrouble', 'hargaDasar','isUpdate'])
            ->make(true);
    }

    public function addProduct(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'sku_code' => 'unique:products',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => "SKU sudah terdaftar. Mohon masukan SKU yang berbeda."
            ]);
        }

        if ($request->has('type') && $request->type == 'prepaid') {
            $productNew = new Product();
            $productNew->group_id = $request->group_id;
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->nominal = $request->nominal;
            $productNew->price = $request->filled('price') ? $request->price : 0;
            $productNew->is_trouble = 0;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = 0;
            $productNew->commission = 0;
            $productNew->biller_id = 0;
            $productNew->is_updated = $request->is_updated;
            $productNew->description = $request->description;
            $productNew->created_at = date("Y-m-d H:i:s");
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->created_by =  $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product biller)',
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product)',
                ]);
            }
        } else {
            $productNew = new Product();
            $productNew->group_id = $request->group_id;
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->nominal = $request->nominal;
            $productNew->price = $request->filled('price') ? $request->price : 0;
            $productNew->is_trouble = 0;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = $request->filled('admin_fee') ? $request->admin_fee : 0;
            $productNew->commission = 0;
            $productNew->biller_id = 0;
            $productNew->description = $request->description;
            $productNew->created_at = date("Y-m-d H:i:s");
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->created_by =  $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product biller)',
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product)',
                ]);
            }
        }
    }
    public function addProductbackupold(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'sku_code' => 'unique:products',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => "SKU sudah terdaftar. Mohon masukan SKU yang berbeda."
            ]);
        }

        if ($request->has('type') && $request->type == 'prepaid') {
            $productNew = new Product();
            $productNew->group_id = $request->group_id;
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->nominal = $request->nominal;
            $productNew->price = $request->price;
            $productNew->is_trouble = 0;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = 0;
            $productNew->commission = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->created_at = date("Y-m-d H:i:s");
            $productNew->updated_at = date("Y-m-d H:i:s");
            // $productNew->created_by = Session::get('user_id');
            $productNew->created_by = 1;
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('sku_biller', $request->sku_biller)->where('biller_id', $request->biller_id)->first();
                $productBiller->product_id = $productNew->id;
                $productBiller->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'error (product biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product)',
                ]);
            }
            return view('produk_prepaid_edit')->withBiller($biller)->withGroup($group)->withData($data);
        } else {

            $productNew = new Product();
            $productNew->group_id = $request->group_id;
            $productNew->sku_code = $request->sku_code;
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->nominal = 0;
            $productNew->price = 0;
            $productNew->is_trouble = 0;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = $request->admin_fee;
            $productNew->commission = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->created_at = date("Y-m-d H:i:s");
            $productNew->updated_at = date("Y-m-d H:i:s");
            // $productNew->created_by = Session::get('user_id');
            $productNew->created_by = 1;
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('sku_biller', $request->sku_biller)->where('biller_id', $request->biller_id)->first();
                $productBiller->product_id = $productNew->id;
                $productBiller->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'error (product biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'error (product)',
                ]);
            }
        }
    }


    public function deleteProduct(Request $request, $id)
    {

        $product =   Product::where('id', $id)->first();
        $product = collect($product)->toArray();
        $group = ProductGroup::where('id', $product['group_id'])->where('group_name', 'Prabayar')->count();
        if ($group >= 1) {
            if (isset($product)) {
                $productbin = ProductBin::create($product);
                if ($productbin) {
                    Product::where('id', $id)->delete();
                }
                Product::where('id', $id)->delete();
            }
        } else {
            Product::where('id', $id)->delete();
        }

        return response()->json([
            'status' => 200,
            'message' => "Produk berhasil dihapus"
        ]);
    }
    public function indexTerlaris(Request $request)
    {

        $biller = Biller::all();
        $group = ProductGroup::all();
        $group1 = [
            [
                'group_name' => 'Prabayar'
            ],
            [
                'group_name' => 'Pascabayar'
            ],
        ];
        $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        $jenis = [];
        foreach ($jenis1 as $k => $v) {
            $jenis[$k]['id'] = $v;
            $jenis[$k]['text'] = $v;
        }
        $provider1 = collect($group)->pluck('provider')->unique()->toArray();
        $provider = [];
        foreach ($provider1 as $k => $v) {
            $provider[$k]['id'] = $v;
            $provider[$k]['text'] = $v;
        }
        return view('produk_terlaris')->withBiller($biller)
            ->withJenis($jenis)
            ->withProvider($provider)
            ->withGroup($group1);
    }
    public function postPaidIndex(Request $request)
    {

        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
        // $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        // $jenis=[];
        // foreach($jenis1 as $k=>$v){
        //     $jenis[$k]['id']=$v;
        //     $jenis[$k]['text']=$v;
        // }
        // $provider1 = collect($group)->pluck('provider')->unique()->toArray();
        // $provider=[];
        // foreach($provider1 as $k=>$v){
        //     $provider[$k]['id']=$v;
        //     $provider[$k]['text']=$v;
        // }
        // return view('produk_postpaid')->withBiller($biller)
        //  ->withJenis($jenis)
        //  ->withProvider($provider)
        //  ->withGroup($group);
        $tipePar = ProductGroup::where('is_deleted', 0)->get();
        $tipePars = collect($tipePar)->pluck(['group_name'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }
        $biller = Biller::all();
        $group = ProductGroup::whereIn('group_name', ['prabayar', 'prepaid'])->where('is_deleted', 0)->get();
        $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        $jenis = [];
        foreach ($jenis1 as $k => $v) {
            $jenis[$k]['id'] = $v;
            $jenis[$k]['text'] = $v;
        }
        $provider1 = collect($group)->pluck('provider')->unique()->toArray();
        $provider = [];
        foreach ($provider1 as $k => $v) {
            $provider[$k]['id'] = $v;
            $provider[$k]['text'] = $v;
        }
        return view('produk_postpaid')->withBiller($biller)
            ->withJenis($jenis)
            ->withTipe($tipe)
            ->withProvider($provider)
            ->withGroup($group);
    }
    public function prePaidIndex(Request $request)
    {
        $tipePar = ProductGroup::where('is_deleted', 0)->get();
        $tipePars = collect($tipePar)->pluck(['group_name'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }
        $biller = Biller::all();
        $group = ProductGroup::whereIn('group_name', ['prabayar', 'prepaid'])->where('is_deleted', 0)->get();
        $jenis1 = collect($group)->pluck('command')->unique()->toArray();
        $jenis = [];
        foreach ($jenis1 as $k => $v) {
            $jenis[$k]['id'] = $v;
            $jenis[$k]['text'] = $v;
        }
        $provider1 = collect($group)->pluck('provider')->unique()->toArray();
        $provider = [];
        foreach ($provider1 as $k => $v) {
            $provider[$k]['id'] = $v;
            $provider[$k]['text'] = $v;
        }
        return view('produk_prepaid')->withBiller($biller)
            ->withJenis($jenis)
            ->withTipe($tipe)
            ->withProvider($provider)
            ->withGroup($group);
    }


    public function editProduct(Request $request, $id)
    {
        $data = Product::where('id', $request->id)->with(['productGroup', 'productBiller.biller', 'biller'])->first();

        $biller = Biller::all();
        $product =   Product::where('group_id', $data->group_id)->where('is_deleted', 0)->get();
        $idP = collect($product)->pluck('id')->toArray();
        $pBiller = ProductBiller::where('biller_id', $data->biller_id)->whereIn('product_id', $idP)->get();
        foreach ($pBiller as $k => $v) {
            if ($v['description'] != null) {
                $pBiller[$k]['text'] = $v['sku_biller'] . "&nbsp;|&nbsp;" . $v['description'];
            } else {
                $pBiller[$k]['text'] = $v['sku_biller'] . "&nbsp;|&nbsp;Belum ada deskripsi";
            }
        }
        if ($request->has('type') && $request->type == 'prepaid') {
            // $group = ProductGroup::whereIn('group_name',['prabayar','prepaid'])->where('is_deleted',0)->get();
            $group = ProductGroup::where('is_deleted', 0)->get();
            $cat = ProductCategory::where('group_id', $data->group_id)->where('status', 1)->get();
            // return $data;

            $tipePar = ProductGroup::where('is_deleted', 0)->get();
            $tipePars = collect($tipePar)->pluck(['group_name'])->unique()->toArray();
            $tipe = [];
            foreach ($tipePars as $k => $v) {
                $tipe[$k]['id'] = $v;
                $tipe[$k]['text'] = $v;
            }
            // dd($data);
            // return $tipe;
            return view('produk_prepaid_edit')->withBiller($biller)->withGroup($group)->withCat($cat)->withData($data)->withTipe($tipe)->withPbiller($pBiller);
        } else {
            // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->where('is_deleted',0)   ->get();
            // $cat = ProductCategory::where('group_id',$data->group_id)->where('status',1)->get();

            // return view('produk_postpaid_edit')->withBiller($biller)->withGroup($group)->withCat($cat)->withPbiller($pBiller)->withData($data);
            $group = ProductGroup::where('is_deleted', 0)->get();
            $cat = ProductCategory::where('group_id', $data->group_id)->where('status', 1)->get();
            // return $data;

            $tipePar = ProductGroup::where('is_deleted', 0)->get();
            $tipePars = collect($tipePar)->pluck(['group_name'])->unique()->toArray();
            $tipe = [];
            foreach ($tipePars as $k => $v) {
                $tipe[$k]['id'] = $v;
                $tipe[$k]['text'] = $v;
            }
            // return $tipe;
            return view('produk_postpaid_edit')->withBiller($biller)->withGroup($group)->withCat($cat)->withData($data)->withTipe($tipe)->withPbiller($pBiller);
        }
    }
    public function updateProduct(Request $request, $id)
    {
        if ($request->has('type') && $request->type == 'prepaid') {
            $productNew =  Product::where('id', $id)->first();
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            if ($request->filled('group_id')) {
                $productNew->group_id = $request->group_id;
            }
            $productNew->sku_code = $request->sku_code;
            $productNew->admin_fee = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->price = $request->price;
            $productNew->nominal = $request->nominal;
            $productNew->commission = 0;
            $productNew->description = $request->description;
            $productNew->is_updated = $request->is_updated;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                return response()->json([
                    'status' => 200,
                    'message' => "Produk berhasil diupdate",
                    'data' => $request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        } else {
            $productNew =  Product::where('id', $id)->first();
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->admin_fee = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->price = $request->price;
            $productNew->nominal = $request->nominal;
            $productNew->commission = 0;
            $productNew->description = $request->description;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                return response()->json([
                    'status' => 200,
                    'message' => "Produk berhasil diupdate",
                    'data' => $request->all()
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        }
    }
    public function updateProductOldBackupo(Request $request, $id)
    {
        if ($request->has('type') && $request->type == 'prepaid') {
            $productNew =  Product::where('id', $id)->first();
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->admin_fee = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->price = $request->price;
            $productNew->nominal = $request->nominal;
            $productNew->commission = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('sku_biller', $request->sku_billerEdit)->where('biller_id', $request->biller_id)->first();
                $productBiller->product_id = $id;
                $productBiller->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data' => $request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        } else {

            $productNew =  Product::where('id', $id)->first();
            $productNew->category_id = $request->filled('category_id') ? $request->category_id : 0;
            $productNew->sku_code = $request->sku_code;
            $productNew->nominal = 0;
            $productNew->price = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = $request->admin_fee;
            $productNew->commission = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->updated_at = date("Y-m-d H:i:s");
            // $productNew->created_by = Session::get('user_id');
            $productNew->updated_by = 1;
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('sku_biller', $request->sku_billerEdit)->where('biller_id', $request->biller_id)->first();
                $productBiller->product_id = $id;
                $productBiller->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data' => $request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        }
    }


    public function download(Request $request)
    {
        if ($request->type == "postpaid") {
            return Excel::download(new ProductExportPostpaid, 'Product-Postpaid(' . date('d-m-Y_H-i-s') . ').xlsx');
        } else if ($request->type == "prepaid") {
            return Excel::download(new ProductExport, 'Product-Prepaid(' . date('d-m-Y_H-i-s') . ').xlsx');
        } else {
            return Excel::download(new ProductExportBiller, 'Product-Biller(' . date('d-m-Y_H-i-s') . ').xlsx');
        }
    }
    public function import(Request $request)
    {

        $data = Excel::toArray(new ProductImport(), $request->file);

        return response()->json([
            'status' => 200,
            'message' => "Apakah anda yakin untuk import " . (count($data[0]) - 1) . ' Produk',
            'data' => ($data[0])
        ]);
    }
    public function doimport(Request $request)
    {
        $data = Excel::toArray(new ProductImport(), $request->file);
        $countSuccess = 0;
        $countFail = 0;
        $countDp = 0;
        $nameUser = 'Import Oleh ' . $request->session()->get('SESS_USERDATA')['nama'];
        $today =  date('Y-m-d H:i:s');
        $all = count($data[0]) - 1;
        DB::beginTransaction();
        $message = [];
        foreach ($data[0] as $k => $v) {
            $cekSku = Product::where('sku_code', trim($v['5']))->first();
            if (!$cekSku) {
                //
                if ($k != 0) {
                    $getIdGroup = ProductGroup::where('group_name', trim($v['1']) )->where('command',trim($v['2']) )->where('provider', trim($v['3']))->first();
                    if ($getIdGroup) {
                        $idGroup = $getIdGroup->id;
                        //category Id GET
                        if (trim($v['4']) != "") {
                            $idCategoryData = ProductCategory::where('name', trim($v['4']))->where('group_id', $idGroup)->first();
                            if ($idCategoryData) {
                                $idCategory = $idCategoryData->id;
                            } else {
                                $message[] = 'Kategori ' . trim($v['4']) . " tidak di temukan. SKU(".trim($v['5']).")";
                                $countFail++;
                            }
                        } else {
                            $idCategory = 0;
                        }
                    } else {
                        $message[] = '<br>Group dengan nama: ' . trim($v['1']) . "<br>Jenis: ".trim($v['2']). "<br>Provider: ".trim($v['3'])." <br>tidak di temukan. (SKU".trim($v['5']).") row ke-".$k;
                        $countFail++;
                    }
                    if (isset($idGroup) && isset($idCategory)) {
                        $params['biller_id'] = 0;
                        $params['group_id'] = $idGroup;
                        $params['category_id'] = $idCategory;
                        $params['sku_code'] = trim($v['5'])  != ""  ? trim($v['5']) : "";
                        $params['name'] = trim($v['6'])  != ""  ? trim($v['6']) : "";
                        $params['description'] = trim($v['7'])  != ""  ? trim($v['7']) : "";
                        $params['price'] = trim($v['9'])  != ""  ? trim($v['9']) : 0;
                        $params['nominal'] = trim($v['8'])  != ""  ? trim($v['8']) : 0;
                        $params['admin_fee'] =  0;
                        $params['commission'] =  0;
                        $params['is_trouble'] = 0;
                        $params['created_at'] = $today;
                        $params['updated_at'] = $today;
                        $params['created_by'] = $nameUser;
                        // if( trim($v['5']) == "XTX5"){
                        //     dd($params);
                        // }
                        try {
                            $result = DB::table('products')->insertGetId($params);
                            $countSuccess++;
                        } catch (\Exception $e) {
                            return response()->json([
                                'status' => 400,
                                'message' => "main import Gagal di row ke-" . $k,
                                'data' => ($e),
                            ]);
                            $countFail++;
                        }
                    }
                }
                //
            } else {
                $countDp++;
            }
        }
 
            DB::commit();
        if($countFail >0 && !empty($message)) {
 
            return response()->json([
                'status' => 200,
                'message' => "<b>Berhasil Import.</b><br><br>  Sukses: <b>" . $countSuccess . "</b> data!<br> Batal (Duplikat SKU): <b>" . $countDp . "</b> data!<br> Total Data: <b>" . $all . "</b> data!<br>GAGAL:<b>" . $countFail . "</b> data!<br>".$message[0],
                'data' => [
                    'all' => $all,
                    'sukses' => $countSuccess,
                    'gagal' => $countFail,
                ]
            ]);
        }

        return response()->json([
            'status' => 200,
            'message' => "<b>Berhasil Import.</b><br><br>  Sukses: <b>" . $countSuccess . "</b> data!<br> Batal (Duplikat SKU): <b>" . $countDp . "</b> data!<br> Total Data: <b>" . $all . "</b> data!",
            'data' => [
                'all' => $all,
                'sukses' => $countSuccess,
                'gagal' => $countFail,
            ]
        ]);
    }
    public function doimportold2(Request $request)
    {
        $data = Excel::toArray(new ProductImport(), $request->file);
        $countSuccess = 0;
        $countFail = 0;
        $countDp = 0;
        $nameUser = 'Import Oleh ' . $request->session()->get('SESS_USERDATA')['nama'];
        $today =  date('Y-m-d H:i:s');
        $all = count($data[0]) - 1;
        DB::beginTransaction();
        foreach ($data[0] as $k => $v) {
            $cekSku = Product::where('sku_code', $v['5'])->first();
            if (!$cekSku) {
                //
                if ($k != 0) {
                    $getIdGroup = ProductGroup::where('group_name', 'like', '%' . $v['1'] . '%')->where('command', 'like', '%' . $v['2'] . '%')->where('provider', 'like', '%' . $v['3'] . '%')->first();
                    if ($getIdGroup) {
                        $idGroup = $getIdGroup->id;
                        //category Id GET
                        if ($v['4'] != "") {
                            $idCategoryData = ProductCategory::where('name', 'like', '%' . $v['4'] . '%')->where('group_id', $idGroup)->first();
                            if ($idCategoryData) {
                                $idCategory = $idCategoryData->id;
                            } else {
                                $arrangement = ProductCategory::where('group_id', $idGroup)->count() + 1;
                                $newIdCategory['status'] = 1;
                                $newIdCategory['group_id'] = $idGroup;
                                $newIdCategory['name'] = $v['4'];
                                $newIdCategory['created_at'] = $today;
                                $newIdCategory['created_by'] = $nameUser;
                                $newIdCategory['arrangement'] = $arrangement;
                                $saveCategory = DB::table('product_category')->insertGetId($newIdCategory);
                                if ($saveCategory) {
                                    $idCategory = $saveCategory;
                                } else {
                                    return response()->json([
                                        'status' => 400,
                                        'message' => "(Kategori)import Gagal di row ke-" . $k,
                                        'data' => ($e)
                                    ]);
                                    $countFail++;
                                }
                            }
                        } else {
                            $idCategory = 0;
                        }
                    } else {
                        $newIdGroup['group_name'] = $v['1'];
                        $newIdGroup['command'] = $v['2'];
                        $newIdGroup['provider'] = $v['3'];
                        $newIdGroup['provider_icon'] = 'https://zanpay.co.id/images/provider/ico_noimages.png';
                        $newIdGroup['created_at'] = $today;
                        $newIdGroup['created_by'] = $nameUser;
                        $saveGroup = DB::table('product_group')->insertGetId($newIdGroup);
                        if ($saveGroup) {
                            $idGroup = $saveGroup;
                            //
                            if ($v['4'] != "") {
                                $idCategoryData = ProductCategory::where('name', 'like', '%' . $v['4'] . '%')->where('group_id', $idGroup)->first();
                                if ($idCategoryData) {
                                    $idCategory = $idCategoryData->id;
                                } else {
                                    $arrangement = ProductCategory::where('group_id', $idGroup)->count() + 1;

                                    $newIdCategory['status'] = 1;
                                    $newIdCategory['group_id'] = $idGroup;
                                    $newIdCategory['name'] = $v['4'];
                                    $newIdCategory['created_at'] = $today;
                                    $newIdCategory['created_by'] = $nameUser;
                                    $newIdCategory['arrangement'] = $arrangement;
                                    $saveCategory = DB::table('product_category')->insertGetId($newIdCategory);
                                    if ($saveCategory) {
                                        $idCategory = $saveCategory;
                                    } else {
                                        return response()->json([
                                            'status' => 400,
                                            'message' => "(Kategori)import Gagal di row ke-" . $k,
                                            'data' => ($e)
                                        ]);
                                        $countFail++;
                                    }
                                }
                            } else {
                                $idCategory = 0;
                            }
                            //


                        } else {
                            return response()->json([
                                'status' => 400,
                                'message' => "(Product Group)import Gagal di row ke-" . $k,
                                'data' => ($e)
                            ]);
                            $countFail++;
                        }
                    }

                    $params['biller_id'] = 0;
                    $params['group_id'] = $idGroup;
                    $params['category_id'] = $idCategory;
                    $params['sku_code'] = $v['5']  != ""  ? $v['5'] : "";
                    $params['name'] = $v['6']  != ""  ? $v['6'] : "";
                    $params['description'] = $v['7']  != ""  ? $v['7'] : "";
                    $params['price'] = $v['9']  != ""  ? $v['9'] : 0;
                    $params['nominal'] = $v['8']  != ""  ? $v['8'] : 0;
                    $params['admin_fee'] =  0;
                    $params['commission'] =  0;
                    $params['is_trouble'] = 0;
                    $params['created_at'] = $today;
                    $params['updated_at'] = $today;
                    $params['created_by'] = $nameUser;

                    try {
                        $result = DB::table('products')->insertGetId($params);
                        $countSuccess++;
                    } catch (\Exception $e) {
                        return response()->json([
                            'status' => 400,
                            'message' => "main import Gagal di row ke-" . $k,
                            'data' => ($e),
                        ]);
                        $countFail++;
                    }
                }
                //
            } else {
                $countDp++;
            }
        }
        if ($countFail > 0) {
            DB::rollback();
        } else {
            DB::commit();
        }


        return response()->json([
            'status' => 200,
            'message' => "<b>Berhasil Import.</b><br><br>  Sukses: <b>" . $countSuccess . "</b> data!<br> Batal (Duplikat SKU): <b>" . $countDp . "</b> data!<br> Total Data: <b>" . $all . "</b> data!",
            'data' => [
                'all' => $all,
                'sukses' => $countSuccess,
                'gagal' => $countFail,
            ]
        ]);
    }
    public function doimportOld(Request $request)
    {
        $data = Excel::toArray(new ProductImport(), $request->file);
        $countSuccess = 0;
        $countFail = 0;
        $countDp = 0;
        $nameUser = 'Import Oleh ' . $request->session()->get('SESS_USERDATA')['nama'];
        $today =  date('Y-m-d H:i:s');
        $all = count($data[0]) - 1;
        DB::beginTransaction();
        foreach ($data[0] as $k => $v) {
            $cekSku = Product::where('sku_code', $v['1'])->first();
            if (!$cekSku) {
                //
                if ($k != 0) {
                    $idBiller =  Biller::where('name', $v['13'])->first();
                    if ($idBiller) {
                        $idBiller = $idBiller->id;
                        $getIdGroup = ProductGroup::where('group_name', 'like', '%' . $v['2'] . '%')->where('command', 'like', '%' . $v['3'] . '%')->where('provider', 'like', '%' . $v['4'] . '%')->first();
                        if ($getIdGroup) {
                            $idGroup = $getIdGroup->id;
                            //category Id GET
                            if ($v['5'] != "") {
                                $idCategoryData = ProductCategory::where('name', 'like', '%' . $v['5'] . '%')->where('group_id', $idGroup)->first();
                                if ($idCategoryData) {
                                    $idCategory = $idCategoryData->id;
                                } else {
                                    $arrangement = ProductCategory::where('group_id', $idGroup)->count() + 1;
                                    $newIdCategory['status'] = 1;
                                    $newIdCategory['group_id'] = $idGroup;
                                    $newIdCategory['name'] = $v['5'];
                                    $newIdCategory['created_at'] = $today;
                                    $newIdCategory['created_by'] = $nameUser;
                                    $newIdCategory['arrangement'] = $arrangement;
                                    $saveCategory = DB::table('product_category')->insertGetId($newIdCategory);
                                    if ($saveCategory) {
                                        $idCategory = $saveCategory;
                                    } else {
                                        return response()->json([
                                            'status' => 400,
                                            'message' => "(Kategori)import Gagal di row ke-" . $k,
                                            'data' => ($e)
                                        ]);
                                        $countFail++;
                                    }
                                }
                            } else {
                                $idCategory = 0;
                            }
                        } else {
                            $newIdGroup['group_name'] = $v['2'];
                            $newIdGroup['command'] = $v['3'];
                            $newIdGroup['provider'] = $v['4'];
                            $newIdGroup['provider_icon'] = 'https://zanpay.co.id/images/provider/ico_noimages.png';
                            $newIdGroup['created_at'] = $today;
                            $newIdGroup['created_by'] = $nameUser;
                            $saveGroup = DB::table('product_group')->insertGetId($newIdGroup);
                            if ($saveGroup) {
                                $idGroup = $saveGroup;
                                //
                                if ($v['5'] != "") {
                                    $idCategoryData = ProductCategory::where('name', 'like', '%' . $v['5'] . '%')->where('group_id', $idGroup)->first();
                                    if ($idCategoryData) {
                                        $idCategory = $idCategoryData->id;
                                    } else {
                                        $arrangement = ProductCategory::where('group_id', $idGroup)->count() + 1;

                                        $newIdCategory['status'] = 1;
                                        $newIdCategory['group_id'] = $idGroup;
                                        $newIdCategory['name'] = $v['5'];
                                        $newIdCategory['created_at'] = $today;
                                        $newIdCategory['created_by'] = $nameUser;
                                        $newIdCategory['arrangement'] = $arrangement;
                                        $saveCategory = DB::table('product_category')->insertGetId($newIdCategory);
                                        if ($saveCategory) {
                                            $idCategory = $saveCategory;
                                        } else {
                                            return response()->json([
                                                'status' => 400,
                                                'message' => "(Kategori)import Gagal di row ke-" . $k,
                                                'data' => ($e)
                                            ]);
                                            $countFail++;
                                        }
                                    }
                                } else {
                                    $idCategory = 0;
                                }
                                //


                            } else {
                                return response()->json([
                                    'status' => 400,
                                    'message' => "(Product Group)import Gagal di row ke-" . $k,
                                    'data' => ($e)
                                ]);
                                $countFail++;
                            }
                        }

                        $params['biller_id'] = $idBiller;
                        $params['group_id'] = $idGroup;
                        $params['category_id'] = $idCategory;
                        $params['sku_code'] = $v['1']  != ""  ? $v['1'] : "";
                        $params['name'] = $v['6']  != ""  ? $v['6'] : "";
                        $params['description'] = $v['7']  != ""  ? $v['7'] : "";
                        $params['price'] = $v['9']  != ""  ? $v['9'] : 0;
                        $params['nominal'] = $v['8']  != ""  ? $v['8'] : 0;
                        $params['admin_fee'] = $v['10']  != ""  ? $v['10'] : 0;
                        $params['commission'] = $v['11']  != ""  ? $v['11'] : 0;
                        $params['is_trouble'] = $v['12'] == 'Normal' ? 0 : 1;
                        $params['created_at'] = $today;
                        $params['updated_at'] = $today;
                        $params['created_by'] = $nameUser;

                        try {
                            $result = DB::table('products')->insertGetId($params);
                            if ($v['14'] != "") {
                                //check produk biller exist
                                $productBillerData = ProductBiller::where('sku_biller', $v['14'])->where('biller_id', $idBiller)->first();


                                if ($productBillerData) {
                                    // $productBillerData->product_id=$result->id;
                                    // $productBillerData->updated_by=$result->nameUser;
                                    // $productBillerData->save();
                                    $final = ProductBiller::where('id', $productBillerData->id)->update([
                                        'product_id' => $result,
                                        'updated_by' => $nameUser
                                    ]);
                                } else {
                                    // new product biller
                                    $newProductBiller['product_id'] = $result;
                                    $newProductBiller['biller_id'] = $idBiller;
                                    $newProductBiller['sku_biller'] = $v['14'];
                                    $newProductBiller['price_basic'] = $v['16'];
                                    $newProductBiller['admin_fee'] = $v['17'] != "" ? $v['17'] : 0;
                                    $newProductBiller['priority'] = 2;
                                    $newProductBiller['is_trouble'] = 0;
                                    $newProductBiller['description'] = $v['15'];
                                    $newProductBiller['created_at'] = $today;
                                    $newProductBiller['updated_at'] = $today;
                                    $newProductBiller['created_by'] = $nameUser;
                                    $saveNewProductBiller = DB::table('product_biller')->insertGetId($newProductBiller);
                                    if (!$saveNewProductBiller) {
                                        return response()->json([
                                            'status' => 400,
                                            'message' => "(product biller)import Gagal di row ke-" . $k,
                                            'data' => ($e)
                                        ]);
                                    }
                                }
                            }
                            $countSuccess++;
                        } catch (\Exception $e) {
                            return response()->json([
                                'status' => 400,
                                'message' => "main import Gagal di row ke-" . $k,
                                'data' => ($e),
                            ]);
                            $countFail++;
                        }
                    } else {
                        $countFail++;
                    }
                }
                //
            } else {
                $countDp++;
            }
        }
        if ($countFail > 0) {
            DB::rollback();
        } else {
            DB::commit();
        }


        return response()->json([
            'status' => 200,
            'message' => "<b>Berhasil Import.</b><br><br>  Sukses: <b>" . $countSuccess . "</b> data!<br> Batal (Duplikat SKU): <b>" . $countDp . "</b> data!<br> Total Data: <b>" . $all . "</b> data!",
            'data' => [
                'all' => $all,
                'sukses' => $countSuccess,
                'gagal' => $countFail,
            ]
        ]);
    }
}
