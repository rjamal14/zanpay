<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\Product;
use App\Models\ProductBiller;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = ProductCategory::where('status',1)->with('group')->withCount('products')->orderBy('group_id','asc')->orderBy('arrangement','asc')->get();
        // return $query;
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
                if($q->products_count > 0){
                    return '<a    class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn  btn-sm btn-default" href="javascript:void(0)"title="Hapus" disabled><i class="fa fa-trash-o"></i></a>';
                 
                } else {
                    return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';

                }
        })
        ->addColumn('nama', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            if($q->group == null ){

                return ' - ';
            } else {

                return $q->group->command.' - '.$q->group->provider;
            }
        })
        ->addColumn('groupname', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            if($q->group == null ){

                return ' - ';
            } else {

                return $q->group->group_name;
            }
        })
        ->addColumn('urutan', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            if($q->arrangement == null ){

                return ' - ';
            } else {

                return 'Ke-'.$q->arrangement;
            }
        })
    
        ->rawColumns( ['action'])
        ->make(true);
    }

  
    public function index (Request $request)
    {
        $type = ProductGroup::where('is_deleted',0)->get();
        return view('product_category')->withType($type);

    }

    public function edit (Request $request,$id)
    {
            $data = ProductCategory::where('id',$id)->first();
            $type = ProductGroup::where('is_deleted',0)->get();
           
         
            return view('product_category_edit')
            ->withData($data)
            ->withType($type);
    }
    public function update (Request $request,$id)
    {
        
            $productNew =  ProductCategory::where('id',$id)->first();
            if($productNew->arrangement != $request->arrangement){
                ProductCategory::where('group_id',$productNew->group_id)->where('arrangement',$request->arrangement)->update([
                    'arrangement'=>$productNew->arrangement
                ]);
            }
            $productNew->group_id = $request->group_id;
            $productNew->name = $request->name;
            $productNew->status = 1;
            $productNew->arrangement =  $request->arrangement;
            // $productNew->status = $request->status;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
    }
    public function create (Request $request)
    {
        
        $arrangement = ProductCategory::where('group_id',$request->group_id)->count() + 1;
            $productNew =  new ProductCategory();
            $productNew->group_id = $request->group_id;
            $productNew->name = $request->name;
            $productNew->status = 1;
            $productNew->arrangement = $arrangement;
            // $productNew->status = $request->status;
            $productNew->updated_at = date("Y-m-d H:i:s");
            $productNew->created_at = date("Y-m-d H:i:s");
            $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
            $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
            $saveProduct = $productNew->save();
            if($saveProduct){
                return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$saveProduct
                    ]);
            } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
            }
    }
    public function deleteData (Request $request,$id)
    {


    

        // ProductCategory::where('id',$id)->delete();
        ProductCategory::destroy($id);
    
        return response()->json([
            'status' => 200,
            'message' => "sukses"
        ]);


    }
    public function byGroupId ($id)
    {
    
            $productNew =   ProductCategory::where('group_id',$id)->where('status',1)->get();
                return response()->json([
                        'status' => 200,
                        'message' => "Sukes",
                        'data'=>collect($productNew)->toArray()
                    ]);
                        
    }
    public function byBiller (Request $request,$id)
    {
    
            $product =   Product::where('group_id',$request->group_id)->where('is_deleted',0)->get();
            $idP = collect($product)->pluck('id')->toArray();
            $pBiller = ProductBiller::selectRaw('*')->where('biller_id',$id)->whereIn('product_id',$idP)->get();
            foreach($pBiller as $k=>$v){
                if($v['description'] != null){
                    $pBiller[$k]['text']=$v['sku_biller']."&nbsp;|&nbsp;".$v['description'];
                } else {
                    $pBiller[$k]['text']=$v['sku_biller']."&nbsp;|&nbsp;Belum ada deskripsi";
                }
            }
            
            return response()->json([
                    'status' => 200,
                    'message' => "Sukes",
                    'data'=>$pBiller
                ]);
                        
    }
 
}