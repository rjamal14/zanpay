<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductParsing;
use App\Models\Biller;
use App\Models\ProductGroupHolder;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ParsingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {


        $query = ProductParsing::where('is_deleted', 0)->with(['biller','holder'])->get();
        // return $query;
        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($q) {

                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData(' . $q->id . ')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct(' . $q->id . ')"><i class="fa fa-trash-o"></i></a>';
            })
            ->addColumn('holder', function ($q) {
                if(isset($q->holder)){
                    return $q->holder->name;
                } else {
                    return 'Belum ada Holder';
                }
            })
            ->addColumn('typeT', function ($q) {
                if(($q->type == 0)){
                    return 'Transaksi';
                } else {
                    return 'Bukan Transaksi';
                }
            })

            ->rawColumns(['action'])
            ->make(true);
    }


    public function index(Request $request)
    {
        $biller = Biller::where('is_deleted', 0)->get();
        // dd($biller);
        $groupH = ProductGroupHolder::where('type', 0)->get();
        $res = [];
        foreach ($groupH as $k => $d) {
            $res[$k]['id'] = $d['id'];
            $res[$k]['text'] = $d['name'];
        }
        return view('parsing')->withBiller($biller)->withGroupH($res);
    }

    public function groupCombobox2(Request $request)
    {
        if ($request->tipe != "") {
            $data = ProductGroupHolder::where('type', $request->tipe)->get();
            $res = [];
            foreach ($data as $k => $d) {
                $res[$k]['id'] = $d['id'];
                $res[$k]['text'] = $d['name'];
            }
            return response()->json([
                'data' => $res,
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'data' => [],
                'message' => 'success'
            ]);
        }
    }
    public function detail(Request $request, $id)
    {
        $data = ProductParsing::where('id', $id)->first();
        $groupH = ProductGroupHolder::where('type', $data->type)->get();
        $res = [];
        foreach ($groupH as $k => $d) {
            $res[$k]['id'] = $d['id'];
            $res[$k]['text'] = $d['name'];
        }
        $biller = Biller::where('is_deleted', 0)->get();

        return view('parsing_edit')
            ->withData($data)
            ->withGroupH($res)
            ->withBiller($biller);
    }
    public function update(Request $request, $id)
    {


        if ($request->holderId !== "0") {
            //cek holder
            $cekH = ProductGroupHolder::where('id', $request->holderId)->first();
            if (!$cekH) {
                $newPGH = new ProductGroupHolder();
                $newPGH->name = $request->holderId;
                $newPGH->type  = $request->parsingType;
                $newPGH->date_created  = date('Y-m-d H:i:s');
                $newPGH->created_by  = $request->session()->get('SESS_USERDATA')['nama'];
                $newPGH->save();
                $holderId = $newPGH->id;
                $titttleANehhh = $request->holderId;
            } else {
                $holderId = $cekH->id;
                $titttleANehhh = $cekH->name;
            }
        }
        $productNew =  ProductParsing::where('id', $id)->first();
        $productNew->biller_id = $request->group_id;
        $productNew->title = $titttleANehhh;
        $productNew->parsing = $request->parsing;
        $productNew->holder_id = $holderId;
        $productNew->type = $request->parsingType;
        // $productNew->status = $request->status;
        $productNew->updated_at = date("Y-m-d H:i:s");
        $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
        $saveProduct = $productNew->save();
        if ($saveProduct) {
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil diupdate",
                'data' => $saveProduct
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
        }
    }
    public function store(Request $request)
    {


        if ($request->holderId !== "0") {
            //cek holder
            $cekH = ProductGroupHolder::where('id', $request->holderId)->first();
            if (!$cekH) {
                $newPGH = new ProductGroupHolder();
                $newPGH->name = $request->holderId;
                $newPGH->type  = $request->parsingType;
                $newPGH->date_created  = date('Y-m-d H:i:s');
                $newPGH->created_by  = $request->session()->get('SESS_USERDATA')['nama'];
                $newPGH->save();
                $holderId = $newPGH->id;
                $titttleANehhh = $request->holderId;
            } else {
                $holderId = $cekH->id;
                $titttleANehhh = $cekH->name;
            }
        }
        // dd($cekH);
        $productNew =  new ProductParsing();
        $productNew->biller_id = $request->biller_id;
        $productNew->holder_id = $holderId;
        $productNew->type = $request->parsingType;
        $productNew->title = $titttleANehhh;
        $productNew->parsing = $request->parsing;
        $productNew->updated_at = date("Y-m-d H:i:s");
        $productNew->created_at = date("Y-m-d H:i:s");
        $productNew->updated_by = $request->session()->get('SESS_USERDATA')['nama'];
        $productNew->created_by = $request->session()->get('SESS_USERDATA')['nama'];
        $saveProduct = $productNew->save();
        if ($saveProduct) {
            return response()->json([
                'status' => 200,
                'message' => "Data berhasil diupdate",
                'data' => $saveProduct
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
        }
    }
    public function delete(Request $request, $id)
    {




        // ProductParsing::where('id', $id)->update([
        //     'is_deleted' => 1,
        //     'deleted_at' => date("Y-m-d H:i:s"),
        //     'deleted_by' => $request->session()->get('SESS_USERDATA')['nama']
        // ]);
        ProductParsing::where('id', $id)->delete();

        return response()->json([
            'status' => 200,
            'message' => "sukses"
        ]);
    }
}
