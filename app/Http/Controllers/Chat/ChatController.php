<?php

namespace App\Http\Controllers\Chat;
use BeyondCode\LaravelWebSockets\Apps\AppProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Reseller;
use App\Models\Message;
use App\Models\MessageDetail;
use App\Events\TestEvent;
use App\Events\NewMessage;
use Pusher\Pusher;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request,$id)
    {
        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        $getMessage = Message::where('sender',$id)->orWhere('receiver',$id)->first();
        if($getMessage){
            $messageId = $getMessage->message_id;
            $chatHistory['data'] = MessageDetail::with('sender')->where('message_id',$messageId)->orderBy('datetime','asc')->get();
            foreach( $chatHistory['data'] as $k=>$v){
                $chatHistory['data'][$k]['adminAvatar'] = $user->foto;
                $checkdt =  Carbon::parse($v['datetime']);
                $today = Carbon::now();

                $result=preg_match_all('/([#])\S*/',$v['message'],$matches);
                // $result=substr($v['message'], strpos($v['message'], "#"));

                if($result > 0){
                    // $chatHistory['data'][$k]['parsedMessage'] = $matches[0];
                    $chatHistory['data'][$k]['parsedMessage'] = str_replace($matches[0][0],'<a href="transaksi"><b>'.$matches[0][0].'</b></a>',$v['message']);
                
                } else {

                    $chatHistory['data'][$k]['parsedMessage'] =  $v['message'];
                }

                if($today->diffInDays($checkdt) >2){
                    $dt = date('H:i:s',strtotime($v['datetime'])).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime'])));

            
                } else {
                    $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
                }
                $chatHistory['data'][$k]['datetime'] = $dt;
            }
            MessageDetail::where('message_id',$messageId)->where('read_at',null)->where('sender','!=',0)->update(['read_at'=>date('Y-m-d H:i:s')]);
        }
        else {
            $chatHistory['data'] =[];
        }
        $chatHistory['sender'] = Reseller::where('id',$id)->with('group')->first();
        $chatHistory['messageId'] = ($messageId) ?? false;
        $chatHistory['resellerId'] = $id ;
       
        return $chatHistory;
    }
    public function chatRecent (Request $request)
    {
        $message['data'] = MessageDetail::where('sender','!=',0)->orderBy('datetime','desc')->groupBy('sender','message_id')->with('sender')->get();
        $message['unread_count'] = MessageDetail::where('sender','!=',0)->where('read_at',null)->orderBy('datetime','desc')->groupBy('message_id')->with('sender')->count();
        foreach( $message['data'] as $k=>$v){
            $checkdt =  Carbon::parse($v['datetime']);
            $today = Carbon::now();
            if($today->diffInDays($checkdt) >2){
                $dt = date('H:i:s',strtotime($v['datetime'])).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime'])));

        
            } else {
                $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
            }
            $message['data'][$k]['pesan'] = strlen($v['message']) > 50 ? substr($v['message'],0,50)."..." : $v['message'];
            $message['data'][$k]['datetime'] = $dt;
        }
        return    $message;
    }
    public function sendChat (Request $request,$id)
    {

        $newMessage = new MessageDetail();
        $newMessage->message_id = $id; 
        $newMessage->message = $request->message; 
        $newMessage->sender = $request->sender; 
        $newMessage->datetime = date('Y-m-d H:i:s');
        $save = $newMessage->save();

        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        if($save){
            $chatHistory['data'] = MessageDetail::with('sender')->where('message_id',$id)->orderBy('datetime','asc')->get();
            foreach( $chatHistory['data'] as $k=>$v){
                $chatHistory['data'][$k]['adminAvatar'] = $user->foto;
                $checkdt =  Carbon::parse($v['datetime']);
                $today = Carbon::now();
                if($today->diffInDays($checkdt) >2){
                    $dt = date('H:i:s',strtotime($v['datetime'])).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime'])));

            
                } else {
                    $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
                }
                $chatHistory['data'][$k]['datetime'] = $dt;
            }
           
        }
        else {
            $chatHistory['data'] =[];
        }
        $chatHistory['sender'] = Reseller::where('id',$request->sender)->first();
        $chatHistory['messageId'] = $id ?? false;
        $chatHistory['resellerId'] = $request->sender ;


        // event(new NewMessage($id,$request->message,0));
        $options = array(
            'cluster' => env('MIX_PUSHER_APP_CLUSTER'),
            'encrypted' => false
        );

        $pusher = new Pusher(
            env('MIX_PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'), 
            $options
        );
        $resellerids = Message::where('message_id',$id)->first();
        $idss = $resellerids->sender == 0 ?  $resellerids->receiver : $resellerids->sender;
        $data['message'] = $request->message;
        $data['messageId'] = $request->sender;
        $data['resellerId'] = $idss;
        $chatHistory['resellerId'] =$idss ;

        $pusher->trigger('new-message', 'App\\Events\\NewMessage', $data);

        return $chatHistory;
    }
    public function createChat (Request $request)
    {
        $data = new Message();
        $data->message_id = $this->generateChar(4).'-'.$this->generateNumeric(10);
        $data->sender = $request->sender;
        $data->receiver = $request->reseller_id;
        $data->datetime = date('Y-m-d H:i:s');
        $saveData = $data->save();
        if($saveData) {
            $newMessage = new MessageDetail();
            $newMessage->message_id = $data->message_id; 
            $newMessage->message = $request->message; 
            $newMessage->sender = $request->sender; 
            $newMessage->datetime = date('Y-m-d H:i:s');
            $save = $newMessage->save();
        }
       

        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        if($save){
            $chatHistory['data'] = MessageDetail::with('sender')->where('message_id',$data->message_id)->orderBy('datetime','asc')->get();
            foreach( $chatHistory['data'] as $k=>$v){
                $chatHistory['data'][$k]['adminAvatar'] = $user->foto;
                $checkdt =  Carbon::parse($v['datetime']);
                $today = Carbon::now();
                if($today->diffInDays($checkdt) >2){
                    $dt = date('H:i:s',strtotime($v['datetime'])).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime'])));

            
                } else {
                    $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
                }
                $chatHistory['data'][$k]['datetime'] = $dt;
            }
           
        }
        else {
            $chatHistory['data'] =[];
        }
        $chatHistory['sender'] = Reseller::where('id',$request->sender)->first();
        $chatHistory['messageId'] = $data->message_id ?? false;
        $chatHistory['resellerId'] = $request->sender ;

        return $chatHistory;
    }

    public function fnReseller (Request $request,$id)
    {
        $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
        $getMessage = Message::where('sender',$id)->orWhere('receiver',$id)->first();
        if($getMessage){
            $messageId = $getMessage->message_id;
            $chatHistory = MessageDetail::with('sender')->where('message_id',$messageId)->orderBy('datetime','asc')->get();
            foreach( $chatHistory as $k=>$v){
                $chatHistory[$k]['adminAvatar'] = $user->foto;
                $checkdt =  Carbon::parse($v['datetime']);
                $today = Carbon::now();
                return $today->diffInDays($checkdt);
                if($today->diffInDays($checkdt) > 2){
                    $dt = $this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime']))).', '.$this->hariIndonesia(date('Y-m-d',strtotime($v['datetime'])));

            
                } else {
                    $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
                }
                $chatHistory[$k]['datetime'] = $dt;
            }
        }
        else {
            $chatHistory =[];
        }
        return $chatHistory;
    }

  
    public function index (Request $request, AppProvider $apps)
    {
        if($request->has('sender_id') && $request->sender_id != ''){
            $id=Message::where('sender',$request->sender_id)->first()->sender;
        } else {
            $id=Message::first()->sender;
        }
        // 'apps' => $apps->all(),
        // 'port' => config('websockets.dashboard.port', 6001),
        // dd($apps->all()[0]->key);
        $appsKey = collect($apps->all()[0])->toArray();
// dd($id);
        return view('chat')->withId($id)->withAa($id)->withApp( $appsKey )->withPort(config('websockets.dashboard.port', 6001));

    }
    public function whatsapp (Request $request)
    {
     
    return view('whatsapp');

    }
    public function bulanIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    function hariIndonesia($date){
        $hari = date ("D",strtotime($date));
     
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
     
            case 'Mon':			
                $hari_ini = "Senin";
            break;
     
            case 'Tue':
                $hari_ini = "Selasa";
            break;
     
            case 'Wed':
                $hari_ini = "Rabu";
            break;
     
            case 'Thu':
                $hari_ini = "Kamis";
            break;
     
            case 'Fri':
                $hari_ini = "Jumat";
            break;
     
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
     
        return $hari_ini;
     
    }
    public function notification(Request $request)
    {
        if($request['token'] === 'b8vRkmkXLL0mpjjeW0fLwOfHdSip9XFemGhsVbAoBGEuzwsneWJ7o5Cy4T5jzgGM') {
            $options = array(
                'cluster' => env('MIX_PUSHER_APP_CLUSTER'),
                'encrypted' => false
            );
            $pusher = new Pusher(
                env('MIX_PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'), 
                $options
            );
            $data['message'] = "new message";
            $data['messageId'] =0;
            $data['resellerId'] = $request->reseller_id;
            $pusher->trigger('new-message', 'App\\Events\\NewMessage', $data);
            return response()->json([
                'status'=>'success',
                'message'=>'ok',
                'data'=>$pusher
                            ]);
        } else {
            return response()->json([
'status'=>'fail',
'message'=>'invalid_token'
            ]);
        }
        
    }
}