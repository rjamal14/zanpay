<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Routing\Controller as BaseController;
use Validator;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function apiService($params = [])
    {
        $client = new Client();
        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => '7sLc3MLG5PJaSEJMgOpwmGjOvLzd093bjsXT3JiTh4yp2J15rsjMztjam8%2FF0Nvn',
                'platform' => 'webadmin'
            ]
        ];
        try {
            $options['json'] = $params;
            $res = $client->request('POST', 'https://zanpay.co.id/api/transaction.php', $options);
            $response = json_decode($res->getBody()->getContents());
            return response()->json($response);
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            $errorData = json_decode($exception->getResponse()->getBody(true));
            return response()->json($errorData);
        }
    }
    public function otpService($params = [])
    {
        $client = new Client();
        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'platform' => 'webadmin'
            ]
        ];
        try {
            $options['json'] = $params;
            $res = $client->request('POST', 'https://zanpay.co.id/api/v1/sendotp.php', $options);
            $response = json_decode($res->getBody()->getContents());
            return response()->json($response);
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            $errorData = json_decode($exception->getResponse()->getBody(true));
            return response()->json($errorData);
        }
    }
    public function notifService($params = [])
    {
        $client = new Client();
        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'platform' => 'webadmin'
            ]
        ];
        try {
            $options['json'] = $params;
            $res = $client->request('POST', 'https://zanpay.co.id/api/payload.php', $options);
            // $response = json_decode($res->getBody()->getContents());
            return $res;
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            $errorData = json_decode($exception->getResponse()->getBody(true));
            return response()->json($errorData);
        }
    }
    public function syncService($params = [])
    {
        $client = new Client();
        $options = [
            'headers' => [
                'Accept' => 'application/json',
            ]
        ];
        try {
            $options['json'] = $params;
            $res = $client->request('POST', 'https://zanpay.co.id/api/balance.php', $options);
            // $response = json_decode($res->getBody()->getContents());
            return $res;
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            $errorData = json_decode($exception->getResponse()->getBody(true));
            return response()->json($errorData);
        }
    }

	public function upload($image)
	{
	
		$defaultDir = base_path() . '/public/media/' ;
		if (!is_dir($defaultDir)) {
			@mkdir($defaultDir, 0777, true);
		}

			$file = $image;
			$fileName = $this->generateAllChar(6) . '_' . $this->generateAllChar(6) . '.png';
			$move = $file->move($defaultDir, $fileName);

			if ($move) {
				return response()->json([
                    'status' => 200,
					'filepath' =>  url('/') .'/media/' . $fileName,
                ]);
			
			
			} else {
				return response()->json([
                    'status' => 500,
					'filepath' => "error",
                ]);
			}

	}
	function generateAllChar($length = 8)
    {
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789abcdefghijklmnpqrstuvwxyz";
        $max = strlen($codeAlphabet);
        $code = '';
        for ($i=0; $i < $length; $i++) {
            $code .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $code;
    }
	function generateChar($length = 8)
    {
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $max = strlen($codeAlphabet);
        $code = '';
        for ($i=0; $i < $length; $i++) {
            $code .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $code;
    }
	function generateNumeric($length = 8)
    {
        $codeAlphabet = "012356789";
        $max = strlen($codeAlphabet);
        $code = '';
        for ($i=0; $i < $length; $i++) {
            $code .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $code;
    }
}
