<?php

namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = Message::with(['detail','sender','receiver'])->get();
        return DataTables::of($query)
        ->addIndexColumn()
        // ->addColumn('action', function ($q)  {
      
        //         // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

        //         return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->no.')"><i class="fa fa-trash-o"></i></a>';
        // })
     
        // ->addColumn('status', function ($q)  {
        //     if($q->status == "0"){
        //         return '<span class="badge badge-default">Unread</span>';
        //     } else if($q->status == "1" ) {
        //         return '<span class="badge badge-success">Read</span>';
        //     } 
        // })
        ->rawColumns(['content','status'])
        ->make(true);
    }

  
    public function index (Request $request)
    {
   
        return view('message');

    }
 

}