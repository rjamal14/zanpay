<?php

namespace App\Http\Controllers\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsCategory;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = News::withCount('view')
        ->withCount('like')
        ->withCount('comment')
        ->with('category')
        ->where("is_deleted",0);
        $query = $query->get();
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('gambar', function ($q)  {
            return '<img src="'.$q->image.'" style="width:120px;" ">'. '&nbsp;';
        })
        ->addColumn('tanggal', function ($q)  {
             
            return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
    
        })
        ->rawColumns( ['action','nama','gambar'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        // $biller = Biller::all();
        $group = NewsCategory::where('is_deleted',0)->get();
     	return view('news')->withGroup($group);
    }
 

    public function add (Request $request)
    {   
          
            $param=$request->except('avatar');
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['image'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }


            $data = new News();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $create = $data::create($param);
            if($create){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {

        News::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = News::where('id',$request->id)->first();
        $group = NewsCategory::where('is_deleted',0)->get();
        return view('news_edit')->withData($data)->withGroup($group);
    }
    public function update (Request $request,$id)
    {
        $param = $request->except('avatar','id=','_token');
        // dd($param);
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['image'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }
            $update =  News::where('id',$id)->update($param);

            // $update = $data;
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
    }

}