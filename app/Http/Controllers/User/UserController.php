<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserLog;
use Carbon\Carbon;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = User::where('is_deleted',0)->orderBy('id', 'desc')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
                return '<button class="btn btn-sm btnEdit btn-primary" data-toggle="modal" data-target="#EditModalUser" data-id="'.$q->id.'"><i class="fa fa-edit"></i></button> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteUser('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('updated_at', function ($q)  {
            if($q->updated_at == null){
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            } else {
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->updated_at)))." " . date('H:i:s',strtotime($q->updated_at));
            }
        })
    
        ->rawColumns( ['action'])
        ->make(true);
    }


    public function index (Request $request)
    {
       
        // $data = User::where('is_deleted',0)->get();

        return view('user');

    }
   
    public function show (Request $request,$id)
    {
        $data = User::where('id',$request->id)->first();  
        if($data){
            $res = [
                "status" => 'success',
                "data" => $data
            ];
        }else{
            $res = [
                "status" => 'error',
                "data" => $data
            ];
        } 
        return $res;
    }
    public function update (Request $request,$id)
    {
        $User = User::where('id',$id)->first(); 
        $param = $request->except('foto');
       
        if($request->foto){
            $urlFoto = $this->upload(
                $request->file('foto')
            ); 
            if($urlFoto->getData()->status == 200){
                $param['foto'] = $urlFoto->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
        }
        if($request->password){
            $param['password'] = \Hash::make($request->password);
        }
        $User = $User->update($param);
        if($User){
                return response()->json([
                    'status' => 200,
                    'message' => 'Success',
                ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error (product)',
            ]);
        }
    }


    public function store (Request $request)
    {
        $User = new User();
        $param = $request->except('foto');
        $urlFoto = $this->upload(
            $request->file('foto')
        ); 
        if($urlFoto->getData()->status == 200){
            $param['foto'] = $urlFoto->getData()->filepath;
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error uploading image',
            ]);
        }
        $param['password'] = \Hash::make($request->password);
        $param['pin'] = 123123;
        $param['alamat'] = '-';
        $param['email_verified_at'] = Carbon::now();
        $param['phone_verified_at'] = Carbon::now();
        // dd($param);
        $User = $User::create($param);
        if($User){
                return response()->json([
                    'status' => 200,
                    'message' => 'Success',
                ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error (product)',
            ]);
        }
    }
    public function delete (Request $request,$id)
    {
        User::where('id',$id)->delete();

        return response()->json([
            'status' => 200,
            'message' => "User berhasil dihapus"
        ]);
    }

    public function tanggalIndonesia ($tanggal)
    {
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}