<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Term;
use App\Models\Privacy;
use App\Models\AboutUs;
use App\Models\ContactMessage;
use App\Models\Icon;
use App\Models\TermDetail;
use App\Models\PrivacyDetail;
use App\Models\Team;
use App\Models\Faq;
use App\Models\SocialMedia;
use App\Models\Feature;
use App\Models\WebHome;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use DataTables;
use Illuminate\Support\Collection;

class WebController extends Controller
{
    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function videoIndex (Request $request)
    {
        $data = Contact::first();
        return view('web_contact')->withData($data);
    }
  
    public function videoUpdate (Request $request)
    {   
       
        if($request->hasFile('ads_video_thumbnail') ){
            $urlAvatar = $this->upload(
                $request->file('ads_video_thumbnail')
            ); 
         
            if($urlAvatar->getData()->status == 200){
                $adsThumbnail= $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
        }
        if($request->hasFile('ads_video') ){
            $urlAvatar2 = $this->upload(
                $request->file('ads_video')
            ); 
           
            if($urlAvatar2->getData()->status == 200){
                     $adsVideo = $urlAvatar2->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
        }

        $data = Contact::first();
        if(isset($adsThumbnail)){
            $data->ads_video_thumbnail = $adsThumbnail;
        }
        if(isset($adsVideo)){
            $data->ads_video = $adsVideo;
        }
        $save =$data->update();
        // dd($save);
        if($save){
            $data = Contact::first();
            return view('web_contact')->withData($data);
            
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error (payment method)',
            ]);
        }
        $data = Contact::first();
        return view('web_contact')->withData($data);
    }

    public function webHomeIndex (Request $request)
    {
        $data = WebHome::first();
        return view('web_home')->withData($data);
    }
  
    public function webHomeUpdate (Request $request)
    {   
        // dd($request->all());
        if($request->hasFile('main_image') ){
            $urlAvatar = $this->upload(
                $request->file('main_image')
            ); 
         
            if($urlAvatar->getData()->status == 200){
                $main_image= $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
        }
        if($request->hasFile('secondary_image') ){
            $urlAvatar2 = $this->upload(
                $request->file('secondary_image')
            ); 
           
            if($urlAvatar2->getData()->status == 200){
                     $secondary_image = $urlAvatar2->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }
        }

        $data = WebHome::first();
        if(isset($secondary_image)){
            $data->secondary_image = $secondary_image;
        }
        if(isset($main_image)){
            $data->main_image = $main_image;
        }
        $data->playstore_url=$request->playstore_url;
        $data->app_store_url=$request->app_store_url;
        $data->title=$request->title;
        $data->description=$request->description;
        $save =$data->update();
        // dd($save);
        if($save){
            $data = WebHome::first();
            return view('web_home')->withData($data);
            
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error (payment method)',
            ]);
        }
        $data = WebHome::first();
        return view('web_home')->withData($data);
    }


    public function termsList (Request $request)
    {
        $query =  TermDetail::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
      
        ->addColumn('judul', function ($q)  {
            
                return '<b>'.$q->title.'</b>';
        })
 
    
        ->rawColumns( ['judul','action','content'])
        ->make(true);
        
      
    }
    public function termsPrivacyList (Request $request)
    {
        $query =  PrivacyDetail::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
      
        ->addColumn('judul', function ($q)  {
            
                return '<b>'.$q->title.'</b>';
        })
 
    
        ->rawColumns( ['judul','action','content'])
        ->make(true);
        
      
    }

    public function termsIndex (Request $request)
    {
        $term = Term::first();
        $privacy = Privacy::first();
        return view('terms')->withTerm($term)->withPrivacy($privacy);
    }
    public function termsUpdate (Request $request)
    {   
       
        $term = Term::first();
        $term->update([
            'content' =>$request->term
        ]);

       return response()->json([
           'status' => 200
       ]);
    }
    public function termsListStore (Request $request)
    {   
       
        $term = new TermDetail();
        $term->term_id=1;
        $term->title=$request->title;
        $term->content=$request->content;
        $term->save();
       return response()->json([
           'status' => 200
       ]);
    }
    public function termsListEdit (Request $request,$id)
    {   
       
        $data =  TermDetail::where('id',$id)->first();
        return view('terms_edit')->withData($data);
    }
    public function termsListUpdate (Request $request,$id)
    {   
       
        $data =  TermDetail::where('id',$id)->first();
        $data->title=$request->title;
        $data->content=$request->content;
        $data->save();
        return response()->json([
            'status' => 200
        ]);
    }
    public function termsListDelete  (Request $request,$id)
    {   
        $data =  TermDetail::where('id',$id)->delete();
        return response()->json([
            'status' => 200
        ]);
    }


    public function termsPrivacyIndex (Request $request)
    {
        $term = Privacy::first();
        $privacy = Privacy::first();
        return view('privacy')->withTerm($term)->withPrivacy($privacy);
    }
    public function termsPrivacyUpdate (Request $request)
    {   
       
        $term = Privacy::first();
        $term->update([
            'content' =>$request->term
        ]);

       return response()->json([
           'status' => 200
       ]);
    }
    public function termsPrivacyListStore (Request $request)
    {   
       
        $term = new PrivacyDetail();
        $term->privacy_id=1;
        $term->title=$request->title;
        $term->content=$request->content;
        $term->save();
       return response()->json([
           'status' => 200
       ]);
    }
    public function termsPrivacyListEdit (Request $request,$id)
    {   
       
        $data =  PrivacyDetail::where('id',$id)->first();
        return view('privacy_edit')->withData($data);
    }
    public function termsPrivacyListUpdate (Request $request,$id)
    {   
       
        $data =  PrivacyDetail::where('id',$id)->first();
        $data->title=$request->title;
        $data->content=$request->content;
        $data->save();
        return response()->json([
            'status' => 200
        ]);
    }
    public function termsPrivacyListDelete  (Request $request,$id)
    {   
        $data =  PrivacyDetail::where('id',$id)->delete();
        return response()->json([
            'status' => 200
        ]);
    }

    public function aboutUsIndex (Request $request)
    {
        $data = AboutUs::get();
        return view('about_us')->withData($data);
    }
    public function aboutUsUpdate (Request $request)
    {   
        if($request->aboutusimage != "undefined"){
            $urlAvatar = $this->upload(
                $request->file('aboutusimage')
            ); 
            if($urlAvatar->getData()->status == 200){
                $aboutusimage = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            AboutUs::where('id',1)->update([
                'content'=>$request->aboutus,
                'image'=>$aboutusimage
            ]);
        } else {
            AboutUs::where('id',1)->update([
                'content'=>$request->aboutus
            ]);
        }
        if($request->missionimage != "undefined"){
            $urlAvatar = $this->upload(
                $request->file('missionimage')
            ); 
            if($urlAvatar->getData()->status == 200){
                $missionimage = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            AboutUs::where('id',3)->update([
                'content'=>$request->mission,
                'image'=>$missionimage
            ]);
        } else {
            AboutUs::where('id',3)->update([
                'content'=>$request->mission
            ]);
        }
        if($request->visionimage != "undefined"){
            $urlAvatar = $this->upload(
                $request->file('visionimage')
            ); 
            if($urlAvatar->getData()->status == 200){
                $visionimage = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            AboutUs::where('id',2)->update([
                'content'=>$request->vision,
                'image'=>$visionimage
            ]);
        } else {
            AboutUs::where('id',2)->update([
                'content'=>$request->vision
            ]);
        }
       return response()->json([
           'status' => 200,
           'data'=>$request->all()
       ]);
    }

    public function contactUsIndex (Request $request)
    {
        $data = contact::first();
        return view('contact_us')->withData($data);
    }
    public function socialMediaIndex (Request $request)
    {
        $icon = icon::all();
        return view('social_media')->withIcon($icon);
    }
    public function socialMediaEdit (Request $request,$id)
    {
        $icon = icon::all();
        $data= SocialMedia::where('id',$id)->first();
        return view('social_media_edit')->withIcon($icon)->withData($data);
    }
    public function socialMediaStore (Request $request)
    {
        $data = new SocialMedia();
        $data->title=$request->title;
        $data->icon=$request->icon;
        $data->address=$request->address;
        $data->created_by= $request->session()->get('SESS_USERDATA')['nama'];
        $data->created_at= date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function socialMediaUpdate (Request $request,$id)
    {
        $data =  SocialMedia::where('id',$id)->first();
        $data->title=$request->title;
        $data->icon=$request->icon;
        $data->address=$request->address;
        $data->save();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function socialMediaDelete (Request $request,$id)
    {
        $data =  SocialMedia::where('id',$id)->delete();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function socialMediaData (Request $request)
    {
        $query =  SocialMedia::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('icon', function ($q)  {
                return '<i class="fa '.$q->icon.'"></i>';
        })
        ->addColumn('url', function ($q)  {
           return '<a target="_blank" href="'.$q->address.'">'.$q->address.'</a>';
        })
        ->addColumn('updatedAt', function ($q)  {
            if($q->updated_at == null){
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            } else {
                return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->updated_at)))." " . date('H:i:s',strtotime($q->updated_at));
            }
        })
        ->addColumn('updatedBy', function ($q)  {
            if($q->updated_by == null){
                return ($q->created_by);
            } else {
                return $q->updated_by;
            }
        })
    
        ->rawColumns( ['icon','action','url'])
        ->make(true);
    }
    public function contactUsUpdate (Request $request)
    {   
        $term = contact::first();
        $term->update([
            'phone' =>$request->phone,
            'address' =>$request->address,
            'email' =>$request->email,
            'lng' =>$request->lng,
            'lat' =>$request->lat,
        ]);
       return response()->json([
           'status' => 200
       ]);
    }
    public function contactMessageIndex (Request $request)
    {
        return view('contact_message');
    }

    public function contactMessageEdit  (Request $request,$id)
    {
        $data  =  ContactMessage::where('id',$id)->first();
        $data['tanggal']= $this->tanggalIndonesia(date("Y-m-d",strtotime($data->created_at))).' '.date('H:i:s',strtotime($data->created_at));
        return view('contact_message_detail')->withData($data);
    }
    public function fnContactMessageData (Request $request)
    {
        $query = ContactMessage::orderBy('created_at','desc');
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
            return ' <a   class="btn btn-sm  btnEdit btn-info" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-eye"></i></a>';
        })
        ->addColumn('tanggal', function ($q)  {
            return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->created_at))).'<br>'.date('H:i:s',strtotime($q->created_at));
        })
        ->rawColumns( ['action','tanggal'])
        ->make(true);
    }

    public function teamIndex (Request $request)
    {
        return view('teams');
    }

    public function teamEdit (Request $request,$id)
    {
        $data  =  Team::where('id',$id)->first();
        return view('teams_edit')->withData($data);
    }

    public function teamAdd (Request $request)
    {   
            $param=$request->except(['avatar','facebook_url','instagram_url','twitter_url']);
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['image'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            $param['facebook_url']=$request["facebook_url"] == "" ? "!#" : $request["facebook_url"];
            $param['instagram_url']=$request["instagram_url"] == "" ? "!#" : $request["instagram_url"];
            $param['twitter_url']=$request["twitter_url"] == "" ? "!#" : $request["twitter_url"];
            $data = new Team();
            $data = $data::create($param);
            if($data){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
    }
    public function deleteTeam (Request $request,$id)
    {
        Team::where('id',$id)->delete();
        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);
    }
    public function updateTeam (Request $request,$id)
    {
            $team =  Team::where('id',$id)->first();
            $params = $request->except(['_token','avatar']);
            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $params['image'] = $urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $param['facebook_url']=$request["facebook_url"] == "" ? "!#" : $request["facebook_url"];
            $param['instagram_url']=$request["instagram_url"] == "" ? "!#" : $request["instagram_url"];
            $param['twitter_url']=$request["twitter_url"] == "" ? "!#" : $request["twitter_url"];
            $update = $team->update($params);
            if($update){
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Team)',
                ]);
            }
    }
    public function fnTeamData (Request $request)
    {
        $query = Team::orderBy('index');
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
                if($q->index == 1 ){
                    return ' <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
                } else {
                    return ' <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger disabled" href="javascript:void(0)"title="Hapus" disabled><i disabled class="fa fa-trash-o"></i></a>';
                }
        })
     
        ->addColumn('facebook', function ($q)  {
               if ($q->facebook_url== "!#"){
                    return 'Belum ada data';
               } else {
                   return '<a target="_blank" href="'.$q->facebook_url.'">'.$q->facebook_url.'</a>';
               }
        })
        ->addColumn('instagram', function ($q)  {
               if ($q->instagram_url == "!#"){
                    return 'Belum ada data';
               } else {
                   return '<a target="_blank" href="'.$q->instagram_url.'">'.$q->instagram_url.'</a>';
               }
        })
        ->addColumn('twitter', function ($q)  {
               if ($q->twitter_url == "!#"){
                    return 'Belum ada data';
               } else {
                   return '<a target="_blank" href="'.$q->twitter_url.'">'.$q->twitter_url.'</a>';
               }
        })
        ->addColumn('avatar', function ($q)  {
        
                return '<img src="'.$q->image.'" class="rounded-circle avatar" style="max-height:60px !important;">';
         
        })
        ->rawColumns( ['avatar','action','facebook','twitter','instagram'])
        ->make(true);
    }


    public function faqsIndex (Request $request)
    {
        return view('faqs');
    }

    public function faqsEdit (Request $request,$id)
    {
        $data  =  Faq::where('id',$id)->first();
        return view('faqs_edit')->withData($data);
    }

    public function faqsAdd (Request $request)
    {   
            $param=$request->except(['_token']);
            $data = new Faq();
            $data = $data::create($param);
            if($data){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
    }
    public function deleteFaqs (Request $request,$id)
    {
        Faq::where('id',$id)->delete();
        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);
    }
    public function updateFaqs (Request $request,$id)
    {
            $data =  Faq::where('id',$id)->first();
            $params = $request->except(['_token']);
           
            $update = $data->update($params);
            if($update){
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Team)',
                ]);
            }
    }
    public function fnFaqsData (Request $request)
    {
        $query = Faq::orderBy('id');
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
            return ' <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
               
        })
        ->rawColumns( ['action'])
        ->make(true);
    }


    public function featureIndex (Request $request)
    {
        return view('feature');
    }
    public function featureEdit (Request $request,$id)
    {
        $data= Feature::where('id',$id)->first();
        return view('feature_edit')->withData($data);
    }
    public function featureStore (Request $request)
    {
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $image= $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }

        $data = new Feature();
        $data->title=$request->title;
        $data->description=$request->description;
        $data->image=$image;
        $data->save();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function featureUpdate (Request $request,$id)
    {
        $data =  Feature::where('id',$id)->first();
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $data->image=$urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }
        $data->title=$request->title;
        $data->description=$request->description;
        $data->save();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function featureDelete (Request $request,$id)
    {
        $data =  Feature::where('id',$id)->delete();
        return response()->json([
            'status' => 200,
            'message' => "Sukes",
            'data'=>[]
        ]);
    }
    public function featureData (Request $request)
    {
        $query =  Feature::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> ';
        })
        ->addColumn('gambar', function ($q)  {
            return '
            <div class="add-data d-flex justify-content-center">
          <a href="" data-toggle="modal" data-target="#largeModal'.$q->id.'"><img class="img-responsive" src="'.$q->image.'" alt="" style="max-height:50px;">
        </div>
        
        
        <div class="modal fade" id="largeModal'.$q->id.'" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content">
             
              <div class="modal-body">
                <div class="zoom1 d-flex justify-content-center">
                  <img class="img-responsive img-center" style="max-width:1000px;" src="'.$q->image.'" alt="">
                </div>
              </div>
             
            </div>
          </div>
        </div>
        ';
        })

        ->rawColumns( ['action','gambar'])
        ->make(true);
    }
}