<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\ProductGroup;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = Provider::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->no.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->rawColumns( ['action'])
        ->make(true);
    }

  
    public function index (Request $request)
    {
        $tipePar = ProductGroup::all();
        $tipePars = collect($tipePar)->pluck(['provider'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }

        return view('provider')->withProvider($tipe);
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 
    public function edit (Request $request,$id)
    {
        $data = Provider::where('no',$id)->first();
        $tipePar = ProductGroup::all();
        $tipePars = collect($tipePar)->pluck(['provider'])->unique()->toArray();
        $tipe = [];
        foreach ($tipePars as $k => $v) {
            $tipe[$k]['id'] = $v;
            $tipe[$k]['text'] = $v;
        }

            return view('provider_edit')->withData($data)->withProvider($tipe);
    }
    public function add (Request $request)
    {
        $data = new Provider();
        $data->provider = $request->provider;
        $data->prefix_header = $request->prefix_header;
        $data->date_inserted = date('Y-m-d H:i:s');
        $save = $data->save();
        if($save){
            return response()->json([
                'status' => 200,
                'message' => "Provider berhasil ditambahkan",
                'data'=>$request->all()
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => "error",
                'data'=>$request->all()
            ]);
        }

    }
    public function update (Request $request,$id)
    {

            $provider =  Provider::where('no',$id)->first();
            $saveProvider = $provider->update($request->all());
            if($saveProvider){
                    return response()->json([
                        'status' => 200,
                        'message' => "Provider berhasil diupdate",
                        'data'=>$request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
    }
    public function delete (Request $request,$id)
    {

            $provider =  Provider::where('no',$id);
            $saveProvider = $provider->delete();
            if($saveProvider){
                    return response()->json([
                        'status' => 200,
                        'message' => "Provider berhasil dihapus",
                        'data'=>$request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
    }

}