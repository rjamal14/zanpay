<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class GlobalStoreController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api');
	}

	public function read(Request $request, $path)
	{
		switch ($path) {
			case 'user_groups':
				return response()->json($this->userGroups($request));
				break;
			default:
				return response()->json(['success' => false, 'message' => 'The server has not found anything matching the Request-URI.', 'data' => []], 401);
		}
	}

	public function userGroups($request)
	{
		$data = array();
		$error = array();
		try {
			$user_groups = DB::table('user_groups')
				->where('is_deleted', '=', false)
				->select('id', 'name');
			$data = $user_groups->get();
		} catch (QueryException $exception) {
			$error[] = $exception->getMessage();
		}

		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'data' => $data];
		}
		return $content;
	}
}
