<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Reseller;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = Document::with('reseller')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        // ->addColumn('action', function ($q)  {
      
        //         // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

        //         return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->no.')"><i class="fa fa-trash-o"></i></a>';
        // })
        ->addColumn('selfie', function ($q)  {
            if(isset($q->kyc_selfie)){
                return '
                <div class="add-data d-flex justify-content-center">
              <a href="" data-toggle="modal" data-target="#largeModal'.$q->no.'"><img class="img-responsive" src="'.$q->kyc_selfie.'" alt="" style="max-height:50px;">
            </div>
            
            
            <div class="modal fade" id="largeModal'.$q->no.'" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                 
                  <div class="modal-body">
                    <div class="zoom1 d-flex justify-content-center">
                      <img class="img-responsive img-center" src="'.$q->kyc_selfie.'" alt="">
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            ';
            } else {
                return "";
            }
        })
        ->addColumn('idcard', function ($q)  {
            if(isset($q->kyc_idcard)){
                return '
                <div class="add-data d-flex justify-content-center">
              <a href="" data-toggle="modal" data-target="#largeModalidCard'.$q->no.'"><img class="img-responsive" src="'.$q->kyc_idcard.'" alt="" style="max-height:50px;">
            </div>
            
            
            <div class="modal fade" id="largeModalidCard'.$q->no.'" tabindex="-1" role="dialog" aria-labelledby="largeModalidCard" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                 
                  <div class="modal-body">
                    <div class="zoom1 d-flex justify-content-center">
                      <img class="img-responsive img-center" src="'.$q->kyc_idcard.'" alt="">
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            ';
            } else {
                return "";
            }
        })
        ->addColumn('jamTanggal', function ($q)  {
            return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->date_created))).' '.date('H:i:s',strtotime($q->date_created));
        })
        ->addColumn('status', function ($q)  {
            if($q->status == "0"){
                return   '<select class="form-control form-control-sm select request-status" style="width:120px;">
                    <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Waiting</option>
                    <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Approved</option>
                    <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Rejected</option>
                </select>';
            } else if($q->status == "1" ) {
                return   '<select class="form-control form-control-sm select request-status" disabled style="width:120px;">
                <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Waiting</option>
                <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Approved</option>
                <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Rejected</option>
            </select>';
            } else if($q->status == "2" ) {
                return   '<select class="form-control form-control-sm select request-status" style="width:120px;">
                    <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Waiting</option>
                    <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Approved</option>
                    <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Rejected</option>
                </select>';
            } 
        })
        ->rawColumns( ['selfie','idcard','status'])
        ->make(true);
    }

    public function updateStatus (Request $request,$id)
    {

            $data =  Document::where('no',$id)->first();

            $params = $request->except('_token');
            if($data){

              if($data->status == 1){
                return response()->json([
                  'status' => 500,
                  'message' => 'error ',
                ]);
              } 
                $update = $data->update([
                    'status'=>$request->status
                ]);
                
                if($update){
                  if ($request->status == 1){
                    Reseller::where('id',$data->reseller_id)->update([
                      'status'=>2
                    ]);
                  }
                        return response()->json([
                            'status' => 200,
                            'message' => "Deposit berhasil diupdate",
                            'data'=>$request->all()
                        ]);
                
            
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error ',
                    ]);
                }

            }
            return response()->json([
                'status' => 500,
                'message' => 'error ',
            ]);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {

        return view('document');

    }
 

}