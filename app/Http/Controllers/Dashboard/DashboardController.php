<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\Reseller;
use App\Models\ResellerBalance;
use App\Models\ResellerHistory;
use App\Models\Biller;
use App\Models\BillerHistory;
use App\Models\Transaction;
use App\Models\ProductGroup;
use App\Models\Product;
use App\Models\ProductBiller;
use App\Models\DaftarPekerjaan;
use App\Models\Deposit;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
Use Carbon\CarbonPeriod;
Use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {


        $query = Provider::all();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->no.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->rawColumns( ['action'])
        ->make(true);
    }
    public function fnDataDaftarPekerjaan (Request $request)
    {
        $query = DaftarPekerjaan::orderBy('date','desc')->limit(4)->get();
        return $query;
    }
    public function fnUpdateDaftarPekerjaan (Request $request)
    {

        $status=$request->status;
        $query = DaftarPekerjaan::where('id',$request->id)->update(['status'=>$status]);
        return response()->json([
            'status' => 200,
            'message' => "Sukses",
        ]);
    }
    public function fnDataReseller (Request $request)
    {
        // $data = Transaction::selectRaw('
        // transaction.*,
        // COUNT(DISTINCT(reseller_id)) as total_trx
        // ')->where('status','s')->orderBy('total_trx','desc')->limit(10)->get();
        // if (isset($data)){
        //     $resellerId = collect($data)->pluck('reseller_id')->toArray();
        // }
        // $query = Reseller::whereIn('id',$resellerId)->with(['balance','attribute','group','latestHistory'])->withCount('successTransaction')->where("is_deleted",0)
        // // ->orderBy('balance.balance','desc')
        // ->get()
        // ->sortByDesc('balance.balance');
        $resellerId = [];
        $data = ResellerBalance::where('is_deleted',0)->get();
        $resellerId = collect($data)->pluck('reseller_id')->toArray();
        $query = Reseller::whereIn('id',$resellerId)->with(['balance','attribute','group','latestHistory','downline'])->withCount('successTransaction')->where("is_deleted",0)
        ->withCount('downline')
        ->orderBy('success_transaction_count', 'desc')
        // ->orderBy('balance.balance','desc')
        ->limit(10)
        ->get()
        ->sortByDesc('balance.balance');

        return DataTables::of($query)
   
    
        ->addColumn('balance', function ($q)  {
                  if($q->balance == null){
                      return formatCurrency(0);
                  } else{
                      return formatCurrency($q->balance->balance);
                  }
        })
        ->addColumn('balances', function ($q)  {
              
                      return $q->balance->balance;
        })
        ->addColumn('nama', function ($q)  {
              
                      return strtoupper($q->name);
        })
        ->addColumn('groups', function ($q)  {
                   if($q->group == null){
                       return "Belum ada data";
                   } else {
                        return strtoupper($q->group->name);
                   }
        })
        ->addColumn('activity', function ($q)  {
            if($q->latestHistory == null){
                return "Belum ada data";
            } else {
                 return $q->latestHistory->description;
            }
        })
        ->addColumn('image', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class="rounded-circle avatar" style="width: 45px!important;height:45px;!important"> ';
         
        })
        ->addColumn('image2', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class="rounded-circle avatar" style="width: 30px!important;height:30px;!important"> ';
         
        })
        ->addColumn('count', function ($q)  {
        
                return $q->success_transaction_count .' Trx';
         
        })
        ->addColumn('qty', function ($q)  {
        
                return $q->success_transaction_count;
         
        })
        ->addColumn('cashback', function ($q)  {
        
            if($q->group == null){
                return formatCurrency(0);
            } else{
                return formatCurrency($q->group->cashback);
            }
         
        })
        ->addColumn('downline', function ($q)  {
            if(count($q->downline) > 0){
                $x = '';
                foreach($q->downline as $v){
                    $x .= $v['reseller_code'].', ';
                }
                return $x;
            } else {
                return 'Belum ada data';
            }
        })
        ->addColumn('point', function ($q)  {
        
            if($q->balance == null){
                return formatNumber(0);
            } else{
                return formatNumber($q->balance->point);
            }
         
        })
 
        ->rawColumns( ['image','image2','balance'])
        ->make(true);
    }

  
    public function index (Request $request)
    {


        //get lastweek Date Range
        $lastWeekStart = date("Y-m-d", strtotime("last week monday"));
        $lastWeekEnd = date("Y-m-d", strtotime("last week sunday"));
        //get thisWeek Date Range
        $thisWeekStart = date("Y-m-d", strtotime("last monday"));
        $yesterday = date('Y-m-d',strtotime("-1 days"));
        $today = date('Y-m-d');
        //jumlahMember
        $member = Reseller::where('is_deleted',0);
        $jumlahMember = $member->count();
        $jumlahMemberText = '';
        $jumlahMemberColor = 'custom-color-green';

        //jumlahSaldoReseller
        $reseller = Reseller::where('is_deleted',0)->with('balance')->get();
        // detail 6 hari jumlah saldo reseller
        $maxIncrements = 5;
        $jumlahSaldoResellerDetailArr = [];
        while ($maxIncrements >= 0 )
            {
                $dateToCheck = date("Y-m-d", strtotime('-'.($maxIncrements).' days'));
                foreach ($reseller as $k=>$v){
                    $checkThisDate = ResellerHistory::whereDate('created_at',$dateToCheck)->where('reseller_id',$v->id)->orderBy('created_at','desc')->first();
                    if($checkThisDate){
                        $jumlahSaldoResellerDetailArr[$k]['saldoTerakhirD'] = $checkThisDate->balance_after;
                    } else {
                        if (isset($v->balance)){
                            $jumlahSaldoResellerDetailArr[$k]['saldoTerakhirD'] = $v->balance->balance;
                        } else {
                            $jumlahSaldoResellerDetailArr[$k]['saldoTerakhirD'] = 0;
                        }
                    }
                }
                $saldoTaggalIni =  collect($jumlahSaldoResellerDetailArr)->sum('saldoTerakhirD');
                $jumlahSaldoResellerD[$maxIncrements]['saldo'] = $saldoTaggalIni;
                $jumlahSaldoResellerD[$maxIncrements]['tanggal'] =  $dateToCheck;
            
                $maxArray[$maxIncrements] =  collect($jumlahSaldoResellerDetailArr)->sum('saldoTerakhirD');
                $maxIncrements--;   
            }
        // $max = max($maxArray);
        // dd($jumlahSaldoResellerD[5]['saldo']);   
        foreach($jumlahSaldoResellerD as $k=>$val){
            // dd($k);
            $indexKemarin = $k+1;
            if($k < 5 && $indexKemarin > 0 ){
             
                $saldoSekarang =   $jumlahSaldoResellerD[$k]['saldo'];
                $saldoKemarin = $jumlahSaldoResellerD[$indexKemarin]['saldo'];
                if($saldoKemarin > 0){
                    $percentageSaldoR =   round(($saldoSekarang-$saldoKemarin) / $saldoKemarin * 100,2);
                } else {
                    $percentageSaldoR = 0;
                }
                $jumlahSaldoResellerD[$k]['kemarin'] = $saldoKemarin;
                $jumlahSaldoResellerD[$k]['percentage'] = $percentageSaldoR;
                if($percentageSaldoR >= 0){
                    $jumlahSaldoResellerD[$k]['class'] =  'progress-bar-success';
                }else {
                    $jumlahSaldoResellerD[$k]['class'] =  'progress-bar-danger';
                }
            } else {
                $jumlahSaldoResellerD[$k]['percentage'] = 100;
                $jumlahSaldoResellerD[$k]['class'] =  'progress-bar-info';
                $jumlahSaldoResellerD[$k]['kemarin'] = 0;
            }
            $jumlahSaldoResellerD[$k]['saldoRp'] = formatCurrency($jumlahSaldoResellerD[$k]['saldo']);
            $jumlahSaldoResellerD[$k]['tanggal'] = $this->hariIndonesia(date('Y-m-d',strtotime($jumlahSaldoResellerD[$k]['tanggal']))).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($jumlahSaldoResellerD[$k]['tanggal'])));

            
        }
            // dd($jumlahSaldoResellerD);
        $jumlahSaldoResellerArr = [];
        foreach ($reseller as $k=>$v){
            $jumlahSaldoResellerArr[$k] = $v;
            $cekLastWeek = ResellerHistory::whereDate('created_at',">=",$lastWeekStart)
            ->whereDate('created_at',"<=",$lastWeekEnd)->where('reseller_id',$v->id)->orderBy('created_at','desc')->first();
            $cekThisWeek = ResellerHistory::whereDate('created_at',">=",$thisWeekStart)->where('reseller_id',$v->id)->orderBy('created_at','desc')->first();
            if($cekLastWeek){
                $jumlahSaldoResellerArr[$k]['saldoTerakhirLW'] = $cekLastWeek->balance_after;
            } else {
                if (isset($v->balance)){
                    $jumlahSaldoResellerArr[$k]['saldoTerakhirLW'] = $v->balance->balance;
                } else {
                    $jumlahSaldoResellerArr[$k]['saldoTerakhirLW'] = 0;
                }
            }
            if($cekThisWeek){
                $jumlahSaldoResellerArr[$k]['saldoTerakhir'] = $cekThisWeek->balance_after;
            } else {
                if (isset($v->balance)){
                    $jumlahSaldoResellerArr[$k]['saldoTerakhir'] = $v->balance->balance;
                } else {
                    $jumlahSaldoResellerArr[$k]['saldoTerakhir'] = 0;
                }
            }
        } 

        $jumlahSaldoResellerLastWeek = collect($jumlahSaldoResellerArr)->sum('saldoTerakhirLW');
        $jumlahSaldoResellerThisWeek = collect($jumlahSaldoResellerArr)->sum('saldoTerakhir');
        if( $jumlahSaldoResellerLastWeek > 0){
            $jumlahSaldoResellerPercentage =  ($jumlahSaldoResellerThisWeek -   $jumlahSaldoResellerLastWeek) / $jumlahSaldoResellerLastWeek * 100;
            if($jumlahSaldoResellerPercentage > 0){
                $jumlahSaldoResellerColor =  'custom-color-green';
                
            } else {
                $jumlahSaldoResellerColor =  'custom-color-orange';
            }
            $jumlahSaldoResellerText =  $jumlahSaldoResellerPercentage."% dibandingkan minggu lalu ";
        } else {
            $jumlahSaldoResellerColor =  'custom-color-green';
            $jumlahSaldoResellerText ="";
            $jumlahSaldoResellerPercentage =0;
        }
        $jumlahSaldoReseller = formatNumber($jumlahSaldoResellerThisWeek) ;
       
        if($request->has('startDate') && $request->startDate != ""){
            $tanggalAwal = $request->startDate;
        } else {
            $tanggalAwal = date('y-m-d H:i:s');
        }
        if($request->has('endDate') && $request->endDate != ""){
            $tanggalAkhir = $request->endDate;
        } else {
            $tanggalAkhir = date('y-m-d H:i:s');
        }
        $jumlahSaldoBiller2 = Deposit::where('status',3)->whereDate('date_created','>=',$tanggalAwal)->whereDate('date_created','<=',$tanggalAkhir)->get();
        if(count($jumlahSaldoBiller2) == 0){
            $jumlahSaldoBiller2 = 0;
        } else {
            $jumlahSaldoBiller2 = collect( $jumlahSaldoBiller2)->sum('amount');
        }
        $jumlahSaldoBiller2  = formatNumber($jumlahSaldoBiller2) ;

         $biller = Biller::where('is_deleted',0)->get();
         $jumlahSaldoBillerArr = [];
         foreach ($biller as $k=>$v){
             $jumlahSaldoBillerArr[$k] = $v;
             $cekLastWeek = BillerHistory::whereDate('created_at',$yesterday)->where('biller_id',$v->id)->orderBy('created_at','desc')->first();
             $cekThisWeek = BillerHistory::whereDate('created_at',$today)->where('biller_id',$v->id)->orderBy('created_at','desc')->first();
             if($cekLastWeek){
                 $jumlahSaldoBillerArr[$k]['saldoTerakhirLW'] = $cekLastWeek->balance_after;
             } else {
                 if (isset($v->balance)){
                     $jumlahSaldoBillerArr[$k]['saldoTerakhirLW'] = $v->balance;
                 } else {
                     $jumlahSaldoBillerArr[$k]['saldoTerakhirLW'] = 0;
                 }
             }
             if($cekThisWeek){
                 $jumlahSaldoBillerArr[$k]['saldoTerakhir'] = $cekThisWeek->balance_after;
             } else {
                 if (isset($v->balance)){
                     $jumlahSaldoBillerArr[$k]['saldoTerakhir'] = $v->balance;
                 } else {
                     $jumlahSaldoBillerArr[$k]['saldoTerakhir'] = 0;
                 }
             }
         } 
 
         $jumlahSaldoBillerLastWeek = collect($jumlahSaldoBillerArr)->sum('saldoTerakhirLW');
         $jumlahSaldoBillerThisWeek = collect($jumlahSaldoBillerArr)->sum('saldoTerakhir');
         if( $jumlahSaldoBillerLastWeek > 0){
             $jumlahSaldoBillerPercentage =  ($jumlahSaldoBillerThisWeek -   $jumlahSaldoBillerLastWeek) / $jumlahSaldoBillerLastWeek * 100;
             if($jumlahSaldoBillerPercentage > 0){
                 $jumlahSaldoBillerColor =  'custom-color-green';
                 
             } else {
                 $jumlahSaldoBillerColor =  'custom-color-orange';
             }
             $jumlahSaldoBillerText =  $jumlahSaldoBillerPercentage."% dibandingkan Kemarin";
         } else {
             $jumlahSaldoBillerColor =  'custom-color-green';
             $jumlahSaldoBillerText ="";
             $jumlahSaldoBillerPercentage=0;
         }
         $jumlahSaldoBiller = formatNumber($jumlahSaldoBillerThisWeek) ;


         $omzetToday = Transaction::where('is_deleted',0)->where('status','S')->whereDate('created_at',$today)->get();
         $omzetYesterday = Transaction::where('is_deleted',0)->where('status','S')->whereDate('created_at',$yesterday)->get();
         $JumlahOmzetToday = collect($omzetToday)->sum('price_reseller');
         $JumlahOmzetYesterday = collect($omzetYesterday)->sum('price_reseller');
         if( $JumlahOmzetYesterday > 0){
            $jumlahOmzetPercentage =  ($JumlahOmzetToday -   $JumlahOmzetYesterday) / $JumlahOmzetYesterday * 100;
            if($jumlahOmzetPercentage > 0){
                $jumlahOmzetColor =  'custom-color-green';
                
            } else {
                $jumlahOmzetColor =  'custom-color-orange';
            }
            $jumlahOmzetText =  round($jumlahOmzetPercentage,2)."% dibandingkan Kemarin";
        } else {
            $jumlahOmzetColor =  'custom-color-green';
            $jumlahOmzetText ="";
            $jumlahOmzetPercentage = 0;
        }
        $jumlahOmzet = formatNumber($JumlahOmzetToday) ;
        $typez = "yearly";
        $chartJs = $this->trChart($typez);
        $chartJsProduk = $this->productSoldChartByBiller('yearly');
        
        $chartJsSaldoBiller = $this->saldoBillerChart('yearly');
        return view('index')
        ->withJumlahSaldoResellerColor($jumlahSaldoResellerColor)
        ->withJumlahSaldoResellerText($jumlahSaldoResellerText)
        ->withJumlahSaldoReseller($jumlahSaldoReseller)
        ->withJumlahSaldoResellerP(abs($jumlahSaldoResellerPercentage))
        ->withJumlahSaldoBillerColor($jumlahSaldoBillerColor)
        ->withJumlahSaldoBillerText($jumlahSaldoBillerText)
        ->withJumlahSaldoBiller($jumlahSaldoBiller2)
        ->withJumlahSaldoBillerP(abs($jumlahSaldoBillerPercentage))
        ->withJumlahMemberColor($jumlahMemberColor)
        ->withJumlahMemberText($jumlahMemberText)
        ->withJumlahMember($jumlahMember)
        ->withJumlahOmzetColor($jumlahOmzetColor)
        ->withJumlahOmzetText($jumlahOmzetText)
        ->withJumlahOmzetP(abs($jumlahOmzetPercentage))
        ->withJumlahOmzet($jumlahOmzet)
        ->withJumlahSaldoResellerD($jumlahSaldoResellerD)
        ->withChartJs($chartJs)
        ->withChartJsSaldoBiller($chartJsSaldoBiller)
        ->withChartJsProductSold($chartJsProduk);
 
    }
    public function badge (Request $request)
    {
        if($request->has('startDate') && $request->startDate != ""){
            $tanggalAwal = $request->startDate;
        } else {
            $tanggalAwal = date('y-m-d H:i:s');
        }
        if($request->has('endDate') && $request->endDate != ""){
            $tanggalAkhir = $request->endDate;
        } else {
            $tanggalAkhir = date('y-m-d H:i:s');
        }
        $jumlahSaldoBiller2 = Deposit::where('status',3)->whereDate('date_created','>=',$tanggalAwal)->whereDate('date_created','<=',$tanggalAkhir)->get();
     
        if(count($jumlahSaldoBiller2) == 0){
            $jumlahSaldoBiller2 = 0;
        } else {
            $jumlahSaldoBiller2 = collect( $jumlahSaldoBiller2)->sum('amount');
        }
        $x  = formatCurrency($jumlahSaldoBiller2) ;

        return  $x;
    }

    public function trChart ($type)
    {
        $a=1;
        $i = date('m');
        $month=[];
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        while($a <= $i ){
            $month[$a]['num']=$a;
            $month[$a]['bul']=$bulan[$a];
            $bulans[] = $bulan[$a];
            $a++;
        }
        foreach($month as $k=>$v){
            $successQuery = Transaction::where('status',"S")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
            $success[]=collect($successQuery)->sum('price_reseller');
            $processQuery = Transaction::where('status',"Q")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
            $process[]=collect($processQuery)->sum('price_reseller');
            $failedQuery = Transaction::where('status',"F")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
            $failed[]=collect($failedQuery)->sum('price_reseller');
        }
    
        $chartjs = app()->chartjs
        ->name('RekapTransaksi')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($bulans)
        ->datasets([
            [
                "label" => "Sukses",
                'backgroundColor' => "rgba(0, 130, 30)",
                'pointStyle'=> 'rect',
                'pointRadius'=> 5,
                'fill'=> false,
                'borderColor' => "rgba(0, 130, 30)",
                "pointBorderColor" => "rgba(0, 130, 30)",
                "pointBackgroundColor" => "rgba(0, 130, 30)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $success,
                'lineTension'=> 0,
                'datalabels'=> 
                    [
                        'color'=> 'rgba(0, 130, 30)',
                        'anchor'=> 'end',
                        'align'=> 'top',
                        'formatter'=> 'Math.round',
                        'font'=> [
                            'weight'=> 'bold'
                            ]
                        ],

            ], 
            [
                "label" => "Proses",
                'backgroundColor' => "rgba(252, 236, 3)",
                'pointStyle'=> 'rect',
                'pointRadius'=> 5,
                'fill'=> false,
                'borderColor' => "rgba(252, 236, 3)",
                "pointBorderColor" => "rgba(252, 236, 3)",
                "pointBackgroundColor" => "rgba(252, 236, 3)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $process,
                'lineTension'=> 0,
                'datalabels'=> 
                    [
                        'color'=> 'rgba(252, 236, 3)',
                        'anchor'=> 'end',
                        'align'=> 'top',
                        'formatter'=> 'Math.round',
                        'font'=> [
                            'weight'=> 'bold'
                            ]
                        ],
            ],
            [
                "label" => "Gagal",
                'backgroundColor' => "rgba(252, 28, 3)",
                'pointStyle'=> 'rect',
                'pointRadius'=> 5,
                'fill'=> false,
                'borderColor' => "rgba(252, 28, 3)",
                "pointBorderColor" => "rgba(252, 28, 3)",
                "pointBackgroundColor" => "rgba(252, 28, 3)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $failed,
                'lineTension'=> 0,  
                'datalabels'=> 
                [
                    'color'=> 'rgba(252, 28, 3)',
                    'anchor'=> 'end',
                    'align'=> 'top',
                    'formatter'=> 'Math.round',
                    'font'=> [
                        'weight'=> 'bold'
                        ]
                    ],
            ]
        ])
       
        ->optionsRaw(
            [
            'legend' => [
                'position' => 'bottom',
                'labels' => [
                    'fontColor' => 'rgb(0, 0, 0)'
                ]
            ],
            'plugins'=> [
                'datalabels'=> 
                    [
                        'color'=> 'rgb(0, 0, 0)',
                        'anchor'=> 'end',
                        'align'=> 'top',
                        'formatter'=> 'Math.round',
                        'font'=> [
                            'weight'=> 'bold'
                            ]
                    ]
            ],
            'scales'=> [
                'xAxes'=> [[
                    'gridLines'=> [
                        'color'=> "rgba(0, 0, 0, 0)",
                    ]
                ]],
                'yAxes'=> [[
                    'gridLines'=> [
                        'color'=> "rgba(0, 0, 0, 0)",
                    ]   
                ]]
            ]

            ]);
            
   

        return  $chartjs;
    }
    public function transaksiChart (Request $request)
    {
        // return $request->all();
        if($request->bulanan == "false" ){
            $a=1;
            $i = date('m');
            $month=[];
            $bulan = array (
                1 =>   'Jan',
                'Feb',
                'Mar',
                'Apr',
                'Mei',
                'Jun',
                'Jul',
                'Ags',
                'Sep',
                'Okt',
                'Nov',
                'Des'
            );
            while($a <= $i ){
                $month[$a]['num']=$a;
                $month[$a]['bul']=$bulan[$a];
                $bulans[] = $bulan[$a];
                $a++;
            }
            foreach($month as $k=>$v){
                $successQuery = Transaction::where('status',"S")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
                $success[]=collect($successQuery)->sum('price_reseller');
                $processQuery = Transaction::where('status',"Q")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
                $process[]=collect($processQuery)->sum('price_reseller');
                $failedQuery = Transaction::where('status',"F")->whereYear('created_at',date('Y'))->whereMonth('created_at',$v['num'])->where('is_deleted',0)->get();
                $failed[]=collect($failedQuery)->sum('price_reseller');
            }
            $data['label']=$bulans;
            array_unshift($success , 'data1');
            array_unshift($process , 'data2');
            array_unshift($failed , 'data3');
            $data['data']=[$success,$process,$failed];
            return  $data;
        } else {
            // $startDate = date('Y-m-').'01';
            // $endDate = date('Y-m-d');
            // $period = new CarbonPeriod($startDate,  $endDate);
            $startDate = Carbon::now()->startOfMonth();
            $endDate = Carbon::now()->endOfMonth();
            $period = new CarbonPeriod($startDate,  $endDate);
            $period = collect($period)->toArray();
            foreach($period as $k=>$v){
                $label[$k]=date('d',strtotime($v));
                $dateFilter[$k]=date('Y-m-d',strtotime($v));
            }
            
            foreach($dateFilter as $k=>$v){
                $successQuery = Transaction::where('status',"S")->whereDate('created_at',$v)->where('is_deleted',0)->get();
                $success[]=collect($successQuery)->sum('price_reseller');
                $processQuery = Transaction::where('status',"Q")->whereDate('created_at',$v)->where('is_deleted',0)->get();
                $process[]=collect($processQuery)->sum('price_reseller');
                $failedQuery = Transaction::where('status',"F")->whereDate('created_at',$v)->where('is_deleted',0)->get();
                $failed[]=collect($failedQuery)->sum('price_reseller');
            }
            $data['label']=$label;
            array_unshift($success , 'data1');
            array_unshift($process , 'data2');
            array_unshift($failed , 'data3');
            $data['data']=[$success,$process,$failed];
            return  $data;

        }
       
    }
    public function productSoldChartByBiller ($type)
    {
        $randomColors=[
            'rgb(48,56,68)','rgb(78,156,227)','rgb(240,174,25)','rgb(124,252,162)'
        ];
        $biller = Biller::where('is_deleted',0)->get();
        $label = collect($biller)->pluck('name')->unique()->toArray();
        //jenis
        $group = Transaction::select('command')->groupBy('command')->distinct()->get();
        $group = collect($group)->pluck('command')->toArray();
        foreach($label as $k=>$v){
            foreach($group as $aa=>$g){
                $trans = Transaction::where('status',"S")->where('biller',$v)->where('is_deleted',0)->where('command',$g)->get(); 
                // if (collect($trans)->sum('price_reseller') != 0){
                    $transaction['data'][$k][$aa] = collect($trans)->sum('price_reseller'); 
                    // }
                }
                $transaction['biller'][] = $v;
        }
        $lab = [];
        $dataChart = [];
        $dataColor = [];
        // foreach($transaction['data'] as $kr=>$tr){
        //     if ($tr != 0){
        //         $lab[]=$transaction['grup'][$kr];
        //         $dataChart[]=$tr;
        //         $dataColor[]=$this->RandomColor(1);
        //     }
        // }
        $x=[];
        $result['legend'] = $label;
        $result['series'] = $transaction['data'];
        $result['label'] = $group;
        return $result;
  

        foreach($lab as $kk=>$gg){
                $x[] = [
                    "label" => $gg,
                    'backgroundColor' => $dataColor[$kk],
                    'borderColor' => $dataColor[$kk],
                    "pointBorderColor" => $dataColor[$kk],
                    "pointBackgroundColor" => $dataColor[$kk],
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => (array)$dataChart[$kk],
                ];
        }
        return $transaction;
        $chartjs = app()->chartjs
        ->name('ProductSold')
        ->type('bar')
        ->size(['width' => 400, 'height' => 450])
        ->labels($label)
        ->datasets($x)
        ->options([
            'responsive' => true,
            'scales' => 
            [
                'x' => [
                    [
                        'stacked' => true,
                     
                    ]
                ],
                'y' => [
                    [
                        'stacked' => true,
                     
                    ]
                ],
                'yAxes'=> [[
                    'gridLines'=> [
                        'color'=> "rgb(219, 214, 200)",
                            'borderDash'=> [2],
                    ]   
                ]],
                'xAxes'=> [[
                    'gridLines'=> [
                        'color'=> "rgba(0, 0, 0, 0)",
                    ]
                ]],
            ]
        ]);

      
        // $chartjs = app()->chartjs
        //  ->name('barChartTest')
        //  ->type('bar')
        //  ->size(['width' => 400, 'height' => 200])
        //  ->labels(['Label x', 'Label y'])
        //  ->datasets([
        //      [
        //          "label" => "My First dataset",
        //          'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
        //          'data' => [69]
        //      ],
        //      [
        //          "label" => "My First dataset",
        //          'backgroundColor' => ['rgba(255, 99, 132, 0.3)', 'rgba(54, 162, 235, 0.3)'],
        //          'data' => [65, 12]
        //      ]
        //  ])
        //  ->options([]);

        return  $chartjs;
    }
    public function productSoldChart ()
    {
        $randomColors=[
            'rgb(48,56,68)','rgb(78,156,227)','rgb(240,174,25)','rgb(124,252,162)'
        ];
        $biller = Biller::where('is_deleted',0)->get();
        $label = collect($biller)->pluck('name')->unique()->toArray();
        //jenis
        $group = Transaction::select('command')->groupBy('command')->distinct()->get();
        $group = collect($group)->pluck('command')->toArray();
        foreach($label as $k=>$v){
            foreach($group as $aa=>$g){
                $trans = Transaction::where('status',"S")->where('biller',$v)->where('is_deleted',0)->where('command',$g)->get(); 
                // if (collect($trans)->sum('price_reseller') != 0){
                    $transaction['data'][$k][$aa] = collect($trans)->count('price_reseller'); 
                    // }
                }
                $transaction['biller'][] = $v;
        }
        $lab = [];
        $dataChart = [];
        $dataColor = [];
        // foreach($transaction['data'] as $kr=>$tr){
        //     if ($tr != 0){
        //         $lab[]=$transaction['grup'][$kr];
        //         $dataChart[]=$tr;
        //         $dataColor[]=$this->RandomColor(1);
        //     }
        // }
        $x=[];
        $result['legend'] = $group;
        $result['series'] = $transaction['data'];
        $result['label'] = $label;
        return $result;
    }


    public function saldoBillerChart ($type)
    {
        $randomColors=[
            'rgb(91,190,116)','rgb(24,41,116)','rgb(245,205,83)'
        ];
        $biller = Biller::where('is_deleted',0)->get();
        $label = collect($biller)->pluck('name')->toArray();
        $data = collect($biller)->pluck('balance')->toArray();
        $Totaldata = array_sum($data); 
        //jenis
        $dataz=[];
        foreach($data as $k=>$d){
            $color[] = isset($randomColors[$k]) ? $randomColors[$k] : $this->RandomColor(1) ;
            $dataz[]=round($d/$Totaldata * 100,2) ;
        }
        
        $chartjs = app()->chartjs
        ->name('SaldoBiller')
        ->type('pie')
        ->size(['width' => 300, 'height' => 150])
        ->labels($label)
        ->datasets([
            [
                'backgroundColor' => $color,
                'hoverBackgroundColor' => $color,
                'data' => $dataz,
                'datalabels'=> 
                [
                    'color'=> 'rgb(255,255,255)',
                    'formatter'=> 'Math.round',
                    'font'=> [
                        'weight'=> 'bold',
                        'size'=> 12
                        ]
                    ],

            ]
        ])
        ->options([
            'scales' => [
                'x' => [
                    [
                        'stacked' => true,
                     
                    ]
                ],
                'y' => [
                    [
                        'stacked' => true,
                     
                    ]
                ]
            ],
            'legend'=> [
                'position'=> 'right',
            ]
           
        ]);
      
        return  $chartjs;
    }
    public function saldoBillerChart2 ()
    {
        $randomColors=[
            'rgb(91,190,116)','rgb(24,41,116)','rgb(245,205,83)'
        ];
        $biller = Biller::where('is_deleted',0)->orderBy('balance','asc')->get();
        $label = collect($biller)->pluck('name')->unique()->toArray();
        $data = collect($biller)->pluck('balance')->toArray();
        $Totaldata = array_sum($data); 
        //jenis
        $dataz=[];
        // foreach($data as $k=>$d){
        //     $color[] = isset($randomColors[$k]) ? $randomColors[$k] : $this->RandomColor(1) ;
        //     $dataz[]=round($d/$Totaldata * 100,2) ;
        // }
        foreach($biller as $k=>$d){
            $result['color'][$k] = $this->RandomColor2() ;
            $dataz[$k]=$d['balance'] ;
            $result['biller'][$k]=$d['name'];
            $helper[$k]['biller'] =$d['name'];
            $helper[$k]['balance'] =   formatCurrency($d['balance']);
            $helper[$k]['color'] = 'color:'.$result['color'][$k];
        }
        $result['data']=implode(',',$dataz);
        $result['helper']=$helper;

        // $result['data']=implode(',',$dataz);
        // $result['biller']=$label;
        // $result['color']=$color;
        return $result;
     
    }
    public function platformChart(){
        $all = Reseller::where('is_deleted',0)->count();
        $web = Reseller::where('is_deleted',0)->where('platform',"web")->count();
        $android = Reseller::where('is_deleted',0)->where('platform',"android")->count();
        $ios = Reseller::where('is_deleted',0)->where('platform',"ios")->count();

        $web = abs(round(($web ) / $all * 100,2));
        $android = abs(round(($android ) / $all * 100,2));
        $ios = abs(round(($ios ) / $all * 100,2));



        return response()->json([
            'status' => 200,
            'message' => "Saldo Biller berhasil diupdate",
            'data' => [
                'web' =>$web,
                'ios' =>$ios,
                'android' =>$android,
            ]
        ]);


    }
    public function RandomColor($a){
                
        $rgbColor = array();
        //Create a loop.
        foreach(array('r', 'g', 'b') as $color){
            //Generate a random number between 0 and 255.
            $rgbColor[$color] = mt_rand($a, 255);
        }
        return "rgba(".$rgbColor['r'].','.$rgbColor['g'].','.$rgbColor['b'].')';
    }
    public function RandomColor2(){
                
        $rand = str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT);
        return('#' . $rand);
    }

    public function bulanIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    function hariIndonesia($date){
        $hari = date ("D",strtotime($date));
     
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
     
            case 'Mon':			
                $hari_ini = "Senin";
            break;
     
            case 'Tue':
                $hari_ini = "Selasa";
            break;
     
            case 'Wed':
                $hari_ini = "Rabu";
            break;
     
            case 'Thu':
                $hari_ini = "Kamis";
            break;
     
            case 'Fri':
                $hari_ini = "Jumat";
            break;
     
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
     
        return $hari_ini;
     
    }
     

}