<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\Reseller;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use App\Models\PaymentMethod;
use App\Models\User;
use App\Models\ResellerHistory;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Deposit::with(['reseller','method'])->orderBy("date_created",'desc');
        if($request->has('limit') && $request->limit != ""){
            $query = $query->limit($request->limit);
        }
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {

            $status = '
            <select class="form-control select request-status" style="width:200px;">
                <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Requested</option>
                <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Confirmed</option>
                <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Canceled</option>
                <option value="3" '.(($q->status == 3) ? 'selected="selected"' : '').'>Approved</option>
                <option value="4" '.(($q->status == 4) ? 'selected="selected"' : '').'>Rejected</option>
            </select>
            ';
            return $status;

        })
        ->addColumn('action2', function ($q)  {

            $status = '
            <select class="form-control select request-status" style="width:120px;">
                <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Requested</option>
                <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Confirmed</option>
                <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Canceled</option>
                <option value="3" '.(($q->status == 3) ? 'selected="selected"' : '').'>Approved</option>
                <option value="4" '.(($q->status == 4) ? 'selected="selected"' : '').'>Rejected</option>
            </select>
            ';
            return $status;

        })
        ->addColumn('action3', function ($q)  {
            if($q->status == 0){
                $status = '
                <select class="form-control form-control-sm select request-status" style="width:120px;">
                    <option  value="0" '.(($q->status == 0) ? 'selected="selected"' : '').'>Requested</option>
                    <option value="1" '.(($q->status == 1) ? 'selected="selected"' : '').'>Confirmed</option>
                    <option value="2" '.(($q->status == 2) ? 'selected="selected"' : '').'>Canceled</option>
                    <option value="3" '.(($q->status == 3) ? 'selected="selected"' : '').'>Approved</option>
                    <option value="4" '.(($q->status == 4) ? 'selected="selected"' : '').'>Rejected</option>
                </select>
                ';
                return $status;
            } else if($q->status == 1){
                return '<span class="badge badge-success">Confirmed</span>';
            } else if($q->status == 2){
                return '<span class="badge badge-danger">Canceled</span>';
            } else if($q->status == 3){
                return '<span class="badge badge-default">Approved</span>';
             }else if($q->status == 4){
                return '<span class="badge badge-danger">Rejected</span>';
            }else{
                return '<span class="badge badge-default">expired</span>';
            }

            

        })

        ->addColumn('actionBaru', function ($q)  {
            if($q->status == "0"){
                $text ='
                <div class="d-flex justify-content-center">
                <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editDepositBaru('.$q->no.')"><i class="fa fa-edit "></i></a>
                </div>';
            }  else {
                $text ='
                <div class="d-flex disabled  justify-content-center">
                <a disabled style="width:32px;!important" class="btn btn-sm btn-default" href="javascript:void(0)" onclick=""><i disabled class="fa fa-edit "></i></a>
                </div>';

            }
            return $text;

        })
        // ->addColumn('action', function ($q)  {
      
        //         // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->no.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

        //         return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->no.')"><i class="fa fa-edit"></i></a> ';
        // })
     
        ->addColumn('image', function ($q)  {
            return '<img src="'.$q->image_uploaded.'" class=" avatar" ">&nbsp';
        })
        ->addColumn('nama', function ($q)  {
              
            return strtoupper($q->reseller->name);
            })
        ->addColumn('amount', function ($q)  {
            return formatCurrency($q->amount);
        })
        ->addColumn('jumlah', function ($q)  {
            return formatCurrency($q->amount-$q->unique_code);
        })
        ->addColumn('payment', function ($q)  {
            if(isset($q->method)){
                return $q->method->name . " - " .$q->method->account;
            } else {
                return "";
            }
        })
        ->addColumn('payment2', function ($q)  {
            if(isset($q->method)){
                return $q->method->name . " <br>" .$q->method->account;
            } else {
                return "";
            }
        })
        ->addColumn('jamTanggal', function ($q)  {
                return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->date_created))).' '.date('H:i:s',strtotime($q->date_created));
        })
        ->addColumn('jamTanggal2', function ($q)  {
                return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->date_created))).'<br>'.date('H:i:s',strtotime($q->date_created));
        })
        ->addColumn('jamTanggalu', function ($q)  {
            if($q->date_updated != null){
                return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->date_updated))).' '.date('H:i:s',strtotime($q->date_updated));
            } else {
                return $this->tanggalIndonesia(date("Y-m-d",strtotime($q->date_created))).' '.date('H:i:s',strtotime($q->date_created));
            }
        })
        ->addColumn('status', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-default">Requested</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">confirmed</span>';
            } else if($q->status == "2" ) {
                return '<span class="badge badge-danger">canceled</span>';
            } else if($q->status == "3" ) {
                return '<span class="badge badge-success">approved</span>';
            } else {
                return '<span class="badge badge-danger">rejected</span>';
            }
        })
        ->addColumn('status2', function ($q)  {
          return $q->status;
        })
    

     
        ->rawColumns( ['image','action','action3','actionBaru','phone','email','status','jamTanggal2','payment2'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        return view('deposit');

    }
 

    public function edit (Request $request,$id)
    {
        $data = Deposit::where('no',$id)->with('reseller')->first();
        // dd($data);
        return view('deposit_edit')->withData($data);
    }
    public function update (Request $request,$id)
    {


            $data =  Deposit::where('no',$id)->first();
            $params = $request->except('_token');
            if($data){

                $update = $data->update([
                    'status'=>$request->status
                ]);
            
                if($update){
            
                        return response()->json([
                            'status' => 200,
                            'message' => "Deposit berhasil diupdate",
                            'data'=>$request->all()
                        ]);
                
            
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (Deposit)',
                    ]);
                }

            }
            return response()->json([
                'status' => 500,
                'message' => 'error (Deposit)',
            ]);
    }
    public function updateStatus (Request $request,$id)
    {


            $data =  Deposit::where('no',$id)->first();
            $params = $request->except('_token');
            if($data){

                $update = $data->update([
                    'status'=>$request->status
                ]);
            
                if($update){
            
                        return response()->json([
                            'status' => 200,
                            'message' => "Deposit berhasil diupdate",
                            'data'=>$request->all()
                        ]);
                
            
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (Deposit)',
                    ]);
                }

            }
            return response()->json([
                'status' => 500,
                'message' => 'error (Deposit)',
            ]);
    }

    public function depositManual (Request $request)
    {
        $group = Reseller::where('status','!=','3')->get();
        $payment = PaymentMethod::where('status','1')->get();
        return view('deposit_manual')
        ->withPayment($payment)
        ->withGroup($group);

    }
    public function depositManualStore (Request $request)
    {
        // dd($request->all());
        $cekPin = User::where('id',$request->session()->get('SESS_USERDATA')['id'])->first()->pin;
        if($cekPin != $request->pin){
            return response()->json([
                'status' => 400,
                'message' => "Pin Salah",
                'data'=>$request->all()
            ]);
        }

        // - insert ke tabel deposit dengan status 3 (approve)
        // - insert ke tabel reseller_history --> filed reference_id di isi deposit_id, description di isi "Topup saldo lewat admin", created_by di isi nama si admin
        // - update ke tabel reseller_balance
        $depositid = 'TD'.$this->generateNumeric(2).'-'.$this->generateChar(8);
        $newD = new Deposit();
        $newD->deposit_id = $depositid;
        $newD->reseller_id = $request->name;
        $newD->nominal = $request->nominal;
        $newD->unique_code =0;
        $newD->status =3;
        $newD->amount =$request->nominal;
        $newD->nominal = $request->nominal;
        $newD->bank_code = $request->payment_method;
        $newD->date_updated = date('Y-m-d H:i:s');
        $newD->date_created = date('Y-m-d H:i:s');
        $newD->date_expired = date('Y-m-d H:i:s');
        $save = $newD->save();
        if($save){
            $bankCode = PaymentMethod::where('id',$newD->bank_code)->first()->name;
            $reseller = Reseller::where('id',$request->name)->with('balance')->first();
            $newBalanceR =$reseller->balance->balance + $newD->nominal ;
            $newH = new ResellerHistory();
            $newH->reseller_id = $reseller->id;
            $newH->amount = $newD->amount;
            $newH->balance_before = $reseller->balance->balance;
            $newH->type = 1;
            $newH->balance_after = $newBalanceR; 
            $newH->reference_id = $depositid; 
            $newH->description = "Topup saldo lewat Admin (".$bankCode.' - '. $request->session()->get('SESS_USERDATA')['nama'].')';
            $newH->created_at =  date('Y-m-d H:i:s');
            $newH->created_by =  $request->session()->get('SESS_USERDATA')['nama'];
            $save2 = $newH->save();
            if($save2){
                $updateRB = ResellerBalance::where('reseller_id',$request->name)->update([
                    'balance'=> $newBalanceR,
                    'updated_by' => $request->session()->get('SESS_USERDATA')['nama'],
                    'updated_at' =>  date('Y-m-d H:i:s')
                ]);
                if($updateRB){
                    $amOunt=formatCurrency((float)$request->nominal);
                  


                    $params = (Object)[];
                    $params->regid = $reseller->attribute->regid;
                    // dd($request->nominal);
                
                    $params->params = (Object)[];
                    $params->params->body = 'Topup saldo kamu lewat '.$bankCode.' sebesar '.$amOunt.' sudah berhasil.';
                    $params->params->title = "Yaaayy! Saldo kamu sudah bertambah !!";
                    $params->params->intent = "Deposit";
                    $params->params->timestamp = date('Y-m-d H:i:s');
                    $params->params->image = "";

                    // $params['regid'] = $reseller->attribute->regid;
                    // // dd($request->nominal);
                
                    // $params['params']['body'] = 'Topup saldo kamu lewat '.$bankCode.' sebesar '.$amOunt.' sudah berhasil.';
                    // $params['params']['title'] = "Yaaayy! Saldo kamu sudah bertambah !!";
                    // $params['params']['intent'] = "Deposit";
                    // $params['params']['timestamp'] = date('Y-m-d H:i:s');
                    // $params['params']['image'] = "";
                    $this->notifService($params);
                    return response()->json([
                        'status' => 200,
                        'message' => "Deposit Berhasil Ditambahkan",
                        'data'=>''
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 500,
            'message' => "Server Error",
            'data'=>$request->all()
        ]);

    }

    public function depositEdit (Request $request,$id)
    {
        $query = Deposit::with(['reseller','method'])->where('no',$id)->orderBy("date_created",'desc')->get();
        $payment = PaymentMethod::where('status','1')->get();
        return view('deposit_edit2')
        ->withPayment($payment)
        ->withData($query[0]);

    }
    public function depositUpdate (Request $request,$id)
    {
        $query = Deposit::where('no',$id)->update([
            'bank_code' => $request->status,
            'amount' => $request->amount,
            'status' => $request->statusRequest,
            'unique_code' => $request->unique_code,
            'date_updated' => date('Y-m-d H:i:s')
        ]);
        $data = Deposit::where('no',$id)->first();
        if($request->statusRequest ==3){
            if($query){
                $today = date('Y-m-d H:i:s');
                $bankCode = PaymentMethod::where('id',$data->bank_code)->first()->name;
                $reseller = Reseller::where('reseller_code',$request->reseller_code)->with(['balance','attribute'])->first();
                $newBalanceR =$reseller->balance->balance + $data->amount;
                $newH = new ResellerHistory();
                $newH->reseller_id = $reseller->id;
                $newH->amount = $data->amount;
                $newH->balance_before = $reseller->balance->balance;
                $newH->type = 1;
                $newH->balance_after = $newBalanceR; 
                $newH->reference_id = $data->deposit_id; 
                $newH->description = "Topup saldo lewat Admin (".$bankCode.' - '. $request->session()->get('SESS_USERDATA')['nama'].')';
                $newH->created_at =  $today;
                $newH->created_by =  $request->session()->get('SESS_USERDATA')['nama'];
                $save2 = $newH->save();
                if($save2){
                    $updateRB = ResellerBalance::where('reseller_id',$reseller->id)->update([
                        'balance'=> $newBalanceR,
                        'updated_by' => $request->session()->get('SESS_USERDATA')['nama'],
                        'updated_at' =>  $today
                    ]);
                    if($updateRB){
                        $amOunt=formatCurrency((float)$data->amount);
                        $bankCode = PaymentMethod::where('id',$data->bank_code)->first()->name;
    
    
                        $params = (Object)[];
                        $params->regid = $reseller->attribute->regid;
                        // dd($request->nominal);
                    
                        $params->params = (Object)[];
                        $params->params->body = 'Topup saldo kamu lewat '.$bankCode.' sebesar '.$amOunt.' sudah berhasil.';
                        $params->params->title = "Yaaayy! Saldo kamu sudah bertambah !!";
                        $params->params->intent = "Deposit";
                        $params->params->timestamp =   $today;
                        $params->params->image = "";
    
                        // $params['regid'] = $reseller->attribute->regid;
                        // // dd($request->nominal);
                    
                        // $params['params']['body'] = 'Topup saldo kamu lewat '.$bankCode.' sebesar '.$amOunt.' sudah berhasil.';
                        // $params['params']['title'] = "Yaaayy! Saldo kamu sudah bertambah !!";
                        // $params['params']['intent'] = "Deposit";
                        // $params['params']['timestamp'] = date('Y-m-d H:i:s');
                        // $params['params']['image'] = "";
                        $this->notifService($params);
                        return response()->json([
                            'status' => 200,
                            'message' =>"Deposit Berhasil diapprove",
                            'data'=>''
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'status' => 200,
            'message' => "Deposit Berhasil direject",
            'data'=>''
        ]);
    }
}