<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Biller;
use App\Models\BillerHistory;
use App\Models\BillerParsing;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class MutasiSaldoBillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData (Request $request)
    {



      




        $query = BillerHistory::with(['biller','parsing']);
        if($request->has('startDate') && $request->startDate != ""){

            $query = $query->whereDate('created_at',">=",$request->startDate);
        }
        if($request->has('endDate') && $request->endDate != ""){

            $query = $query->whereDate('created_at',"<=",$request->endDate);
        }


    
       
        $tanggaldipilih = $this->tanggalIndonesia(date('Y-m-d',strtotime($request->startDate))) . " - ".$this->tanggalIndonesia(date('Y-m-d',strtotime($request->endDate)));
        $query = $query->get();
        $saldoTekumpul = formatCurrency(collect($query)->sum('amount'));
        $saldoDebit = formatCurrency(collect($query)->sum('debit'));
        $saldoKredit = formatCurrency(collect($query)->sum('kredit'));

        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('jamTanggal', function ($q)  {
            return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at))) ." ".date('H:i:s',strtotime($q->created_at));
        })
        ->addColumn('jumlah', function ($q)  {
                return formatCurrency($q->amount);
        })
        ->addColumn('debit', function ($q)  {
             
            return formatCurrency($q->debit);
            
        })
        ->addColumn('kredit', function ($q)  {
            return formatCurrency($q->kredit);
        })
      
        ->addColumn('request', function ($q)  {
            if ($q->parsing != null){
                return ($q->request);
            } else {
                return '';
            }
        })
        ->addColumn('response', function ($q)  {
            if ($q->parsing != null){
                return ($q->request);
            } else {
                return '';
            }
        })
      
        ->addColumn('balance_after', function ($q)  {
            return formatCurrency($q->balance_after);
        })

        ->with('tanggalTerpilih',$tanggaldipilih)
        ->with('saldoTekumpul',$saldoTekumpul)
        ->with('saldoDebit',$saldoDebit)
        ->with('saldoKredit',$saldoKredit)
        ->rawColumns( ['action','status','SN'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {

        return view('mutasi_saldo_biller')->withCurDate($this->tanggalIndonesia(date('Y-m-d')));
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 

}