<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Reseller;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\Cronjob;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnDataTransaksi(Request $request)
    {



        $jumlahTransaksi = Transaction::where("is_deleted", 0)->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->count() . " Trx";
        $jumlahTransaksiSukses = Transaction::where("is_deleted", 0)->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->where("status", "s")->count() . " Trx";
        $jumlahTransaksiGagal = Transaction::where("is_deleted", 0)->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->where("status", "f")->count() . " Trx";
        $jumlahTransaksiAntrian = Transaction::where("is_deleted", 0)->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->where("status", "q")->count() . " Trx";
        $jumlahTransaksiProses = Transaction::where("is_deleted", 0)->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->where("status", "p")->count() . " Trx";
        $omzet = Transaction::where("is_deleted", 0)->where("status", "s")->whereDate('created_at', ">=", $request->startDate)->whereDate('created_at', "<=", $request->endDate)->get();
        $omzet = formatCurrency(collect($omzet)->sum('price_reseller'));
        $saldoH2h = Biller::where('is_deleted', 0)->get();
        $c = "";
        foreach ($saldoH2h as $k => $v) {
            $c .= '<tr>
            <td>Total Saldo ' . $v["name"] . '</td>
            <td style="padding: 0 10px;">:</td>
            <td style="" class="">' . formatCurrency($v["balance"]) . '</td>
            </tr>';
        }

        $saldoH2h = formatCurrency(collect($saldoH2h)->sum('balance'));
        $saldoAgen = ResellerBalance::where('is_deleted', 0)->get();
        $saldoAgen = formatCurrency(collect($saldoAgen)->sum('balance'));

        //START
        $query = Transaction::select('*', 'transaction.id as Aid')->with(['history', 'bill', 'reseller.balance', 'reseller.attribute', 'produk'])->withCount('commands')->where("transaction.is_deleted", 0);
        if ($request->has('startDate') && $request->startDate != "") {

            $query = $query->whereDate('transaction.created_at', ">=", $request->startDate);
        }
        if ($request->has('endDate') && $request->endDate != "") {

            $query = $query->whereDate('transaction.created_at', "<=", $request->endDate);
        }
        if ($request->has('filterBiller') && $request->filterBiller != "") {
            $query = $query->where('transaction.biller', "like", '%' . $request->filterBiller . '%');
        }
        if ($request->has('filterProduk') && $request->filterProduk != "") {
            $query = $query->where('transaction.sku_biller', "like", '%' . $request->filterProduk . '%');
        }
        if ($request->has('filterTujuan') && $request->filterTujuan != "") {
            $query = $query->where('transaction.custid', "like", '%' . $request->filterTujuan . '%');
        }
        if ($request->has('filterStatus') && $request->filterStatus != "") {
            if ($request->has('filterStatus') && $request->filterStatus != "qweqweqwe") {
                $query = $query->where('transaction.status', $request->filterStatus);
            } else {
                $query = $query->whereNotIn('transaction.status', ['S', 'F']);
            }
        }


        $tanggaldipilih = $this->tanggalIndonesia(date('Y-m-d', strtotime($request->startDate))) . " - " . $this->tanggalIndonesia(date('Y-m-d', strtotime($request->endDate)));


        if ($request->has('sortType') && $request->sortType != '') {
            switch ($request->sortType) {
                case 0:
                    $query = $query->orderBy('commands_count', 'desc');
                    break;
                case 1:
                    $query = $query->join('products', 'products.sku_code', '=', 'transaction.sku_code')->orderBy('products.created_at', 'desc');
                    break;
                case 2:
                    $query = $query->join('products', 'products.sku_code', '=', 'transaction.sku_code')->orderBy('products.description', 'asc');
                    break;
                case 3:
                    $query = $query->join('products', 'products.sku_code', '=', 'transaction.sku_code')->orderBy('products.description', 'desc');
                    break;
            }
        } else {
            $query = $query->orderBy('transaction.created_at', 'desc');
        }

        $query = $query->get();
        $c = $c . '
        <tr>
            <td>Total Saldo H2H</td>
            <td style="padding: 0 10px;">:</td>
            <td style="font-weight:bold;" class="saldoH2h"><b>' . $saldoH2h . '</b></td>
        </tr>
        <tr>
            <td>Total Saldo Agen</td>
            <td style="padding: 0 10px;">:</td>
            <td  style="font-weight:bold;" class="saldoAgen"><b>' . $saldoAgen . '</b></td>
        </tr>
        ';
        return DataTables::of($query)
            ->addIndexColumn()

            ->addColumn('group', function ($q) {
                return $q->command;
            })
            ->addColumn('statusOrigin', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = 'ANTRIAN';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return 'SUKSES';
                } else if (strtoupper($q->status) == "F") {
                    return 'GAGAL';
                } else if (strtoupper($q->status) == "P") {
                    return 'PROSES';
                }
            })
            ->addColumn('jenis', function ($q) {
                return $q->group_name;
            })
            ->addColumn('tanggal_masuk', function ($q) {

                return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->created_at))) . '<br> ' . date('H:i:s', strtotime($q->created_at));
            })
            ->addColumn('tanggalUpdate', function ($q) {
                if ($q->updated_at != null) {
                    return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->updated_at))) . '<br> ' . date('H:i:s', strtotime($q->updated_at));
                } else if (strtoupper($q->status) == "F") {
                    return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->created_at))) . '<br> ' . date('H:i:s', strtotime($q->created_at));
                } else {
                    return 'Sedang diproses';
                }
            })
            ->addColumn('laba', function ($q) {
                $laba = $q->price_reseller - $q->price_biller;
                return formatCurrency((int)$laba);
            })
            ->addColumn('SN', function ($q) {
                if ($q->respond != null) {
                    if ($q->status == "S") {
                        $result = preg_match('/SN:(.*)/', $q->respond, $matches);
                        if (!empty($matches)) {
                            $res = explode('/', $matches[1]);
                            $ress = explode('.', $res[0]);
                            // return substr_replace($ress[0], '<br>', 15, 0);
                            return substr_replace(substr_replace(substr_replace($matches[0], '<br>', 60, 0), '<br>', 120, 0), '<br>', 180, 0);
                        } else {
                            $string = $q->respond;

                            $json = json_decode($string, true);
                            return substr_replace($json['data']['sn'], '<br>', 15, 0);
                        }
                    }
                    return '<p class="text-center"></p>';
                } else {
                    return '<p class="text-center"></p>';
                }
            })
            ->addColumn('idAgent', function ($q) {
                return "";
            })
            ->addColumn('resellername', function ($q) {
                return strtoupper($q->reseller->name ?? "");
            })
            ->addColumn('saldoBiller', function ($q) {
                $res = "";
                if ($q->history != null) {
                    $res = $q->history->balance_after;
                }
                return $res;
            })
            ->addColumn('status', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = '<div class="d-flex justify-content-center">
                    <span class="badge badge-warning">ANTRIAN</span>   </div>
                  ';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return '<div class="d-flex justify-content-center">
                <span class="badge badge-success">SUKSES</span>   </div>
               ';
                } else if (strtoupper($q->status) == "F") {
                    return '
                <div class="d-flex justify-content-center">
                <span class="badge badge-danger">GAGAL</span>   </div>
                  ';
                } else if (strtoupper($q->status) == "P") {
                    return '<div class="d-flex justify-content-center">
                <span class=" badge badge-info">PROSES</span>   </div>
                  ';
                }
            })
            ->addColumn('statusText', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = '<div class="d-flex justify-content-center">
                    <span class="badge badge-warning">ANTRIAN</span>   </div>
                  ';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return '<div class="d-flex justify-content-center">
                <span class="badge badge-success">SUKSES</span>   </div>
               ';
                } else if (strtoupper($q->status) == "F") {
                    return '
                <div class="d-flex justify-content-center">
                <span class="badge badge-danger">GAGAL</span>   </div>
                  ';
                } else if (strtoupper($q->status) == "P") {
                    return '<div class="d-flex justify-content-center">
                <span class=" badge badge-info">PROSES</span>   </div>
                  ';
                }
            })
            // ->addColumn('status', function ($q)  {
            //     if($q->status == "Q"){
            //             $text ='<div class="d-flex justify-content-center">
            //             <span class="badge badge-default">ANTRIAN</span>   </div>
            //             <div class="d-flex justify-content-center"  style="margin-top:5px;!important">
            //             <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest btn-danger" href="javascript:void(0)"  onclick="cancelRequest('.$q->id.')"><i class="fa fa-info"></i></a>&nbsp; <a style="width:32px;!important" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="resendRequest('.$q->id.')"><i class="fa fa-send "></i></a></div>';
            //         return $text;
            //     } else if($q->status == "S" ) {
            //         return '<div class="d-flex justify-content-center">
            //         <span class="badge badge-success">SUKSES</span>   </div>
            //         <div class="d-flex justify-content-center disabled"  style="margin-top:5px;!important">
            //         <a disabled  class="btn disabled btn-sm  btnCancelRequest btn-default" style="width:32px;!important" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a></div>';
            //     } else if($q->status == "F" ) {
            //         return '
            //         <div class="d-flex justify-content-center">
            //         <span class="badge badge-danger">GAGAL</span>   </div>
            //           <div class="d-flex justify-content-center disabled"  style="margin-top:5px;!important">
            //         <a  style="width:32px;!important"  disabled   class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a></div>';
            //     } else {
            //         return '<div class="d-flex justify-content-center">
            //         <span class=" badge badge-warning">PROSES</span>   </div>
            //           <div class="d-flex justify-content-center disabled"  style="margin-top:5px;!important">
            //         <a  style="width:32px;!important" disabled  class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>
            //         </div>';
            //     }
            // })
            ->addColumn('price_biller', function ($q) {
                return formatCurrency($q->price_biller);
            })
            ->addColumn('price_reseller', function ($q) {
                return formatCurrency($q->price_reseller);
            })
            ->addColumn('action', function ($q) {
                if ($q->status == "Q") {
                    $text = '
                    <div class="d-flex justify-content-center">
                    <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="width:32px;!important" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="resendRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-send "></i></a>&nbsp; 
                    <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a>
                    </div>';
                    return $text;
                } else if ($q->status == "S") {
                    return '
                <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest2 btn-danger" href="javascript:void(0)"  onclick="cancelRequest2(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>&nbsp; 
                <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a></div>';
                } else if ($q->status == "F") {
                    return '
                
                  <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important"  disabled   class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>&nbsp;                     <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a></div>';
                } else {
                    $text = '
                    <div class="d-flex justify-content-center">
                    <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="width:32px;!important" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="resendRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-send "></i></a>&nbsp; 
                    <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a>
                    </div>';
                    return $text;
                }
            })






            ->with('tanggalTerpilih', $tanggaldipilih)
            ->with('jumlahTransaksi', $jumlahTransaksi)
            ->with('jumlahTransaksiSukses', $jumlahTransaksiSukses)
            ->with('jumlahTransaksiGagal', $jumlahTransaksiGagal)
            ->with('jumlahTransaksiAntrian', $jumlahTransaksiAntrian)
            ->with('jumlahTransaksiProses', $jumlahTransaksiProses)
            ->with('omzet', $omzet)
            ->with('saldoH2h', $saldoH2h)
            ->with('saldoAgen', $saldoAgen)
            ->with('c', $c)
            ->rawColumns(['action', 'status', 'statusText', 'SN', 'tanggal_masuk', 'tanggalUpdate'])
            ->make(true);
    }
    public function fnDataTransaksiLive(Request $request)
    {




        $query = Transaction::select('*', 'transaction.id as Aid')->with(['history', 'bill', 'reseller.balance', 'reseller.attribute', 'produk'])->withCount('commands')->where("transaction.is_deleted", 0)
        ->whereDate('transaction.created_at', ">=", date('Y-m-d'))
        ->orderBy('transaction.created_at', 'desc')
        ->limit(200);

        if ($request->has('filterStatus') && $request->filterStatus != "") {
            if ($request->has('filterStatus') && $request->filterStatus == "belum_selesai") {
                $query = $query->whereIn('transaction.status', ['Q', 'P']);
            }
        }

        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()

            ->addColumn('group', function ($q) {
                return $q->command;
            })
            ->addColumn('statusOrigin', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = 'ANTRIAN';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return 'SUKSES';
                } else if (strtoupper($q->status) == "F") {
                    return 'GAGAL';
                } else if (strtoupper($q->status) == "P") {
                    return 'PROSES';
                }
            })
            ->addColumn('jenis', function ($q) {
                return $q->group_name;
            })
            ->addColumn('productSKU', function ($q) {
                if(isset($q->produk)){
                    return $q->produk->sku_code;
                } else {
                    return "";
                }
            })
            ->addColumn('tanggal_masuk', function ($q) {

                return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->created_at))) . '<br> ' . date('H:i:s', strtotime($q->created_at));
            })
            ->addColumn('tanggalUpdate', function ($q) {
                if ($q->updated_at != null) {
                    return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->updated_at))) . '<br> ' . date('H:i:s', strtotime($q->updated_at));
                } else if (strtoupper($q->status) == "F") {
                    return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->created_at))) . '<br> ' . date('H:i:s', strtotime($q->created_at));
                } else {
                    return 'Sedang diproses';
                }
            })
            ->addColumn('laba', function ($q) {
                $laba = $q->price_reseller - $q->price_biller;
                return formatCurrency((int)$laba);
            })
            ->addColumn('SN', function ($q) {
                if ($q->respond != null) {
                    if ($q->status == "S") {
                        $result = preg_match('/SN:(.*)/', $q->respond, $matches);
                        if (!empty($matches)) {
                            $res = explode('/', $matches[1]);
                            $ress = explode('.', $res[0]);
                            return substr_replace(substr_replace(substr_replace($matches[0], '<br>', 60, 0), '<br>', 120, 0), '<br>', 180, 0);
                            // return substr_replace($ress[0], '<br>', 15, 0);
                        } else {
                            $string = $q->respond;

                            $json = json_decode($string, true);
                            return substr_replace($json['data']['sn'], '<br>', 15, 0);
                        }
                    }
                } else {
                    return '<p class="text-center"></p>';
                }
            })
            ->addColumn('idAgent', function ($q) {
                return "";
            })
            ->addColumn('price_biller', function ($q) {
                return formatCurrency($q->price_biller);
            })
            ->addColumn('price_reseller', function ($q) {
                return formatCurrency($q->price_reseller);
            })
            ->addColumn('saldoBiller', function ($q) {
                $res = "";
                if ($q->history != null) {
                    $res = $q->history->balance_after;
                }
                // return $res;
                return formatCurrency((int)$res);
            })
            ->addColumn('status', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = '<div class="d-flex justify-content-center">
                    <span class="badge badge-warning">ANTRIAN</span>   </div>
                  ';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return '<div class="d-flex justify-content-center">
                <span class="badge badge-success">SUKSES</span>   </div>
               ';
                } else if (strtoupper($q->status) == "F") {
                    return '
                <div class="d-flex justify-content-center">
                <span class="badge badge-danger">GAGAL</span>   </div>
                  ';
                } else if (strtoupper($q->status) == "P") {
                    return '<div class="d-flex justify-content-center">
                <span class=" badge badge-info">PROSES</span>   </div>
                  ';
                }
            })
            ->addColumn('statusText', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = '<div class="d-flex justify-content-center">
                    <span class="badge badge-warning">ANTRIAN</span>   </div>
                  ';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return '<div class="d-flex justify-content-center">
                <span class="badge badge-success">SUKSES</span>   </div>
               ';
                } else if (strtoupper($q->status) == "F") {
                    return '
                <div class="d-flex justify-content-center">
                <span class="badge badge-danger">GAGAL</span>   </div>
                  ';
                } else if (strtoupper($q->status) == "P") {
                    return '<div class="d-flex justify-content-center">
                <span class=" badge badge-info">PROSES</span>   </div>
                  ';
                }
            })

            ->addColumn('action', function ($q) {
                if ($q->status == "Q") {
                    $text = '
                    <div class="d-flex justify-content-center">
                    <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="width:32px;!important" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="resendRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-send "></i></a>&nbsp; 
                    <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a>
                    </div>';
                    return $text;
                } else if ($q->status == "S") {
                    return '
                <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest2 btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>&nbsp; 
                <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a></div>';
                } else if ($q->status == "F") {
                    return '
                
                  <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important"  disabled   class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>&nbsp;                     <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a></div>';
                } else {
                    return '
                  <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important" disabled  class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>    &nbsp;                 <a style="width:32px;!important" class="btn btn-sm btn-warning" href="javascript:void(0)" onclick="editData(' . $q->Aid . ')"><i class="fa fa-edit "></i></a>
                </div>';
                }
            })

            ->rawColumns(['action', 'status', 'statusText', 'SN', 'tanggal_masuk', 'tanggalUpdate'])
            ->make(true);
    }

    public function fnDataInboxBiller(Request $request)
    {

        $query = Transaction::selectRaw('* ')->with(['bill', 'reseller.balance', 'reseller.attribute', 'produk', 'inbox'])->withCount('commands')->where("transaction.is_deleted", 0);
        if ($request->has('startDate') && $request->startDate != "") {

            $query = $query->whereDate('transaction.created_at', ">=", $request->startDate);
        }
        if ($request->has('endDate') && $request->endDate != "") {

            $query = $query->whereDate('transaction.created_at', "<=", $request->endDate);
        }
        if ($request->has('filterBiller') && $request->filterBiller != "") {
            $query = $query->where('transaction.biller', "like", '%' . $request->filterBiller . '%');
        }
        if ($request->has('filterOrderID') && $request->filterOrderID != "") {
            $query = $query->where('trx_code', "like", '%' . $request->filterOrderID . '%');
        }
        // if($request->has('searchInputInbox') && $request->searchInputInbox != ""){
        //     $query = $query->orWhere('request',"like",'%'.$request->searchInputInbox.'%');
        //     $query = $query->orWhere('request',"like",'%'.$request->searchInputInbox.'%');
        // }
        $query = $query->orderBy('transaction.created_at', 'desc')->get();
        return DataTables::of($query)
            ->addIndexColumn()

            ->addColumn('group', function ($q) {
                return $q->command;
            })
            ->addColumn('billerName', function ($q) {
                if (isset($q->bill)) {
                    return $q->bill->name;
                } else {

                    return "";
                }
            })
            ->addColumn('billerUrl', function ($q) {
                if (isset($q->bill)) {
                    return $q->bill->biller_url;
                } else {

                    return "";
                }
            })

            ->addColumn('statusOrigin', function ($q) {
                return $q->status;
            })
            ->addColumn('jenis', function ($q) {
                return $q->group_name;
            })
            ->addColumn('tanggal_masuk', function ($q) {

                return $this->tanggalIndonesia(date('Y-m-d', strtotime($q->created_at))) . '<br> ' . date('H:i:s', strtotime($q->created_at));
            })
            ->addColumn('laba', function ($q) {
                $laba = $q->price_reseller - $q->price_biller;
                return formatCurrency((int)$laba);
            })
            ->addColumn('SN', function ($q) {
                if ($q->respond != null) {
                    if ($q->status == "S") {
                        $result = preg_match('/SN:(.*)/', $q->respond, $matches);
                        if (!empty($matches)) {
                            $res = explode('/', $matches[1]);
                            $ress = explode('.', $res[0]);
                            return substr_replace(substr_replace(substr_replace($matches[0], '<br>', 60, 0), '<br>', 120, 0), '<br>', 180, 0);
                            // return substr_replace($ress[0], '<br>', 15, 0);
                        } else {
                            $string = $q->respond;

                            $json = json_decode($string, true);
                            return substr_replace($json['data']['sn'], '<br>', 15, 0);
                        }
                    }
                } else {
                    return '<p class="text-center"> - </p>';
                }
            })
            ->addColumn('callback', function ($q) {
                if ($q->respond != null) {
                    return ($q->respond);
                } else {
                    return 'Belum ada callback';
                }
            })
            ->addColumn('respon', function ($q) {
                if ($q->inbox != null) {
                    return ($q->inbox->respond);
                } else {
                    return 'Belum ada respon';
                }
            })
            ->addColumn('helper', function ($q) {
                if ($q->inbox == null) {
                    return ($q->respond . ' Belum ada respon ' . $q->request);
                } else {
                    return ($q->respond . ' ' . $q->inbox->respond . ' ' . $q->request);
                }
            })
            ->addColumn('reqd', function ($q) {
                if ($q->request != null || $q->request != "-") {
                    if ($q->request != "-") {
                        return ($q->request);
                    } else {
                        return "";
                    }
                } else {
                    return '';
                }
            })
            ->addColumn('idAgent', function ($q) {

                return "";
            })
            ->addColumn('status', function ($q) {
                if (strtoupper($q->status) == "Q") {
                    $text = '<div class="d-flex justify-content-center">
                    <span class="badge badge-warning">ANTRIAN</span>   </div>
                  ';
                    return $text;
                } else if (strtoupper($q->status) == "S") {
                    return '<div class="d-flex justify-content-center">
                <span class="badge badge-success">SUKSES</span>   </div>
               ';
                } else if (strtoupper($q->status) == "F") {
                    return '
                <div class="d-flex justify-content-center">
                <span class="badge badge-danger">GAGAL</span>   </div>
                  ';
                } else if (strtoupper($q->status) == "P") {
                    return '<div class="d-flex justify-content-center">
                <span class=" badge badge-info">PROSES</span>   </div>
                  ';
                }
            })

            ->addColumn('action', function ($q) {
                if ($q->status == "Q") {
                    $text = '
                    <div class="d-flex justify-content-center">
                    <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="width:32px;!important" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="resendRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-send "></i></a></div>';
                    return $text;
                } else if ($q->status == "S") {
                    return '
                <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important" class="btn btn-sm  btnCancelRequest2 btn-danger" href="javascript:void(0)"  onclick="cancelRequest(' . '`' . $q->trx_code . '`' . ',`' . $q->sku_code . '`,`' . $q->custid . '`)"><i class="fa fa-info"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a></div>';
                } else if ($q->status == "F") {
                    return '
                
                  <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important"  disabled   class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a></div>';
                } else {
                    return '
                  <div class="d-flex justify-content-center disabled">
                <a  style="width:32px;!important" disabled  class="btn disabled btn-sm  btnCancelRequest btn-default" href="javascript:void(0)"  ><i class="fa fa-info disabled"></i></a>&nbsp; <a style="" disabled  class="btn disabled btn-sm btn-default" href="javascript:void(0)" ><i class="fa fa-send disabled"></i></a>
                </div>';
                }
            })

            ->rawColumns(['action', 'status', 'SN', 'tanggal_masuk', 'helper'])
            ->make(true);
    }

    public function tanggalIndonesia($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array(
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function transaksiIndex(Request $request)
    {

        return view('transaksi')->withCurDate($this->tanggalIndonesia(date('Y-m-d')))->withData(["user"=>$request->session()->get('SESS_USERDATA')['id']]);
    }

    public function transaksiLiveIndex(Request $request)
    {

        return view('transaksi_live')->withCurDate($this->tanggalIndonesia(date('Y-m-d')))->withData(["user"=>$request->session()->get('SESS_USERDATA')['id']]);
    }
    public function transaksiEdit(Request $request, $id)
    {
        $data = Transaction::where('id', $id)->first();
        if ($data->respond != null) {
            if ($data->status == "S") {

                $result = preg_match('/SN:(.*)/', $data->respond, $matches);
                if (!empty($matches)) {
                    $res = explode('/', $matches[1]);
                    $ress = explode('.', $res[0]);
                    $sn = ($ress[0]);
                } else {
                    $string = $data->respond;
                    $json = json_decode($string, true);
                    $sn =   $json['data']['sn'];
                }
            } else {

                $sn = '';
            }
        } else {
            $sn = '';
        }
        return view('transaksi_edit')->withCurDate($this->tanggalIndonesia(date('Y-m-d')))->withData($data)->withSn($sn);
    }
    public function transaksiUpdate(Request $request, $id)
    {
        $update = Transaction::where('id', $id)->update([
            'respond' => 'di Update Oleh ' . $request->session()->get('SESS_USERDATA')['nama'] . '. SN:' . $request->sn,
            'status' => $request->status
        ]);


        if ($update) {

            return response()->json([
                'status' => 200,
                'message' => "Transaksi berhasil diupdate",
                'data' => $request->all()
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'error (Transaksi)',
            ]);
        }
    }

    public function resend(Request $request)
    {
        $data = new \StdClass;
        $detail[0] = [
            'trx_code' => $request->trx_code,
            'sku_code' => $request->sku_code,
            'custid' => $request->custid,
        ];
        $data->admin_id = $request->session()->get('SESS_USERDATA')['id'];
        $data->request = "resend";
        $data->role = "pasien";
        $data->detail = $detail;
        $result = $this->apiService($data);
        if ($result->getData()->rc == 0) {
            if ($request->reason != "") {
                Transaction::where('trx_code', $request->trx_code)->update([
                    'added_data' => $request->reason,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $request->session()->get('SESS_USERDATA')['nama']
                ]);
            }
        }
        return response()->json([
            'message'=>$result->getData()->message
        ]);
   
    }
    public function cancel(Request $request)
    {
        $data = new \StdClass;
        $detail[0] = [
            'trx_code' => $request->trx_code,
            'sku_code' => $request->sku_code,
            'custid' => $request->custid,
        ];
        $data->admin_id = $request->session()->get('SESS_USERDATA')['id'];
        $data->request = "refund";
        $data->role = "pasien";
        $data->detail = $detail;

        $result = $this->apiService($data);
    
        if ($result->getData()->rc == 0) {
            if ($request->reason != "") {
                Transaction::where('trx_code', $request->trx_code)->update([
                    'added_data' => $request->reason,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $request->session()->get('SESS_USERDATA')['nama']
                ]);
            }
    
        }
        return response()->json([
            'message'=>$result->getData()->message
        ]);
    }
    public function resendBulk(Request $request)
    {
        //get status antrian
        $res = Transaction::where('status', 'Q')->where('is_deleted', 0)->get();
        $x = 0;
        foreach ($res as $v) {
            $data = new \StdClass;
            $detail[0] = [
                'trx_code' => $v['trx_code'],
                'sku_code' => $v['sku_code'],
                'custid' => $v['custid'],
            ];
            $data->admin_id = $request->session()->get('SESS_USERDATA')['id'];
            $data->request = "resend";
            $data->role = "pasien";
            $data->detail = $detail;
            $has = $this->apiService($data);
            $x++;
        }
        return response()->json([
            'message'=>$x . ' transaksi sudah di kirim ulang'
        ]);
    }
    public function cancelBulk(Request $request)
    {
        //get status antrian
        $res = Transaction::where('status', 'Q')->where('is_deleted', 0)->get();
        $x = 0;
        foreach ($res as $v) {
            $data = new \StdClass;
            $detail[0] = [
                'trx_code' => $v['trx_code'],
                'sku_code' => $v['sku_code'],
                'custid' => $v['custid'],
            ];
            $data->admin_id = $request->session()->get('SESS_USERDATA')['id'];
            $data->request = "refund";
            $data->role = "pasien";
            $data->detail = $detail;
            $has = $this->apiService($data);
            $x++;
        }
        return response()->json([
            'message'=>$x . ' transaksi sudah di kirim ulang'
        ]);
    }
    public function editProduct(Request $request, $id)
    {
        $data = Product::where('id', $request->id)->with(['productGroup', 'productBiller.biller', 'biller'])->first();
        $biller = Biller::all();
        if ($request->has('type') && $request->type == 'prepaid') {
            $group = ProductGroup::whereIn('group_name', ['prabayar', 'prepaid'])->get();

            return view('produk_prepaid_edit')->withBiller($biller)->withGroup($group)->withData($data);
        } else {
            $group = ProductGroup::whereIn('group_name', ['pascabayar', 'postpaid'])->get();

            return view('produk_postpaid_edit')->withBiller($biller)->withGroup($group)->withData($data);
        }
    }
    public function updateProduct(Request $request, $id)
    {


        if ($request->has('type') && $request->type == 'prepaid') {

            $productNew =  Product::where('id', $id)->first();
            $productNew->group_id = $request->group_id;
            $productNew->sku_code = $request->sku_code;
            $productNew->admin_fee = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->price = $request->price;
            $productNew->nominal = $request->nominal;
            $productNew->cashback = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->updated_at = date("Y-m-d H:i:s");
            // $productNew->created_by = Session::get('user_id');
            $productNew->updated_by = 1;
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('product_id', $id)->first();
                $productBiller->biller_id = $request->biller_id;
                $productBiller->sku_biller = $request->sku_code;
                $productBiller->price_basic = $request->price_basic;
                $productBiller->updated_by = 1;
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data' => $request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        } else {

            $productNew =  Product::where('id', $id)->first();
            $productNew->group_id = $request->group_id;
            $productNew->sku_code = $request->sku_code;
            $productNew->nominal = 0;
            $productNew->price = 0;
            $productNew->is_trouble = $request->is_trouble;
            $productNew->is_deleted = 0;
            $productNew->name = $request->name;
            $productNew->admin_fee = $request->admin_fee;
            $productNew->cashback = $request->cashback;
            $productNew->biller_id = $request->biller_id;
            $productNew->description = $request->description;
            $productNew->updated_at = date("Y-m-d H:i:s");
            // $productNew->created_by = Session::get('user_id');
            $productNew->updated_by = 1;
            $saveProduct = $productNew->save();
            if ($saveProduct) {
                $productBiller =  productBiller::where('product_id', $id)->first();
                $productBiller->biller_id = $request->biller_id;
                $productBiller->sku_biller = $request->sku_code;
                $productBiller->price_basic = 0;
                $productBiller->admin_fee = $request->admin_fee;
                $productBiller->updated_by = 1;
                $saveProduct = $productBiller->save();
                if ($saveProduct) {
                    return response()->json([
                        'status' => 200,
                        'message' => "Produk berhasil diupdate",
                        'data' => $request->all()
                    ]);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error (product Biller)',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        }
    }

    public function transactionModuleStatus(){
        try {
            $data = Cronjob::first();
            return response()->json([
                'status' => 200,
                'data'=>$data
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 500,
                'message' => 'error (cronjob)',
            ]);
        }
        
    }
}
