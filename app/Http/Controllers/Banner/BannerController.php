<?php

namespace App\Http\Controllers\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Banner::where('is_deleted','0');
        $query = $query->get();
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('gambar', function ($q)  {
            return '
            <div class="add-data d-flex justify-content-center">
          <a href="" data-toggle="modal" data-target="#largeModal'.$q->id.'"><img class="img-responsive" src="'.$q->url_image.'" alt="" style="max-height:50px;">
        </div>
        
        
        <div class="modal fade" id="largeModal'.$q->id.'" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content">
             
              <div class="modal-body">
                <div class="zoom1 d-flex justify-content-center">
                  <img class="img-responsive img-center" style="max-width:1000px;" src="'.$q->url_image.'" alt="">
                </div>
              </div>
             
            </div>
          </div>
        </div>
        ';
        })
        
        ->addColumn('tanggal', function ($q)  {
             if($q->updated_at == null){
                 return 'Belum di Update';
             } else {
                 return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at)).'<br>Oleh: '. $q->updated_by;
             }
        })
        ->addColumn('penempatan', function ($q)  {
            if($q->placement == 1){
                return 'HOME';
            } else if($q->placement == 2) {
                return 'MERCHANDISE';
            } else {
                return 'ZANPOS';
            }
       })
        ->addColumn('referensi', function ($q)  {
            if($q->reference_activity == 1){
                return 'NEWS ID '.$q->reference_id;
            } else if($q->reference_activity == 2) {
                return 'MERCHANDISE ID '.$q->reference_id;
            } else {
                return 'Belum ada data.';
            }
       })
        ->rawColumns( ['action','nama','gambar','tanggal'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
     	return view('banner');
    }
 

    public function add (Request $request)
    {   
          
            $param=$request->except('avatar');
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['url_image'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading image',
                ]);
            }


            $data = new Banner();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $param['created_at']=date('Y-m-d H:i:s');
            $create = $data::create($param);
            if($create){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {

        Banner::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' =>$request->session()->get('SESS_USERDATA')['nama']
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = Banner::where('id',$request->id)->first();
        return view('banner_edit')->withData($data);
    }
    public function update (Request $request,$id)
    {
        $param = $request->except('avatar','id=','_token');
        // dd($param);
        if($request->hasFile('avatar')){
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['url_image'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
        }
        $param['updated_at']=date('Y-m-d H:i:s');
        $param['updated_by']=$request->session()->get('SESS_USERDATA')['nama'];

            $update =  Banner::where('id',$id)->update($param);

            // $update = $data;
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
    }

}