<?php

namespace App\Http\Controllers\DaftarPekerjaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DaftarPekerjaan;

use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class DaftarPekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = DaftarPekerjaan::orderBy('date','desc')->get();
        // return ($query);
        return DataTables::of($query)
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
      
        ->addColumn('tanggal', function ($q)  {
             
            return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->date)));
    
        })
        ->addColumn('status', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-warning">Belum Selesai</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">Selesai</span>';
            } 
        })
        ->rawColumns( ['action','nama','status'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        // $biller = Biller::all();
     	return view('daftar_pekerjaan');
    }
 

    public function add (Request $request)
    {   
          
            $param=$request->except('status');
            if($request->has('status') && $request->status != ""){
                $param['status']=$request->status;
            }
            $data = new DaftarPekerjaan();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $create = $data::create($param);
            if($create){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {

        DaftarPekerjaan::where('id',$id)->delete();

        return response()->json([
            'status' => 200,
            'message' => "Data berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = DaftarPekerjaan::where('id',$request->id)->first();
        return view('daftar_pekerjaan_edit')->withData($data);
    }
    public function update (Request $request,$id)
    {
        $param = $request->except('status','id=','_token');
        // dd($param);
        if($request->has('status') && $request->status != ""){
            $param['status']=$request->status;
        }
      
        // dd($param);
            $update =  DaftarPekerjaan::where('id',$id)->update($param);
            // $update = $data;
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Data berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error',
                ]);
            }
    }

}