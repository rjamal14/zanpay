<?php

namespace App\Http\Controllers\Reseller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Reseller;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use App\Models\CashbackHistory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ResellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {
        // if($request->has('search') && $request->search != ""){
        //     $query = Reseller::with(['balance','attribute','group','latestHistory'])->where("is_deleted",0)->where("name",'like','%'.$request->search.'%');
        // } else {
            $query = Reseller::with(['balance','attribute','group','latestHistory'])->withCount('downline')->where("is_deleted",0);
        // }
        $status = [
            [
                "id"=>0,
                "text"=>"Verifikasi",
            ],
            [
                "id"=>1,
                "text"=>"Kyc",
            ],
            [
                "id"=>2,
                "text"=>"Verify",
            ],
            [
                "id"=>3,
                "text"=>"Suspend",
            ],
        ];
        $populasi = Reseller::where('is_deleted',0)->count();
        $memberAktif1 =Reseller::where('is_deleted',0)->where('status','2')->count();
        $memberAktif = ($populasi-$memberAktif1) / $populasi *100 ." %";
        $chartPopulasi = $memberAktif1 .', '.($populasi-$memberAktif1) ;
        $platform = Reseller::select('platform as id','platform as text')->groupBy('platform')->get();
        
        $group = ResellerGroup::select('id','name as text')->get()->toArray();

        // if($request->has('filterStatus') && $request->filterStatus != ""){
        //     $query=$query->where('status',$request->filterStatus);
        // }
        // if($request->has('filterPlatform') && $request->filterPlatform != "Semua Platform"){
        //     $query=$query->where('platform',$request->filterPlatform);
        // }
        // if($request->has('filterGroup') && $request->filterGroup != ""){
        //     $query=$query->where('reseller_group',$request->filterGroup);
        // }
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            $nama = $q->id.",".(string)("'".$q->name."'");
                return ' <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
                // return '<a   class="btn btn-sm  btnEdit btn-success" href="javascript:void(0)" onclick="cashbackHistory('.$nama.')"><i class="fa fa-gift"></i></a> <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('jenis', function ($q)  {
                return $q->group_name;
        })
        ->addColumn('custom_code', function ($q)  {
                return $q->custom_code ?? $q->reseller_code;
        })
        ->addColumn('tanggalJoin', function ($q)  {
             
                    return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            
        })
        ->addColumn('laba', function ($q)  {
                    $laba = $q->price_reseller - $q->price_biller;
                    return $laba;
        })
        ->addColumn('SN', function ($q)  {
            if($q->respond != null){
                $result=preg_match_all('/(?<=(SN:))(\s\w*)/',$q->respond,$matches);
                return ($matches[0]);

            }
            else {
                return '<p class="text-center"> - </p>';
            }
        })
        ->addColumn('pinP', function ($q)  {
                    return "******";
        })
        ->addColumn('resellerName', function ($q)  {
                    return strtoupper($q->name);
        })
        ->addColumn('Balance', function ($q)  {
                  if($q->balance == null){
                      return 0;
                  } else{
                      return formatCurrency($q->balance->balance);
                  }
        })
        ->addColumn('groupName', function ($q)  {
                   if($q->group == null){
                       return "-";
                   } else {
                        return $q->group->name;
                   }
        })
        ->addColumn('lastActivity', function ($q)  {
            if($q->latestHistory == null){
                return "-";
            } else {
                 return $q->latestHistory->description;
            }
        })
        ->addColumn('avatar', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class="rounded-circle avatar" style="max-height:40px !important;">';
         
        })
        ->addColumn('keagenan', function ($q) {
            if ($q->is_active == 1) {
                return '<span class="badge badge-success">Aktif</span>';
            }else {
                return '<span class="badge badge-danger">Non Aktif</span>';
            }
        })
        ->addColumn('rawImage', function ($q)  {
        
                return $q->avatar;
         
        })
        ->addColumn('name', function ($q)  {
        
                return strtoupper($q->name);
         
        })
        ->addColumn('name2', function ($q)  {
        
                return $q->name;
         
        })
        ->addColumn('statusReseller', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-default">Belum Verifikasi Email</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">Belum Upload Dokumen</span>';
            } else if($q->status == "2" ) {
                return '<span class="badge badge-primary">Sudah Verifikasi</span>';
            } else {
                return '<span class="badge badge-warning">suspend</span>';
            }
        })

        ->with('group',$group)
        ->with('platform',$platform)
        ->with('status',$status)
        ->with('kunjungan',"1,195")
        ->with('pertumbuhan',"$1,243")
        ->with('populasi',$populasi)
        ->with('memberAktif',$memberAktif)
        ->with('chartPopulasi',$chartPopulasi)
        ->rawColumns( ['avatar','action','statusReseller','SN','keagenan'])
        ->make(true);
    }
    public function fnHistoryData(Request $request,$id)
    {
        $query = CashbackHistory::where('reseller_id',$id)->orderBy("created_at",'asc')->get();
        $cashbackDidapat = collect($query)->where('type',1)->pluck('amount')->toArray();
        $cashbackDidapat = array_sum($cashbackDidapat);

        $cashbackDipakai = collect($query)->where('type',0)->pluck('amount')->toArray();
        $cashbackDipakai = array_sum($cashbackDipakai);
        $cashbackSekarang = $cashbackDidapat - $cashbackDipakai;

        $nama = Reseller::where('id',$id)->first()->name;
        return DataTables::of($query)
        ->addIndexColumn()
     
        ->addColumn('kredit', function ($q)  {
            if($q->type == 1){
                return $q->amount;
            }    
            else {
                return 0;
            }
        })
        ->addColumn('debit', function ($q)  {
            if($q->type == 0){
                return $q->amount;
            }    
            else {
                return 0;
            }
        })
        ->addColumn('tanggal', function ($q)  {
             
                    return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            
        })
        ->with('nama',$nama)
        ->with('cashbackDipakai',$cashbackDipakai)
        ->with('cashbackSekarang',$cashbackSekarang)
        ->with('cashbackDidapat',$cashbackDidapat)
        ->rawColumns( ['avatar','action','statusReseller','SN'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function indexComm (Request $request)
    {

        $query = Reseller::with(['balance','attribute','group','historyComm','latestHistory','downline'])->where("is_deleted",0)->with('downline',function ($q){
            $q->withCount('successTransaction');
        })->withCount('downline')->first();
        $query->transaksi_downline = collect($query->downline)->sum('success_transaction_count');
        $query->komisi = formatCurrency(collect($query->historyComm)->sum('amount'));
       

        $status = [
            [
                'id'=>0,
                'text'=>'Belum Verifikasi Email'
            ],
            [
                'id'=>1,
                'text'=>'Belum Upload Dokumen'
            ],
            [
                'id'=>2,
                'text'=>'Sudah Verifikasi'
            ],
            [
                'id'=>3,
                'text'=>'suspend'
            ],
        ];
        return view('reseller_comm')->withData($query)->withGroup($status);

    }
    public function index (Request $request)
    {

        $group = ResellerGroup::where('is_deleted',0)->get();
        return view('reseller')->withGroup($group);


    }
    public function detailComm(Request $request,$id)
    {

        $query = Reseller::with(['balance','attribute','group','historyComm','latestHistory','downline'])->where("is_deleted",0)->with('downline',function ($q){
            $q->withCount('successTransaction');
        })->withCount('downline')->where('reseller_code',$id)->first();
        if($query){
            $query->transaksi_downline = collect($query->downline)->sum('success_transaction_count');
            $query->komisi = formatCurrency(collect($query->historyComm)->sum('amount'));
            return response()->json([
                'data'=>$query,
                'atr'=>$query->attribute,
            ]);;
        } else {
            return 500;
        }
           
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            $nama = $q->id.",".(string)("'".$q->name."'");
                return ' <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
                // return '<a   class="btn btn-sm  btnEdit btn-success" href="javascript:void(0)" onclick="cashbackHistory('.$nama.')"><i class="fa fa-gift"></i></a> <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('jenis', function ($q)  {
                return $q->group_name;
        })
        ->addColumn('tanggalJoin', function ($q)  {
             
                    return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            
        })
        ->addColumn('laba', function ($q)  {
                    $laba = $q->price_reseller - $q->price_biller;
                    return $laba;
        })
        ->addColumn('SN', function ($q)  {
            if($q->respond != null){
                $result=preg_match_all('/(?<=(SN:))(\s\w*)/',$q->respond,$matches);
                return ($matches[0]);

            }
            else {
                return '<p class="text-center"> - </p>';
            }
        })
        ->addColumn('pinP', function ($q)  {
                    return "******";
        })
        ->addColumn('Balance', function ($q)  {
                  if($q->balance == null){
                      return 0;
                  } else{
                      return formatCurrency($q->balance->balance);
                  }
        })
        ->addColumn('groupName', function ($q)  {
                   if($q->group == null){
                       return "-";
                   } else {
                        return $q->group->name;
                   }
        })
        ->addColumn('lastActivity', function ($q)  {
            if($q->latestHistory == null){
                return "-";
            } else {
                 return $q->latestHistory->description;
            }
        })
        ->addColumn('avatar', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class="rounded-circle avatar" style="max-height:40px !important;">';
         
        })
        ->addColumn('rawImage', function ($q)  {
        
                return $q->avatar;
         
        })
        ->addColumn('name', function ($q)  {
        
                return strtoupper($q->name);
         
        })
        ->addColumn('name2', function ($q)  {
        
                return $q->name;
         
        })
        ->addColumn('statusReseller', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-default">Belum Verifikasi Email</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">Belum Upload Dokumen</span>';
            } else if($q->status == "2" ) {
                return '<span class="badge badge-primary">Sudah Verifikasi</span>';
            } else {
                return '<span class="badge badge-warning">suspend</span>';
            }
        })

        ->with('group',$group)
        ->with('platform',$platform)
        ->with('status',$status)
        ->with('kunjungan',"1,195")
        ->with('pertumbuhan',"$1,243")
        ->with('populasi',$populasi)
        ->with('memberAktif',$memberAktif)
        ->with('chartPopulasi',$chartPopulasi)
        ->rawColumns( ['avatar','action','statusReseller','SN'])
        ->make(true);
    }
 

    public function addReseller (Request $request)
    {   
            $param=$request->except('avatar');
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['avatar'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            $param['password']=$request["pin"];
            $param['pin']=$request["pin"];
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $reseller = new Reseller();
            $reseller = $reseller::create($param);
            if($reseller){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function deleteReseller (Request $request,$id)
    {

        $reseller = Reseller::where('id',$id)
                    ->with(['balance','attribute','group','latestHistory'])
                    ->withCount('downline')
                    ->first();
    
        $check_balance = $reseller->balance ?? false;
        $check_balance_amount = ($check_balance && $check_balance->balance > 0) ? true : false;
        $check_downline = ($reseller->downline && $reseller->downline->count() > 0) ? true : false;

        if($check_balance_amount){
            return response()->json([
                'status' => 500,
                'message' => "Reseller gagal dihapus, Reseller mempunyai saldo",
            ]);   
        }

        if($check_downline){
            return response()->json([
                'status' => 500,
                'message' => "Reseller gagal dihapus, Reseller mempunyai Downline",
            ]);   
        }

        // Reseller::where('id',$id)->update([
        //     'is_deleted' => 1,
        //     'deleted_at' => date("Y-m-d H:i:s")
        // ]);

        Reseller::destroy($id);

        return response()->json([
            'status' => 200,
            'message' => "Reseller berhasil dihapus",
        ]);


    }

    public function editReseller (Request $request,$id)
    {
        $data = Reseller::where('id',$request->id)->first();
        $group = ResellerGroup::where('is_deleted',0)->get();
        return view('reseller_edit')->withGroup($group)->withData($data);
    }
    public function updateReseller (Request $request,$id)
    {

// dd($request->all());
            $reseller =  Reseller::where('id',$id)->first();
            $params = $request->except('_token');


            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $reseller->avatar =$urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            // $reseller->password =$request->pin;
            $reseller->pin =$request->pin;
            $reseller->reseller_group =$request->reseller_group;
            $reseller->reseller_code =$request->reseller_code;
            $reseller->virtual_account =$request->virtual_account;
            $reseller->name =$request->name;
            $reseller->phone =$request->phone;
            $reseller->email =$request->email;
            $reseller->platform =$request->platform;
            $reseller->status =$request->status;
            $reseller->is_active =$request->is_active;
   
            $update = $reseller->save();
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Reseller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Reseller)',
                ]);
            }



       




    
    }

}