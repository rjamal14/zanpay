<?php

namespace App\Http\Controllers\Reseller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Reseller;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ResellerGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = ResellerGroup::withCount('reseller')->where("is_deleted",0);
        
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('cashback', function ($q)  {
                return formatCurrency($q->cashback);
        })
        ->addColumn('markup', function ($q)  {
             
                return formatCurrency($q->point);
        })
        ->rawColumns( ['action'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        return view('reseller_group');
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 

    public function addResellerGroup (Request $request)
    {   
          
            $resellerGroup = new ResellerGroup();
            $param = $request->all();
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $resellerGroup = $resellerGroup::create($param);
            if($resellerGroup){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function deleteResellerGroup (Request $request,$id)
    {
        ResellerGroup::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Reseler Group berhasil dihapus"
        ]);


    }

    public function editResellerGroup (Request $request,$id)
    {
        $data = ResellerGroup::where('id',$request->id)->first();
        return view('reseller_group_edit')->withData($data);
    }
    public function updateResellerGroup (Request $request,$id)
    {


            $reseller =  ResellerGroup::where('id',$id)->first();

            $update = $reseller->update($request->all());
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Reseller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Reseller Group)',
                ]);
            }
    }

}