<?php

namespace App\Http\Controllers\Reseller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Reseller;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class ResellerSaldoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Reseller::with(['balance','attribute','group','latestHistory'])->where("is_deleted",0);
        
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> ';
        })
     
        ->addColumn('nama', function ($q)  {
            return '<img src="'.$q->avatar.'" class=" avatar" ">&nbsp'.$q->name;
        })
    
        ->addColumn('groupName', function ($q)  {
                   if($q->group == null){
                       return "-";
                   } else {
                        return $q->group->name;
                   }
        })
        ->addColumn('phone', function ($q)  {
            return '<span class="phone"><i class="fa fa-phone m-r-10"></i>'. $q->phone.'</span>';
        })
        ->addColumn('email', function ($q)  {
            return '<span class="phone"><i class="fa fa-envelope m-r-10"></i>'. $q->email.'</span>';
        })
        ->addColumn('saldo', function ($q)  {
            if(isset($q->balance)) {
                return '<span class="money"><i class="fa fa-money m-r-10"></i>'. formatCurrency($q->balance->balance).'</span>';
            } else {
                return '<span class="money"><i class="fa fa-money m-r-10"></i>'. formatCurrency(0).'</span>';
            }
        })
    

     
        ->rawColumns( ['nama','action','phone','email','saldo'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        return view('saldo_agen');
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
    public function fnDatav2(Request $request)
    {
        // if($request->has('search') && $request->search != ""){
        //     $query = Reseller::with(['balance','attribute','group','latestHistory'])->where("is_deleted",0)->where("name",'like','%'.$request->search.'%');
        // } else {
            $query = Reseller::with(['balance','attribute','group','latestHistory','downline'])->where("is_deleted",0);
        // }
        $status = [
            [
                "id"=>0,
                "text"=>"Verifikasi",
            ],
            [
                "id"=>1,
                "text"=>"Kyc",
            ],
            [
                "id"=>2,
                "text"=>"Verify",
            ],
            [
                "id"=>3,
                "text"=>"Suspend",
            ],
        ];
        $populasi = Reseller::where('is_deleted',0)->count();
        $memberAktif1 =Reseller::where('is_deleted',0)->where('status','2')->count();
        $memberAktif = ($populasi-$memberAktif1) / $populasi *100 ." %";
        $chartPopulasi = $memberAktif1 .', '.($populasi-$memberAktif1) ;
        $platform = Reseller::select('platform as id','platform as text')->groupBy('platform')->get();
        
        $group = ResellerGroup::select('id','name as text')->get()->toArray();

        if($request->has('filterStatus') && $request->filterStatus != ""){
            $query=$query->where('status',$request->filterStatus);
        }
        if($request->has('filterPlatform') && $request->filterPlatform != "Semua Platform"){
            $query=$query->where('platform',$request->filterPlatform);
        }
        if($request->has('filterGroup') && $request->filterGroup != ""){
            $query=$query->where('reseller_group',$request->filterGroup);
        }
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';
            $nama = $q->id.",".(string)("'".$q->name."'");
                return '<a   class="btn btn-sm  btnEdit btn-success" href="javascript:void(0)" onclick="cashbackHistory('.$nama.')"><i class="fa fa-gift"></i></a> <a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
        ->addColumn('jenis', function ($q)  {
                return $q->group_name;
        })
        ->addColumn('tanggalJoin', function ($q)  {
             
                    return $this->tanggalIndonesia(date('Y-m-d',strtotime($q->created_at)))." " . date('H:i:s',strtotime($q->created_at));
            
        })
        ->addColumn('laba', function ($q)  {
                    $laba = $q->price_reseller - $q->price_biller;
                    return $laba;
        })
        ->addColumn('SN', function ($q)  {
            if($q->respond != null){
                $result=preg_match_all('/(?<=(SN:))(\s\w*)/',$q->respond,$matches);
                return ($matches[0]);

            }
            else {
                return '<p class="text-center"> - </p>';
            }
        })
        ->addColumn('pinP', function ($q)  {
                    return "******";
        })
        ->addColumn('pinP2', function ($q)  {
            
                    return '<input id="password-field'.$q->id.'" type="password" class="form-control" name="password'.$q->id.'"  value="'.$q->password.'">
                    <span toggle="#password-field'.$q->id.'" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    ';
        })
        ->addColumn('Balance', function ($q)  {
                  if($q->balance == null){
                      return 0;
                  } else{
                      return formatCurrency($q->balance->balance);
                  }
        })
        ->addColumn('saldo', function ($q)  {
                  if($q->balance == null){
                      return 0;
                  } else{
                      return formatCurrency($q->balance->balance);
                  }
        })
        ->addColumn('groupName', function ($q)  {
                   if($q->group == null){
                       return "-";
                   } else {
                        return $q->group->name;
                   }
        })
        ->addColumn('lastActivity', function ($q)  {
            if($q->lastHistory == null){
                return "-";
            } else {
                 return $q->lastHistory->description;
            }
        })
        ->addColumn('avatar', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class="rounded-circle avatar">';
         
        })
        ->addColumn('rawImage', function ($q)  {
        
                return $q->avatar;
         
        })
        ->addColumn('name', function ($q)  {
        
                return strtoupper($q->name);
         
        })
        ->addColumn('name2', function ($q)  {
        
                return $q->name;
         
        })
        ->addColumn('statusReseller', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-default">Verifikasi</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">kyc</span>';
            } else if($q->status == "2" ) {
                return '<span class="badge badge-primary">verify</span>';
            } else {
                return '<span class="badge badge-warning">suspend</span>';
            }
        })
        ->addColumn('downline', function ($q)  {
            if(count($q->downline) > 0){
                $x = '';
                foreach($q->downline as $v){
                    $x .= $v['reseller_code'].', ';
                }
                return $x;
            } else {
                return '-';
            }
        })

        ->with('group',$group)
        ->with('platform',$platform)
        ->with('status',$status)
        ->with('kunjungan',"1,195")
        ->with('pertumbuhan',"$1,243")
        ->with('populasi',$populasi)
        ->with('memberAktif',$memberAktif)
        ->with('chartPopulasi',$chartPopulasi)
        ->rawColumns( ['avatar','action','statusReseller','SN','pinP2'])
        ->make(true);
    }

    public function editReseller (Request $request,$id)
    {
        $data = Reseller::where('id',$request->id)->with('balance')->first();
        return view('reseller_saldo_edit')->withData($data);
    }
    public function updateReseller (Request $request,$id)
    {


            $reseller =  ResellerBalance::where('reseller_id',$id)->where('is_deleted',0)->first();
            $params = $request->except('_token');
            if($reseller){

                $update = $reseller->update([
                    'balance'=>$request->balance
                ]);
            } else {
                $reseller = new ResellerBalance();
                $reseller->reseller_id =$id;
                $reseller->balance =$request->balance;
                $reseller->point =0;
                $reseller->created_by =$request->session()->get('SESS_USERDATA')['nama'];
                $reseller->created_at =date('Y-m-d H:i:s');
                $reseller->updated_at =date('Y-m-d H:i:s');
                $update = $reseller->save();
            }
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Saldo Reseller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Reseller)',
                ]);
            }

    }

}