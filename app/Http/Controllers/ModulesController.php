<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class ModulesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api');
	}

	public function read(Request $request, $path)
	{
		switch ($path) {
			case 'data':
				return response()->json($this->data($request));
				break;
			case 'detail':
				return response()->json($this->detail($request));
				break;
			default:
				return response()->json(['success' => false, 'message' => 'The server has not found anything matching the Request-URI.', 'data' => []], 401);
		}
	}

	public function data($request)
	{
		$rowCount = 0;
		$data = array();
		$error = array();
		$search = $request->input('search');
		$page = $request->input('page') != null ? $request->input('page') : 0;
		$pageSize = $request->input('pageSize') != null ? $request->input('pageSize') : 10;
		$field = $request->input('field');
		$sort = $request->input('sort');
		$offset = $page * $pageSize;
		$limit = $pageSize;
		try {
			$modules = DB::table('modules');
			$rowCount = $modules->count();
			$modules->select('id', 'code', 'name', 'description', 'icon', 'class', 'path', 'is_active')
				->where('is_deleted', '=', false)
				->offset($offset)
				->limit($limit);
			if ($search) {
				$modules->orWhere(function ($query) use ($search) {
					$query->where('code', 'ILIKE', '%' . $search . '%')->where('name', 'ILIKE', '%' . $search . '%')->where('class', 'ILIKE', '%' . $search . '%');
				});
			}
			if ($sort) {
				$modules->reorder($field, $sort);
			} else {
				$modules->reorder('index', 'ASC');
			}
			$data = $modules->get();
		} catch (QueryException $exception) {
			$error[] = $exception->getMessage();
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'rowCount' => $rowCount, 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'rowCount' => $rowCount, 'data' => $data];
		}
		return $content;
	}

	public function detail($request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);
		if (!$validator->fails()) {
			try {
				$data = DB::table('modules')
					->select('id', 'code', 'name', 'description', 'icon', 'class', 'path', 'is_active')
					->where('is_deleted', '=', false)
					->find($request->input('id'));
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'data' => $data];
		}
		return $content;
	}

	public function insert(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'code' => 'required|unique:modules',
			'name' => 'required|max:45',
			'icon' => 'required|max:45',
			'class' => 'required|max:45',
			'path' => 'required|max:45',
		]);
		$data = $request->all();
		$data['created_at'] = Carbon::now();
		$data['created_by'] = auth()->id();
		if (!$validator->fails()) {
			try {
				DB::table('modules')->insert($data);
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.add_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.add_data_success'), 'data' => []];
		}
		return response()->json($content);
	}

	public function update(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
			'code' => 'required|unique:modules',
			'name' => 'required|max:45',
			'icon' => 'required|max:45',
			'class' => 'required|max:45',
			'path' => 'required|max:45',
		]);
		$data = $request->all();
		$data['updated_at'] = Carbon::now();
		$data['update_by'] = auth()->id();
		if (!$validator->fails()) {
			try {
				DB::table('modules')->where('id', $data['id'])->update($data);
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.update_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.update_data_success'), 'data' => []];
		}
		return response()->json($content);
	}

	public function delete(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);
		$data = array(
			'is_deleted' => true,
			'updated_at' => Carbon::now(),
			'deleted_by' => auth()->id(),
		);
		if (!$validator->fails()) {
			try {
				DB::table('modules')->where('id', $data['id'])->update($data);
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.delete_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.delete_data_success'), 'data' => $data];
		}
		return response()->json($content);
	}
}
