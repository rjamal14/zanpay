<?php

namespace App\Http\Controllers\Biller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Biller;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Collection;

class BillerSaldoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Biller::where("is_deleted",0);
        
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                return '<a   class="btn btn-sm  btnEdit btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> ';
        })
     
        ->addColumn('nama', function ($q)  {
            return '<img src="'.$q->avatar.'" class=" avatar" ">&nbsp'.$q->name;
        })
     
        ->addColumn('phone', function ($q)  {
            return '<span class="phone"><i class="fa fa-phone m-r-10"></i>'. $q->phone.'</span>';
        })
        ->addColumn('apikey', function ($q)  {
            return '<span class="phone"><i class="fa fa-lock m-r-10"></i> ******</span>';
        })
        ->addColumn('ip_address', function ($q)  {
            return '<span class="phone"><i class="fa fa-lock m-r-10"></i>'. $q->biller_url.'</span>';
        })
        ->addColumn('saldo', function ($q)  {
            if(isset($q->balance)) {
                return '<span class="phone"><i class="fa fa-envelope m-r-10"></i>'. formatCurrency($q->balance).'</span>';
            } else {
                return '<span class="phone"><i class="fa fa-envelope m-r-10"></i>'. formatCurrency(0).'</span>';
            }
        })
    

     
        ->rawColumns( ['nama','action','phone','apikey','saldo','ip_address'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
        return view('saldo_biller');
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 

    public function editBiller (Request $request,$id)
    {
        $data = Biller::where('id',$request->id)->first();
        return view('biller_saldo_edit')->withData($data);
    }
    public function updateBiller (Request $request,$id)
    {


            $reseller =  Biller::where('id',$id)->where('is_deleted',0)->first();
            $params = $request->except('_token');
            if($reseller){

                $update = $reseller->update([
                    'balance'=>$request->balance
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Saldo Biller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }

    }

    public function chart (Request $request,$id)
    {


            $reseller =  Biller::where('id',$id)->where('is_deleted',0)->first();
            $params = $request->except('_token');
            if($reseller){

                $update = $reseller->update([
                    'balance'=>$request->balance
                ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Saldo Biller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }

    }
}