<?php

namespace App\Http\Controllers\Biller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Biller;
use App\Models\BillerHistory;
use App\Models\ResellerAttribute;
use App\Models\ResellerBalance;
use App\Models\ResellerGroup;
use App\Models\ProductBiller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use Illuminate\Validation\Rule;
use Validator;
use DB;
use Illuminate\Support\Collection;

class BillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fnData(Request $request)
    {

        $query = Biller::where("is_deleted",0)->with('latestActivity');
        
        $query = $query->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function ($q)  {
      
                // return '<a   class="btn btn-sm btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.','.$urlEdit.')"><i class="glyphicon glyphicon-edit"></i>&nbsp EDIT</a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('.$q->id.')"><i class="glyphicon glyphicon-trash"></i>&nbsp HAPUS</a>';

                // return '<a   class="btn btn-sm  btnEdit editBtn btn-success" href="javascript:void(0)"  onclick="editDataSaldo('.$q->id.')"><i class="fa fa-money"></i></a> <a class="btn btn-sm  btnEdit editBtn btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';

                return '<a class="btn btn-sm  btnEdit editBtn btn-primary" href="javascript:void(0)"  onclick="editData('.$q->id.')"><i class="fa fa-edit"></i></a> <a  class="btn btn-sm btn-danger" href="javascript:void(0)"title="Hapus" onclick="deleteProduct('.$q->id.')"><i class="fa fa-trash-o"></i></a>';
        })
     
      
        ->addColumn('avatar', function ($q)  {
        
                return '<img src="'.$q->avatar.'" class=" avatar">';
         
        })
      
        ->addColumn('latest', function ($q)  {
        if($q->latestActivity != null){
            $x = $q->latestActivity->description;
        }else {
            $x = 'Belum ada data ';
        }
        return $x;
         
        })
        ->addColumn('avatar2', function ($q)  {
        
                return '<img src="'.$q->avatar.'" style="height:45px;width:45px;" class="avatar">';
         
        })
        ->addColumn('avatar3', function ($q)  {
        
                return '<img src="'.$q->avatar.'" style="height:30px;width:30px;" class="avatar">';
         
        })
        ->addColumn('statusText', function ($q)  {
            if($q->status == "0"){
                return '<span class="badge badge-default">Non Aktif</span>';
            } else if($q->status == "1" ) {
                return '<span class="badge badge-success">Aktif</span>';
            } 
         
        })
        ->addColumn('saldo', function ($q)  {
        
                return formatcurrency($q->balance);
         
        })
       

     
        ->rawColumns( ['avatar','action','avatar2','avatar3','statusText'])
        ->make(true);
    }

    public function tanggalIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
 
    public function index (Request $request)
    {
      
        return view('biller');
        // $biller = Biller::all();
        // $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
     	// return view('produk_postpaid')->withBiller($biller)->withGroup($group);
    }
 

    public function add (Request $request)
    {   
            $param=$request->except('avatar');
            $urlAvatar = $this->upload(
                $request->file('avatar')
            ); 
            if($urlAvatar->getData()->status == 200){
                $param['avatar'] = $urlAvatar->getData()->filepath;
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error uploading avatar',
                ]);
            }
            $param['password']=$request["password"];
            $param['added_data']=$request["added_data"];
            $param['created_by']=$request->session()->get('SESS_USERDATA')['nama'];
            $param['balance']=  0;
            $param['web_report']=  "";
            $biller = new Biller();
            $biller = $biller::create($param);
            if($biller){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Success',
                    ]);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (product)',
                ]);
            }
        

    }
    public function delete (Request $request,$id)
    {


    

        Biller::where('id',$id)->update([
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return response()->json([
            'status' => 200,
            'message' => "Biller berhasil dihapus"
        ]);


    }

    public function edit (Request $request,$id)
    {
        $data = Biller::where('id',$request->id)->first();
        $a = formatcurrency($data->balance);
        return view('biller_edit')->withData($data)->withA($a);
    }
    public function update (Request $request,$id)
    {


            $biller =  Biller::where('id',$id)->first();
            $params = $request->except('_token');


            if($request->hasFile('avatar')){
                $urlAvatar = $this->upload(
                    $request->file('avatar')
                ); 
                if($urlAvatar->getData()->status == 200){
                    $params['avatar'] = $urlAvatar->getData()->filepath;
                    $biller->avatar =$urlAvatar->getData()->filepath;
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'error uploading avatar',
                    ]);
                }
            }
            $biller->added_data = $request->added_data;
            $biller->biller_url = $request->biller_url;
            $biller->name = $request->name;
            $biller->apikey = $request->apikey;
            $biller->phone = $request->phone;
            $biller->email = $request->email;
            $biller->username = $request->username;
            $biller->password = $request->password;
            $biller->status = $request->status;
            $biller->updated_at = date('Y-m-d H:i:s');
            $biller->updated_by = Session::get('SESS_USERDATA')['nama'];
            $update = $biller->save();
            if($update){
                    
                    ProductBiller::where(["biller_id"=>$biller->id])->update(["is_trouble"=>($biller->status==0?1:0)]);

                    return response()->json([
                        'status' => 200,
                        'message' => "Biller berhasil diupdate",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }



       




    
    }
    public function syncSaldo (Request $request)
    {
        
            $params['biller_id'] = $request->id;
            $cek = $this->syncService($params);
            if($cek->getStatusCode() != 200 ){
                return response()->json([
                    'status' => 400,
                    'message' => "Pengecekan Saldo Gagal ke https://zanpay.co.id/api/payload.php",
                    'data'=>[]
                ]);
            }
            $data=json_decode($cek->getBody()->getContents());
            if($data == null){
                $res = 0;
            } else {
                $res =$data->balance;
            }
            return response()->json([
                'status' => 200,
                'message' => "Success",
                'data'=> formatcurrency($res),
                'data2'=> $res
            ]);
    }
    public function doSyncSaldo (Request $request)
    {
            $biller =  Biller::where('id',$request->biller_id)->first();
            $newSaldo = $request->nominal;
            $curSaldo = $biller->balance;
            // dd($newSaldo != $curSaldo,$newSaldo,$curSaldo);
            if ($newSaldo != $curSaldo){
                DB::beginTransaction();
                $history = new BillerHistory();
                $history->biller_id = $biller->id;
                $history->amount = abs($newSaldo - $curSaldo);
                $history->balance_before = $biller->balance;
                $history->type = ($newSaldo - $curSaldo > 0) ? 1 : 0;
                $history->balance_after = $newSaldo;
                $history->description = 'Sinkronisasi Saldo';
                $history->created_at = date("Y-m-d H:i:s");
                $history->created_by = Session::get('SESS_USERDATA')['nama'];
                $save = $history->save();
                if($save){
                    $update = $biller->update([
                        'balance' => $newSaldo
                    ]);
                    DB::commit();
                    if($update){
                        return response()->json([
                            'status' => 200,
                            'message' => "Sinkronisasi Berhasil",
                            'data'=>formatCurrency($newSaldo)
                        ]);
                    }
                }
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => "Saldo Sudah Sama Tidak Perlu Sinkronisasi",
                    'data'=>formatCurrency($request->nominal)
                ]);
            }
            return response()->json([
                'status' => 400,
                'message' => "Sinkronisasi gagal",
                'data'=>$request->all()
            ]);
                   
          
    }


    public function editSaldo (Request $request,$id)
    {
        $data = Biller::where('id',$request->id)->first();
        return view('biller_edit_saldo')->withData($data);
    }
    public function updateSaldo (Request $request,$id)
    {


            $biller =  Biller::where('id',$id)->first();
            $params = $request->except('_token');
            if($request->saldo <= 0) {
                return response()->json([
                    'status' => 400,
                    'message' => "Mohion isi saldo",
                    'data'=>[]
                ]);
            }
            DB::beginTransaction();
            $saldo = $biller->balance + $request->saldo;
            $update = $biller->update([
                'balance' => $saldo
            ]);
            $history = new BillerHistory();
            $history->biller_id = $biller->id;
            $history->amount = $request->saldo;
            $history->balance_before = $biller->balance;
            $history->type = 1;
            $history->balance_after = $saldo;
            $history->description = 'Di input oleh '. Session::get('SESS_USERDATA')['nama'];
            $history->created_at = date("Y-m-d H:i:s");
            $history->created_by = "Backoffice";
            $history->save();
            DB::commit();
            if($update){
        
                    return response()->json([
                        'status' => 200,
                        'message' => "Saldo Biller berhasil ditambahkan",
                        'data'=>$request->all()
                    ]);
              
          
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'error (Biller)',
                ]);
            }   
    }

}
