<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class UserGroupPrivilegesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:api');
	}

	public function read(Request $request, $path)
	{
		switch ($path) {
			case 'data':
				return response()->json($this->data($request));
				break;
			case 'detail':
				return response()->json($this->detail($request));
				break;
			default:
				return response()->json(['success' => false, 'message' => 'The server has not found anything matching the Request-URI.', 'data' => []], 401);
		}
	}

	public function data($request)
	{
		$search = $request->input('search');
		$data = array();
		$error = array();
		try {
			$modules = DB::table('modules');
			$modules->select('id AS id_modules', 'code AS code_modules', 'name AS name_modules');
			$modules->where('is_active', true);
			$modules->where('is_deleted', false);
			if ($search) {
				$modules->where(function ($query) use ($search) {
					$query->where('code', 'ILIKE', '%' . $search . '%')->orWhere('name', 'ILIKE', '%' . $search . '%');
				});
			}
			foreach ($modules->get() as $row) {
				$row->actions = DB::table('module_actions')
					->select('id AS id_module_actions', 'name AS name_module_actions')
					->where('is_deleted', '=', false)
					->where('id_modules', $row->id_modules)->get();
				$data[] = $row;
			}
		} catch (QueryException $exception) {
			$error[] = $exception->getMessage();
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'data' => $data, 'search' => $search];
		}
		return $content;
	}

	public function detail($request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id_user_groups' => 'required',
		]);
		if (!$validator->fails()) {
			try {
				$user_group_privileges = DB::table('user_group_privileges')
					->select('id', 'id_user_groups', 'id_module_actions', 'privileged')
					->where('id_user_groups', '=', $request->input('id_user_groups'));
				foreach ($user_group_privileges->get() as $row) {
					$row->id_module_actions = $row->privileged;
					$data[] = $row;
				}
			} catch (QueryException $exception) {
				$error[] = $exception->getMessage();
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.error', ['Attribute' => null]), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.success', ['Attribute' => null]), 'data' => $data];
		}
		return $content;
	}

	public function update(Request $request)
	{
		$error = array();
		$data = array();
		$validator = Validator::make($request->all(), [
			'id_user_groups' => 'required',
		]);
		$id_user_groups = $request->input('id_user_groups');
		if (!$validator->fails()) {
			foreach ($request->all() as $k => $v) {
				try {
					$user_group_privileges = DB::table('user_group_privileges')
						->where('id_user_groups', $id_user_groups)
						->where('id_module_actions', $k);
					$data = array(
						'id_user_groups' => $id_user_groups,
						'id_module_actions' => $k,
						'privileged' => $v,
					);
					if ($user_group_privileges->count() === 0) {
						try {
							DB::table('user_group_privileges')->insert($data);
						} catch (QueryException $exception) {
							$error[] = $exception->getMessage();
						}
					} else {
						try {
							$row = $user_group_privileges->latest()->first();
							$data['id'] = $row->id;
							DB::table('user_group_privileges')->where('id', $data['id'])->update($data);
						} catch (QueryException $exception) {
							$error[] = $exception->getMessage();
						}
					}
				} catch (QueryException $exception) {
					$error[] = $exception->getMessage();
				}
			}
		} else {
			foreach ($validator->errors()->all() as $row) {
				$error[] = $row;
			}
		}
		if ($error) {
			$content = ['success' => false, 'message' => __('messages.update_data_error'), 'data' => $error];
		} else {
			$content = ['success' => true, 'message' => __('messages.update_data_success'), 'data' => []];
		}
		return response()->json($content);
	}
}
