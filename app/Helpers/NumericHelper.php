<?php



/*
|--------------------------------------------------------------------------
| Clean Up Currency Format
|--------------------------------------------------------------------------
|
*/
if(!function_exists('cleanNumeric')) {
    function cleanNumeric($num = 0)
    {
        return number_format($num, 0, '', '');
    }
}

/*
|--------------------------------------------------------------------------
| Format Number Numeric
|--------------------------------------------------------------------------
|
*/
if(!function_exists('formatNumber')) {
    function formatNumber($num = 0, $digit = 0)
    {
        $decimal = ',';
        $thousand = '.';

        return number_format($num, $digit, $decimal, $thousand);
    }
}



/*
|--------------------------------------------------------------------------
| Format Number Currency
|--------------------------------------------------------------------------
|
*/
if(!function_exists('formatCurrency')) {
    function formatCurrency($num = 0, $currency = 'IDR', $digit = 0)
    {
        $prefix = '';
        $decimal = ',';
        $thousand = '.';

        switch ($currency) {
            case 'IDR':
                $prefix = 'Rp ';
                $decimal = ',';
                $thousand = '.';
            break;
            default:
            break;
        }

        return $prefix . number_format($num, $digit, $decimal, $thousand);
    }
}

if (!function_exists('cleanInputCurrency')) {
    function cleanInputCurrency($str)
    {
        return str_replace(['Rp ', ','], '', $str);
    }
}