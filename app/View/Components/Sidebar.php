<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Session;
use App\Models\User;
class Sidebar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (isset(Session::get('SESS_USERDATA')['id'])){
            $user = User::where('id',Session::get('SESS_USERDATA')['id'])->first();
            return view('components.sidebar')->withUser($user);
        } else {
            
        }
        
    }
}
