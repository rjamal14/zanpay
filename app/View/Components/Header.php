<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\MessageDetail;
use Carbon\Carbon;
class Header extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data['data'] = MessageDetail::where('sender','!=',0)->where('read_at',null)->orderBy('datetime','desc')->groupBy('message_id')->with('sender')->get();
        $data['unread_count'] = MessageDetail::where('sender','!=',0)->where('read_at',null)->orderBy('datetime','desc')->groupBy('message_id')->with('sender')->count();
        // dd($data);
        foreach( $data['data'] as $k=>$v){
            $checkdt =  Carbon::parse($v['datetime']);
            $today = Carbon::now();
            if($today->diffInDays($checkdt) >2){
                $dt = date('H:i:s',strtotime($v['datetime'])).', '.$this->bulanIndonesia(date('Y-m-d',strtotime($v['datetime'])));

        
            } else {
                $dt = date('H:i:s',strtotime($v['datetime'])).", ".Carbon::parse($v['datetime'])->locale('id')->diffForHumans();
            }
            $data['data'][$k]['pesan'] = strlen($v['message']) > 50 ? substr($v['message'],0,50)."..." : $v['message'];
            $data['data'][$k]['datetime'] = $dt;

        }   
        $data['data'] = collect($data['data'])->toArray();
        return view('components.header')->withData($data);
    }
    public function bulanIndonesia ($tanggal)
    {

        // $bulan = array (
        //     1 =>   'Januari',
        //     'Februari',
        //     'Maret',
        //     'April',
        //     'Mei',
        //     'Juni',
        //     'Juli',
        //     'Agustus',
        //     'September',
        //     'Oktober',
        //     'November',
        //     'Desember'
        // );
        $bulan = array (
            1 =>   'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Ags',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
    function hariIndonesia($date){
        $hari = date ("D",strtotime($date));
     
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
     
            case 'Mon':			
                $hari_ini = "Senin";
            break;
     
            case 'Tue':
                $hari_ini = "Selasa";
            break;
     
            case 'Wed':
                $hari_ini = "Rabu";
            break;
     
            case 'Thu':
                $hari_ini = "Kamis";
            break;
     
            case 'Fri':
                $hari_ini = "Jumat";
            break;
     
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
     
        return $hari_ini;
     
    }
}
