<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'document';
    protected $guarded = [];
    public $primaryKey = 'no';
    public $timestamps = false;

	public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id', 'reseller_id');
    }


}
