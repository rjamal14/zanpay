<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $table = 'product_group';

    public function products(){
        return $this->hasMany('App\Models\Product', 'group_id', 'id');
    }

    public function prefix(){
        return $this->hasOne('App\Models\Prefix', 'no', 'hlr_prefix');
    }
    public function getFullnameAttribute()
    {
        return $this->command .' '.$this->provider;
    }
}
