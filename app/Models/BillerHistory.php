<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillerHistory extends Model
{
    protected $table = 'biller_history';
    protected $guarded = [];
    public $timestamps = false;

    public function parsing(){
        return $this->hasOne('App\Models\BillerParsing', 'id','reference_id');
    }
    public function biller(){
        return $this->hasOne('App\Models\Biller', 'id','biller_id');
    }
}
