<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGroupHolder extends Model
{
    protected $table = 'product_group_holder';
    protected $guarded = [];
    public $timestamps = false;


    public function biller()
    {
        return $this->hasOne('App\Models\Biller', 'id', 'biller_id');
    }
}
