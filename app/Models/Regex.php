<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regex extends Model
{
    protected $table = 'regex';
    protected $guarded = [];
    public $timestamps = false;


    public function biller()
    {
        return $this->hasOne('App\Models\Biller', 'id', 'biller_id');
    }
    public function group()
    {
        return $this->hasOne('App\Models\ProductGroup', 'id', 'group_id');
    }
}
