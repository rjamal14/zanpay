<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

	public function productGroup(){
        return $this->hasOne('App\Models\ProductGroup', 'id', 'group_id');
    }
	public function group(){
        return $this->hasOne('App\Models\ProductGroup', 'id', 'group_id');
    }
	public function biller(){
        return $this->hasOne('App\Models\Biller', 'id','biller_id');
    }
	public function category(){
        return $this->hasOne('App\Models\ProductCategory', 'id','category_id');
    }
	public function productBiller(){
        return $this->hasOne('App\Models\ProductBiller', 'product_id', 'id');
    }
	public function sold(){
        return $this->hasMany('App\Models\Transaction', 'sku_code', 'sku_code')->where('transaction.status',"S");
    }
    public function getFullnameAttribute()
    {
        return $this->name .' ('.$this->sku_code.")";
    }
}
