<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'prefix';
    protected $guarded = [];
    public $timestamps = false;
    public $primaryKey = 'no';


}
