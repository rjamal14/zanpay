<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
    protected $table = 'merchandise';
    public $primaryKeys = 'id';
    protected $guarded = [];
	
	public function category(){
        return $this->hasOne('App\Models\MerchandiseCategory', 'id','category');
    }
	public function view(){
        return $this->hasMany('App\Models\MerchandiseView', 'merchandise_id','id');
    }
	public function like(){
        return $this->hasMany('App\Models\MerchandiseLike', 'merchandise_id','id');
    }
	public function variant(){
        return $this->hasMany('App\Models\MerchandiseVariant', 'merchandise_id','id');
    }
	public function images(){
        return $this->hasMany('App\Models\MerchandiseImage', 'merchandise_id','id')->where('image_prio',1);
    }
	public function image(){
        return $this->hasOne('App\Models\MerchandiseImage', 'merchandise_id','id')->where('image_prio',0);
    }

}
