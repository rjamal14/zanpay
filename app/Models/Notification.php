<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    public $primaryKeys = 'id';
    protected $guarded = [];
	
	public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id','reseller_id');
    }

}
