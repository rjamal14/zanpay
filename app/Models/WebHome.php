<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebHOme extends Model
{
    protected $table = 'web_home';
    public $primaryKeys = 'id';
    public $timestamps = false;
    protected $guarded = [];

}
