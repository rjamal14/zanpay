<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $table = 'news_category';
    protected $guarded = [];
	
	public function news(){
        return $this->hasMany('App\Models\News', 'category','id')->where('is_deleted',0);
    }
}
