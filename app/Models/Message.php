<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';
    protected $guarded = [];
    public $primaryKey = 'no';
    public $timestamps = false;

	public function sender(){
        return $this->hasOne('App\Models\Reseller', 'id', 'sender');
    }
	public function receiver(){
        return $this->hasOne('App\Models\Reseller', 'id', 'receiver');
    }
	public function detail(){
        return $this->hasOne('App\Models\MessageDetail', 'message_id', 'no');
    }


}
