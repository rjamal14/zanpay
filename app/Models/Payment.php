<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    // protected $table = 'Payment';
    protected $table = 'payment';
    protected $guarded = [];

	public function method(){
        return $this->hasMany('App\Models\PaymendtMethod', 'payment_id', 'id');
    }


}
