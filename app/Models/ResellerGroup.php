<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResellerGroup extends Model
{
    protected $table = 'reseller_group';
    protected $guarded = [];
	public function reseller(){
        return $this->hasMany('App\Models\Reseller', 'reseller_group','id');
    }

}
