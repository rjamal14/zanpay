<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchandiseVariant extends Model
{
    protected $table = 'merchandise_variant';
    protected $guarded = [];
    public $timestamps = false;
	

}
