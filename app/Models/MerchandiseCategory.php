<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchandiseCategory extends Model
{
    protected $table = 'merchandise_category';
    protected $guarded = [];
	
	public function merchandise(){
        return $this->hasMany('App\Models\Merchandise', 'category','id')->where('is_deleted',0);
    }
}
