<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_category';

    public function products(){
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }
    public function group(){
        return $this->hasOne('App\Models\ProductGroup', 'id', 'group_id');
    }

}
