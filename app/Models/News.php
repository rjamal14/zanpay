<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $primaryKeys = 'id';
    protected $guarded = [];
	
	public function category(){
        return $this->hasOne('App\Models\NewsCategory', 'id','category');
    }
	public function view(){
        return $this->hasMany('App\Models\NewsView', 'news_id','id');
    }
	public function like(){
        return $this->hasMany('App\Models\NewsLike', 'news_id','id');
    }
	public function comment(){
        return $this->hasMany('App\Models\NewsComment', 'news_id','id');
    }
}
