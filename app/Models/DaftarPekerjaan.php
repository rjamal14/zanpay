<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaftarPekerjaan extends Model
{
    protected $table = 'daftar_pekerjaan';
    public $primaryKeys = 'id';
    protected $guarded = [];
	
}
