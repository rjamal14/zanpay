<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reseller extends Model
{
    protected $table = 'reseller';
    protected $guarded = [];

	public function attribute(){
        return $this->hasOne('App\Models\ResellerAttribute', 'reseller_id', 'id');
    }
	public function balance(){
        return $this->hasOne('App\Models\ResellerBalance', 'reseller_id','id');
    }
	public function group(){
        return $this->hasOne('App\Models\ResellerGroup', 'id','reseller_group');
    }
	public function latestHistory(){
        return $this->hasOne('App\Models\ResellerHistory', 'reseller_id','id')->orderBy('created_at','desc');
    }
	public function history(){
        return $this->hasMany('App\Models\ResellerHistory', 'reseller_id','id');
    }
	public function historyComm(){
        return $this->hasMany('App\Models\ResellerHistory', 'reseller_id','id')->where('description','Komisi transaksi downline');
    }
	public function allTransaction(){
        return $this->hasMany('App\Models\Transaction', 'reseller_id','id');
    }
	public function successTransaction(){
        return $this->hasMany('App\Models\Transaction', 'reseller_id','id')->whereIn('status',['S','s']);
    }
	public function failedTransaction(){
        return $this->hasMany('App\Models\Transaction', 'reseller_id','id')->whereIn('status',['F','f']);
    }
	public function queueTransaction(){
        return $this->hasMany('App\Models\Transaction', 'reseller_id','id')->whereIn('status',['Q','q']);
    }
	public function cashback_history(){
        return $this->hasMany('App\Models\CashbackHistory', 'reseller_id','id');
    }
	public function downline(){
        return $this->hasMany('App\Models\Reseller', 'referral_code','reseller_code');
    }


}
