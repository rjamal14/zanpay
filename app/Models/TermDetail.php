<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermDetail extends Model
{
    protected $table = 'term_details';
    public $primaryKeys = 'id';
    public $timestamps = false;
    protected $guarded = [];

}
