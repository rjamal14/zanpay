<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biller extends Model
{
    protected $table = 'biller';
    protected $guarded = [];
    public function latestActivity(){
        return $this->hasOne('App\Models\BillerHistory', 'biller_id','id')->orderBy('created_at','desc');
    }
}
