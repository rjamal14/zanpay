<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyDetail extends Model
{
    protected $table = 'privacy_details';
    public $primaryKeys = 'id';
    public $timestamps = false;
    protected $guarded = [];

}
