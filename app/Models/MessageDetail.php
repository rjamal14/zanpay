<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
    protected $table = 'message_detail';
    protected $guarded = [];
    public $primaryKey = 'no';
    public $timestamps = false;

    public function sender(){
        return $this->hasOne('App\Models\Reseller', 'id', 'sender');
    }
    public function receiver(){
        return $this->hasOne('App\Models\Reseller', 'id', 'receiver');
    }
}
