<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    protected $table = 'privacy';
    public $primaryKeys = 'id';
    public $timestamps = false;
    protected $guarded = [];

}
