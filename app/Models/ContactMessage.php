<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactMessage extends Model
{
    protected $table = 'contact_message';
    public $primaryKeys = 'id';
    protected $guarded = [];

}
