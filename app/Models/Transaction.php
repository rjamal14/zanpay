<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $guarded = [];

    public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id', 'reseller_id');
    }
    public function biller(){
        return $this->hasOne('App\Models\Biller', 'id', 'biller_id');
    }
    public function bill(){
        return $this->hasOne('App\Models\Biller', 'name', 'biller');
    }
    public function produk(){
        return $this->hasOne('App\Models\Product', 'sku_code', 'sku_code');
    }
    public function commands(){
        return $this->hasMany('App\Models\Transaction', 'command', 'command');
    }
    public function history(){
        return $this->hasOne('App\Models\BillerHistory', 'reference_id', 'trx_code');
    }
    public function inbox(){
        return $this->hasOne('App\Models\TransactionInbox', 'trx_code', 'trx_code')->orderBy('transaction_inbox.date_created','desc');
    }

}
