<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherLog extends Model
{
    protected $table = 'voucher_log';
    protected $guarded = [];
	
    public function voucher(){
        return $this->hasOne('App\Models\Voucher', 'code', 'voucher_code');
    }
    public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id', 'reseller_id');
    }

}
