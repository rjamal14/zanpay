<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductParsing extends Model
{
    protected $table = 'product_parsing';
    protected $guarded = [];
    public function biller()
    {
        return $this->hasOne('App\Models\Biller', 'id', 'biller_id');
    }
    public function holder()
    {
        return $this->hasOne('App\Models\ProductGroupHolder', 'id', 'holder_id');
    }
}
