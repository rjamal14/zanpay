<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResellerHistory extends Model
{
    protected $table = 'reseller_history';
    protected $guarded = [];
    public $timestamps = false;
	
	public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id','reseller_id');
    }
}
