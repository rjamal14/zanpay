<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    public $primaryKeys = 'id';
    public $timestamps = false;
    protected $guarded = [];

}
