<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'voucher';
    protected $guarded = [];
	
    public function log(){
        return $this->hasMany('App\Models\VoucherLog', 'voucher_code', 'code');
    }

}
