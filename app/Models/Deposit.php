<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table = 'deposit';
    protected $guarded = [];
    public $primaryKey = 'no';
    public $timestamps = false;

	public function reseller(){
        return $this->hasOne('App\Models\Reseller', 'id', 'reseller_id');
    }

	public function method(){
        return $this->hasOne('App\Models\PaymentMethod', 'id', 'bank_code');
    }

}
