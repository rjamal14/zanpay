<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prefix extends Model
{
    protected $table = 'prefix';
    protected $guarded = [];
    public $primaryKey = 'no';
    public $timestamps = false;

}
