<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillerParsing extends Model
{
    protected $table = 'biller_parsing';
    protected $guarded = [];
}
