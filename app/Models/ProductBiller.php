<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBiller extends Model
{
    protected $table = 'product_biller';
    protected $guarded = [];
    	public function biller(){
        return $this->hasOne('App\Models\Biller', 'id','biller_id');
    }
    	public function product(){
        return $this->hasOne('App\Models\Product', 'id','product_id');
    }
}
