<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\ProductBiller;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\ToArray;

class ImportProduct implements ToCollection, WithHeadingRow,WithBatchInserts,WithChunkReading
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function batchSize(): int
    {
        return 100;
    }
    
    public function chunkSize(): int
    {
        return 100;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $key=>$row) 
        {
        
            if($row['group'] != 0 ){
                $nama =[];
                $nama =explode('-',$row['provider']);
                if (count($nama) == 2){
                    $namaString = $nama[1];
                } else if (count($nama) == 3){
                    $namaString=$nama[2]; 
                } else if (count($nama) == 4){
                    $namaString=$nama[3];
                } else if (count($nama) == 5){
                    $namaString=$nama[5];
                } else {
                    $namaString=$nama[0];
                }
                $product = Product::create([
                    "biller_id"=> 1,
                    "group_id"=>$row['group'],
                    "sku_code"=>$row['kode'],
                    "description"=>$row["nama"],
                    "nominal"=>$row["nominal"],
                    "price"=>$row["harga_jual"],
                    "cashback"=>0,
                    "name"=>$namaString,
                    "created_by"=>"import",
                    "is_trouble"=>$row["gangguan"] == true ? 1 : 0,
    
                ]);
    
                ProductBiller::create([
                    "product_id"=> $product->id,
                    "biller_id"=> 1,
                    "sku_biller"=>$row['kode'],
                    "price_basic"=>$row["harga_beli"],
                    "is_trouble"=>$row["gangguan"] == true ? 1 : 0,
                    "created_by"=>"import",
                ]);
            }
        }
    }

}
