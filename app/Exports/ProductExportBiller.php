<?php

namespace App\Exports;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductBiller;
use App\Models\Biller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat as NB;
class ProductExportBiller implements  FromCollection, WithHeadings,ShouldAutoSize,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection 
    */

    public function collection()
    {
        // $group = ProductGroup::whereIn('group_name',['prabayar','prepaid'])->get();
        // $groupId = collect($group)->pluck('id')->toArray();
        $product= ProductBiller::with(['biller'])->where("is_deleted",0)->get();
        $data=[];
        foreach  ($product as $k=>$p){
            $data[$k]['no']=$k+1;
            $data[$k]['biller']=$p['biller']['name'] ?? "";
            $data[$k]['sku_biller']=$p['sku_biller'];
            $data[$k]['sku_produk']=$p['product']['sku_code'];
            $data[$k]['description']=$p['description'];
            $data[$k]['price_basic']=$p['price_basic'];
            $data[$k]['admin_fee']=$p['admin_fee'];
            $data[$k]['priority']=($p['priority']==1 ? 'Tinggi' : ($p['priority']==2 ?  "Normal" : "Rendah" ) );
            $data[$k]['status']=$p['is_trouble'] == 1 ? "Gangguan" : "Normal";
          
        }
        // dd($data);
        return collect($data);
    }

    public function headings(): array
    {
        return ["No",'Biller', "SKU Biller ", "SKU Produk ", 'Deskripsi','Harga Dasar','Biaya Admin','Prioritas','Status'];
    }
    public function columnFormats(): array
    {
        return [
            // 'J' => NB::FORMAT_NUMBER_COMMA_SEPARATED2,
        ];
    }
}
