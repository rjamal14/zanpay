<?php

namespace App\Exports;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductBiller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat as NB;
class ProductExport implements  FromCollection, WithHeadings,ShouldAutoSize,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection 
    */

    public function collection()
    {
        $group = ProductGroup::whereIn('group_name',['prabayar','prepaid'])->get();
        $groupId = collect($group)->pluck('id')->toArray();
        $product= Product::with(['productGroup','productBiller.biller','biller','category'])->where("is_deleted",0)->whereIn('group_id',$groupId)->get();
        $data=[];
        foreach  ($product as $k=>$p){
            $data[$k]['no']=$k+1;
            $data[$k]['group']=$p['productGroup']['group_name'] ?? "";
            $data[$k]['command']=$p['productGroup']['command'] ?? "";
            $data[$k]['provider']=$p['productGroup']['provider'] ?? "";
            $data[$k]['category']=$p['category']['name']??"";
            $data[$k]['sku_code']=$p['sku_code'];
            $data[$k]['name']=$p['name'];
            $data[$k]['description']=$p['description'];
            $data[$k]['nominal']=$p['nominal'];
            $data[$k]['price']=$p['price'];
            // $data[$k]['admin_fee']=$p['admin_fee'];
            // $data[$k]['commission']=$p['commission'];
            // $data[$k]['status']=$p['is_trouble'] == 1 ? "Gangguan" : "Normal";
            // $data[$k]['biller']=$p['biller']['name'] ?? "";
            // $data[$k]['sku_biller']=$p['productBiller']['sku_biller'] ?? "";
            // $data[$k]['description_biller']=$p['productBiller']['description'] ?? "";
            // $data[$k]['price_basic']=$p['productBiller']['price_basic'] ?? "";
            // $data[$k]['biller_admin_fee']=$p['productBiller']['admin_fee'] ?? "";
        }
        // dd($data);
        return collect($data);
    }

    public function headings(): array
    {
        return ["No", 'Tipe Parsing','Jenis','Provider','Kategori', "SKU ","Nama Produk",'Deskripsi','Nominal','Harga Jual',
        // 'Biaya Admin','Komisi','Status','Biller','SKU Biller','Deskripsi Produk Biller','Harga Beli','Biaya Admin Biller'
    ];
    }
    public function columnFormats(): array
    {
        return [
            // 'J' => NB::FORMAT_NUMBER_COMMA_SEPARATED2,
        ];
    }
}
