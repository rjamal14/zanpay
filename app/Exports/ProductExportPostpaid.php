<?php

namespace App\Exports;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductBiller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat as NB;
class ProductExportPostpaid implements  FromCollection, WithHeadings,ShouldAutoSize,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection 
    */

    public function collection()
    {
        $group = ProductGroup::whereIn('group_name',['pascabayar','postpaid'])->get();
        $groupId = collect($group)->pluck('id')->toArray();
        $product= Product::with(['productGroup','productBiller.biller','biller','category'])->where("is_deleted",0)->whereIn('group_id',$groupId)->get();
        $data=[];
        foreach  ($product as $k=>$p){
            $data[$k]['no']=$k+1;
            $data[$k]['sku_code']=$p['sku_code'];
            $data[$k]['group']=$p['productGroup']['group_name'] ?? "";
            $data[$k]['command']=$p['productGroup']['command'] ?? "";
            $data[$k]['provider']=$p['productGroup']['provider'] ?? "";
            $data[$k]['category']=$p['category']['name']??"";
            $data[$k]['name']=$p['name'];
            $data[$k]['description']=$p['description'];
            $data[$k]['nominal']=$p['nominal'];
            $data[$k]['price']=$p['price'];
            $data[$k]['admin_fee']=$p['admin_fee'];
            $data[$k]['commission']=$p['commission'];
            $data[$k]['status']=$p['is_trouble'] == 1 ? "Gangguan" : "Normal";

        }
        // dd($data);
        return collect($data);
    }

    public function headings(): array
    {
        return ["No", "SKU ", 'Group Produk','Jenis','Provider','Kategori',"Nama Produk",'Deskripsi','Nominal','Harga Jual','Biaya Admin','Komisi','Status',
        // 'Biller','SKU Biller','Deskripsi Produk Biller','Harga Beli','Biaya Admin Biller'
    ];
    }
    public function columnFormats(): array
    {
        return [
            // 'J' => NB::FORMAT_NUMBER_COMMA_SEPARATED2,
        ];
    }
}
